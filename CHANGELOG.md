# Changelog

### Upcoming [TBD]

- Mouse:
  - optimised slice-to-volume configurations for sagittal slices
  - multislice-to-volume registration
  
### 2 Aug 2023 -- version 31

- Bugfix: MND Stage 4 chain reduction no longer fails at reading the image resolution
- Updated copyright information


### 28 Mar 2023 -- version 30:

- BigMac: allowed to complete registration with missing input files


### 13 Nov 2022 -- version 29:

- Added Monkey module v1.0 


### 5 Nov 2022 -- version 28:

- BigMac: v3.0 - completely redesigned the multislice algorithm


### 1 Nov 2022 -- version 27:

- BigMac: built-in checkpoint to prevent detrimental optimisation


### 30 Oct 2022 -- version 26:

- BigMac: bugfixes related to TImage mask resolution 


### 29 Oct 2022 -- version 25:

- BigMac: loading files in HDD mode at start
- BigMac: minimised memory impact for registering all slices
- BigMac: added debugging mode and more detailed log messages


### 24 Oct 2022 -- version 24:

- Made cliutils a separate module in tirlscripts
- Added maskoverlay.py to visualise masked and unmasked areas in 2-D TImages
- Moved convertconfig.py to the cliutils module
- BigMac: the snapshot resolution is now configurable


### 19 Oct 2022 -- version 23:

- MND: fixed mask-handling bug in histology-to-block registration
- Added CLI to convertconfig.py and made it a registered tirlscript 


### 15 Oct 2022 -- version 22

- BigMac: multislice_to_volume.py no longer throws on exception if the output directory exists
- BigMac: fixed mask handling in warp.py
- BigMac: major changes in slice resolution optimisation


### 14 Oct 2022 -- version 21

- Change to sequential versioning of tirlscripts (semantic version control remains in place for the modules)
- Added: logger instances now record the SHA1 hash of the executing script for strict version control
- Added: tirlscripts executable to query module versions
- Mouse: slice_to_volume.py can load histology from pyramidal TIFF
- Mouse: slice_to_volume.py: fixed automatic MRI resolution detection
- BigMac: bugfixes in multi-resolution registration


### 13 Oct 2022 -- v2.0.0

- Changed BigMac configuration file: multi-resolution registration is now defined by the target resolution as opposed to scaling factors.


### 11 Oct 2022 -- v1.2.1

- Started project-specific versioning (all projects start from v1.0.0 today)
- Bugfix in BigMac pyramid.py: managing TIRL 2.1 differences w.r.t resolution setting


### 9 Oct 2022 -- v1.2

- Added command-line processing tools for the BigMac project


### 3 Oct 2022 -- v1.1

- Added manchester.neurosurgery collection supports screenshot-to-volume registration
- Added sliceview.py to the oxford.mouse project
- Bugfix: oxford.mouse.sliceview now relies on the best affine matrix of NIfTI files instead of the sform
  

### 6 June 2022 -- v1.0.0
- First release as a separate package
- Included all scripts for these Oxford projects: MND, BigMac, Mouse
- Included helper scripts for 2D image segmentation (formerly known as stripbg)
- Included a script to fix MRI volumes corrupted by erroneous reconstruction
- Changed former "midlevel" MND scripts to universal resources "scriptutils"
- Added support for YAML configuration files, which allow comments; JSON remains supported
- Added detailed comments to explain configurations
- Deprecated lots of MND helper scripts; now found under "archive" for reference


