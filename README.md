# tirlscripts


## Disambiguation

- **tirl**: Tensor Image Registration Library
- **tirlscript**: a specialised python script that uses tirl

    
This package is not TIRL. It is a growing and evolving collection of image 
processing scripts that are based on TIRL. 

## Pre-requisites

The tirl>=3.1 package must be already installed on your computer in a conda 
virtual environment, e.g. `tirlenv`.


## Installation

1. Choose a destination for downloading this repository: e.g.

    ```shell script
    mkdir -p ~/Applications/
    cd ~/Applications
    git clone https://git.fmrib.ox.ac.uk/ihuszar/tirlscripts.git
    cd tirlscripts
    ```

2. Activate your virtual environment where tirl>=3.1 is installed:

    ```shell script
    conda activate tirlenv
    ```

3. Install this package:

    ```shell script
    python setup.py install
    ```

## Registering tirlscripts

1. By registering the installed scripts, TIRL can recognise and run them:

    ```shell script
    tirl register
    ```
    After running this command, you should see a report of which scripts were 
    registered by your local TIRL installation:

    ```
    Registered the following tirlscripts:
    mnd.sliceview: /home/fs0/ihuszar/.conda/envs/tirlenv/lib/python3.8/site-packages/tirlscripts-1.0.0-py3.8.egg/tirlscripts/oxford/mnd/sliceview.py
    mnd.h2v: /home/fs0/ihuszar/.conda/envs/tirlenv/lib/python3.8/site-packages/tirlscripts-1.0.0-py3.8.egg/tirlscripts/oxford/mnd/histology_to_volume.py
    mnd.h2h: /home/fs0/ihuszar/.conda/envs/tirlenv/lib/python3.8/site-packages/tirlscripts-1.0.0-py3.8.egg/tirlscripts/oxford/mnd/histology_to_histology.py
    mnd.b2s: /home/fs0/ihuszar/.conda/envs/tirlenv/lib/python3.8/site-packages/tirlscripts-1.0.0-py3.8.egg/tirlscripts/oxford/mnd/block_to_slice.py
    mnd.h2b: /home/fs0/ihuszar/.conda/envs/tirlenv/lib/python3.8/site-packages/tirlscripts-1.0.0-py3.8.egg/tirlscripts/oxford/mnd/histology_to_block.py
    mnd.b2s.find: /home/fs0/ihuszar/.conda/envs/tirlenv/lib/python3.8/site-packages/tirlscripts-1.0.0-py3.8.egg/tirlscripts/oxford/mnd/find_sites.py
    mnd.s2v: /home/fs0/ihuszar/.conda/envs/tirlenv/lib/python3.8/site-packages/tirlscripts-1.0.0-py3.8.egg/tirlscripts/oxford/mnd/slice_to_volume.py
    mouse.h2h: /home/fs0/ihuszar/.conda/envs/tirlenv/lib/python3.8/site-packages/tirlscripts-1.0.0-py3.8.egg/tirlscripts/oxford/mouse/histology_to_histology.py
    mouse.s2v: /home/fs0/ihuszar/.conda/envs/tirlenv/lib/python3.8/site-packages/tirlscripts-1.0.0-py3.8.egg/tirlscripts/oxford/mouse/slice_to_volume.py
    stripbg: /home/fs0/ihuszar/.conda/envs/tirlenv/lib/python3.8/site-packages/tirlscripts-1.0.0-py3.8.egg/tirlscripts/oxford/segmentation/stripbg.py
    ```
    Each line shows the name of the script, followed by the location of the script. 
    If the location includes "site-packages", it is an installed version, that you 
    should not try to modify. 


2. If you need to see what registered scripts are available for you to run, 
you can do either of the following:

    ```shell script
    tirl scripts
    ```
    
    or 
    
    ```shell script
    tirl config
    ```
    and scroll down to see the list under the "tirlscripts" entry in this 
    configuration file.


3. If you need to see what tirl scripts are available in an arbitrary directory,

    ```shell script
    tirl scripts <dir>
    ```
   will give you a full list by recursively searching the specified directory 
   for `.py` files that define the `__tirlscript__` constant.


4. TIRL scripts are designed in a way that you will likely have to 
modify configurations, but not the script itself. 


5. Template configuration files for each script are provided as satellite 
`.yml` files in this repository. You can also find detailed explanations about 
each of the configurations in these files.


6. If you ever need to modify a script, or write your own, you can register a 
local (non-installed) copy of it, and run it the same way as installed scripts:  

    ```shell script
    tirl register </path/to/script>
    ```
   
    or
   
    ```shell script
    tirl register <directory_with_multiple_scripts> 
    ```
    In the latter case, the directory will be searched recursively for `.py` 
    files that define the `__tirlscript__` constant. The scripts will be 
    registered with the name that is defined by `__tirlscript__`. 
    **Please note that in both cases, the inventory of registered 
    scripts will be overwritten in the tirl configuration file.**


7. You can also register scripts manually without overwriting the registry:

    ```shell script
    tirl config
    ```
    and add them by hand under the `tirlscripts` entry with a unique name.


## Running tirlscripts

1. Installed scripts can be called by their names after the `tirl` 
command. E.g., to do histology-to-histology registration, type the following 
and follow the instructions:

    ```shell script
    tirl mnd.h2h
    ```

2. Non-registered source files can also be run with the `tirl` command, but they 
must be specified by their full path, e.g.:

    ```shell script
    tirl /home/inhuszar/apps/tirl-dev/tirlscripts/tirlscripts/oxford/mnd/histology_to_histology.py
    ```