#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


import tirlscripts
from setuptools import setup, find_packages


#---------------------------- TIRLscripts Installer ---------------------------#

setup(
    name="tirlscripts",
    version=tirlscripts.__version__,
    description="Extensions for the Tensor Image Registration Library",
    author="Istvan N. Huszar",
    packages=find_packages("."),
    package_dir={"tirlscripts": "tirlscripts"},
    scripts=["tirlscripts/tirlscripts"]
)
