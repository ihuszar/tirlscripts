#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


__author__ = "Istvan N. Huszar"
__copyright__ = "Copyright (C) 2018-2023 University of Oxford"
__credits__ = ["Mark Jenkinson"]
__license__ = "FSL"
__version__ = "31"
__maintainer__ = "Istvan N Huszar"
__status__ = "active"


__all__ = [
    "oxford",
    "manchester",
    "cliutils"
]


__modules__ = [
    "cliutils",
    "manchester.neurosurgery",
    "oxford.bigmac",
    "oxford.bigmac3",
    "oxford.monkey",
    "oxford.mnd",
    "oxford.mouse",
    "oxford.reconstruction",
    "oxford.scriptutils",
    "oxford.segmentation"
]


def home():
    """ Returns the home directory of TIRLscripts. """
    import os
    return os.path.realpath(os.path.dirname(__file__))


def srchash(fname):
    from hashlib import sha1
    with open(fname, "rb") as fp:
        hash = sha1(fp.read()).hexdigest()
    return hash
