### 24 Oct 2022 -- v1.0.0

- Made cliutils a separate module in tirlscripts
- Added maskoverlay.py to visualise masked and unmasked areas in 2-D TImages
