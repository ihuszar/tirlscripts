#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


__tirlscript__ = "convertconfig"


# DEPENDENCIES

import os
import sys
import json
import yaml
import argparse


# TIRL IMPORTS

import tirl.utils as tu


# DEFINITIONS

YAML_EXT = (".yaml", ".yml")
JSON_EXT = (".json",)


# IMPLEMENTATION

def json2yaml(jfile, yfile=None):
    """ Loads a JSON configuration file, and converts it to YAML. """
    if yfile is None:
        yfile = jfile.replace(".json", ".yml")
    with open(jfile, "r") as fp:
        cnf = json.load(fp)
    with open(yfile, "w") as fp:
        yaml.safe_dump(cnf, fp, default_flow_style=None,
                       sort_keys=False, indent=2)


def yaml2json(yfile, jfile=None):
    """ Loads a JSON configuration file, and converts it to YAML. """
    if jfile is None:
        jfile = yfile.replace(".yml", ".json")
    with open(yfile, "r") as fp:
        cnf = yaml.safe_load(fp)
    with open(jfile, "w") as fp:
        json.dump(cnf, fp, indent=4, sort_keys=False)


def create_parser():
    parser = argparse.ArgumentParser(prog="convertconfig")
    parser.add_argument("files", nargs="+", help="Input and output files")
    parser.add_argument("--overwrite", "-o", action="store_true", default=False,
                        help="Overwrite exsting output file.")
    return parser


def main(args):

    input_, *output_ = args.files
    fname, ext = os.path.splitext(input_)
    ext = ext.lower()
    if ext in JSON_EXT:
        altext = ".yml"
        converter = json2yaml
    elif ext in YAML_EXT:
        altext = ".json"
        converter = yaml2json
    else:
        raise TypeError(f"Unrecognised input file type: {ext}")

    if output_:
        outname = output_[0]
    else:
        outname = fname
    if not outname.endswith(altext):
        outname = outname + altext

    if tu.verify_fname(outname, overwrite=args.overwrite):
        try:
            converter(input_, outname)
        except Exception as exc:
            print("An ERROR occurred during the conversion.")
            raise exc
        else:
            print(f"Saved: {outname}")


if __name__ == "__main__":

    parser = create_parser()
    if len(sys.argv) > 1:
        args = parser.parse_args()
        main(args)
    else:
        parser.print_help()
