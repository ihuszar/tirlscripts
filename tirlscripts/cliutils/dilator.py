#!/usr/bin/env python

import imageio
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from skimage.morphology.binary import binary_dilation

img = imageio.imread_v2("/Volumes/xenon/AureaMartinsBach/cortex/region_02_seg_only_cortex_bin.png")
dilated = img
for i in range(20):
    print(i)
    dilated = binary_dilation(dilated, np.ones((10, 10)))

dilated = np.asarray(dilated, dtype=np.uint8) * 255
# diff = np.subtract(dilated, img)
# plt.imshow(diff)
# plt.show()
imageio.imsave("/Volumes/xenon/AureaMartinsBach/cortex/region_02_seg_only_cortex_bin_dil200.png", dilated)

