#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


__tirlscript__ = "euler2normal"


# DEPENDENCIES

import sys
import argparse
import numpy as np


# TIRL IMPORTS

from tirl.transformations.euler import TxEulerAngles


# DEFINITIONS

VECTOR = [0, 0, -1]


# IMPLEMENTATION


def main(args):
    """ Main program code. """

    angles = np.asarray(args.rotations, dtype=np.float64)
    vector = np.asarray(args.vector, dtype=np.float64)
    rot = TxEulerAngles(*angles, order="zyx", mode="deg")
    rotated = rot.map(vector).ravel().tolist()
    print("Rotated: ", rotated)


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--vector", nargs="+", default=VECTOR, required=False,
                        help=f"Input vector (default: {VECTOR})")
    parser.add_argument("--rotations", nargs="+", required=True,
                        help="Euler angles (deg, XYZ)")
    return parser


if __name__ == "__main__":
    parser = create_parser()
    if len(sys.argv) > 1:
        args = parser.parse_args()
        main(args)
    else:
        parser.print_help()
