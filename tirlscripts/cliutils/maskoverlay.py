#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


__tirlscript__ = "mask.overlay"


# DEPENDENCIES

import os
import sys
import argparse
import numpy as np
import matplotlib.pyplot as plt
from skimage.exposure import rescale_intensity


# TIRL IMPORTS

import tirl
from tirl.utils import verify_fname


# DEFINITIONS

ALPHA = 0.25
THRESHOLD = 0
OVERWRITE = False


# IMPLEMENTATION

def cmap():
    """
    Returns a colour map for the mask overlay.

    """
    # Red-Green colour map (not colourblind-friendly)
    from matplotlib.colors import LinearSegmentedColormap
    # cmap = LinearSegmentedColormap.from_list('rg', ["r", "w", "g"], N=256)

    # Red-Blue colour map
    cmap = "RdBu"

    return cmap


def overlay(img, threshold=THRESHOLD, alpha=ALPHA):
    """
    Creates a Matplotlib figure with the image and a coloured overlay
    of the image mask.

    :param img: input TImage instance
    :type img: TImage
    :param threshold: threshold to binarise the image mask (0 <= threshold <= 1)
    :type threshold: float
    :param alpha: opacity of the colour overlay
    :type alpha: float

    :returns: Matplotlib figure
    :rtype: matplotlib.Figure
    """

    fig, axes = plt.subplots(1, 1)
    if img.tsize > 1:
        rgb = rescale_intensity(img.data, out_range=np.uint8)
        axes.imshow(np.asarray(rgb, dtype=np.uint8))
    else:
        axes.imshow(img.data, cmap="gray")
    if img.mask is not None:
        mask = np.where(img.mask > threshold, 1, 0)
    else:
        mask = np.ones(img.vshape)
    axes.imshow(mask, cmap=cmap(), alpha=alpha)
    return fig


def main(args):
    """ Main program code. """

    img = tirl.load(args.imfile)
    img.preview()
    fig = overlay(img, alpha=args.alpha, threshold=args.threshold)
    if args.show:
        plt.show()
    if args.save:
        fname, ext = os.path.splitext(args.imfile)
        outfile = fname + "_maskoverlay.png"
        if verify_fname(outfile, overwrite=OVERWRITE):
            fig.savefig(outfile)


def create_parser():
    parser = argparse.ArgumentParser(prog="maskoverlay.py")
    parser.add_argument("imfile", type=str, help="TImage file (.timg)")
    parser.add_argument("--show", action="store_true", default=False,
                        help="Display overlay")
    parser.add_argument("--save", action="store_true", default=False,
                        help="Save overlay")
    parser.add_argument("--alpha", type=float, default=ALPHA,
                        help=f"Transparency (default={ALPHA})")
    parser.add_argument("--threshold", type=float, default=THRESHOLD,
                        help="Treshold to binarise mask.")
    return parser


if __name__ == "__main__":
    parser = create_parser()
    if len(sys.argv) > 1:
        main(parser.parse_args())
    else:
        parser.print_help()
