#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


__tirlscript__ = "normal2euler"


# DEPENDENCIES

import sys
import argparse
import numpy as np


# TIRL IMPORTS

from tirl.transformations.axisangle import TxAxisAngle


# DEFINITIONS

REFERENCE = [0, 0, -1]


# IMPLEMENTATION


def main(args):
    """ Main program code. """

    reference = np.asarray(args.reference, dtype=np.float64)
    vector = np.asarray(args.vector, dtype=np.float64)
    angles = _calc_rotation(reference, vector)
    print(angles)


def _calc_rotation(a, b):
    a = np.asarray(a, dtype=np.float64)
    b = np.asarray(b, dtype=np.float64)
    assert a.size == b.size == 3, "Vectors must be 3-dimensional."
    a = a / np.linalg.norm(a)
    b = b / np.linalg.norm(b)
    axis = np.cross(a, b)
    s = np.linalg.norm(axis)
    c = np.sum(a * b)
    angle = np.arctan2(s, c)
    return TxAxisAngle(angle, axis, mode="rad").euler(mode="deg", order="zyx")


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--reference", nargs="+",
                        default=REFERENCE, required=False,
                        help=f"Reference vector (default: {REFERENCE})")
    parser.add_argument("--vector", nargs="+", required=True,
                        help="Normal vector")
    return parser


if __name__ == "__main__":
    parser = create_parser()
    if len(sys.argv) > 1:
        args = parser.parse_args()
        main(args)
    else:
        parser.print_help()
