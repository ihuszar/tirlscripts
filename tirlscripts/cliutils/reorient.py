#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

"""
tirl reorient --

This tool can be used to redefine voxel axes of a NIfTI image based on
anatomical ground truth, to fix corrupted headers. The qform is set such that
the image is mapped into RAS space in neurological voxel layout.

Warning: it is possible to flip left-right orientation with this tool.

"""


__tirlscript__ = "reorient"


# DEPENDENCIES

import sys
import argparse
import numpy as np
import nibabel as nib


# DEFINITIONS


# IMPLEMENTATION

def main(args):
    """ Main program code. """

    # Load input
    mri = nib.load(args.input)
    dtype = mri.header.get_data_dtype()
    data = np.asarray(mri.get_fdata(), dtype)
    # Reorient voxel array
    data, transposition, flip = reorient_voxels(data, args.axes)
    # Create header
    hdr = get_header(mri, data, args.size)
    # Save output
    nib.save(nib.Nifti1Image(data, hdr.get_qform(), header=hdr), args.output)


def reorient_voxels(data, order):
    ras = "RAS"
    lpi = "LPI"
    transposition = []
    flip = [False, False, False]

    for ix, axis in enumerate(order):
        axis = axis.upper()
        if axis in lpi:
            axis = ras[lpi.index(axis)]
            data = np.flip(data, axis=ix)
            flip[ix] = True
            transposition.append(ras.index(axis))
        elif axis in ras:
            transposition.append(ras.index(axis))
        else:
            raise ValueError(f"Invalid voxel axis direction: {axis}")
    else:
        data = np.moveaxis(data, range(3), transposition)

    return data, transposition, flip


def get_voxel_origin(mri, data, transposition, flip):
    origin = np.asarray([0, 0, 0, 1])
    vcentre = np.linalg.inv(mri.header.get_best_affine()) @ origin
    vcentre = np.asarray(vcentre)[:3][np.asarray(transposition)]
    for ix, axis_flipped in enumerate(flip):
        if axis_flipped:
            vcentre[ix] = data.shape[ix] - vcentre[ix]
    return vcentre


def get_header(mri, data, voxelsize):
    hdr = nib.Nifti1Header()
    hdr.set_intent(mri.header.get_intent("code")[0])
    hdr.set_xyzt_units(*mri.header.get_xyzt_units())
    hdr.set_data_dtype(data.dtype)
    hdr.set_data_shape(data.shape)
    hdr.set_zooms(np.asarray(voxelsize))
    # descr = mri.header["descrip"].tobytes().rstrip(b"\0").decode()
    newdescr = f"Reoriented by TIRL".encode()
    hdr["descrip"] = newdescr + b"\0" * (80 - len(newdescr))
    qform = np.diag([*voxelsize, 1])
    vcentre = np.divide(data.shape, 2)
    qform[:3, 3] -= np.asarray(qform @ np.asarray([*vcentre, 1]))[:3]
    hdr.set_qform(qform, code=1)
    hdr.set_sform(np.eye(4), code=0)
    return hdr


def create_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-i", "--input", type=str, required=True, help="Input file.")
    parser.add_argument(
        "-a", "--axes", type=str, nargs=3, required=True,
        help="Current voxel directions: (R or L) and (P or A) and (S or I)")
    parser.add_argument(
        "-s", "--size", type=float, nargs=3, required=True,
        help="Voxel size in RAS order.")
    parser.add_argument(
        "-o", "--output", type=str, default=None, required=True,
        help="Output file.")

    return parser


if __name__ == "__main__":
    parser = create_parser()
    if len(sys.argv) > 1:
        args = parser.parse_args()
        main(args)
    else:
        parser.print_help()
