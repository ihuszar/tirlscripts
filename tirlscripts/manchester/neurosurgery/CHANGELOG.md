# Changelog


## 14 Oct 2022 -- v1.0.1

- Added: logger instance now records the SHA1 hash of the executing script for strict version control

### 11 Oct 2022 -- v1.0.0

- Project-specific version v1.0.0 assigned to the current state of the library
