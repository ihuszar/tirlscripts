#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


RADIOLOGICAL = "R"
NEUROLOGICAL = "N"
NEUROLOGICAL_ROTATIONS = dict(
                axial=(-90, 0, 0),
                coronal=(-90, 90, 0),
                sagittal=(180, 90, 0)
            )
RADIOLOGICAL_ROTATIONS = dict(
                axial=(-90, 0, 180),
                coronal=(90, 90, 0),
                sagittal=(180, 90, 0)
            )
PLANES = dict(A="axial", C="coronal", S="sagittal")
