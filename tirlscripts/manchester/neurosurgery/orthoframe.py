#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES


import numpy as np
from scipy.linalg import solve
import matplotlib.pyplot as plt
from types import SimpleNamespace


# TIRL IMPORTS

import tirl.settings as ts
from tirl.chain import Chain
from tirl.domain import Domain
from tirl.timage import TImage
from tirl.transformations.embedding import TxEmbed
from tirl.transformations.euler import TxEulerAngles
from tirl.transformations.scale import TxScale, TxIsoScale
from tirl.transformations.translation import TxTranslation


# DEFINITIONS

from tirlscripts.manchester.neurosurgery.constants import *


# IMPLEMENTATION

class OrthoFrame(object):
    """
    Object that treats the rigid transformation of three orthogonal Domains
    collectively.

    """

    def __init__(self, axial, coronal, sagittal, scale=1, origin=(0, 0, 0),
                 rotation=(0, 0, 0), orientation=NEUROLOGICAL):
        """
        Initialiser of OrthoFrame.

        :param axial: axial frame or image
        :type axial: Domain or TImage
        :param coronal: coronol frame or image
        :type coronal: Domain or TImage
        :param sagittal: sagittal frame or image
        :type sagittal: Domain or TImage
        :param scale: scaling factor for all frames, aka pixel size
        :type scale: float
        :param origin: intersection point of the 3 frames
        :type origin: Iterable[float]

        """
        super(OrthoFrame, self).__init__()

        self._scaletx = TxIsoScale(scale, dim=2, name="scale")
        self._origintx = TxTranslation(*origin, name="origin")
        self._rottx = TxEulerAngles(
            *rotation, mode="deg", order="zyx", name="rotation")

        self.axial = axial
        self.coronal = coronal
        self.sagittal = sagittal
        self.scale = scale
        self.origin = np.asarray(origin, dtype=np.float64)
        self.rotation = np.asarray(rotation, dtype=np.float64)
        self.orientation = orientation

        # Set internal and external transformations for all three planes
        for plane, arg in zip(PLANES.values(), (axial, coronal, sagittal)):
            # Internal
            self._set_internal_transformations(
                arg, sampling=(1, 1), offset=(0, 0))
            # External
            if orientation.upper() == RADIOLOGICAL:
                slice_rotations = RADIOLOGICAL_ROTATIONS[plane]
            elif orientation.upper() == NEUROLOGICAL:
                slice_rotations = NEUROLOGICAL_ROTATIONS[plane]
            else:
                raise NameError(f"Unrecognised orientation: {orientation}")
            self._set_external_transformations(
                getattr(self, plane),
                slice_rotations, self.rottx, self.scaletx, self.origintx
            )

        # Set shared parameter bounds
        self.scaletx.parameters.set_bounds()
        self.origintx.parameters.set_bounds()

    @property
    def scale(self):
        return float(self._scaletx.parameters[:])

    @scale.setter
    def scale(self, s):
        self._scaletx.parameters[:] = float(s)

    @property
    def origin(self):
        return self._origintx.parameters[:]

    @origin.setter
    def origin(self, o):
        self._origintx.parameters[:] = np.asarray(o)

    @property
    def rotation(self):
        return self._rottx.angles

    @rotation.setter
    def rotation(self, r):
        self._rottx.angles = np.asarray(r)

    @property
    def rottx(self):
        return self._rottx

    @property
    def scaletx(self):
        return self._scaletx

    @property
    def origintx(self):
        return self._origintx

    @property
    def intersection(self):
        """
        Returns the coordinates of true intersection point
        of the three planes.

        The intersection point may be different from the "origin" as defined
        by the OrthoFrame class. The origin represents the intersection of the
        planes before any slice-specific adjustments. Hence, the "true" origin
        is in fact given by the intersection point.

        :returns: the coordinates of the intersection point
        :rtype: tuple[float]

        """
        normals = []
        centres = []
        distances = []
        for plane in PLANES.values():
            domain = getattr(self, plane).domain
            centres.append(domain.pcentre())
            n_plane = domain.map_voxel_vectors([0, 0, 1], [0, 0]).ravel()
            normals.append(n_plane[:3])
            distances.append(np.dot(normals[-1], centres[-1]))
        else:
            A = np.vstack(normals)
            b = np.asarray(distances)

        return tuple(solve(A, b).ravel().tolist())

    @staticmethod
    def _set_internal_transformations(arg, sampling, offset):
        if isinstance(arg, Domain):
            domain = arg
        elif isinstance(arg, TImage):
            domain = arg.domain
        else:
            raise TypeError(f"Expected a Domain or TImage instance, "
                            f"got {arg.__class__.__name__}")
        try:
            domain.get_internal_transformation("resample")
        except IndexError:
            domain.internal.append(TxScale(*sampling, name="resample"))
        try:
            domain.get_internal_transformation("offset")
        except IndexError:
            domain.internal.append(TxTranslation(*offset, name="offset"))

    @staticmethod
    def _set_external_transformations(
            arg, slice_rotations, tx_rot, tx_scale, tx_origin):
        if isinstance(arg, Domain):
            domain = arg
        elif isinstance(arg, TImage):
            domain = arg.domain
        else:
            raise TypeError(f"Expected a Domain or TImage instance, "
                            f"got {arg.__class__.__name__}")
        cshift = -domain.internal.map(domain.vcentre())
        tx_cent = TxTranslation(*cshift, name="centralise")
        embed = TxEmbed(1, name="embed")
        tx_slicerot = TxEulerAngles(
            slice_rotations, order="zyx", mode="deg", name="slice-rotation")
        tx_nudge = TxTranslation(0, 0, 0, name="nudge")
        domain.external = Chain([tx_cent, tx_scale, embed, tx_slicerot,
                                 tx_rot, tx_origin, tx_nudge])

    def sample(self, source, reset=False):
        results = dict()
        for plane in PLANES.values():
            arg = getattr(self, plane)
            if isinstance(arg, Domain):
                domain = arg
            elif isinstance(arg, TImage):
                domain = arg.domain
            else:
                raise TypeError(f"Expected Domain or TImage, "
                                f"got {arg.__class__.__name__} instead.")
            results[plane] = source.evaluate(domain)
            if reset:
                results[plane].domain.reset(internal=False)
        return SimpleNamespace(**results)

    def _rescale_domain_arg(self, plane, factor):
        olddomain = getattr(self, plane)
        newshape = np.multiply(olddomain.shape, factor).astype(int)
        factors = np.divide(olddomain.shape, newshape)
        newdomain = Domain(*newshape, name=plane)
        self._set_internal_transformations(newdomain, factors, (0, 0))
        shift = np.subtract(
            newdomain.internal.map(newdomain.vcentre()),
            olddomain.internal.map(olddomain.vcentre())
        )
        newdomain.internal["offset"] = TxTranslation(*shift, name="offset")
        return newdomain

    def _rescale_image_arg(self, plane, factor):
        return getattr(self, plane).rescale(factor, copy=True)

    def rescale(self, factor):
        args = dict()
        for plane in PLANES.values():
            if isinstance(getattr(self, plane), Domain):
                args[plane] = self._rescale_domain_arg(plane, factor)
            elif isinstance(getattr(self, plane), TImage):
                args[plane] = self._rescale_image_arg(plane, factor)
        return OrthoFrame(
            args["axial"], args["coronal"], args["sagittal"],
            scale=self.scale, origin=self.origin, orientation=self.orientation
        )

    def save(self, basename, source=None, overwrite=False):
        for plane in PLANES.values():
            arg = getattr(self, plane)
            ext = ts.EXTENSIONS[arg.__class__.__name__]
            if source is not None:
                arg = source.evaluate(arg.domain)
            arg.save(basename + plane + "." + ext, overwrite=overwrite)

    def snapshot(self, basename, source=None, overwrite=False):
        for plane in PLANES.values():
            arg = getattr(self, plane)
            ext = ts.TIMAGE_DEFAULT_SNAPSHOT_EXT
            if source is not None:
                arg = source.evaluate(arg.domain)
            arg.snapshot(basename + plane + "." + ext, overwrite=overwrite)

    def preview(self, source=None):
        if source is not None:
            result = self.sample(source)
        else:
            result = self
        fig, axes = plt.subplots(1, 3, figsize=(15, 5))
        axes[0].imshow(result.axial.data, cmap="gray")
        axes[0].set_title("Axial")
        axes[1].imshow(result.coronal.data, cmap="gray")
        axes[1].set_title("Coronal")
        axes[2].imshow(result.sagittal.data, cmap="gray")
        axes[2].set_title("Sagittal")
        fig.suptitle(f"Origin: {self.origin} mm, Pixelsize: {self.scale} mm")
        plt.show()

    def savefig(self, filename, source=None):
        if source is not None:
            result = self.sample(source)
        else:
            result = self
        fig, axes = plt.subplots(1, 3, figsize=(15, 5))
        axes[0].imshow(result.axial.data, cmap="gray")
        axes[0].set_title("Axial")
        axes[1].imshow(result.coronal.data, cmap="gray")
        axes[1].set_title("Coronal")
        axes[2].imshow(result.sagittal.data, cmap="gray")
        axes[2].set_title("Sagittal")
        fig.suptitle(f"Origin: {self.origin} mm, Pixelsize: {self.scale} mm")
        fig.savefig(filename)
        plt.close(fig)
