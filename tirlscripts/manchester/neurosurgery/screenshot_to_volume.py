#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


__tirlscript__ = "nsurg.ss2v"


# DESCRIPTION

"""
screenshot_to_volume.py --

Registers the panels of a three-panel orthogonal view from a screenshot to
the corresponding 3D volume. The algorithm finds the precise position and
scaling of the orthogonal view, but it requires a reasonable initial
guess of the screenshot image resolution.

"""


# DEVELOPMENT NOTES

"""
3 Oct 2022 - INH

Assumptions:
============
- the segmentation routine assumes vertical gray lines between the panels
- the panels intersect each other at their non-weighted geometrical centre
- the panels have a constant scale
- the panels are pairwise orthogonal views into the MRI
- the axes of the orthogonal view are parallel to the MRI axes
- the screenshot is taken from the volume file (not a different scan)


Critical considerations:
========================
- the panel orientations in the screenshots may vary between images
- the scale of the panels is identical, but may not be the same as the MRI
- the panels may show the brain in partial coverage
- the screenshots may be overlaid with graphics or text
- In essence, only four parameters need to be optimised: the 3D coordinates
of the centre and the isotropic scaling factor. However, uncertainties can 
make it difficult to optimise these parameters directly.


UI considerations:
==================
- panel orientaitons in the screenshots should be defined by user input
- NIfTI orientation may not be consistent (fslreorient2std as prerequisite?)


Algorithm:
==========
- split the panels into separate images
- determine the panel size very accurately by segmenting the middle panel
- create an orthogonal tri-domain construct
- segment brain using BET and define N test points based on the volume mask
- alternatively, ask for a point input, and define a scattered N-cloud around it
- measure the total cost at the N points at low resolution (coarse search)
- fine-tune the position and scale at the best k<N points


Eliminating colour overlays from the screenshot:
================================================
- convert screenshot to Lab colour space
- do a max intensity projection on the (ab) substack
- low-pass at Otsu threshold to create an inclusion mask
- use L component image for registration with the above mask


Limitations:
============
- if the panel segmentation is inaccurate, one or more panels might be off
- the NMI may not perform well if the zoom is large enough to fill the panels
- the search space of the "coarse search" stage is not optimised


Notes for further development:
==============================
- Option to take user input for the most probable location (e.g. a single point)
- An MCMC on the MRI mask voxel set would probably be more efficient and
reliable than the current direct search.
- Discrete optimisation would be preferred here, since orthogonal view comes
from a voxel grid.

"""


# DEPENDENCIES

import os
import cv2
import sys
import logging
import imageio
import argparse
import numpy as np
from attrdict import AttrMap
from datetime import datetime
import matplotlib.pyplot as plt
from types import SimpleNamespace
from skimage.color import rgb2lab
from sklearn.cluster import KMeans
from skimage.morphology import dilation
from skimage.filters.thresholding import threshold_otsu


# TIRL IMPORTS

import tirl
import tirl.settings as ts
from tirl.timage import TImage
from tirl.cost.mi import CostMI
from tirl.optimisation.optnl import OptNL
from tirl.optimisation.optgroup import OptimisationGroup


# TIRLSCRIPTS IMPORTS

from tirlscripts import srchash
from tirlscripts.oxford.scriptutils import general
from tirlscripts.manchester.neurosurgery.orthoframe import OrthoFrame


# DEFINITIONS

from tirlscripts.manchester.neurosurgery import __version__
from tirlscripts.manchester.neurosurgery.constants import *

LOGGER_NAME = "tirlscripts.manchester.neurosurgery.screenshot_to_volume"
logger = logging.getLogger(LOGGER_NAME)
KMEANS_SUBSAMPLING_FACTOR = 200

DEFAULTS = dict(
    pmsf_sp=5, pmsf_sr=5,
    canny_low=5, canny_high=100,
    line_occupancy=0.55
)


# IMPLEMENTATION

def run(cnf=None, **options):
    """
    Runs TIRL screenshot-to-volume registration.

    :param cnf:
        Configuration file. If no file is specified (default), the default
        parameters will be used that are embedded in the source code file
        (see Definitions section above). Instead of a file, a dictionary with
        suitable content may also be specified.
    :type cnf: Union[str, dict, None]
    :param options:
        Overriding configuration parameters.
    :type options: Any

    """
    # Load script configuration
    if cnf is not None:
        if isinstance(cnf, dict):
            cnf = dict(cnf)
        elif isinstance(cnf, str):
            cnf = general.load_configurations(cnf)
        else:
            raise TypeError(
                f"Unrecognised configuration format: "
                f"{cnf.__.class__.__name__}")
    cnf.update(options)
    global logger
    p, logger = general.initialise_script(**cnf)
    logger.debug(f"tirlscripts neurosurgery library version: {__version__}")
    logger.debug(f"screenshot_to_volume.py SHA1: {srchash(__file__)}")

    # Load and configure input images
    volume = load_volume(cnf)
    ortho, rgbortho = load_orthogonal_frame(cnf)

    # Run the registration routine
    try:
        register(ortho, volume, cnf)
    except Exception as exc:
        logger.error(exc.args)
        logger.fatal(f"The registration terminated with an exception.")
        raise exc

    # Conclude run: generate outputs
    if rgbortho is not None:
        # Preserve RGB images
        rgbortho = OrthoFrame(
            rgbortho.axial, rgbortho.coronal, rgbortho.sagittal,
            scale=ortho.scale, origin=ortho.intersection,
            rotation=ortho.rotation, orientation=ortho.orientation
        )
        # Transfer the optimised transformations to the new OrthoFrame
        for plane in PLANES.values():
            getattr(rgbortho, plane).domain = getattr(ortho, plane).domain
    else:
        rgbortho = ortho

    logger.info("Concluding run, generating outputs...")
    conclude(rgbortho, volume, cnf)

    logger.fatal("The registration was completed successfully.")


def load_screenshot(filename, mask=None):
    screenshot = imageio.imread_v2(filename)
    if isinstance(mask, str) and os.path.isfile(mask):
        mask = imageio.imread_v2(mask)
    return screenshot, mask


def get_extractors(monochrome, std_tresh, mean_tresh):
    """
    Exports masked panels as TImages from an orthographic screenshot.

    """
    from scipy.ndimage.filters import gaussian_filter
    # Find panel separators
    h, w = monochrome.shape[:2]
    diff = np.subtract(monochrome, gaussian_filter(monochrome, 5))
    std_plot = np.std(diff, axis=0)
    mean_plot = np.mean(diff, axis=0)
    select = np.logical_and(std_plot < std_tresh, mean_plot > mean_tresh)
    cs = np.cumsum(select.astype(int))

    # Define panel regions
    central_extractor = np.asarray(cs == cs[w // 2], dtype=int)
    central_size = np.count_nonzero(central_extractor)
    left_extractor = np.asarray(cs == cs[w // 2 - central_size], dtype=int)
    right_extractor = np.asarray(cs == cs[w // 2 + central_size], dtype=int)

    return dict(
        left=left_extractor,
        centre=central_extractor,
        right=right_extractor
    )


def extract_panel(img, extractor):
    newshape = np.asarray(img.shape)
    newshape[1] = np.count_nonzero(extractor)
    return img[:, extractor > 0, ...].reshape(newshape)


def split_panels(scr, layout, mask=None):
    scr = img2array(scr)
    xsep, ysep = find_separators(scr)
    panels = extract_panels(scr, layout, xsep, ysep)
    for view, panel in panels.items():
        plt.imshow(panel)
        plt.suptitle(view)
        plt.show()
    exit()


def extract_panels(img, layout, xsep, ysep):

    # Define the expected locations of cell centres
    h, w, c = img.shape
    layout = np.atleast_2d(layout)
    n_rows, n_cols = layout.shape
    cell_height = h / n_rows
    cell_width = w / n_cols
    xsep = np.sort(xsep)
    ysep = np.sort(ysep)

    margin = 0.2

    # Determine rows
    rowsep = []
    row_centres = np.round(cell_height / 2 + np.arange(0, h, cell_height))
    for r, y0 in enumerate(row_centres):

        # Top border
        y1 = max(y0 - cell_height / 2 - cell_height * margin, 0)
        y2 = max(min(y0 - cell_height / 2 + cell_height * margin, y0), 0)
        r = (ysep >= y1) & (ysep <= y2)
        if np.any(r):
            top = int(np.max(ysep[r]))
        else:
            top = int(max(y0 - cell_height / 2, 0))

        # Bottom border
        y1 = min(max(y0 + cell_height / 2 - cell_height * margin, y0), h)
        y2 = min(y0 + cell_height / 2 + cell_height * margin, h)
        r = (ysep >= y1) & (ysep <= y2)
        if np.any(r):
            bottom = int(np.min(ysep[r]))
        else:
            bottom = int(max(y0 + cell_height / 2, h))

        rowsep.append((top, bottom))

    # Determine columns
    colsep = []
    col_centres = np.round(cell_width / 2 + np.arange(0, w, cell_width))
    for c, x0 in enumerate(col_centres):

        # Left border
        x1 = max(x0 - cell_width / 2 - cell_width * margin, 0)
        x2 = max(min(x0 - cell_width / 2 + cell_width * margin, x0), 0)
        r = (xsep >= x1) & (xsep <= x2)
        if np.any(r):
            left = int(np.max(xsep[r]))
        else:
            left = int(max(x0 - cell_width / 2, 0))

        # Right border
        x1 = min(max(x0 + cell_width / 2 - cell_width * margin, x0), w)
        x2 = min(x0 + cell_width / 2 + cell_width * margin, w)
        r = (xsep >= x1) & (xsep <= x2)
        if np.any(r):
            right = int(np.min(xsep[r]))
        else:
            right = int(max(x0 + cell_width / 2, w))

        colsep.append((left, right))

    panels = dict()
    for r, row in enumerate(layout):
        top, bottom = rowsep[r]
        for c, col in enumerate(row):
            left, right = colsep[c]
            panels[col] = img[top:bottom, left:right]

    return panels


def img2array(img):
    """
    Returns the input image as an 8-bit array with the shape (h, w, c).
    For grayscale images, c = 1.

    """
    img = np.squeeze(np.asarray(img, dtype=np.uint8))
    if img.ndim == 3:
        if img.shape[-1] in (3, 4):
            img = img[..., :3]
        else:
            raise AssertionError(
                f"Expected a 3 or 4-channel image, "
                f"got {int(img.shape[-1])} channels instead."
            )
    elif img.ndim == 2:
        img = img[..., np.newaxis]
    else:
        raise AssertionError(
            f"Expected a 2D image, got an image with "
            f"{int(img.ndim)} dimension(s) instead."
        )
    return img


def split_panels_old(rgb, mask, order, scheme, std_tresh=1.0, mean_tresh=1.0,
                 outputrgb=True):
    rgb = np.squeeze(np.asarray(rgb))
    isrgb = (rgb.ndim == 3) and (rgb.shape[-1] in (3, 4))
    if isrgb:
        # solve(rgb, scheme)
        # analysergb(rgb)
        # cv(rgb)
        unified(rgb)
        L, a, b = rgb2lab(np.asarray(rgb)[..., :3]).transpose((2, 0, 1))
    else:
        L = rgb
        a = b = None

    # analyse(L, a, b)

    exit()

    # Get extractors (1-pixel-tall binary masks)
    extractors = get_extractors(
        monochrome=L,
        std_tresh=std_tresh,
        mean_tresh=mean_tresh
    )

    # Split grayscale image (and RGB image, if exists) into panels
    keys = ("left", "centre", "right")
    graypanels = {key: extract_panel(L, extractors[key]) for key in keys}
    if isrgb and outputrgb:
        rgbpanels = {key: extract_panel(rgb, extractors[key]) for key in keys}
    else:
        rgbpanels = None

    # No mask --> don't use them
    if mask is None:
        masks = {key: None for key in keys}
    # Auto mask --> create them
    elif isinstance(mask, str) and (mask.lower() == "auto"):
        mask = generate_panel_masks(a, b)
        masks = {key: extract_panel(mask, extractors[key]) for key in keys}
    # User-specified mask --> use them
    else:
        mask = np.asarray(mask)
        if mask.ndim == 3:
            mask = np.linalg.norm(mask, axis=-1)
        masks = {key: extract_panel(mask, extractors[key]) for key in keys}

    # Cast panels and their masks into TImage instances
    mapper = {key: PLANES[letter.upper()] for key, letter in zip(keys, order)}

    graypanels = {
        mapper[key]: TImage(graypanels[key], mask=masks[key], name=key)
        for key in keys
    }
    if (rgbpanels is not None) and outputrgb:
        rgbpanels = {
            mapper[key]: TImage(rgbpanels[key], mask=masks[key], name=key)
            for key in keys
        }
    else:
        rgbpanels = {mapper[key]: None for key in keys}

    if outputrgb:
        return SimpleNamespace(**graypanels), SimpleNamespace(**rgbpanels)
    else:
        return SimpleNamespace(**graypanels)


def gausskernel(n=11, sigma=1):
    from math import pi, sqrt, exp
    r = range(-int(n / 2), int(n / 2) + 1)
    return [
        1 / (sigma * sqrt(2 * pi)) * exp(-float(x) ** 2 / (2 * sigma ** 2))
        for x in r]


def lineliness(img, threshold=None, smooth=0, direction="vertical", eps=1e-10,
               normalise=True):

    if threshold is not None:
        img = np.where(img > threshold, 1, 0)
    direction = str(direction).lower()
    k = 1

    if direction == "vertical":
        grad = np.abs(np.gradient(img, axis=1))
        result = (np.where(grad > eps, 1, 0)) / \
                 (np.abs(np.gradient(img, axis=0)) + eps)
        result /= np.max(result)
        result = np.exp(1 + k * np.mean(result, axis=0))
    elif direction == "horizontal":
        grad = np.abs(np.gradient(img, axis=0))
        result = (np.where(grad > eps, 1, 0)) / \
                 (np.abs(np.gradient(img, axis=1)) + eps)
        result /= np.max(result)
        result = np.exp(1 + k * np.mean(result, axis=1))
    else:
        raise ValueError("Direction must be either horizontal or vertical.")

    if smooth > 0:
        kernel = gausskernel(2 * smooth + 1, sigma=smooth)
        result = np.convolve(result, kernel)
    elif smooth < 0:
        raise ValueError("Smmothing radius must be non-negative.")

    if normalise:
        result = result / np.max(result)

    return result


def lineliness_diff(img, threshold=None, smooth=0, direction="vertical", eps=1e-10,
               normalise=True):

    # if threshold is not None:
    #     img = np.where(img > threshold, 1, 0)
    direction = str(direction).lower()

    if direction == "vertical":
        result = (np.diff(img, axis=1)[:-1, :] ** 2) / \
                 (np.diff(img, axis=0)[:, :-1] ** 2 + eps)
        result = np.append(np.mean(result, axis=0), 0)
    elif direction == "horizontal":
        result = (np.diff(img, axis=0)[:, :-1] ** 2) / \
                 (np.diff(img, axis=1)[:-1, :] ** 2 + eps)
        result = np.append(np.mean(result, axis=1), 0)
    else:
        raise ValueError("Direction must be either horizontal or vertical.")

    if smooth > 0:
        kernel = gausskernel(2 * smooth + 1, sigma=smooth)
        result = np.convolve(result, kernel)
    elif smooth < 0:
        raise ValueError("Smmothing radius must be non-negative.")

    if normalise:
        result = result / np.max(result)

    return result


def lineliness_var(img, threshold=None, smooth=0, direction="vertical",
                   eps=1e-5, normalise=True):

    if threshold is not None:
        img = np.where(img > threshold, 1, 0)
    else:
        img = img / np.max(img)
    direction = str(direction).lower()

    if direction == "vertical":
        # grad = np.where(np.abs(np.gradient(img, axis=1)) > eps, 1, 0)
        var = np.var(img, axis=0)
        var[var == 0] = np.percentile(var, 1)
        result = np.divide(np.mean(img, axis=0), var)
        # result[~np.isfinite(result)] = np.max(result[np.isfinite(result)])
    elif direction == "horizontal":
        # grad = np.where(np.abs(np.gradient(img, axis=0)) > eps, 1, 0)
        var = np.var(img, axis=1)
        var[var == 0] = np.percentile(var, 1)
        result = np.divide(np.mean(img, axis=1), var)
        # result[~np.isfinite(result)] = np.max(result[np.isfinite(result)])
    else:
        raise ValueError("Direction must be either horizontal or vertical.")

    if smooth > 0:
        kernel = gausskernel(2 * smooth + 1, sigma=smooth)
        result = np.convolve(result, kernel)
    elif smooth < 0:
        raise ValueError("Smmothing radius must be non-negative.")

    if normalise:
        result = result / np.max(result)

    return result


def analyse(L, a, b):
    from scipy.ndimage.filters import sobel, gaussian_filter1d
    from scipy.signal import find_peaks

    Lx = lineliness(L, threshold=0, smooth=3, direction="vertical")
    ax = lineliness(a, threshold=0, smooth=3, direction="vertical")
    bx = lineliness(b, threshold=0, smooth=3, direction="vertical")
    x = Lx - ax - bx
    peaks, _ = find_peaks(x, height=0.5)
    plt.plot(x)
    plt.plot(peaks, x[peaks], "x")
    plt.show()

    Ly = lineliness(L, threshold=0, smooth=3, direction="horizontal")
    ay = lineliness(a, threshold=0, smooth=3, direction="horizontal")
    by = lineliness(b, threshold=0, smooth=3, direction="horizontal")
    y = Ly - ay - by
    peaks, _ = find_peaks(y, height=0.5)
    plt.plot(y)
    plt.plot(peaks, y[peaks], "x")
    plt.show()

    plt.imshow(np.outer(y, x), cmap="gray")
    plt.show()
    plt.imshow(np.outer(np.where(y > 0, y, 0), np.where(x > 0, x, 0)))
    plt.show()


def cv(rgb):
    import cv2

    smooth = cv2.pyrMeanShiftFiltering(rgb[..., :3], 5, 5)
    plt.imshow(smooth)
    plt.show()
    gray = cv2.cvtColor(smooth, cv2.COLOR_RGB2GRAY)
    # kernel_size = 3
    # blur_gray = cv2.GaussianBlur(gray, (kernel_size, kernel_size), 0)
    blur_gray = gray

    low_threshold = 5
    high_threshold = 100  # np.inf

    edges = cv2.Canny(blur_gray, low_threshold, high_threshold)
    plt.imshow(edges, cmap="gray")
    plt.show()

    rho = 1  # distance resolution in pixels of the Hough grid
    theta = np.pi / 2  # angular resolution in radians of the Hough grid
    threshold = 200  # minimum number of votes (intersections in Hough grid cell)
    min_line_length = 250  # minimum number of pixels making up a line
    max_line_gap = 50  # maximum gap in pixels between connectable line segments
    line_image = np.copy(rgb) * 0  # creating a blank to draw lines on

    # Run Hough on edge detected image
    # Output "lines" is an array containing endpoints of detected line segments
    lines = cv2.HoughLinesP(edges, rho, theta, threshold, np.array([]),
                            min_line_length, max_line_gap)

    for line in lines:
        for x1, y1, x2, y2 in line:
            cv2.line(line_image, (x1, y1), (x2, y2), (255, 0, 0), 1)

    # Draw the lines on the  image
    lines_edges = cv2.addWeighted(rgb, 0.8, line_image, 1, 0)

    plt.imshow(lines_edges)
    plt.show()


def find_separators(scr, **kwargs):
    """
    :param scr: screenshot with (h, w, c) dimensions
    :type scr: np.ndarray

    """
    h, w, c = scr.shape

    # Extract separator lines from the image
    if c == 3:
        smooth = cv2.pyrMeanShiftFiltering(
            src=scr,
            sp=kwargs.get("sp", DEFAULTS["pmsf_sp"]),
            sr=kwargs.get("sr", DEFAULTS["pmsf_sr"])
        )
        gray = cv2.cvtColor(smooth, cv2.COLOR_RGB2GRAY)
    else:
        # TODO: Add anisotropic smoothing here with radius = sr
        gray = np.squeeze(scr)

    edges = cv2.Canny(
        gray,
        threshold1=DEFAULTS["canny_low"],
        threshold2=DEFAULTS["canny_high"]
    )
    hsep = cv2.Sobel(edges, -1, 0, 1)  # horizontal separator line image
    vsep = cv2.Sobel(edges, -1, 1, 0)  # vertical separator line image

    plt.imshow(hsep, cmap="gray")
    plt.suptitle("Horizontal separators")
    plt.show()

    plt.imshow(vsep, cmap="gray")
    plt.suptitle("Vertical separators")
    plt.show()

    # Estimate the positions the separator lines based on the layout
    from scipy.signal import find_peaks
    vdata = np.mean(vsep, axis=0)
    xpeaks = find_peaks(vdata, height=DEFAULTS["line_occupancy"] * 255)[0]
    plt.plot(vdata)
    plt.scatter(xpeaks, vdata[xpeaks], marker="x", c="orange")
    plt.show()

    hdata = np.mean(hsep, axis=1)
    ypeaks = find_peaks(hdata, height=DEFAULTS["line_occupancy"] * 255)[0]
    plt.plot(hdata)
    plt.scatter(ypeaks, hdata[ypeaks], marker="x", c="orange")
    plt.show()

    return xpeaks, ypeaks


def find_peaks_mad(data, window=20, k=5):
    peaks = set()
    for shift in range(len(data) - window):
        segment = np.roll(data, -shift)[:window]
        m = np.median(segment)
        mad = np.median(np.abs(segment - m))
        local_peaks = shift + np.where(segment - m > k * mad)[0]
        peaks.update(np.unique(local_peaks))
    else:
        peaks = np.asarray(sorted([*peaks]))

    return np.asarray(sorted([*peaks]))


def find_peaks_diff(data, dist=5):
    plt.plot(data)
    d2x = np.gradient(np.gradient(data))
    peakness = np.where(data > 0, 1, 0) * np.where(d2x < 0, -d2x, 0)
    peakness /= np.max(peakness)
    peaks = np.where(peakness > 0.5)[0]
    # plt.plot(peakness)
    # plt.show()
    return peak_filter(np.asarray(sorted([*peaks])), data, dist)


def peak_filter(peaks, data, dist):

    if len(peaks) > 1:
        peaks = list(peaks)
        values = list(data[peaks])
        i = 0
        while True:
            if (peaks[i + 1] - peaks[i]) < dist:
                if values[i + 1] >= values[i]:
                    peaks.pop(i)
                    values.pop(i)
                else:
                    peaks.pop(i + 1)
                    values.pop(i + 1)
            else:
                i += 1
            if (i + 1) >= len(peaks):
                break
    return np.asarray(peaks)


def analysergb(rgb):
    from scipy.signal import find_peaks

    r, g, b = np.moveaxis(rgb, 2, 0)[:3, ...]
    greenmask = np.where((g > r) & (g > b), 1, 0)
    plt.imshow(greenmask, cmap="gray")
    plt.show()

    sigma = 1
    threshold = None
    window = 50
    k = 100
    dist = 5
    height = 0.1

    Lx = lineliness(
        r + g + b, threshold=threshold, smooth=sigma, direction="vertical",
        normalise=False)
    gx = lineliness(
        greenmask, threshold=threshold, smooth=sigma, direction="vertical",
        normalise=False)
    x = Lx - gx
    x /= np.max(x)
    x[0] = 1
    x[-1] = 1
    # peaks, _ = find_peaks(x, height=height)
    peaks = peak_filter(find_peaks_mad(x, window, k), x, dist)
    peaks = np.asarray([0, *peaks, len(x) - 1])
    peaks = np.sort(np.unique(peaks))
    # peaks = find_peaks_mad(x, window=window, k=k, dist=dist)
    # peaks = [*peaks, *find_peaks_diff(x)]
    # peaks = np.sort(np.unique(peaks))
    print(peaks)
    print("Centre")
    print((peaks + peaks.reshape(-1, 1)) / 2)
    print("Size")
    print(peaks - peaks.reshape(-1, 1))
    plt.plot(np.where(x > 0, x, 0))
    plt.plot(peaks, x[peaks], "x")
    plt.show()

    Ly = lineliness(
        r + g + b, threshold=threshold, smooth=sigma, direction="horizontal",
        normalise=False)
    gy = lineliness(
        greenmask, threshold=threshold, smooth=sigma, direction="horizontal",
        normalise=False)
    y = Ly - gy
    y /= np.max(y)
    y[0] = 1
    y[-1] = 1
    # peaks, _ = find_peaks(y, height=height)
    peaks = peak_filter(find_peaks_mad(y, window, k), y, dist)
    peaks = np.asarray([0, *peaks, len(y) - 1])
    peaks = np.sort(np.unique(peaks))
    # peaks = find_peaks_mad(y, window=window, k=k, dist=dist)
    # peaks = [*peaks, *find_peaks_diff(y)]
    # peaks = np.sort(np.unique(peaks))
    plt.plot(np.where(y > 0, y, 0))
    plt.plot(peaks, y[peaks], "x")
    plt.show()

    # plt.imshow(np.outer(np.where(x > 0, x, 0), np.where(y > 0, y, 0)).T, cmap="gray")
    # plt.show()


def solve(rgb, scheme):
    n_rows, n_cols = scheme
    h, w, d = rgb.shape
    width = w // n_cols
    height = h // n_rows
    rows = np.linspace(0, h, 2 * n_rows + 1)[1::2]
    columns = np.linspace(0, w, 2 * n_cols + 1)[1::2]
    for c in columns:
        j0 = int(max(c - width // 6, 0))
        j1 = int(min(c + width // 6, w))
        stripe = rgb[:, j0:j1, ...]
        print(f"Column {c + 1}:")
        analysergb(stripe)

    for r in rows:
        i0 = int(max(r - height // 6, 0))
        i1 = int(min(r + height // 6, h))
        stripe = rgb[i0:i1, :, ...]
        print(f"Row {r + 1}:")
        analysergb(stripe)

    # for r, c in zip(rows, columns):
    #     i0 = int(max(r - height // 6, 0))
    #     i1 = int(min(r + height // 6, h))
    #     j0 = int(max(c - width // 6, 0))
    #     j1 = int(min(c + width // 6, w))
    #     stripe = rgb[i0:i1, j0:j1, ...]
    #     print(f"Row {r + 1}, Column {c + 1}:")
    #     analysergb(stripe)
    exit()



def generate_panel_masks(a, b):
    maxproj = np.max(np.dstack((a, b)), axis=-1)
    mask = np.asarray(maxproj < threshold_otsu(maxproj), dtype=int)
    mask = dilation(mask, np.ones((5, 5)))
    return mask


def load_orthogonal_frame(cnf):
    p = AttrMap(cnf)
    q = p.screenshot
    screenshot, mask = load_screenshot(filename=str(q.file), mask=q.mask)
    logger.info(f"Loaded screenshot from {q.file}")
    logger.info("Splitting into panels...")
    panels = split_panels(scr=screenshot, layout=q.layout, mask=mask)
    logger.info("Extracting monochrome panels...")
    grayortho = OrthoFrame(
        graypanels.axial, graypanels.coronal, graypanels.sagittal,
        scale=np.atleast_1d(q.resolution)[0],
        origin=np.atleast_1d(q.origin),
        rotation=np.atleast_1d(q.rotation),
        orientation=p.volume.orientation
    )
    if rgbpanels.axial is not None:
        logger.info("Extracting RGB panels...")
        rgbortho = OrthoFrame(
            rgbpanels.axial, rgbpanels.coronal, rgbpanels.sagittal,
            scale=np.atleast_1d(q.resolution)[0],
            origin=np.atleast_1d(q.origin),
            rotation=np.atleast_1d(q.rotation),
            orientation=p.volume.orientation
        )
    else:
        rgbortho = None

    if q.visualise:
        if rgbortho is not None:
            visualortho = rgbortho
        else:
            visualortho = grayortho
        visualortho.preview()

    if q.export:
        if rgbortho is not None:
            visualortho = rgbortho
        else:
            visualortho = grayortho

        bname = os.path.join(p.general.outputdir, "input-")
        try:
            visualortho.save(bname, overwrite=True)
        except Exception as exc:
            logger.error(
                f"Could not save the input slices as {bname}*.timg")
            logger.error(exc.args)
        else:
            logger.info(f"Saved the input slices as {bname}*.timg")
        try:
            visualortho.snapshot(bname, overwrite=True)
        except Exception as exc:
            logger.error(
                f"Could not save the input slices as {bname}*."
                f"{ts.TIMAGE_DEFAULT_SNAPSHOT_EXT}")
            logger.error(exc.args)
        else:
            logger.info(f"Saved the input slices as {bname}*."
                        f"{ts.TIMAGE_DEFAULT_SNAPSHOT_EXT}")

    return grayortho, rgbortho


def load_volume(cnf):
    """
    Returns a masked TImage by loading a file.

    """
    p = AttrMap(cnf)
    q = p.volume
    filename = str(q.file)

    if filename.endswith(ts.EXTENSIONS["TImage"]):
        return tirl.load(filename)

    use_sform = "sform" if q["use-sform"] else None
    mri = TImage(filename, external=use_sform, name="MRI")
    if q.mask is not None:
        mask = TImage(q.mask, external=use_sform, name="mask")
        if mask.vshape != mri.vshape:
            mask = mask.evaluate(mri.domain)
        mri.mask = mask

    logger.info(f"Loaded 3D image volume from {filename}")

    if q.export:
        fname = os.path.join(p.general.outputdir, "volume.timg")
        mri.save(fname, overwrite=True)
        logger.info(f"Saved 3D volume data to {fname}")

    return mri


def register(ortho, volume, cnf):
    """
    Runs the various stages of the registration. The function has no return
    value. The transformation chain is attached to the Domain of the fixed
    TImage object and is optimised in situ.

    :param ortho: tri-planar orthogonal construct
    :type ortho: OrthoFrame
    :param volume: 3D image volume
    :type volume: TImage
    :param cnf: all configuration options
    :type cnf: dict or AttrMap

    """
    p = AttrMap(cnf)

    # Generate initialisations
    if hasattr(p.registration, "init"):
        # inits = points_in_volume(volume, cnf)
        inits = kmeans(volume, cnf)
    else:
        inits = np.atleast_2d(ortho.origin)

    # Coarse search
    logger.info("Starting coarse search...")
    resolutions = np.atleast_1d(p.screenshot.resolution)
    best_inits = coarse_search(ortho, volume, inits, resolutions, cnf)
    logger.info("Finished coarse search...")

    # Fine-tuning
    logger.info(f"Starting fine-tuning at {len(best_inits)} locations...")
    fine_tuning(ortho, volume, best_inits, cnf)
    logger.info("Finished fine-tuning.")

    # Rotational adjustment
    logger.info(f"Starting frame rotation...")
    frame_rotation(ortho, volume, cnf)
    logger.info("Finished frame rotation.")

    # Individual slice adjustment
    logger.info(f"Starting slice adjustment...")
    slice_adjust(ortho, volume, cnf)
    logger.info("Finished slice adjustment.")


def points_in_volume(volume, cnf):
    """
    Defines a cloud on N points for initialising the OrthoFrame instance.

    """
    p = AttrMap(cnf)
    q = p.registration.init

    mask = volume.mask.ravel() > 0.1
    pc = volume.domain.get_physical_coordinates()[mask, :]
    step = pc.shape[0] // int(q["n-points"])
    points = pc[::step, :]
    logger.info(f"Created {points.shape[0]} initialisation points.")

    if q.visualise:
        fig = plt.figure()
        ax = fig.add_subplot(projection='3d')
        ax.scatter(*points.T, "o")
        plt.show()

    if q.export:
        fname = os.path.join(p.general.outputdir, "init_points.txt")
        try:
            if os.path.exists(fname):
                os.remove(fname)
            np.savetxt(fname, points)
            logger.info(f"Saved initialisation points to {fname}")
        except Exception as exc:
            logger.error(f"Could not save initialisation points to {fname}")

    return points


def kmeans(volume, cnf):
    p = AttrMap(cnf)
    q = p.registration.init
    n = int(q["n-points"])

    km = KMeans(n_clusters=n, random_state=0)
    mask = volume.mask.ravel() > 0.1
    pc = volume.domain.get_physical_coordinates()[mask, :]
    step = int(min(pc.shape[0] / n, KMEANS_SUBSAMPLING_FACTOR))
    km.fit(pc[::step, :])
    points = km.cluster_centers_
    logger.info(f"Created {points.shape[0]} initialisation points.")

    if q.visualise:
        fig = plt.figure()
        ax = fig.add_subplot(projection='3d')
        ax.scatter(*points.T, "o")
        plt.show()

    if q.export:
        fname = os.path.join(p.general.outputdir, "init_points.txt")
        try:
            if os.path.exists(fname):
                os.remove(fname)
            np.savetxt(fname, points)
            logger.info(f"Saved initialisation points to {fname}")
        except Exception as exc:
            logger.error(f"Could not save initialisation points to {fname}")

    return points


def get_sigma(scale):
    if np.isclose(scale, 1, atol=1e-3):
        return 0
    else:
        return 1 / scale / (2. * np.sqrt(2 * np.log(2)))


def coarse_search(ortho, volume, inits, resolutions, cnf):
    p = AttrMap(cnf)
    q = AttrMap(p.registration["coarse-search"])
    scale = float(q.scale)
    k = int(min(int(q["k-best"]), len(inits)))

    lortho = ortho.rescale(scale)
    sigma = get_sigma(scale)
    logger.info(f"Smoothing 3D image volume with sigma = {sigma} vx")
    mri_smooth = volume.smooth(sigma, copy=True)
    mri_smooth.mask = None

    results = []
    for resolution in resolutions:
        logger.info(f"Setting the slice resolution to {resolution} mm/px")
        for ix, init in enumerate(inits):
            lortho.origin = init
            lortho.scale = resolution
            if q.visualise:
                lortho.preview(volume)
            # Define cost functions
            cost_axial = CostMI(mri_smooth, lortho.axial)
            cost_coronal = CostMI(mri_smooth, lortho.coronal)
            cost_sagittal = CostMI(mri_smooth, lortho.sagittal)
            # Measure and store cost at the current initialisation point
            total_cost = cost_axial() + cost_coronal() + cost_sagittal()
            results.append((resolution, init, total_cost))
            msg = f"Point {ix + 1}: {init}, Resolution: {resolution}, " \
                  f"Scale: {scale}, Cost: {total_cost}"
            logger.log(5, msg)
        else:
            results = sorted(results, key=lambda r: r[-1])[:k]

    return tuple((resolution, init) for resolution, init, cost in results)


def fine_tuning(ortho, volume, inits, cnf):
    p = AttrMap(cnf)
    q = AttrMap(p.registration["fine-tuning"])
    scales = np.atleast_1d(q.scale)

    results = []
    for scale in scales:
        logger.info(f"Fine-tuning at Scale {scale}...")

        lortho = ortho.rescale(scale)
        sigma = get_sigma(scale)
        logger.info(f"Smoothing 3D image volume with sigma = {sigma} vx")
        mri_smooth = volume.smooth(sigma, copy=True)
        mri_smooth.mask = None

        results = []
        for ix, (resolution, init) in enumerate(inits):
            # Initialise parameters and parameter bounds
            lortho.scale = resolution
            lortho.scaletx.parameters.set_bounds(
                float(q["resolution-min"]),
                float(q["resolution-max"])
            )
            lortho.origin = init
            lortho.origintx.parameters.set_bounds(
                np.subtract(init, float(q.radius)),
                np.add(init, float(q.radius))
            )

            # Define cost function
            cost_axial = CostMI(mri_smooth, lortho.axial)
            cost_coronal = CostMI(mri_smooth, lortho.coronal)
            cost_sagittal = CostMI(mri_smooth, lortho.sagittal)
            opt = OptimisationGroup(lortho.scaletx, lortho.origintx)
            OptNL(opt, cost_axial, cost_coronal, cost_sagittal,
                  normalised=True, xtol_abs=q["xtol-abs"], step=q.step,
                  visualise=q.visualise, logger=logger)()
            total_cost = cost_axial() + cost_coronal() + cost_sagittal()
            results.append(
                (float(lortho.scale), tuple(lortho.origin), total_cost))
            logger.log(
                5,
                f"Point No. {ix + 1}\n"
                f"Centre: {init} --> {lortho.origin}\n"
                f"Pixelsize: {resolution} --> {lortho.scale}\n"
                f"Scale: {scale}\n"
                f"Cost: {total_cost})")
        else:
            results = sorted(results, key=lambda r: r[-1])[0]
            inits = [[results[0], results[1]]]

    # Update the parameters
    ortho.scale = results[0]
    ortho.origin = results[1]
    logger.info(f"Best resolution: {results[0]} mm/px")
    logger.info(f"Best origin: {results[1]} mm")

    if q.visualise:
        ortho.preview(volume)


def frame_rotation(ortho, volume, cnf):
    """
    Optimises the collective 3D rotation of the OrthoFrame instance.
    During the optimisation, the location of the origin may also change.

    """
    p = AttrMap(cnf)
    q = AttrMap(p.registration["frame-rotation"])

    for scale in np.atleast_1d(q.scale):

        lortho = ortho.rescale(scale)
        sigma = get_sigma(scale)
        vol_smooth = volume.smooth(sigma, copy=True)
        logger.info(f"Smoothing volume with sigma = {sigma} vx.")

        # Configure rotation angle range
        ldx = np.radians(q["angles-lower-delta"])
        udx = np.radians(q["angles-upper-delta"])
        lb = np.subtract(lortho.rottx.parameters[:], ldx)
        ub = np.add(lortho.rottx.parameters[:], udx)
        lortho.rottx.parameters.set_bounds(lb, ub)

        # Configure translation range
        lb = np.subtract(lortho.origin, q["translation-lower-delta"])
        ub = np.add(lortho.origin, q["translation-upper-delta"])
        lortho.origintx.parameters.set_bounds(lb, ub)

        # Define the cost function
        cost_axial = CostMI(vol_smooth, lortho.axial)
        cost_coronal = CostMI(vol_smooth, lortho.coronal)
        cost_sagittal = CostMI(vol_smooth, lortho.sagittal)

        # Optimise rotation and translation parameters
        opt = OptimisationGroup(lortho.rottx, lortho.origintx)
        OptNL(opt, cost_axial, cost_coronal, cost_sagittal,
              normalised=True, xtol_abs=q["xtol-abs"], step=q.step,
              visualise=q.visualise, logger=logger)()

        # Pass on the optimised parameters for the next iteration
        ortho.rotation = lortho.rotation
        ortho.origin = lortho.origin


def slice_adjust(ortho, volume, cnf):
    """
    Optimises the resolution and the offset of each slice from the origin of
    the OrthoFrame instance. This is to account for slight variations that are
    anticipated given that the input is a screenshot.

    Note that the "origin" and the "scale" parameters of the OrthoFrame
    instance are no longer relevant after this operation, and a new OrthoFrame
    object should be initialised with the optimised parameters.

    """
    p = AttrMap(cnf)
    q = AttrMap(p.registration["slice-adjustment"])

    for plane in q.planes:
        plane = plane.lower()
        s = getattr(ortho, plane)

        # Configure translation range (offset from the origin)
        tx_nudge = s.domain.get_transformation("nudge")
        lb = np.asarray(q["translation-min"])
        ub = np.asarray(q["translation-max"])
        tx_nudge.parameters.set_bounds(lb, ub)

        # Configure the range of scale (resolution differences)
        # Note that the slice becomes detached from the collective
        # scaling of the OrthoFrame instance after this operation.
        tx_scale, ix = s.domain.get_transformation("scale", index=True)
        tx_scale = tx_scale.copy()
        tx_scale.name = "scale"
        s.domain.external[ix] = tx_scale
        scale = tx_scale.parameters[:]
        lr = float(q["scale-lower-relative"])
        ur = float(q["scale-upper-relative"])
        tx_scale.parameters.set_bounds(lr * scale, ur * scale)

        # Optimise slice-specific scaling and translation
        opt = OptimisationGroup(tx_scale, tx_nudge)
        cost = CostMI(volume, s)
        OptNL(opt, cost, normalised=True, xtol_abs=q["xtol-abs"],
              step=q.step, visualise=q.visualise, logger=logger)()


def summary_figure(ortho, volume):
    """
    top row: input images
    bottom row: reconstructed images

    """
    fig, axes = plt.subplots(2, 3, figsize=(15, 10))

    # Top row: input images
    axes[0, 0].imshow(ortho.sagittal.data)
    axes[0, 1].imshow(ortho.coronal.data)
    axes[0, 2].imshow(ortho.axial.data)

    # Bottom row: reconstructed images
    axes[1, 0].imshow(volume.evaluate(ortho.sagittal.domain).data, cmap="gray")
    axes[1, 1].imshow(volume.evaluate(ortho.coronal.domain).data, cmap="gray")
    axes[1, 2].imshow(volume.evaluate(ortho.axial.domain).data, cmap="gray")

    return fig


def conclude(ortho, volume, cnf):

    logger.info("Exporting orthogonal slices...")
    export_slices(ortho, volume, cnf)
    logger.info("Reconstructing the orthogonal view from registration...")
    three_panel_figure(ortho, volume, cnf)
    logger.info("Generating human-readable output...")
    human_readable_output(ortho, volume, cnf)


def human_readable_output(ortho, volume, cnf):

    p = AttrMap(cnf)

    # Where is the true centre of the orthogonal construct?
    physical_centre = ortho.intersection
    voxel_centre = np.round(volume.domain.map_physical_coordinates(
        physical_centre)).astype(int).ravel()

    # What is the orientation?
    euler_axes = ortho.rottx.order
    euler_angles = ortho.rotation

    # What are the properties of each panel?
    resolutions = []
    plane_texts = []
    for plane in PLANES.values():
        domain = getattr(ortho, plane).domain
        plane_resolution = domain.external["scale"].parameters[:]
        resolutions.append(plane_resolution)
        plane_normal = domain.map_voxel_vectors([0, 0, 1], [0, 0]).ravel()[:3]
        plane_normal = np.divide(plane_normal, np.linalg.norm(plane_normal))
        plane_centre = domain.pcentre()
        plane_voxel_centre = volume.domain.map_physical_coordinates(
            plane_centre).astype(int).ravel()
        plane_texts.append(
            f"{plane.capitalize()} plane:\n"
            f"  shape: {domain.shape} pixels\n"
            f"  resolution: {plane_resolution} mm/pixel\n"
            f"  geometrical centre (exact): {plane_centre} mm\n"
            f"  geometrical centre (discrete): {plane_voxel_centre} voxels\n"
            f"  normal: {plane_normal} mm\n"
        )
    else:
        avg_resolution = np.mean(np.vstack(resolutions), axis=0)

    # What affine matrix defines the patient coordinate system?
    try:
        sform = volume.domain.get_transformation("sform")
    except IndexError:
        sform = np.eye(3, 4)
    else:
        sform = sform.matrix

    # Write human-readable output
    fname = os.path.join(p.general.outputdir, "human_readable_output.txt")
    try:
        with open(fname, "w") as fp:
            fp.write(
                f"Output gererated by {__file__} v{__version__}\n"
                f"Copyright: University of Oxford (C) 2022\n"
                f"Author: Istvan N Huszar\n"
                f"\n"
                f"Created at: {datetime.now()}\n"
                f"Output directory: {p.general.outputdir}\n"
                f"\n"
                f"Screenshot:\n"
                f"  file: {p.screenshot.file}\n"
                f"  mask: {p.screenshot.mask}\n"
                f"  layout: {p.screenshot.layout}\n"
                f"\n"
                f"Volume:\n"
                f"  file: {p.volume.file}\n"
                f"  mask: {p.volume.mask}\n"
                f"  sform:\n"
                + str(sform) +
                f"\n\n"
                f"Patient coordinate system:\n"
                f"  x: left --> right\n"
                f"  y: posterior --> anterior\n"
                f"  z: inferior --> superior\n"
                f"  units: mm\n"
                f"\n"
                f"Orthogonal construct:\n"
                f"  intersection (exact): {physical_centre} mm\n"
                f"  intersection (discrete): {voxel_centre} voxels\n"
                f"  rotation ({euler_axes}): {euler_angles} deg\n"
                f"  resolution (average): {avg_resolution} mm/pixel\n"
                f"\n"
                + "\n".join(plane_texts)
            )
    except Exception as exc:
        logger.error(f"Could not save human-readable output to {fname}")
        logger.error(exc.args)
    else:
        logger.info(f"Saved human-readable output to: {fname}")


def three_panel_figure(ortho, volume, cnf):

    p = AttrMap(cnf)
    q = AttrMap(p.registration.conclude)

    if q.export or q.visualise:
        fig = summary_figure(ortho, volume)
    else:
        fig = None

    # Save a three-panel orthogonal view of the reconstruction
    if q.export:
        fname = os.path.join(p.general.outputdir, "reconstruction-overview.png")
        try:
            fig.savefig(fname)
        except Exception as exc:
            logger.error(
                f"Could not save the best orthogonal reconstruction to {fname}")
            logger.error(exc.args)
        else:
            logger.info(f"Best orthogonal reconstruction was saved to {fname}")

    if q.visualise:
        plt.show()


def export_slices(ortho, volume, cnf):

    p = AttrMap(cnf)

    # Save the aligned orthogonal slices in TImage format
    bname = os.path.join(p.general.outputdir, "reconstructed-")
    try:
        ortho.save(bname, source=volume, overwrite=True)
    except Exception as exc:
        logger.error(
            f"Could not save the aligned orthogonal slices as {bname}*.timg")
        logger.error(exc.args)
    else:
        logger.info(f"Saved the aligned orthogonal slices as {bname}*.timg")
    try:
        ortho.snapshot(bname, source=volume, overwrite=True)
    except Exception as exc:
        logger.error(
            f"Could not save the aligned orthogonal slices as {bname}*."
            f"{ts.TIMAGE_DEFAULT_SNAPSHOT_EXT}")
        logger.error(exc.args)
    else:
        logger.info(f"Saved the aligned orthogonal slices as {bname}*."
                    f"{ts.TIMAGE_DEFAULT_SNAPSHOT_EXT}")


                            # PROGRAM FLOW CONTROL

def screenshot_to_volume(args):
    """
    Main program code. Controls program flow, handles command-line arguments.

    """
    cnf = args.config
    if os.path.isfile(cnf):
        cnf = general.load_configurations(cnf)
    else:
        raise FileNotFoundError(f"The provided configuration file "
                                f"does not exist: {args.config}")

    # Override screenshot and volume file paths in the configuration file with
    # those provided on the command line.
    if args.screenshot and os.path.isfile(args.screenshot):
        cnf["screenshot"]["file"] = args.screenshot
    if args.volume and os.path.isfile(args.volume):
        cnf["volume"]["file"] = args.volume
    if args.out:
        cnf["general"]["outputdir"] = args.out

    # Override verbose option in configurations
    cnf["general"]["verbose"] = args.verbose

    # Run registration script
    run(cnf)


def create_cli(parser):
    """
    Sets up the CLI argument parser instance.

    """
    parser.add_argument("--screenshot", metavar="image",
                        help="Screenshot image", default=None, type=str,
                        required=False)
    parser.add_argument("--volume", metavar="image",
                        help="3D Image volume", default=None, type=str,
                        required=False)
    parser.add_argument("--out", metavar="dir",
                        help="Output directory", default=None, type=str,
                        required=False)
    parser.add_argument("--config", metavar="cnf.yml",
                        help="configuration file", default=None, type=str,
                        required=True)
    parser.add_argument("--verbose", default=False, action="store_true",
                        help="Print status messages to the command line",
                        required=False)

    return parser


def main(*args):
    """ Main program code. """

    np.set_printoptions(precision=4, suppress=True)

    parser = argparse.ArgumentParser(
        prog="screenshot_to_volume",
        description="Registers the panels of a three-panel orthogonal view "
                    "from a screenshot to the corresponding 3D volume. "
                    "The algorithm finds the precise position and scaling of "
                    "the orthogonal view, but it requires a reasonable initial "
                    "guess of the screenshot image resolution.")
    parser = create_cli(parser)

    if args:
        screenshot_to_volume(parser.parse_args(args))
    else:
        parser.print_help()


if __name__ == "__main__":
    main(*sys.argv[1:])
