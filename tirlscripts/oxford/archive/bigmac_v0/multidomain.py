#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2021 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from numbers import Integral
from tirl.chain import Chain
from tirl.domain import Domain
from tirl.transformations.embedding import TxEmbed
from tirl.transformations.euler import TxEulerAngles
from tirl.transformations.rotation import TxRotation2D
from tirl.transformations.scale import TxScale, TxIsoScale
from tirl.transformations.translation import TxTranslation


# DEFINITIONS


# IMPLEMENTATION

class MultiDomain(object):
    """
    Arbitrarily indexed set of Domain objects.

    """

    def __init__(self, *domains, indices=None):
        """
        Initialisation of MultiDomain.

        :param domains: Input Domain objects
        :type domains: Domain
        :param indices:
            Indices of the input domains. If None (default), indices are
            automatically assigned as 0..N, where N is the number of input
            domains.
        :type indices: None or Iterable[int]

        """
        super(MultiDomain, self).__init__()

        self._domains = dict()

        if indices is None:
            iterator = enumerate(domains)
        else:
            l_ind = len(indices)
            l_dom = len(domains)
            if l_ind != l_dom:
                raise AssertionError(
                    f"Invalid number of inputs: "
                    f"indices: {l_ind} vs. domains {l_dom}")
            else:
                iterator = zip(indices, domains)

        for ix, dom in iterator:
            if isinstance(ix, Integral):
                ix = int(ix)
                if hasattr(dom, "__domain__"):
                    self._domains.update({ix: dom})
                else:
                    raise TypeError(
                        f"Input No. {ix} is not a valid domain: {dom}")
            else:
                raise TypeError(f"Invalid index: {ix}")

    @property
    def n(self):
        return len(self._domains.items())

    @property
    def domains(self):
        return self._domains

    def apply(self, timg):
        result = dict()
        for ix, dom in self.domains.items():
            result.update({ix: timg.evaluate(dom)})
        else:
            return result


class Grating(MultiDomain):

    def __init__(self, gap, offset, rotation, shapes, resolutions,
                 indices=None, internals=None):
        """
        Initialisation of Grating.

        :param gap: Slicing gap [mm]
        :type gap: float
        :param offset: 3D offset of the grating [mm]
        :type offset: Iterable[float]
        :param rotation: 3D rotation of the grating [deg]
        :type rotation: Iterable[float]
        :param shapes: Shape of the slices [pixels]
        :type shapes: Iterable[int] or Iterable[Iterable[int]]
        :param resolutions: Pixel sizes of the individual slices.
        :type resolutions: Iterable[float] or Iterable[Iterable[float]]
        :param indices:
            Slices to be generated. If None (default), indices are
            automatically assigned to each input shape: 0..n, where n is
            the number of shape inputs.
        :type indices: Iterable[int]
        :param internals: internal transformations for each domain
        :type internals: Iterable

        """
        # Verify inputs
        self.gap = float(gap)
        offset = np.asarray(offset, dtype=float)
        assert offset.size == 3, f"Invalid offset: {offset}"
        rotation = np.asarray(rotation, dtype=float)
        assert rotation.size == 3, f"Invalid rotation: {rotation}"
        shapes = np.atleast_2d(shapes)
        n, dim = shapes.shape
        assert n >= 1, f"Invalid number of slices: {n}"
        assert dim == 2, f"Slices must be 2-dimensional"
        resolutions = np.atleast_2d(resolutions)
        rn, rdim = resolutions.shape
        assert rdim in (1, dim), \
            f"Invalid dimensions in resolution specification: {rdim}"
        if rn == 1:
            resolutions = np.tile(resolutions, (n, 1))
        assert resolutions.shape[0] == n
        self.resolutions = resolutions
        if indices is None:
            indices = np.arange(n)
        else:
            indices = np.asarray(indices, dtype=int)
        assert (internals is None) or (len(internals) == n), \
            "Invalid number of internal transformations."
        self.internals = internals

        # Create grating transformations (these are shared by the slices)
        self.offset = TxTranslation(*offset, name="grating-offset")
        self.rotation = TxEulerAngles(*rotation, mode="deg", order="xyz",
                                      name="grating-rotation")

        # Create slice domains with transformations
        domains = []
        for ix in range(n):
            # Define slice-specific transformations
            res = self.resolutions[ix]
            if res.size > 1:
                s_res = TxScale(*res, name="slice-resolution")
            else:
                s_res = TxIsoScale(*res, name="slice-resolution")
            cy, cx = \
                Chain(*internals[ix], s_res).map(np.divide(shapes[ix], 2.))[0]
            s_centralise = TxTranslation(-cy, -cx, name="slice-centralise")
            s_rotation = TxRotation2D(90, mode="deg", name="slice-rotation")
            s_nudge = TxTranslation(0, 0, name="slice-nudge")
            s_embed = TxEmbed(1, name="slice-embed")
            s_offset = \
                TxTranslation(0, 0, indices[ix] * self.gap, name="slice-offset")
            chain = [s_res, s_centralise, s_rotation, s_nudge, s_embed,
                     s_offset, self.rotation, self.offset]

            # Combine slice and grating txs and store new slice domain
            # resolution -> centralise -> in-plane rotation -> in-plain nudge ->
            # -> 3D embedding -> slice offset -> grating rotation -> grating
            # offset
            slice_no = indices[ix]
            domain = Domain(*shapes[ix], internal=internals[ix],
                            chain=chain, name=f"slice-{slice_no}")
            domains.append(domain)

        # Call the parent-class initialiser
        super(Grating, self).__init__(*domains, indices=indices)

