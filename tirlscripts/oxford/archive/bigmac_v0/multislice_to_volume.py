#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2021 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Updated: 9th November 2021


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import re
import sys
import json
import logging
import pathlib
import argparse
import numpy as np
import pandas as pd
from attrdict import AttrMap
import multiprocessing as mp
from datetime import datetime
from math import radians as rad
from scipy.stats import linregress
from collections import OrderedDict


# TIRL IMPORTS

import tirl
import tirl.settings as ts
from tirl.domain import Domain
from tirl.tfield import TField
from tirl.timage import TImage
from tirl.cost.mind import CostMIND
from tirl.transformations.identity import TxIdentity
from tirl.optimisation.optnl import OptNL, OptMethods
from tirl.transformations.axisangle import TxAxisAngle
from tirl.optimisation.optgroup import OptimisationGroup
from tirl.regularisation.normtxreg import NormTxRegulariser
from tirl.transformations.rbf_displacement import TxRbfDisplacementField


# TIRLSCRIPT IMPORTS

from tirlscripts.oxford.bigmac.multidomain import Grating


# DEFINITIONS

NORMALISE = True        # optimise parameters in the [-1, 1] normalised range
np.set_printoptions(precision=4, suppress=True)
logger = logging.getLogger("tirl.scripts.oxdbb.bigmac.ms2v")
from tirl.constants import *


# IMPLEMENTATION


def troubleshoot():
    """ Alternative main() """

    missing_whole_slices = (57, 65, 66, 78, 85, 89, 90, 93, 95, 98)
    missing_left_slices = (17,)
    missing_right_slices = (3, 16, 23)
    remaining_slices = (3,)

    # Prepare output directory
    prepare_output_dir()

    # Prepare logger
    global logger
    logger = create_logger()
    cmd = " ".join(sys.argv + ["TROUBLESHOOT"])
    logger.info(f"The program started with the command: {cmd}")

    csvfile = "/mnt/knox/bigmac/multislice-ismrm/filtered_input_data.csv"
    filtered_inputs = pd.read_csv(csvfile)
    qdir = "/mnt/iris/BigMac/Microscopy/PLI/Anterior/timg/"
    for ix, sl in filtered_inputs.iterrows():
        slice_no = sl.Slice - 1
        if slice_no not in remaining_slices:
            continue
        tfile = os.path.join(qdir, f"{slice_no + 1:04d}-pyramid.timg")
        dfile = os.path.join("/mnt/knox/bigmac/multislice-ismrm",
                             f"{slice_no:04d}", f"rigid2d-slice-{slice_no}.dom")
        domain = tirl.load(dfile)
        job = domain, slice_no, sl.Part, tfile, sl.SliceMask
        print(slice_no)
        try:
            warp_worker(job)
        except Exception as exc:
            logger.error(f"ERROR while optimising in-plane warps "
                         f"for slice {slice_no}: {exc.args}")


def main():
    """
    Main program code.

    """
    # Prepare output directory
    prepare_output_dir()

    # Prepare logger
    global logger
    logger = create_logger()
    cmd = " ".join(sys.argv)
    logger.info(f"The program started with the command: {cmd}")

    # Create a stack of TImages from the input data slices (microscopy)
    stack = create_stack()

    # Find the best grating for the input stack
    g = get_best_grating(stack, batchsize=10)

    # Save grating parameters
    with open(os.path.join(cnf["outputdir"], "best_grating.txt"), "w") as fp:
        fp.write(f"Offset (0th slice, mm): {g.offset.parameters[:]}\n")
        rotation_angles = g.rotation.euler(mode="deg", order="xyz")
        fp.write(f"Rotation (deg): {rotation_angles}\n")
        fp.write(f"Slicing gap (mm): {g.gap}\n")

    # In-plane warping
    warp(g)


def create_stack(sort=True):
    """
    Creates an indexed set from the input slices. Compound indexing is used to
    identify multi-resolution TImages by their slice number (position in
    grating), and any submasking, i.e. left/right/whole. The choose_data()
    function is used to filter out any unnecessary items (e.g. duplicates)
    from the input.

    If sort is True (default), the stack is sorted by the pre-registration cost
    (best towards worst).

    """
    global cnf
    logger.info("Creating a stack from the input slices...")
    filtered_slices = choose_data(pd.read_csv(cnf["slices"]))
    if sort:
        filtered_slices.sort_values("PreRegistrationCost", inplace=True)

    # Save the filtered inputs as a CSV file
    datafile = os.path.join(cnf["outputdir"], "filtered_input_data.csv")
    filtered_slices.to_csv(datafile)
    logger.info(f"Exported the filtered input data to: {datafile}")
    cnf["slices-filtered"] = filtered_slices

    # Load pyramidal TImage files
    stack = OrderedDict()
    for ix, sl in filtered_slices.iterrows():
        tfile = os.path.join(cnf["quickload"], f"{sl.Slice:04d}-pyramid.timg")
        slice_no = sl.Slice - 1
        img = TImage(tirl.load(tfile), storage=MEM, name=f"slice-{slice_no}")
        key = (slice_no, str(sl.Part).lower())
        stack.update({key: img})

    return stack


def get_best_grating(stack, batchsize=None):
    """ Main loop of the iterative multislice algorithm. """

    g0 = None
    n_slices = len(stack.items())
    batchsize = n_slices if batchsize is None else int(batchsize)

    # Keep recruiting new slices until the best grating is assumed
    items = tuple(stack.items())
    for start in range(0, n_slices, batchsize):
        stop = int(min(start + batchsize, n_slices))

        # Create new grating from estimated parameters
        g = fit_grating(g0, OrderedDict(items[:stop]))
        # 2D rigid (in-plane) optimisation of the grating elements
        optimise_rigid2d(g)
        # 3D rigid optimisation of the grating elements
        optimise_rigid3d(g)
        # Update the previous estimate of the grating
        g0 = g

    else:
        # Fix the orientation and the position of the grating slices
        g0 = fit_grating(g0, stack)
        optimise_rigid2d(g0)

    return g0


def fit_grating(g0, stack):
    """

    """
    indices = []
    normals = []
    centres = []
    domains = []
    z0 = [0, 0, -1]

    # For previously recruited slices, get rotations from the optimised grating
    for key, sl in stack.items():
        slice_no = key[0]
        indices.append(slice_no)
        try:
            domain = g0.domains[slice_no]
        except:
            domain = sl.domain
        finally:
            domains.append(domain)
        try:
            rot = domain.chain["grating-rotation"]
        except:
            rot = domain.chain["rotation_3d"]
        normal = rot.map(z0)
        if np.dot(normal, [0, -1, 0]) >= 0:
            normal *= -1
        logger.log(5, f"Slice {slice_no} normal: {normal}")
        normals.append(normal)
        centres.append(domain.pcentre())
    else:
        normals = np.vstack(normals)
        centres = np.vstack(centres)

    # Estimate the robust orientation iteratively by rejecting outliers
    select = np.full(shape=normals.shape[0], fill_value=True)
    robust_normal = z0
    for i in range(cnf["outlier-iterations"]):
        robust_normal = np.mean(normals[select], axis=0).ravel()
        robust_normal /= np.linalg.norm(robust_normal)
        logger.log(5, f"Outlier removal iteration {i}, robust normal: "
                      f"{robust_normal}")
        diff = np.linalg.norm(normals - robust_normal, axis=-1)
        select[diff > 2 * np.median(diff[select])] = False
    else:
        pass
        # fig = plt.figure()
        # ax = fig.add_subplot(projection='3d')
        # ax.scatter(*normals.T, c="blue", marker="o")
        # ax.scatter(*robust_normal, c="red", marker="x")
        # plt.show()

    # Find the rotation angles corresponding to the robust normal
    angle = np.arccos(np.dot(z0, robust_normal.reshape(-1, 1)))
    axis = np.cross(robust_normal, z0)
    axis /= np.linalg.norm(axis)
    g_rotation = TxAxisAngle(angle, axis, mode="rad")\
        .euler(mode="deg", order="xyz")
    logger.info(f"Grating rotation (robust): {g_rotation} [deg]")

    # Centre of gravity
    cog = np.mean(centres, 0)
    cog_pos = np.dot(robust_normal, np.asarray(cog).reshape(-1, 1))
    logger.info(f"Centre of gravity: {cog} [mm]")

    # Slicing gap
    y = [float(np.dot(robust_normal, c.reshape(-1, 1))) for c in centres]
    result = linregress(indices, y)
    gap = result.slope

    # Grating offset
    g_offset = np.add(
        cog, np.subtract(result.intercept, cog_pos) * robust_normal)

    logger.info(f"Effective slice gap: {gap:.04} +/- {result.stderr:.04} mm")
    logger.info(f"Correlation coefficient: {result.rvalue:.04}")
    logger.info(f"0th slice centre: {g_offset} mm")

    # Define grating using the estimated parameters and the slices
    shapes = []
    internals = []
    resolutions = []
    for d in domains:
        shapes.append(d.shape)
        internals.append(d.internal)
        try:
            resolutions.append(d.chain["slice-resolution"].parameters[:])
        except:
            resolutions.append(d.chain["resolution"].parameters[:])

    return Grating(gap, g_offset, g_rotation, shapes,
                   resolutions, indices, internals)


def warp(g):
    """
    :param g: Grating instance
    :type g: Grating

    """
    if cnf["n_cpu"] > 1:
        with mp.Pool(processes=cnf["n_cpu"], maxtasksperchild=1) as pool:
            pool.map(warp_worker, warp_jobs(g))
    else:
        for job in warp_jobs(g):
            try:
                warp_worker(job)
            except Exception as exc:
                domain, slice_no, part, tfile, maskfile = job
                logger.exception(
                    f"Slice {slice_no}, Part {part}: warp failed.", exc.args)


def warp_jobs(g):
    filtered_inputs = cnf["slices-filtered"]

    for ix, sl in filtered_inputs.iterrows():
        slice_no = sl.Slice - 1
        tfile = \
            os.path.join(cnf["quickload"], f"{slice_no + 1:04d}-pyramid.timg")
        yield g.domains[slice_no], slice_no, sl.Part, tfile, sl.SliceMask


def warp_worker(job):
    domain, slice_no, part, tfile, maskfile = job

    # Create slice-specific output directory
    outdir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    # Load volume
    vol = TImage(cnf["volume"], external="sform", storage=MEM)
    vmask = TImage(cnf["vmask"], external="sform", storage=MEM)
    vol.mask = np.where(vmask.data > 0, 1.0, 0.0)

    # Load slice and mount it on the appropriate grating slice
    img = tirl.load(tfile)
    print(img.vshape)
    if img.domain.shape != domain.shape:
        img.resize(*domain.shape, copy=False)  # added while troubleshooting
    print(img.vshape)
    img.mask = TImage(maskfile).data
    img.domain = domain.copy()
    _, ix = img.domain.get_transformation("slice-embed", index=True)
    img.domain.chain = [
        *img.domain.chain[:ix + 1],
        TxIdentity(name="warp"),
        *img.domain.chain[ix + 1:]
    ]
    img.resmgr.sync()
    img.storage = MEM

    # Pre-align within the plane if this is a split-hemisphere registration
    if part.lower() != "whole":
        # Coarse alignment by image centres
        align_centres(img, vol.tmask())
        # Multi-resolution rigid optimisation
        rigid2d_multires(img, vol)

    # Save the initial (rigid) configuration (for comparison)
    vs = vol.evaluate(img.domain)
    vs.normalise()
    vs.snapshot(
        os.path.join(outdir, f"mri-rigid-{slice_no:04d}-{part}.png"),
        overwrite=True)

    t = AttrMap(cnf["warp"])
    logger = logging.getLogger("tirl.scripts.oxdbb.bigmac.ms2v")

    # Smoothing iteration
    stage_cost = []

    # Initialise warp handle
    warp = None

    states = tuple(zip(t.slice_scaling, t.volume_scaling,
                       t.slice_smoothing, t.volume_smoothing))

    for sm_no, (ssc, vsc, ssm, vsm) in enumerate(states):
        logger.info(f"Smoothing: ssc={ssc}, ssm={ssm}px, "
                    f"vsc={vsc}, vsm={vsm}vx.")
        img.rescale(1. / ssc, copy=False)
        img_smooth = img.smooth(ssm, copy=True)
        img_smooth.storage = MEM
        vol.rescale(1. / vsc, copy=False)
        if vsm != 0:
            vol_smooth = vol.smooth(vsm, copy=True)
        else:
            vol_smooth = vol
        img_smooth.storage = MEM
        cost = CostMIND(vol_smooth, img_smooth, normalise=True,
                        sigma=1.0, truncate=1.5, kernel=MK_STAR)
        logger.info(f"Initial cost: {cost()}")

        # How many support points?
        # Read from configuration file.
        if isinstance(t.support.points, int):
            n_points = t.support.points
            calculate_points = True
        # Read from external file
        elif isinstance(t.support.points, str) and \
                os.path.isfile(t.support.points):
            xi = np.loadtxt(t.support.points)
            n_points = xi.shape[0]
            calculate_points = False
        # Invalid input
        else:
            raise ValueError("Invalid support point specification.")

        # How many points should be optimised at once?
        optsize = min(int(t.optsize), n_points)

        # Optimise all points for any subsequent iteration, as little benefit
        # is anticipated from repeating the whole optimisation sequence.
        if sm_no > 0:
            optsize = n_points

        # ---> POINT OPTIMISATION SEQUENCE
        for n_old in range(0, n_points, optsize):

            # --------------------- CREATE NEW WARP -------------------- #

            # Create new incremental point set
            n_new = min(n_points - n_old, optsize)
            if calculate_points:
                xi_new = calc_km_control_points(n_new, n_old, img, t)
            else:
                xi_new = xi[n_old:n_old + n_new]

            # Concatenate old and new points (voxel coordinates)
            if n_old > 0:
                xi_old = warp.domain.base_coordinates()
                xi_current = np.concatenate((xi_old, xi_new), axis=0)
            else:
                xi_current = xi_new

            # Create new transformation with the old and the new points
            newwarp = create_warp_from_points(img, xi_current, t, outdir)

            # If a transformation was previously optimised, fit the parameters
            # of the new transformation to recreate the displacement field.
            # This carries forward the information from previous optimisations.
            if warp is not None:
                newwarp.fit(warp.std(3).absolute().field())

            warp = newwarp

            # Update transformation parameter bounds
            params = warp.parameters[:]
            params = params.reshape(len(t.vectorder), -1)
            lb = params.copy()
            ub = params.copy()
            if t.vectorder == "xyz":
                lb[:2] -= float(t.lower_dxy)
                lb[2] -= float(t.lower_dz)
                ub[:2] += float(t.upper_dxy)
                ub[2] += float(t.upper_dz)
            elif t.vectorder == "xy":
                lb[:] -= float(t.lower_dxy)
                ub[:] += float(t.upper_dxy)
            elif t.vectorder == "z":
                lb[:] -= float(t.lower_dz)
                ub[:] += float(t.upper_dz)

            if NORMALISE:
                initial_step = t.opt_step
            else:
                initial_step = absolute_step(lb.ravel(), ub.ravel(), t.opt_step)
            warp.parameters.set_bounds(lb.ravel(), ub.ravel())
            warp.parameters.unlock()

            # Add warp to the chain
            _, ix = img_smooth.domain.get_transformation("warp", index=True)
            img_smooth.domain.chain[ix] = warp
            # Recreate cost object, because the chain was modified
            img_smooth.storage = MEM
            cost = CostMIND(vol_smooth, img_smooth, normalise=True,
                            sigma=1.0, truncate=1.5, kernel=MK_STAR)

            # --------------------- OPTIMISE NEW WARP -------------------- #

            # Regularisation
            if t.regweight > 0:
                # reg = RegBendingEnergy(warp, weight=t.regweight)
                reg = NormTxRegulariser(warp, order=2, weight=t.regweight)
                cost_items = [cost, reg]
            else:
                cost_items = [cost]

            # Optimisation
            logger.debug(f"Optimising the following control points:"
                         f"\n{warp.domain.get_voxel_coordinates()}")
            OptNL(warp, *cost_items, method=OptMethods.LN_BOBYQA,
                  step=initial_step, xtol_abs=t.xtol_abs, logger=logger,
                  visualise=t.visualise, normalised=NORMALISE)()

        # <--- POINT OPTIMISATION SEQUENCE

        # Record the final cost at the current smoothing level
        if sm_no + 1 == len(states):
            stage_cost.append(cost())
            logger.info(f"Final cost @ [ssm={ssm}px vsm={vsm}vx]: "
                        f"{stage_cost[-1]}")

        # Copy optimised transformations before creating a new image from
        # the original. [This may no longer be necessary.]
        img.domain = img_smooth.domain
        img.resmgr.sync()

    # img.rescale(1)
    # vol.rescale(1)

    # Generate visible output
    vs = vol.evaluate(img.domain)
    vs.normalise()
    vs.snapshot(
        os.path.join(outdir, f"mri-warped-{slice_no:04d}-{part}.png"),
        overwrite=True)
    img.normalise()
    img.snapshot(
        os.path.join(outdir, f"slice-warped-{slice_no:04d}-{part}.png"),
        overwrite=True
    )
    img.domain.save(
        os.path.join(outdir, f"slice-warped-{slice_no:04d}-{part}.dom"),
        overwrite=True
    )

    return stage_cost[-1]


def absolute_step(lb, ub, step):
    di = ub - lb
    di[~np.isfinite(di)] = 2
    if np.any(di <= 0):
        raise AssertionError("No space for optimisation.")
    return np.multiply(0.5 * di, step)


def calc_km_control_points(n_new, n_old, img, stparams):
    """
    Uses k-means clustering to generate an exact number of tiles,
    distributed evenly and compactly across the ROI of the target image, as
    defined by a mask.

    :param n_new: number of new points
    :type n_new: int
    :param n_old: number of existing points
    :type n_old: int
    :param img: target image
    :type img: TImage
    :param stparams: stage-specific parameters
    :type stparams: Any
    :param cnf: all configurations
    :type cnf: dict or AttrMap

    :returns: (n_points, 2) voxel coordinate array of control points
    :rtype: np.ndarray

    """
    from sklearn.cluster import KMeans
    t = AttrMap(stparams)
    logger = logging.getLogger("tirl.scripts.oxdbb.bigmac.ms2v")

    # Define ROI for the cost map
    tmp = img.copy().tensors.reduce()
    tmp.normalise()
    coords = np.vstack(np.where(tmp.mask > 0.5)).T
    n_points = int(t.support.points)
    km = KMeans(n_clusters=n_points, random_state=0)
    km.fit(coords)
    xi = km.cluster_centers_[n_old:n_old + n_new]
    logger.info(f"Created {xi.shape[0]} new control points: \n{xi}")

    # Measure the average distance between segment centroids
    edges = np.max(xi, axis=0) - np.min(xi, axis=0)
    edges = edges[np.nonzero(edges)]
    mean_distance = np.power(np.prod(edges) / xi.shape[0], 1. / edges.size)
    logger.info(f"Mean control point distance: {mean_distance} px")

    # Return the voxel coordinates of the sorted control points
    return xi


def create_warp_from_points(img, points, t, outdir):
    """
    Creates empty warp field based on the voxel coordinates of the support
    points on the fixed image domain.

    """
    dim = len(t.vectorder)
    _, ix = img.domain.get_transformation("slice-embed", index=True)
    domain = Domain(points, transformations=img.domain.chain[:ix + 1],
                    offset=img.domain.offset)
    field = TField(domain=domain, tshape=(dim,), order=TENSOR_MAJOR,
                   dtype=ts.DEFAULT_FLOAT_TYPE, fill_value=0)

    if t.export.control_points:
        timestamp = datetime.now().strftime("%Y%m%d_%H%M%S_%f")
        fn = os.path.join(outdir, f"control_points_{timestamp}.png")
        np.savetxt(fn.replace(".png", ".txt"), points)
        save_control_point_image(img, points, fn)

    # Define bounds
    if t.vectorder == "xyz":
        lbxy = float(t.lower_dxy) * np.ones((2, field.domain.numel))
        lbz = float(t.lower_dz) * np.ones((1, field.domain.numel))
        ubxy = float(t.upper_dxy) * np.ones((2, field.domain.numel))
        ubz = float(t.upper_dz) * np.ones((1, field.domain.numel))
        lb = -np.concatenate((lbxy, lbz), axis=0).ravel()
        ub = np.concatenate((ubxy, ubz), axis=0).ravel()
    elif t.vectorder == "xy":
        lb = -float(t.lower_dxy) * np.ones((2, field.domain.numel))
        ub = float(t.upper_dxy) * np.ones((2, field.domain.numel))
    elif t.vectorder == "z":
        lb = -float(t.lower_dz) * np.ones(field.domain.numel)
        ub = float(t.upper_dz) * np.ones(field.domain.numel)
    else:
        raise ValueError(f"Invalid vector order: {t.vectorder}")

    # Create warp
    tx_warp = TxRbfDisplacementField(
        field, bounds=(lb, ub), name="warp", invertible=False,
        mode=NL_ABS, vectorder=t.vectorder, dense_shape=img.domain.shape)
    tx_warp.interpolator.model = t.model

    return tx_warp


def save_control_point_image(photo, cpoints, filename):
    logger = logging.getLogger("tirl.scripts.oxdbb.bigmac.ms2v")
    # Creating the figure requires a window management system, so check this
    # first, and change the backend accordingly if it cannot be found.
    from tirl.utils import to_img
    if not os.getenv("DISPLAY") or not ts.ENABLE_VISUALISATION:
        import matplotlib
        matplotlib.use("agg")
    else:
        import matplotlib
        matplotlib.use(ts.MPL_BACKEND)
    import matplotlib.pyplot as plt

    # Create figure with photo and control points
    fig = plt.figure()
    ax = fig.add_subplot(111)
    order = photo.order
    photo.order = VOXEL_MAJOR
    if photo.tshape == ():
        ax.imshow(to_img(photo.data), cmap='gray', zorder=1)
    else:
        ax.imshow(to_img(photo.data), zorder=1)
    photo.order = order
    y, x = cpoints.T
    ax.scatter(x, y, c="magenta", zorder=2)

    # Save figure
    dot_file = os.path.join(filename)
    try:
        fig.savefig(dot_file)
    except Exception as exc:
        logger.error(exc.args)
        logger.info("Control point image could not be saved due to an error.")
    else:
        logger.info(f"Saved control point figure: {filename}")


def optimise_rigid2d(g):
    """
    Optimises the in-plane translation and rotations of the slices of the
    grating.

    :param g: Grating instance
    :type g: Grating

    """
    logger.info("Starting 2D rigid (in-plane) optimisation...")

    if cnf["n_cpu"] > 1:
        with mp.Pool(processes=cnf["n_cpu"]) as pool:
            updated_domains = pool.map(worker_rigid2d, g.domains.items())
    else:
        updated_domains = []
        for job in g.domains.items():
            updated_domains.append(worker_rigid2d(job))

    # Update domains in main process
    for slice_no, d in updated_domains:
        g.domains[slice_no] = d


def worker_rigid2d(job):
    slice_no, domain = job
    logger.info(f"In-plane optimisation of Slice {slice_no}...")

    # Create slice-specific output directory
    outdir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")
    try:
        if not os.path.exists(outdir):
            os.makedirs(outdir)
    except:
        pass

    # Load the volume and binarise the mask
    vol = TImage(cnf["volume"], external="sform", storage=MEM)
    vmask = TImage(cnf["vmask"], external="sform", storage=MEM)
    vol.mask = np.where(vmask.data > 0, 1.0, 0.0)

    # Load the slice image and assign the image data to the grating domain
    tfile = os.path.join(cnf["quickload"], f"{slice_no + 1:04d}-pyramid.timg")
    img = tirl.load(tfile)
    img.domain = domain.copy()
    img.resmgr.sync()
    img.storage = MEM

    try:
        # Coarse alignment by image centres
        align_centres(img, vol.tmask())
        # Multi-resolution rigid optimisation
        rigid2d_multires(img, vol)
        # Update domain
        domain.chain = img.domain.chain
    except Exception as exc:
        logger.exception(exc.args)
        return slice_no, domain

    # Generate output
    vs = vol.evaluate(img.domain)
    vs.normalise()
    img.normalise()
    vs.snapshot(
        os.path.join(outdir, f"rigid2d-mri-{slice_no}.png"), True)
    img.snapshot(
        os.path.join(outdir, f"rigid2d-slice-{slice_no}.png"), True)
    img.domain.save(
        os.path.join(outdir, f"rigid2d-slice-{slice_no}.dom"), True)

    return slice_no, domain


def optimise_rigid3d(g):
    """
    Optimises (locally) the 3D slice rotations and slice positions of the
    slices starting from the collective estimates of the current grating.

    This method may be considered as the "mutation" step in the context
    of the main loop.

    :param g: Grating instance
    :type g: Grating

    """
    logger.info("Starting 3D rigid (full) optimisation...")

    if cnf["n_cpu"] > 1:
        with mp.Pool(processes=cnf["n_cpu"]) as pool:
            updated_domains = pool.map(worker_rigid3d, rigid3d_jobs(g))
    else:
        updated_domains = []
        for job in rigid3d_jobs(g):
            updated_domains.append(worker_rigid3d(job))

    # Update domains in the main process
    for slice_no, d in updated_domains:
        g.domains[slice_no] = d


def rigid3d_jobs(g):
    for slice_no, domain in g.domains.items():
        yield slice_no, domain, g.gap


def worker_rigid3d(job):
    slice_no, domain, gap = job
    params = cnf["rigid3d"]
    logger.info(f"3D rigid optimisation of Slice {slice_no}...")

    # Create slice-specific output directory
    outdir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")
    try:
        if not os.path.exists(outdir):
            os.makedirs(outdir)
    except:
        pass

    try:
        # Load the slice image
        tfile = os.path.join(
            cnf["quickload"], f"{slice_no + 1:04d}-pyramid.timg")
        scales = params["scaling"]
        smoothing = params["smoothing"]
        ldx = np.asarray(params["ldx"], dtype=float)
        udx = np.asarray(params["udx"], dtype=float)
        newimg = tirl.load(tfile)
        newimg.domain = domain.copy()
        newimg.resmgr.sync()
        newimg.storage = MEM

        # Load the volume image
        vol = TImage(cnf["volume"], external="sform", storage=MEM)
        vmask = TImage(cnf["vmask"], external="sform", storage=MEM)
        vol.mask = np.where(vmask.data > 0, 1.0, 0.0)
        vol.storage = MEM

        for sc, sm in zip(scales, smoothing):
            newimg.rescale(1 / sc)
            if sm != 0:
                sm_newimg = newimg.smooth(sm, copy=True)
                sm_newimg.domain = newimg.domain
            else:
                sm_newimg = newimg

            offset = sm_newimg.domain.chain["slice-offset"]
            rotation = sm_newimg.domain.chain["grating-rotation"]
            assert rotation is not domain.chain["grating-rotation"]
            og = OptimisationGroup(offset, rotation)
            logger.log(5, str(og.parameters[:]))
            l_offset = np.subtract(offset.parameters[:], [*ldx[:3]])
            u_offset = np.add(offset.parameters[:], [*udx[:3]])
            l_rot = np.subtract(rotation.parameters[:], np.radians(ldx[3:]))
            u_rot = np.add(rotation.parameters[:], np.radians(udx[3:]))
            og.set_lower_bounds(np.asarray([*l_offset, *l_rot]))
            og.set_upper_bounds(np.asarray([*u_offset, *u_rot]))
            sm_newimg.storage = MEM
            cost = CostMIND(vol, sm_newimg)
            OptNL(og, cost, method="LN_BOBYQA", normalised=True,
                  xtol_abs=[0.01, 0.01, 0.01, 0.01, 0.01, 0.01],
                  step=0.5, logger=logger, visualise=params["visualise"])()
            newimg.domain.chain = sm_newimg.domain.chain
            newimg.resmgr.sync()
        else:
            domain.chain = newimg.domain.chain
            vs = vol.evaluate(newimg.domain)
            vs.normalise()
            newimg.normalise()
            vs.snapshot(
                os.path.join(outdir, f"rigid3d-mri-{slice_no}.png"), True)
            newimg.snapshot(
                os.path.join(outdir, f"rigid3d-slice-{slice_no}.png"), True)
            newimg.domain.save(
                os.path.join(outdir, f"rigid3d-slice-{slice_no}.dom"), True)

    except Exception as exc:
        logger.exception(exc.args)

    finally:
        return slice_no, domain


def align_centres(img, vmask):
    for i in range(3):
        vs = vmask.evaluate(img.domain)
        pc = img.domain.get_physical_coordinates()
        vs_cog = np.sum(pc.T * vs.data.ravel(), axis=-1) / vs.data.sum()
        img_cog = np.sum(pc.T * img.data.ravel(), axis=-1) / img.data.sum()
        shift = img.domain.chain[4:].inverse().map_vector(vs_cog - img_cog)
        img.domain.chain["slice-nudge"].parameters[:] = shift


def rigid2d_multires(img, vol):
    params = cnf["rigid2d"]
    scales = params["scaling"]
    smoothing = params["smoothing"]
    ldx = np.asarray(params["ldx"], dtype=float)
    ldx[0] = rad(ldx[0])
    udx = np.asarray(params["udx"], dtype=float)
    udx[0] = rad(udx[0])
    img.storage = MEM

    for sc, sm in zip(scales, smoothing):
        img.rescale(1 / sc, copy=False)
        if sm != 0:
            sm_img = img.smooth(sm, copy=True)
            sm_img.domain = img.domain
        else:
            sm_img = img

        trans_2d = sm_img.domain.chain["slice-nudge"]
        rot_2d = sm_img.domain.chain["slice-rotation"]
        trans_2d.parameters.set_bounds(None)    # bugfix
        rot_2d.parameters.set_bounds(None)      # bugfix
        og = OptimisationGroup(rot_2d, trans_2d)
        lb = np.subtract(og.parameters[:], ldx)
        ub = np.add(og.parameters[:], udx)
        og.set_lower_bounds(lb)
        og.set_upper_bounds(ub)
        sm_img.storage = MEM
        cost = CostMIND(vol, sm_img)
        OptNL(og, cost, method="LN_BOBYQA", normalised=True,
              xtol_abs=[0.001, 0.01, 0.01], step=0.1,
              visualise=params["visualise"], logger=logger)()
        img.domain.chain = sm_img.domain.chain
        img.resmgr.sync()


def prepare_output_dir():
    """
    Creates the full path to the output directory if it does not exist yet.
    If the output directory is not specified, the current directory will be
    used.

    """
    outdir = cnf["outputdir"]
    if not outdir:
        outdir = os.getcwd()
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
    cnf["outputdir"] = outdir


def choose_data(table):
    """
    Export a subset of the input table that will be used in the registration.
    Modify this function to choose different inputs.

    :param table: input table (slice database)
    :type table: pd.DataFrame

    :returns: reduced table
    :rtype: pd.DataFrame

    """
    # Some slices were captured more than once, and even for the same capture
    # there can be multiple registration attempts. Furthermore, several slices
    # were divided into left and right parts, and registered separately.

    # The following rules are applied to choose the input data:
    # 1/ the latest capture of each slice or hemisphere
    # 2/ for identical inputs, the one with the least registration cost

    def mtime(x):
        return pathlib.Path(x).stat().st_mtime

    def regcost(x):
        regdir = os.path.dirname(x)
        with open(os.path.join(regdir, "logfile.log"), "r") as fp:
            logs = "".join(line for line in fp)
            try:
                cost = float(re.search("(STAGE 1 cost: )(.+)", logs).group(2))
            except Exception as exc:
                cost = np.inf
        return cost

    filtered = []
    for slice_index, slice_group in table.groupby("Slice"):
        latest = slice_group.SliceOriginal.apply(mtime).argmax()
        latest_capture = slice_group.iloc[latest].Capture
        capture_group = slice_group[slice_group.Capture == latest_capture]
        for pindex, part_group in capture_group.groupby("Part"):
            best = part_group.SliceRegistered.apply(regcost).argmin()
            best_registration = part_group.iloc[best].Registration
            part = part_group.iloc[0].Part
            parts = table[(table.Slice == slice_index) &
                          (table.Capture == latest_capture) &
                          (table.Part == part) &
                          (table.Registration == best_registration)]
            assert len(parts) in (0, 1), f"{parts}"
            filtered.append(parts)

    else:
        filtered = pd.concat(filtered)
        costs = filtered.SliceRegistered.apply(regcost)
        filtered["PreRegistrationCost"] = costs
        return filtered


def create_logger():
    """
    Creates a logger that sends log messages to the standard output and
    into a file.

    """
    p = AttrMap(cnf)
    logger_name = "tirl.scripts.oxdbb.bigmac.ms2v"
    global logger
    logger = logging.getLogger(logger_name)
    logger.handlers = []  # delete any existing handlers to avoid duplicate logs
    logger.setLevel(1)
    formatter = logging.Formatter(
        fmt='%(asctime)s Process-%(process)d %(levelname)s (%(lineno)d) '
            '- %(message)s',
        datefmt='[%Y-%m-%d %H:%M:%S]')

    # Make sure that the logfile can be created
    logfile = cnf["logfile"]
    if not logfile:
        logfile = os.path.join(p.outputdir, "logfile.log")
    else:
        logdir = os.path.dirname(logfile)
        if not os.path.isdir(logdir):
            os.makedirs(logdir)

    # Redirect all logs of interest to the logfile
    fh = logging.FileHandler(logfile, mode="a", delay=False)
    if isinstance(p.loglevel, str):
        level = getattr(logging, p.loglevel.upper())
    elif isinstance(p.loglevel, (int, float)):
        level = int(p.loglevel)
    elif not p.loglevel:
        level = logging.INFO
    else:
        raise TypeError(f"Invalid logging level: {p.loglevel}")
    fh.setLevel(level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Redirect only warnings/errors to the standard output, unless "verbose"
    ch = logging.StreamHandler()
    if p.verbose:
        ch.setLevel(level)
    else:
        ch.setLevel(logging.WARNING)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


def create_parser():
    """
    Creates argument parser object.

    """
    parser = argparse.ArgumentParser(
        prog="multislice_to_volume",
        description="Registers parallel sections to a volume.")
    parser.add_argument(
        "--config", metavar="<*.json>", type=str, required=True,
        help="Configuration file.")
    parser.add_argument(
        "--verbose", action="store_true", default=False, required=False,
        help="Logs to STDOUT.")
    return parser


parser = create_parser()
cnf = dict()

### TROUBLESHOOTING

with open("multislice_to_volume.json", "r") as fp:
    cnf = dict(json.load(fp))
troubleshoot()
exit()

### END OF TROUBLESHOOTING


if len(sys.argv) > 1:
    args = parser.parse_args()
    with open(args.config, "r") as fp:
        cnf = dict(json.load(fp))
    if args.verbose:
        cnf["verbose"] = args.verbose


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main()
    else:
        parser.print_help()

    # mp.set_start_method("spawn")
    # parser = create_parser()
    # cnf = dict()
    #
    # if len(sys.argv) > 1:
    #     args = parser.parse_args()
    #     with open(args.config, "r") as fp:
    #         cnf = dict(json.load(fp))
    #     if args.verbose:
    #         cnf["verbose"] = args.verbose
    #     main()
    # else:
    #     parser.print_help()
