#!/usr/bin/env python

import os
import sys
import numpy as np
from glob import glob
import nibabel as nib

import tirl
from tirl.domain import Domain
from tirl.timage import TImage
from scipy.interpolate import griddata


def main():
    datadir = "/mnt/iris/BigMac/Microscopy/PLI/Anterior/timg"
    regdir = "/mnt/knox/bigmac/multislice-ismrm"
    v = "/mnt/iris/BigMac/MRI/Postmortem/struct/data/struct_bias_restore.nii.gz"
    vol = TImage(v, external="sform", storage="mem")
    inv = vol.domain.chain.inverse()

    known_points = []
    data = []
    for slice_no in range(101):
        dfile = glob(os.path.join(
            regdir, f"{slice_no:04d}", f"slice-warped-*whole.dom"))
        if not dfile:
            continue
        else:
            dfile = dfile[0]
        domain = tirl.load(dfile)
        known_points.append(inv.map(domain.get_physical_coordinates()))
        imfile = os.path.join(datadir, f"{slice_no + 1:04d}-pyramid.timg")
        img = tirl.load(imfile)
        print(f"Slice {slice_no}: {img.vshape}, Domain: {domain.shape}")
        img.resize(*domain.shape)
        data.append(img.data.ravel())

    x, y, z = np.vstack(known_points).T
    data = np.concatenate(data)
    xi, yi, zi = np.meshgrid(*tuple(range(dim) for dim in vol.vshape),
                             indexing="ij")

    # Do the interpolation
    interpolants = griddata((x, y, z), data, (xi, yi, zi), fill_value=0)
    pli = interpolants.reshape(vol.data.shape)
    mri_nifti = nib.load(v)
    hdr = mri_nifti.header
    pli_nifti = nib.Nifti1Image(pli, affine=hdr.get_best_affine(), header=hdr)
    nib.save(pli_nifti, "/mnt/knox/bigmac/multislice-ismrm/pli.nii.gz")


if __name__ == "__main__":
    main()
