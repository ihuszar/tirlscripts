#!/usr/bin/env python

import os
import sys
import numpy as np
import pandas as pd
from glob import glob
import seaborn as sns
import matplotlib.pyplot as plt

import tirl
from tirl.timage import TImage


def main():

    # Newly registered slices
    newregdir = "/mnt/knox/bigmac/multislice-ismrm"
    table = "/mnt/knox/bigmac/multislice-ismrm/filtered_input_data.csv"

    slices = []
    errors = []
    for ix, sl in pd.read_csv(table).iterrows():
        oldreg = tirl.load(sl.SliceRegistered)
        oldreg.mask = TImage(sl.SliceMask)
        slice_no = int(sl.Slice) - 1
        dfile = glob(os.path.join(
            newregdir, f"{slice_no:04d}", f"slice-warped-*whole.dom"))
        if not dfile:
            continue
        else:
            dfile = dfile[0]

        newreg = tirl.load(dfile)
        oldreg.resize(*newreg.shape)
        mask = oldreg.mask.ravel() > 0
        old_pc = oldreg.domain.get_physical_coordinates()
        new_pc = newreg.get_physical_coordinates()
        diff = np.linalg.norm(old_pc[mask, :] - new_pc[mask, :], axis=-1)
        slices.append(slice_no)
        errors.append(diff)
        print(slice_no, np.median(diff), np.mean(diff), np.std(diff))

    for slice_no, dist in zip(slices, errors):
        mederr = np.median(dist)
        print(f"Slice {slice_no}: Median error: {mederr} mm")


if __name__ == "__main__":
    main()
