#!/usr/bin/env python

"""
2021-Jul-20 INH:
This script precomputes the low-resolution version of the masked PLI slides, so
that the multislice registration does not need to spend time on this. The main
motivation is development and debugging, as it currently takes too long to run
the multislice script.

"""


import os
import tirl
import pandas as pd
import multiprocessing as mp
from tirl.timage import TImage


outdir = "/mnt/iris/BigMac/Microscopy/PLI/Posterior/MultiSliceInput/"
sheet = "/mnt/iris/BigMac/pli_posterior.csv"


def main(sheet, n_cpu=1):
    """ Main program code. """

    table = pd.read_csv(sheet)
    n_cpu = n_cpu % mp.cpu_count()
    if n_cpu > 1:
        ctx = mp.get_context("spawn")
        with ctx.Pool(processes=n_cpu, maxtasksperchild=1) as pool:
            pool.map_async(worker, table.iterrows()).get()
            pool.close()
            pool.join()
    else:
        for job in table.iterrows():
            worker(job)


def worker(job):
    ix, record = job
    print(f"Process {mp.current_process().pid}: {record.Capture} {record.Part}")
    outname = "_".join([record.Capture, record.Registration, record.Part])
    outfile = os.path.join(outdir, outname + ".timg")
    if os.path.isfile(outfile):
        return
    try:
        orig = TImage(record.SliceOriginal, name=record.Part)
        mask = TImage(record.SliceMask).tensors.reduce()
        orig.mask = mask
        reg = tirl.load(record.SliceRegistered)
        orig.resize(*reg.vshape, copy=False)
        orig.name = record.Part
        orig.save(outfile, overwrite=True)
    except Exception as exc:
        print(record)


if __name__ == "__main__":
    main(sheet, n_cpu=-1)
