#!/usr/bin/env python

import os
import imageio
import numpy as np


def main(rootdir):
    """ Main program code. """

    for root, dirs, files in os.walk(rootdir):
        if ("left" in root.lower()) or ("right" in root.lower()):
            continue
        if not root.lower().endswith("masks"):
            continue
        if "mask.tif" in files:
            continue
        print(root)
        masks = [f for f in files if ("L_mask" in f) or ("R_mask" in f)]
        masks = np.stack([imageio.imread(os.path.join(root, f))
                          for f in masks], axis=-1)
        mask = np.sum(masks, axis=-1).astype(masks[0].dtype)
        outfile = os.path.join(root, "mask.tif")
        imageio.imsave(outfile, mask)


if __name__ == "__main__":
    rootdir = "/mnt/iris/BigMac/Microscopy/PLI/Posterior"
    main(rootdir)
