#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
from tirl.constants import *
from tirl.chain import Chain
from tirl.domain import Domain
from tirl.timage import TImage
from tirl.transformations.rotation import TxRotation2D
from tirl.transformations.translation import TxTranslation


class CompositeImage(object):
    """
    CompositeImage creates a composite image from multiple images.

    """
    def __init__(self, *images, mode="blend", domain=None,
                 transformations=None):
        """
        Initialisation of CompositeImage.

        :param images: stack of TImages
        :type images: TImage
        :param mode:
            Composition scheme. If "blend" (default), the unmasked regions of
            consecutive images will overwrite the respective area on the
            canvas. If "additive", consecutive images will be overlaid on
            previous ones (weighted by the mask).
        :type mode: str
        :param domain: common image domain
        :type domain: Domain
        :param transformations: transformations after composition
        :type transformations: tuple or list or Chain or Transformation or None

        """
        self.images = images
        self.mode = mode
        # Note: the first image must be big enough to host the contents of all
        self.domain = domain or self.images[0].domain
        self._canvas = np.zeros(self.domain.shape)
        if transformations is None:
            self.posttx = Chain()
        else:
            self.posttx = Chain(transformations)

    def __call__(self, *args, **kwargs):
        """
        Evaluates the composite image to return a single TImage.

        :returns: composite image as a TImage
        :rtype: TImage

        """
        self.canvas[:] = 0
        newimgs = [img.evaluate(self.domain) for img in self.images]

        if self.mode == "blend":
            newmask = 1
            for newimg in newimgs:
                mask = Ellipsis if newimg.mask is None else newimg.mask > 0.5
                self._canvas[mask] = newimg.data[mask]
                newmask = np.logical_and(newmask, newimg.mask)
        elif self.mode == "additive":
            newmask = 1
            for newimg in newimgs:
                mask = 1 if newimg.mask is None else newimg.mask
                self._canvas = self.canvas + mask * newimg
                newmask = np.logical_or(newmask, newimg.mask)
        else:
            raise ValueError(f"Invalid composite mode: {self.mode}")

        # Return composite image as TImage
        cs = TImage(self.canvas, domain=self.domain[:],
                    order=VOXEL_MAJOR, mask=newmask.astype(np.uint8))
        if self.posttx:
            cs.domain.chain += Chain(self.posttx)
        return cs

    @property
    def images(self):
        return self._images

    @images.setter
    def images(self, imgs):
        if hasattr(imgs, "__timage__"):
            self._images = [imgs]
        elif hasattr(imgs, "__iter__"):
            self._images = []
            for im in imgs:
                if hasattr(im, "__timage__"):
                    self._images.append(im)
                else:
                    raise TypeError(f"Invalid TImage: {im}")
        else:
            raise TypeError(f"Invalid composite image specification: {imgs}")

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, m):
        m = str(m).lower()
        if m not in ("blend", "additive"):
            raise ValueError(f"Invalid composite mode: {m}")
        self._mode = m

    @property
    def canvas(self):
        return self._canvas

    @property
    def domain(self):
        return self._domain

    @domain.setter
    def domain(self, d):
        if hasattr(d, "__domain__"):
            self._domain = d[:]
        else:
            raise TypeError("Invalid composite domain.")


class CompositeSlice(CompositeImage):
    """
    CompositeSlice is a 2-D parametric subclass of CompositeImage that acts as
    a container for several images that need to be merged to obtain a slice of
    the whole brain. The parameters control the position and orientation of the
    individual components of the slices relative to the first component.

    The domain of the CompositeSlice is automatically inferred from the initial
    input, or it may be specified manually, but should not be changed later.

    """

    def __init__(self, *images, mode="blend", params=None, domain=None,
                 transformations=None):
        """
        Initialisation of CompositeSlice.

        :param images: input images
        :type images: str or np.ndarray or TField or TImage
        :param mode:
            Composition scheme. If "blend" (default), the unmasked regions of
            consecutive images will overwrite the respective area on the
            canvas. If "additive", consecutive images will be overlaid on
            previous ones (weighted by the mask).
        :type mode: str
        :param params:
            Parameters describing the spatial relationship of
            consecutive images. For each image the following must be defined as
            a list: [shift_y, shift_x, rotation about own centre in degrees]
        :type params: list[list[float]] or np.ndarray or None

        """
        # Load the images
        images = [self.load_image(im) for im in images]

        # Initialise single-component CompositeImage
        if len(images) == 1:
            images[0].domain.reset(internal=True)
            super(CompositeSlice, self).__init__(
                *images, mode=mode, domain=domain,
                transformations=transformations)
            self.parameters = np.zeros((1, 3))
            return

        # Initialise multi-component CompositeImage
        # Remove all domain transformations
        for im in images:
            assert im.domain.ndim == 2, "Only 2-D images are allowed."
            im.domain.reset(internal=True)
        # Prepare the input parameters
        if params is not None:
            params = np.atleast_2d(params)
            if params.shape[0] == 1:
                params = np.tile(params[np.newaxis, :], (len(images), 1))
        else:
            params = np.zeros(shape=(len(images), 3))
        assert params.shape == (len(images), 3)
        for im, p in zip(images, params):
            dy, dx, angle = p
            im.centralise(weighted=True)
            r = TxRotation2D(angle, mode="deg", name="cmp-rotation2d")
            t = TxTranslation(dy, dx, name="cmp-translation2d")
            im.domain.external.extend([r, t])
        # Work out the best domain
        if domain is None:
            coords = np.concatenate(
                [im.domain.get_physical_coordinates() for im in images], axis=0)
            low = np.min(coords, axis=0)
            high = np.max(coords, axis=0)
            canvas = Domain(*np.ceil(1.1 * (high - low)).astype(int))
            offset = TxTranslation(canvas.vcentre(), name="global-offset")
        else:
            offset = TxTranslation(domain.vcentre(), name="global-offset")
            canvas = domain
        for im in images:
            im.domain.external.append(offset)

        # Initialise CompositeImage with parameters
        super(CompositeSlice, self).__init__(
            *images, mode=mode, domain=canvas, transformations=transformations)
        self.parameters = params

    @staticmethod
    def load_image(im, **imkwargs):
        """
        Allows images to be loaded from various sources with a uniform call,
        and ensures that the original image domain is not modified by other
        methods.

        :param im: image specification
        :type im: str or np.ndarray or TField or TImage
        :param imkwargs: keyword arguments for the resultant TImage instance
        :type imkwargs: Any

        :returns: image as a TImage with an independent domain
        :rtype: TImage

        """
        try:
            img = TImage(im, **imkwargs)
            img.domain = im.domain[:]
            return img
        except Exception as exc:
            raise TypeError(f"Invalid image specification: {im}") from exc

    @property
    def parameters(self):
        return self._parameters

    @parameters.setter
    def parameters(self, p):
        p = np.atleast_2d(p).astype(float)
        if p.shape == 1:
            p = np.tile(p, (len(self.images), 1))
        else:
            assert p.shape[0] == len(self.images), \
                "The parameter specification does not match the number of " \
                "components in the composite image."
        self._parameters = p

    def update(self):
        for im, p in zip(self.images, self.parameters):
            dy, dx, angle = p
            r = im.domain.external["cmp-rotation2d"]
            t = im.domain.external["cmp-translation2d"]
            r.angle = angle
            t.parameters[:] = dy, dx

    def __call__(self, *args, **kwargs):
        self.update()
        return super(CompositeSlice, self).__call__(*args, **kwargs)
