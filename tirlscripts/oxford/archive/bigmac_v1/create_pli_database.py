#!/usr/bin/env python

import os
import re
import json
import pandas as pd
from glob import glob


def main(input_files):
    """ Main program code. """

    records = []
    columns = ["Capture", "Slice", "Part", "Registration",
               "SliceOriginal", "SliceRegistered", "SliceMask"]

    for f in input_files:
        capture = re.findall(r"P\d{3}\w", f)[0]
        slice_no = int(capture[1:-1])
        maskdir = f"/mnt/iris/BigMac/Microscopy/PLI/Posterior/{capture}/Masks"
        reg_take = re.findall(r"(?<=/)Reg2MRI.*?(?=/1_stage1.timg)", f)
        if reg_take:
            reg_take = reg_take[0]
            reg_take = reg_take.replace("/Right", "").replace("/Left", "")
            reg_take = reg_take.replace("/", "_")
        configfile = os.path.join(os.path.dirname(f), "configuration.json")
        with open(configfile, "r") as fp:
            cnf = dict(json.load(fp))
        orig = cnf["slice"]["file"].replace("/vols/Data/", "/mnt/iris/")
        mask = cnf["slice"]["mask"]["file"]
        if mask:
            mask = mask.replace("/vols/Data/", "/mnt/iris/")
        if "left" in f.lower():
            part = "left"
            if mask is None:
                try:
                    mask = glob(os.path.join(maskdir, "*L_mask*"))[0]
                except:
                    print(f"No mask for: {f}")
        elif "right" in f.lower():
            part = "right"
            if mask is None:
                try:
                    mask = glob(os.path.join(maskdir, "*R_mask*"))[0]
                except:
                    print(f"No mask for: {f}")
        else:
            part = "whole"
            if mask is None:
                try:
                    mask = glob(os.path.join(maskdir, "mask.tif"))[0]
                except:
                    print(f"No mask for: {f}")
        records.append([capture, slice_no, part, reg_take, orig, f, mask])
    else:
        table = pd.DataFrame(data=records, columns=columns)

    table.to_csv("/mnt/iris/BigMac/pli_posterior.csv")
    print(table)


if __name__ == "__main__":
    input_files = "/mnt/iris/BigMac/pli_posterior_stage1.txt"
    with open(input_files, "r") as fp:
        input_files = [f.rstrip("\n\r") for f in fp.readlines()]
    main(input_files)
