#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2021 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Date: 22 June 2021


# SHBASECOPYRIGHT


"""
This module estimates a simplified transformation chain on the basis of
previously optimised and concatenated chains. The so initialised histology
image is registered directly to the volume by the slice-to-volume routine to
fine-tune its alignment.

"""


# DEPENDENCIES

import os
import sys
import json
import logging
import argparse
import numpy as np
import multiprocessing as mp
from attrdict import AttrMap
from datetime import datetime
from numpy.linalg import lstsq
from scipy.optimize import fmin
from sklearn.cluster import KMeans
from collections import namedtuple
from sklearn.decomposition import PCA
from skimage.measure import regionprops


# TIRL IMPORTS

import tirl
import tirl.settings as ts
from tirl.constants import *
from tirl.domain import Domain
from tirl.tfield import TField
from tirl.timage import TImage
from tirl.cost.mi import CostMI
from tirl.cost.mind import CostMIND
from tirl.optimisation.optnl import OptNL
from tirl.optimisation.optnl import OptMethods
from tirl.transformations.affine import TxAffine
from tirl.transformations.embedding import TxEmbed
from tirl.transformations.identity import TxIdentity
from tirl.transformations import TransformationGroup
from tirl.transformations.euler import TxEulerAngles
from tirl.transformations.axisangle import TxAxisAngle
from tirl.transformations.rotation import TxRotation3D
from tirl.optimisation.optgroup import OptimisationGroup
from tirl.transformations.translation import TxTranslation
from tirl.regularisation.normtxreg import NormTxRegulariser
from scripts.oxdbb.pathology.midlevel import general, inout, image
from tirl.transformations.rbf_displacement import TxRbfDisplacementField


# DEFINITIONS

# Named transformation chain
NamedChain = namedtuple("NamedChain", ["tx_warp", "tx_rot3d", "tx_trans3d"])
# NumPy print formatting
np.set_printoptions(precision=4)
# Optimise parameters in the [-1, 1] normalised range
NORMALISE = True


# IMPLEMENTATION

def run(cnf=None, **options):
    """
    Runs TIRL histology-to-volume registration.

    :param cnf:
        Configuration file. If no file is specified (default), the default
        parameters will be used that are embedded in this script (see
        Definitions above). Instead of a file, a dictionary with suitable
        content may also be specified.
    :type cnf: Union[str, dict, None]
    :param options:
        Overriding configuration parameters.
    :type options: Any

    """
    # Load script configuration
    if cnf is not None:
        if isinstance(cnf, dict):
            cnf = dict(cnf)
        elif isinstance(cnf, str):
            with open(cnf, "r") as configfile:
                cnf = dict(json.load(configfile))
        else:
            raise TypeError(
                f"Unrecognised configuration format: {cnf.__.class__.__name__}")
    cnf.update(options)
    global p
    global logger
    p, logger = general.initialise_script(**cnf)
    p.logger = logger.name  # avoid globals

    # Load and configure input images
    if p.histology.export is True:
        ext = ts.EXTENSIONS["TImage"]
        p.histology.export = \
            os.path.join(p.general.outputdir, f"histology.{ext}")
    histo = inout.load_image(scope=globals(), **p.histology)
    histo.storage = p.histology.storage
    # Enforce mask
    # if histo.tmask() is None:
    #     from scripts.oxdbb.pathology.midlevel.image import set_mask
    #     set_mask(histo, scope=globals(), **p.histology.mask)
    histo.rule = None
    histo = histo.tensors.reduce()

    if p.volume.export is True:
        ext = ts.EXTENSIONS["TImage"]
        p.volume.export = os.path.join(p.general.outputdir, f"volume.{ext}")
    vol = inout.load_volume(scope=globals(), **p.volume)
    vol.storage = p.volume.storage
    vol.rule = None

    # Perform actions on the volume prior to registration,
    # unless it was loaded from a TImage file.
    isalternative = p.volume.file.lower().endswith(
        (ts.EXTENSIONS["TImage"], ts.EXTENSIONS["TIRLObject"]))
    if not isalternative:
        vol.normalise()

    # Run the registration routine
    try:
        register(histo, vol, p)
    except Exception as exc:
        logger.error(exc.args)
        logger.fatal(f"The registration terminated with an exception.")
        raise exc
    else:
        logger.fatal("The registration was completed successfully.")


def register(histo, vol, p):

    q = p.regparams

    # Only reduce the chain if the registration starts from stage 1
    if p.general.stages and p.general.stages[0] == 1:
        # Reduce the transformation chain
        domain, *estimates = reduce_chain(
            histo, vol, dof=p.reduce.dof, visualise=p.reduce.visualise)
        histo.domain = domain
        centre, normal = estimates

        # Prepare registration chain
        prepare_chain(histo)

        # Adjust slice-to-volume configurations
        q.stage_1.slab.centre = centre
        q.stage_1.slab.normal = normal
        q.stage_1.slab.original_normal = normal
        q.stage_1.slab.offset = 0

    # Check prerequisites first (better to fail here than later)
    assert isinstance(histo, TImage)
    assert isinstance(vol, TImage)
    for stage_no in p.general.stages:
        dict(q).get(f"stage_{stage_no}")
    if not os.path.isdir(p.general.outputdir):
        raise FileNotFoundError(
            f"Output directory is not ready at {p.general.outputdir}")

    # Run registration stages
    for counter, stage_no in enumerate(p.general.stages):
        general.run_stage(
            histo, vol, stage_no, counter + 1, scope=globals(), config=p)

    # Generate output
    outname = os.path.join(p.general.outputdir, "adjusted_histology.timg")
    histo.save(outname, overwrite=True)
    outname = os.path.join(p.general.outputdir, "resampled_volume_final.png")
    vol.evaluate(histo.domain).snapshot(outname, overwrite=True)


def reduce_chain(histo, mri, dof, visualise):

    logger = logging.getLogger(p.logger)

    # Estimate the centre of gravity in physical space
    logger.info("Started chain reduction.")
    # cog = histo.domain.map_voxel_coordinates(histo.domain.vcentre())
    cog = histo.domain.pcentre()
    logger.info(f"Histology image centre: {cog}.")

    # Estimate the orthonormal basis transformation from the centre of the image
    b0 = np.eye(3)
    # vc = [*histo.domain.vcentre(), 0]
    vc = histo.domain.vcentre()
    b1 = histo.domain.map_voxel_vectors(b0, [vc, vc, vc])
    magn = np.linalg.norm(b1, axis=-1)
    b1 = np.divide(b1.T, magn.ravel()).T
    logger.info(f"New basis: \n{b1[:, :3].T}.")

    # Define a new basis with the robust normal vector
    n = b1[2, :3]
    v1 = np.asarray([-n[1], n[0], 0])
    v1 /= np.linalg.norm(v1)
    v2 = np.cross(n.ravel(), v1.ravel())
    v2 /= np.linalg.norm(v2)

    # Estimate the rotation about the robust normal vector (in-plane rotation)
    inplane = TxAxisAngle(0, axis=n, mode="rad")
    def cost(angle, v1, v2):
        inplane.angle = angle
        w1 = inplane.map(v1)
        w2 = inplane.map(v2)
        return np.arccos(np.dot(b1[0, :3].ravel(), w1.ravel())) + \
               np.arccos(np.dot(b1[1, :3].ravel(), w2.ravel()))
    inplane.angle = fmin(cost, 1.57, (v1, v2))

    # Apply in-plane rotation, but derive v2 from the other two vectors
    # to ensure that the new basis remains orthonormal despite numerical errors
    v1 = inplane.map(v1)
    v2 = np.cross(n, v1)
    v2 /= np.linalg.norm(v2)
    mat = np.vstack([v1, v2, n]).T
    logger.info(f"Robust orthonormal basis: \n{mat}")

    # Define total 3D rotation matrix
    rotation = TxRotation3D(mat, name="rotation")

    # Create rigid transformation chain
    newdomain = histo.domain[:0]
    resolution = histo.domain.chain[0]
    centralise = histo.domain.chain[1]
    embed = TxEmbed(1, name="embed")
    translation = TxTranslation(*cog, name="translation")
    newdomain.chain = [resolution, centralise, embed, rotation, translation]
    logger.info("Created rigid transformation chain.")

    # Create a snapshot of the best rigid alignment
    if visualise:
        res = mri.evaluate(newdomain)
        outname = os.path.join(p.general.outputdir, "init_rigid.png")
        res.snapshot(outname, overwrite=True)
        logger.info(f"Created rigid snapshot: {outname}")

    # # Discard the in-plane component of the transformation, as this could
    # # impose unnecessary torque on the plane at the best affine position.
    # nonlinear_domain = histo.domain.copy()
    # nonlinear_domain.chain.append(TxIdentity(1))  # for homogeneous coordinates
    # linear_domain = newdomain.copy()
    # warp = [tx for tx in nonlinear_domain.chain
    #         if isinstance(tx, TxRbfDisplacementField)][-1]
    # if warp.vectorder == (0, 1, 2):
    #     inplane = warp.parameters[:int(round(warp.parameters.size * 2 / 3))]
    #     warp.parameters[:int(round(warp.parameters.size * 2 / 3))] = 0
    # else:
    #     inplane = 0

    # # Calculate best affine linear->nonlinear mapping (exclude bg pixels)
    # y = nonlinear_domain.get_physical_coordinates().T.astype(np.float64)
    # yl = linear_domain.get_physical_coordinates().T.astype(np.float64)
    # A = lstsq(yl[:, mask] @ yl[:, mask].T, yl[:, mask] @ y[:, mask].T, 0.1)[0]
    # A = np.vstack([*A, [0, 0, 0, 1]])
    # logger.info(f"Best affine estimate: \n{A}")
    #
    # # Create affine transformation chain
    # newdomain.chain.append(TxAffine(A[:3, :].ravel(), name="affine"))
    #
    # # Create a snapshot of the best affine a#lignment
    # if visualise:
    #     res = mri.evaluate(newdomain)
    #     outname = os.path.join(p.general.outputdir, "init_affine.png")
    #     res.snapshot(outname, overwrite=True)
    #     logger.info(f"Created affine snapshot: {outname}")

    # Add low-DOF estimate of the non-linear deformations
    vc = newdomain.get_voxel_coordinates()[mask > 0.1, :]
    km = KMeans(n_clusters=dof, random_state=0)
    if vc.shape[0] > 10000:
        km.fit(vc[::100])
    else:
        km.fit(vc)
    control_points = km.cluster_centers_
    logger.info(f"Defined new control points: \n{control_points}")

    # Create a snapshot of the control points overlaid on the histology image
    if visualise:
        import matplotlib.pyplot as plt
        ax = plt.subplot(111)
        if histo.tshape == (1,):
            ax.imshow(histo.data, cmap="gray")
        else:
            ax.imshow(histo.data)
        ax.scatter(*control_points[:, ::-1].T)
        outname = os.path.join(p.general.outputdir, "init_control_points.png")
        plt.savefig(outname)
        logger.info(f"Created control-point overlay: {outname}")

    # Create an RBF warp field from the low-DOF estimates of the deformation
    ndpc = newdomain.get_physical_coordinates()
    displacements = histo.domain.get_physical_coordinates() - ndpc
    # displacements[:] = 0
    displacements /= 2
    invchain = newdomain.chain[-2:].inverse()
    displacements = invchain.map_vector(displacements, coords=ndpc)
    ddf = TField(displacements.reshape(*newdomain.shape, -1),
                 order=VOXEL_MAJOR, domain=newdomain[:, :-2])
    rbfdomain = Domain(control_points, offset=newdomain.offset[:],
                       transformations=newdomain.chain[:-2])
    warp = TxRbfDisplacementField(
        np.zeros((dof, 3)), domain=rbfdomain, name="warp", mode=NL_ABS)
    warp.fit(ddf)

    # Create an RBD deformation field such that it's components are parellel
    # and orthogonal to the slice. This is equivalent to inserting the warp
    # immediately after the embedding. This parametrisation makes parameter
    # couplings independent of the slide orientation, and provides a more
    # physically intuitive breakdown of the deformations.
    newdomain.chain = [resolution, centralise, embed,
                       warp, rotation, translation]
    logger.info(f"Created the reduced non-linear transformation.")

    # Create a snapshot of the best reduced non-linear alignment
    if visualise:
        res = mri.evaluate(newdomain)
        outname = os.path.join(p.general.outputdir, "init_nonlinear.png")
        res.snapshot(outname, overwrite=True)
        logger.info(f"Created a non-linear snapshot: {outname}")

    return newdomain, cog, normal


def prepare_chain(img):
    resolution, centralise, embed, warp, rotation, translation = \
        img.domain.chain
    rot3d = TxEulerAngles(0, 0, 0, order="zyx", mode="deg", name="rotation_3d")
    trans3d = TxTranslation(0, 0, 0, name="translation_3d")
    img.domain.chain = [resolution, centralise, embed, warp, rotation,
                        rot3d, translation, trans3d]


###############################################################################
#                                  STAGES                                     #
###############################################################################

def stage1(img, vol, cnf):
    """
    Stage 1 aims to optimise the scale of the brain slice photograph, as well
    as to find the best initial position and orientation of the slice in MR
    space.

    """
    # Initialise stage-specific parameters
    p = AttrMap(cnf)
    logger = logging.getLogger(p.logger)
    t = p.regparams.stage_1
    s = t.slab

    # Initialise grid search parameters (slab)

    # Slab parameter: Position
    s.normal = np.divide(s.normal, np.linalg.norm(s.normal))  # unit normal vect
    kpos, s.posstep = np.linspace(-s.thickness / 2., s.thickness / 2.,
                                  s.n_positions, endpoint=True, retstep=True)
    offset = np.asarray(s.offset)
    if offset.size == 1:
        offset = float(offset) * s.normal
    s.positions = offset + np.outer(kpos, s.normal)

    # Slab parameter: Orientation
    s.orientation = [0, 0, 0]  # no additional rotation at the start
    phis = np.meshgrid(*tuple(
        np.linspace(r0 - r / 2, r0 + r / 2, n, endpoint=True)
        for r0, r, n in zip(s.orientation, np.deg2rad(s.range),
                            s.n_orientations)))
    s.rotations = np.stack(phis, axis=-1).reshape(-1, 3)
    s.rotstep = np.divide(np.deg2rad(s.range), np.asarray(s.n_orientations) + 1)

    logger.info(f"Histology image centre: {s.centre + offset}")
    logger.info(f"Test positions ({len(s.positions)}) "
                f"[a.u.] (1 position per row): \n{s.positions}")
    logger.info(f"Test orientations ({len(s.rotations)}) [deg] "
                f"(1 orientation per row): \n{np.rad2deg(s.rotations)}")
    logger.info(f"Number of stage-1 iterations: {t.iterations}")
    n_regs_current = len(s.positions) * len(s.rotations)
    n_regs_total = int(t.iterations) * n_regs_current
    if t.n_cpu < 0:
        n_cpu = max(min(int(t.n_cpu) % mp.cpu_count() + 1, n_regs_current), 1)
    else:
        n_cpu = max(min(int(t.n_cpu), n_regs_current), 1)

    logger.info(f"Altogether {n_regs_total} registrations "
                f"remaining on {n_cpu} CPU core(s).")

    # Raise error if nothing to do
    if (len(s.positions) == 0) and (len(s.rotations) == 0):
        raise AssertionError("Missing initialisation in stage 1.")

    sigfilter = None
    lowest_cost = np.inf
    best_position = None
    best_orientation = None
    best_mapping = None
    best_signature = None
    states = tuple(zip(t.slice_scaling, t.volume_scaling,
                       t.slice_smoothing, t.volume_smoothing))

    # Initialise a listener process and a pool of workers in multi-CPU setting
    if n_cpu > 1:
        # Create message passing queue for exchanging log messages between the
        # processes. Note: this MUST be a Manager queue, otherwise the program
        # breaks down with segmentation fault.
        manager = mp.Manager()
        queue = manager.Queue()
        listener = mp.Process(target=listener_process, args=(queue, cnf))
        listener.start()
        # Note: forking leads to a deadlock, halting where the RBF
        # interpolation is carried out. I couldn't work out the cause, but
        # "spawn" works well here, which is also compatible with Windows and
        # Mac.
        ctx = mp.get_context("spawn")
        workers = ctx.Pool(
            n_cpu, initializer=stage1_worker_initialiser,
            initargs=(queue,), maxtasksperchild=1)

    # Use threading if only a single CPU is utilised
    else:
        import threading
        import queue
        queue = queue.Queue(-1)  # for exchanging log messages
        listener = threading.Thread(target=listener_process, args=(queue, cnf))
        listener.start()

    for state, (ssc, vsc, ssm, vsm) in enumerate(states):
        img.rescale(1. / ssc, copy=False)
        img_smooth = img.smooth(ssm, copy=True)
        vol.rescale(1. / vsc, copy=False)
        if vsm != 0:
            vol_smooth = vol.smooth(vsm, copy=True)
        else:
            vol_smooth = vol

        # Generate jobs for the workers
        jobs = get_stage1_jobs(
            img_smooth, vol_smooth, dict(p), s.positions, s.rotations,
            state, ssc, ssm, vsc, vsm, sigfilter)

        # It is necessary to force single-threaded computation to
        # avoid a performance drop when using multiple processes.
        if n_cpu > 1:
            general.set_single_threading()

        # Run stage 1
        # Using .map instead of .imap to report the number of filtered/executed
        # jobs in the logfile prior to the actual registrations taking place.
        if n_cpu > 1:
            # parallel_result = workers.map_async(stage1_worker, jobs).get()
            import threadpoolctl
            try:
                api = threadpoolctl.threadpool_info()[0]["user_api"]
            except:
                api = "openblas"
            with threadpoolctl.threadpool_limits(1, user_api=api):
                parallel_result = workers.map(stage1_worker, jobs)
        else:
            stage1_worker_initialiser(queue)
            parallel_result = map(stage1_worker, jobs)

        if state == len(states) - 1:
            state_costs, signatures = zip(*parallel_result)
            best = np.argmin(state_costs)
            tmpdir = os.path.join(p.general.outputdir, "stage1_temp")
            tgfile = os.path.join(tmpdir, signatures[best] + ".tg")
            best_mapping = tg = tirl.load(tgfile)
            lowest_cost = tg.metaparameters.get("cost")
            best_position = tg.metaparameters.get("pos")
            best_orientation = tg.metaparameters.get("rot")
            best_signature = signatures[best]
        else:
            # Only proceed with the computation for the top x % (at least 1)
            state_costs, signatures = zip(*parallel_result)
            best = np.argsort(state_costs)
            best = best[:int(max(int(len(best) * float(t.top)), 1))]
            sigfilter = np.asarray(signatures)[best]

        img.domain.chain = img_smooth.domain.chain

    # Terminate any existing parallel processes
    if n_cpu > 1:
        workers.close()
        workers.join()

    # Terminate the listener process or thread
    queue.put_nowait(None)
    listener.join()

    # Revert to full scale and assign the best parameters
    img.rescale(1)
    vol.rescale(1)
    assert best_mapping is not None
    img.domain.chain = best_mapping.elements

    # Report best stats
    logger.info("Stage 1 main iteration complete.")
    logger.info(f"Best position: {best_position} a.u.")
    logger.info(f"Best orientation: {np.rad2deg(best_orientation)} "
                f"[deg] (internal rotations: z->y->x)")
    logger.info(f"Final cost function value: {lowest_cost}")
    logger.info(f"Best signature: {best_signature}")

    t.iterations -= 1

    if (t.iterations == 0) and t.try_unconstrained:
        # Try an unconstrained optimisation at full resolution from the best
        # initial position, with the parameters reset to default. If the cost is
        # higher, proceed with the best bounded optimisation result.
        logger.info("Performing non-constrained optimisation from the best "
                    "initial position and orientation...")
        t.constrained = False
        backup_best_bounded_result = img.domain.chain.copy()
        set_parameters(img, dict(), p)
        c = get_chain(img)
        remove_bounds(img, p)
        c.tx_trans3d.parameters[:] = best_position.copy()
        c.tx_rot3d.parameters[:] = best_orientation.copy()
        cost = create_cost_object(vol, img, p)
        unc_cost = stage1_3d(img, vol, p, cost=cost, logger=logger)
        logger.info(f"Unconstrained optimisation final cost: {unc_cost}")
        if unc_cost > lowest_cost:
            logger.info("Unconstrained optimisation yielded higher cost. "
                        "Falling back on the best bounded optimisation "
                        "result.")
            img.domain.chain = backup_best_bounded_result
            stage_cost = lowest_cost
        else:
            logger.info("Unconstrained optimisation yielded lower cost. "
                        "Keeping the new parameters.")
            stage_cost = unc_cost

        c = get_chain(img)
        st1chain = [c.tx_rot3d, c.tx_trans3d]
        params = np.concatenate([tx.parameters[:] for tx in st1chain])
        logger.info(f"Stage 1 final parameters:\n{params}")

    # Decide on further iterations of stage 1
    # This condition reads as: "if there is stuff to iterate..."
    elif (t.iterations > 0) and \
            ((len(s.positions) > 1) or (len(s.rotations) > 1)):
        s.normal = \
            TxEulerAngles(*best_orientation, order="zyx", mode="rad")\
            .map(s.original_normal).ravel()
        offset = np.asarray(s.offset)
        if offset.size == 1:
            offset = float(offset) * s.normal
        s.centre = best_position - offset
        s.thickness = s.posstep * 2
        s.range = np.rad2deg(s.rotstep * 2)
        stage_cost = stage1(img, vol, p)

    else:
        stage_cost = lowest_cost

    if t.cleanup is True:
        try:
            import shutil
            shutil.rmtree(os.path.join(p.general.outputdir, "stage1_temp"))
        except Exception as exc:
            logger.error(exc.args)
            logger.warning("An error occurred while deleting temporary files: ")
        else:
            logger.info("Removed stage 1 temporary files.")
    else:
        logger.info("Keeping stage 1 temporary files.")

    # Re-enable multi-threaded computation in NumPy
    if n_cpu > 1:
        general.set_multi_threading()

    return stage_cost


def get_chain(img):
    # Get transformation handles
    tx_warp = img.domain.get_transformation("warp")
    tx_rot3d = img.domain.get_transformation("rotation_3d")
    tx_trans3d = img.domain.get_transformation("translation_3d")
    return NamedChain(tx_warp, tx_rot3d, tx_trans3d)


def set_parameters(img, initparams, cnf):
    """
    Assigns a set of parameters to a predefined transformation chain.
    The parameters are copied and modified in-place, hence the function has no
    return value and the values in the parameter source will not be changed
    even when the transformation chain gets updated during optimisation.

    :param img: input image with transformation chain
    :type img: TImage
    :param initparams: source of parameters
    :type initparams: dict
    :param cnf:
        all configurations (used to determine whether the scaling is
        anisotropic)
    :type cnf: dict or AttrMap

    """
    rad = np.deg2rad  # shorthand
    p = AttrMap(cnf)
    logger = logging.getLogger(p.logger)
    logger.debug("Setting the parameters of the transformation chain...")

    # Get transformation chain
    c = get_chain(img)

    r3z, r3y, r3x = [0, 0, 0]
    dtype = c.tx_rot3d.parameters.dtype
    c.tx_rot3d.parameters[:] = rad(np.asarray([r3z, r3y, r3x], dtype=dtype))

    t3x, t3y, t3z = [0, 0, 0]
    dtype = c.tx_trans3d.parameters.dtype
    c.tx_trans3d.parameters[:] = np.asarray([t3x, t3y, t3z], dtype=dtype)


def remove_bounds(img, cnf):
    """
    Removes the optimisation bounds on the transformations of a predefined
    transformation chain. The boundary values are change in-place, hence the
    function has no return value.

    :param img: input image with transformation chain
    :type img: TImage

    """
    p = AttrMap(cnf)
    logger = logging.getLogger(p.logger)
    logger.debug("Removing parameter bounds from the transformation chain...")

    # Get transformation chain
    c = get_chain(img)

    # Remove bounds
    c.tx_warp.parameters.set_bounds(None)
    c.tx_rot3d.parameters.set_bounds(None)
    c.tx_trans3d.parameters.set_bounds(None)


def update_bounds(img, deltas, cnf):
    """
    Updates the optimisation bounds on the transformations of a predefined
    transformation chain. The boundary values are change in-place, hence the
    function has no return value and the source of the boundary updates will
    not change even when the transformation chain is being optimised.

    :param img: input image with transformation chain
    :type img: TImage
    :param deltas: optimisation boundary updates
    :type deltas: dict
    :param cnf: all configurations (used to inform about isotropic scaling)
    :type cnf: dict or AttrMap

    """
    rad = np.deg2rad  # shorthand
    p = AttrMap(cnf)  # global parameters are problematic in multiprocessing
    logger = logging.getLogger(p.logger)
    logger.debug("Updating parameter bounds of the transformation chain...")

    # Get transformation chain
    c = get_chain(img)

    # Get parameter bound updates
    wlz = deltas.get("warp_lower_dz", None)
    wuz = deltas.get("warp_upper_dz", None)
    r3lz, r3ly, r3lx = deltas.get("rot3d_lower_delta", (None, None, None))
    r3uz, r3uy, r3ux = deltas.get("rot3d_upper_delta", (None, None, None))
    t3lx, t3ly, t3lz = deltas.get("trans3d_lower_delta", (None, None, None))
    t3ux, t3uy, t3uz = deltas.get("trans3d_upper_delta", (None, None, None))

    # Set new bounds
    if (wlz is not None) and (wuz is not None):
        warp_params = c.tx_warp.parameters[:]
        c.tx_warp.parameters.set_bounds(
            warp_params - float(wlz), warp_params + float(wuz))
    if (r3lz is not None) and (r3ly is not None) and (r3lx is not None) and \
            (r3uz is not None) and (r3uy is not None) and (r3ux is not None):
        rz, ry, rx = c.tx_rot3d.parameters[:]
        c.tx_rot3d.parameters.set_bounds(
            {0: (rz - rad(float(r3lz)), rz + rad(float(r3uz))),
             1: (ry - rad(float(r3ly)), ry + rad(float(r3uy))),
             2: (rx - rad(float(r3lx)), rx + rad(float(r3ux)))})
    if (t3lz is not None) and (t3ly is not None) and (t3lx is not None) and \
            (t3uz is not None) and (t3uy is not None) and (t3ux is not None):
        d3x, d3y, d3z = c.tx_trans3d.parameters[:]
        c.tx_trans3d.parameters.set_bounds(
            {0: (d3x - float(t3lx), d3x + float(t3ux)),
             1: (d3y - float(t3ly), d3y + float(t3uy)),
             2: (d3z - float(t3lz), d3z + float(t3uz))})


def create_cost_object(source, target, cnf):
    """
    Creates a script-wide uniform cost object based on the general cost
    function type setting.

    """
    p = AttrMap(cnf)

    if str(p.general.cost).lower() == "mind":
        return CostMIND(source, target, normalise=True, sigma=1.0,
                        truncate=1.5, kernel=MK_STAR)
    elif str(p.general.cost).lower() == "mi":
        return CostMI(source, target, normalise=True, bins=32)
    else:
        raise ValueError(f"Unrecognised cost function specification: "
                         f"{p.general.cost}")


def get_stage1_jobs(img, vol, cnf, positions, rotations, state,
                    ssc, ssm, vsc, vsm, sigfilter):

    p = AttrMap(cnf)
    logger = logging.getLogger(p.logger)

    # Dump images to make them accessible by parallel processes
    tmpdir = os.path.join(p.general.outputdir, "stage1_temp")
    try:
        os.makedirs(tmpdir)
    except Exception:
        pass
    imfile = os.path.join(tmpdir, "fixed.timg")
    volfile = os.path.join(tmpdir, "moving.timg")
    img.save(imfile, overwrite=True)
    vol.save(volfile, overwrite=True)
    vol.storage = SHMEM

    filtered = 0
    executed = 0
    for pos in positions:
        for rot in rotations:
            # Filter jobs based osn their signature (to reduce workload)
            if sigfilter is not None:
                if get_signature(pos, rot) in sigfilter:
                    j = imfile, volfile, cnf, state, pos, rot, \
                        ssc, ssm, vsc, vsm, vol
                    executed += 1
                    yield j
                else:
                    filtered += 1
            else:
                j = imfile, volfile, cnf, state, pos, rot, ssc, ssm, vsc, vsm, vol
                executed += 1
                yield j
    else:
        logger.info(f"Filtered {filtered} job(s).")
        logger.info(f"Executed {executed} job(s).")


def get_signature(pos, rot):
    import hashlib
    h = hashlib.md5()
    params = np.concatenate((pos, rot)).astype(ts.DEFAULT_FLOAT_TYPE)
    h.update(params.tobytes())
    return h.hexdigest()


def listener_process(queue, cnf):
    logger = general.create_logger(cnf)
    while True:
        try:
            record = queue.get()
            if record is None:  # tells the listener to quit
                break
            logger.handle(record)
        except Exception:
            import sys, traceback
            print("Error while logging from worker process:", file=sys.stderr)
            traceback.print_exc(file=sys.stderr)


def stage1_worker_initialiser(queue):
    """
    Initialises worker process to allow safe logging from multiple simultaneous
    workers.

    """
    import logging.handlers
    h = logging.handlers.QueueHandler(queue)
    name = f"Process-{mp.current_process().pid}"
    root = logging.getLogger(f"histology_to_volume.stage1.{name}")
    # Avoid duplicate logs on repeated initialisation of the same process
    root.handlers = []
    root.addHandler(h)
    root.setLevel(1)


def stage1_worker(job):
    """
    Job function for stage 1 worker process. The calls in this function are
    executed in parallel for multiple initialisations (position & orientation).

    """
    name = f"Process-{mp.current_process().pid}"
    # Load the process-specific logger instance
    logger = logging.getLogger(f"histology_to_volume.stage1.{name}")
    logger.debug(f"Worker started: {name}")

    imfile, volfile, cnf, state, pos, rot, ssc, ssm, vsc, vsm, _vol = job

    p = AttrMap(cnf)
    t = p.regparams.stage_1

    # Load images
    _img = tirl.load(imfile)
    _img.storage = MEM
    # warp, ix = _img.domain.get_transformation("warp", index=True)
    # assert all(
    #     t1 is t2 for t1, t2 in zip(warp.domain.chain, _img.domain[:ix].chain))
    # _vol = tirl.load(volfile)
    # _vol.storage = MEM

    # Name of the transformation chain dump on disk
    signature = get_signature(pos, rot)
    tgfile = os.path.join(os.path.dirname(imfile), signature + ".tg")

    # Initialise parameters: set to initial values, or load from previous run
    if state == 0:
        set_parameters(_img, dict(), p)
        c = get_chain(_img)
        c.tx_trans3d.parameters[:] = pos
        c.tx_rot3d.parameters[:] = rot
    else:
        # Load parameters that were previously optimised at the given position
        # and orientation, usually at a coarser resolution or with more
        # smoothing.
        tg = tirl.load(tgfile)
        chain = get_chain(_img)
        st1chain = [chain.tx_rot3d, chain.tx_trans3d]
        tgchain = [tg.elements[-3], tg.elements[-1]]
        for itx, tx in enumerate(st1chain):
            tx.parameters[:] = tgchain[itx].parameters[:]

    # Update bounds
    if t.constrained:
        update_bounds(_img, t.stage_1b, p)
    else:
        remove_bounds(_img, p)

    # Create cost object
    cost = create_cost_object(_vol, _img, p)

    # Run local 2D + 3D optimisation
    # proc = f"[Process {os.getpid()}]"
    drot = np.rad2deg(rot)
    logger.debug(f"Initial cost @ pos={pos} rot={drot} "
                 f"ssc={ssc}, ssm={ssm}, vsc={vsc}, vsm={vsm} "
                 f"(sig={signature}): {cost()}")
    _vol.evaluate(_img.domain).snapshot(
        tgfile.replace(".tg", "_init.png"), True)
    lcost = stage1_3d(_img, _vol, p, cost, logger)
    logger.debug(f"Updated cost after 3D optimisation "
                 f"@ pos={pos} rot={drot} "
                 f"ssc={ssc}, ssm={ssm}, vsc={vsc}, vsm={vsm} "
                 f"(sig={signature}): {lcost}")
    _vol.evaluate(_img.domain).snapshot(tgfile.replace(".tg", "_b.png"), True)

    # Dump optimised parameters for the next state or for the main process,
    # which will select the best parameters based on the lowest cost.
    tg = TransformationGroup(*_img.domain.chain)
    c = get_chain(_img)
    newpos = c.tx_trans3d.parameters[:]
    newrot = c.tx_rot3d.parameters[:]
    tg.metaparameters.update(pos=newpos, rot=newrot, cost=lcost)  # for main
    tg.save(tgfile, overwrite=True)

    # This resolves a memory leak that I spent half a day
    # finding on 20 July 2020. The leak is caused by the CostMIND object that
    # stores a copy of the images. The reference to this array must be first
    # deleted from the cost object, then from the _vol object.
    del tg  # this is not strictly necessary
    del cost._source
    del cost._target
    del cost._src
    del cost._trg
    del cost
    del _img, _vol
    # Direct invocation of the garbage collector is not necessary.
    # import gc
    # gc.collect()

    return lcost, signature


def stage1_3d(img, vol, cnf, cost, logger):
    """
    Stage 1a: optimises the 3D linear subchain.

    """
    # (vol.evaluate(img.domain) - img).preview()
    p = AttrMap(cnf)
    t = p.regparams.stage_1

    if t.constrained:
        update_bounds(img, t.stage_1b, p)
    else:
        remove_bounds(img, p)

    # Define optimiser
    c = get_chain(img)
    og3d = OptimisationGroup(c.tx_rot3d, c.tx_trans3d)
    method = OptMethods.LN_BOBYQA if t.constrained else OptMethods.LN_NEWUOA

    if NORMALISE:
        initial_step = t.opt_step
    else:
        lb, ub = og3d.get_bounds().T
        initial_step = absolute_step(lb, ub, t.opt_step)

    # Optimise parameters and update bounds
    xtol_abs = np.asarray(t.stage_1b.xtol_abs)
    OptNL(og3d, cost, method=method, xtol_abs=xtol_abs, logger=logger,
          step=initial_step, visualise=t.visualise, normalised=NORMALISE)()
    update_bounds(img, t.stage_1b, p)

    return cost()


def absolute_step(lb, ub, step):
    di = ub - lb
    di[~np.isfinite(di)] = 2
    if np.any(di <= 0):
        raise AssertionError("No space for optimisation.")
    return np.multiply(0.5 * di, step)


def calc_adaptive_control_points(n_new, n_old, cost, stparams, cnf):
    """
    Uses superpixel segmentation to generate an approximate number of tiles,
    distributed evenly and compactly across the ROI of the target image, as
    defined by a mask. The control points are expected to have a higher
    frequency around non-matching regions, as measured by the regional cost.

    :param n_new: number of new points
    :type n_new: int
    :param n_old: number of existing points
    :type n_old: int
    :param cost: cost function object
    :type cost: Cost
    :param stparams: stage-specific parameters
    :type stparams: Any
    :param cnf: all configurations (used to access the logger)
    :type cnf: dict or AttrMap

    :returns: (n_points, 2) voxel coordinate array of control points
    :rtype: np.ndarray

    """
    from sklearn.cluster import KMeans
    p = AttrMap(cnf)
    t = AttrMap(stparams)
    logger = logging.getLogger(p.logger)

    # Create cost map
    costmap = cost.costmap()
    if costmap.tsize > 1:
        costmap = costmap.tensors.reduce()  # L2-norm
    else:
        costmap = np.abs(costmap)

    # Generate control regions
    n_points = int(t.support.points)
    # mask = image.get_mask(
    #     TImage(cost._trg), scope=globals(), **t.support.mask)
    mask = np.asarray(cost._trg < 0.95 * cost._trg.max(), dtype=int)
    coords = np.vstack(np.where(mask > 0.5)).T
    km = KMeans(n_clusters=n_old+n_new, random_state=0)
    segmented = np.zeros_like(mask, dtype=int)
    labels = km.fit_predict(coords) + 1  # 0 is reserved for the bg
    segmented[mask > 0.5] = labels

    # Smooth the cost map to match the average support size of the ctrl points
    # Constrain the smoothing to the ROI
    area = np.count_nonzero(mask > 0.5)

    assert n_old + n_new <= n_points
    fwhm = 2 * np.sqrt(area / n_points / np.pi)
    sigma = fwhm / 2.355
    costmap.smooth(sigma, copy=False)
    costmap /= TImage(mask).smooth(sigma, copy=True)
    costmap.apply_mask(mask)  # hard masking
    costmap.data[~np.isfinite(costmap.data)] = 0
    logger.info(f"Smoothed cost function map by sigma = {sigma} px")

    # Sort the segments by their contribution to the cost
    cm = costmap.data.astype(np.float64)
    regions = regionprops(segmented, cm)
    regions = sorted(regions, key=lambda r: r.mean_intensity)[::-1]

    # Create control points
    centroids = [r.centroid for r in regions[:n_new]]
    # xi = np.stack(centroids[n_old:n_old + n_new], axis=0)
    xi = np.stack(centroids, axis=0)
    logger.info(f"Created {xi.shape[0]} new control points: \n{xi}")

    # Measure the average diameter of the segments
    mean_diameter = np.mean([r.equivalent_diameter for r in regions])

    # Measure the average distance between segment centroids
    edges = np.max(xi, axis=0) - np.min(xi, axis=0)
    edges = edges[np.nonzero(edges)]
    mean_distance = np.power(np.prod(edges) / len(regions), 1. / edges.size)

    # Report segmentation result
    logger.info(f"Mean segment diameter: {mean_diameter} px")
    logger.info(f"Mean segment distance: {mean_distance} px")

    # Return the voxel coordinates of the sorted control points
    return xi


def calc_km_control_points(n_new, n_old, img, stparams, cnf):
    """
    Uses k-means clustering to generate an exact number of tiles,
    distributed evenly and compactly across the ROI of the target image, as
    defined by a mask.

    :param n_new: number of new points
    :type n_new: int
    :param n_old: number of existing points
    :type n_old: int
    :param img: target image
    :type img: TImage
    :param stparams: stage-specific parameters
    :type stparams: Any
    :param cnf: all configurations
    :type cnf: dict or AttrMap

    :returns: (n_points, 2) voxel coordinate array of control points
    :rtype: np.ndarray

    """
    from sklearn.cluster import KMeans
    p = AttrMap(cnf)
    t = AttrMap(stparams)
    logger = logging.getLogger(p.logger)

    # Define ROI for the cost map
    tmp = img.copy().tensors.reduce()
    tmp.normalise()
    # mask = image.get_mask(
    #     tmp, scope=globals(), **t.support.mask)
    mask = np.asarray(tmp < 0.95 * tmp.max(), dtype=int)
    coords = np.vstack(np.where(mask > 0.5)).T
    n_points = int(t.support.points)
    km = KMeans(n_clusters=n_points, random_state=0)
    km.fit(coords)
    xi = km.cluster_centers_[n_old:n_old + n_new]
    logger.info(f"Created {xi.shape[0]} new control points: \n{xi}")

    # Measure the average distance between segment centroids
    edges = np.max(xi, axis=0) - np.min(xi, axis=0)
    edges = edges[np.nonzero(edges)]
    mean_distance = np.power(np.prod(edges) / xi.shape[0], 1. / edges.size)
    logger.info(f"Mean control point distance: {mean_distance} px")

    # Return the voxel coordinates of the sorted control points
    return xi


def _get_bbox(mask):
    """
    Get 2D bounding box from binary image mask.

    :param mask: binary image mask
    :type mask: np.ndarray

    :return:
        Bounding box parameters.
        xmin: left border
        xmax: right border
        ymin: bottom border
        ymax: top border
    :rtype: dict

    """
    # Ensure that the mask is binary
    mask[mask > 0] = 1
    sy, sx = mask.shape

    # Determine tight bounding box
    x_proj = np.max(mask, axis=0)
    y_proj = np.max(mask, axis=1)
    x_nonzero = np.nonzero(x_proj)
    xmin = np.min(x_nonzero)
    xmax = np.max(x_nonzero)
    y_nonzero = np.nonzero(y_proj)
    ymin = np.min(y_nonzero)
    ymax = np.max(y_nonzero)

    # Create loose bounding box
    rel_margin = 0.05
    x_margin = int(rel_margin * (xmax - xmin))
    xmin = max(0, xmin - x_margin)
    xmax = min(sx, xmax + x_margin)
    y_margin = int(rel_margin * (ymax - ymin))
    ymin = max(0, ymin - y_margin)
    ymax = min(sy, ymax + y_margin)

    # Return bounding box parameters
    return {"xmin": xmin,
            "xmax": xmax,
            "ymin": ymin,
            "ymax": ymax}


def calc_halton_points(n_new, n_old, cost, stparams, cnf):
    """
    Returns an array of voxel coordinates, designated as "Halton support
    points". These pseudorandomly distributed points provide a uniform coverage
    of the ROI.

    """
    p = AttrMap(cnf)
    logger = logging.getLogger(p.logger)

    from skimage.filters import threshold_otsu
    if cost.target.tsize > 1:
        tmp = cost.target.copy().tensors.reduce()
        flatimg = tmp.data
    else:
        flatimg = cost.target.tensors[0].data
    th = threshold_otsu(flatimg)
    flatimg = np.squeeze(np.where(flatimg < th, 0, 1))
    bbox = _get_bbox(flatimg)
    points = _halton(stparams["support"]["points"], 0, **bbox)[:, ::-1]
    xi = points[n_old:n_old + n_new]
    logger.info(f"Created {xi.shape[0]} new control points: \n{xi}")

    return xi


def _halton(n_points, seed, **box):
    """
    Computes the location of Halton points for a given number of points and
    2D bounding box.

    :param n_points:
        Number of control points.
    :type n_points: int
    :param box:
        Bounding box definition by keyword arguments.

    :returns:
        (n_points, 2) table of Halton points, with xy layout (image convention).

    """
    import ghalton

    # Verify inputs
    assert isinstance(n_points, (int, np.integer))
    assert n_points > 0
    xmin = box.get("xmin", 0)
    xmax = box.get("xmax", xmin + 1)
    ymin = box.get("ymin", 0)
    ymax = box.get("ymax", ymin + 1)
    for boundval in [xmin, xmax, ymin, ymax]:
        assert isinstance(boundval, (int, float, np.floating, np.integer))
    assert xmax > xmin
    assert ymax > ymin

    # Create Halton grid
    sequencer = ghalton.Halton(2)
    if seed is not None:
        sequencer.seed(seed)
    points = sequencer.get(n_points)
    points = np.asarray(points)
    points[:, 0] = xmin + (xmax - xmin) * points[:, 0]
    points[:, 1] = ymin + (ymax - ymin) * points[:, 1]
    return points


def save_control_point_image(photo, cpoints, filename, cnf):
    p = AttrMap(cnf)
    logger = logging.getLogger(p.logger)
    # Creating the figure requires a window management system, so check this
    # first, and change the backend accordingly if it cannot be found.
    from tirl.utils import to_img
    if not os.getenv("DISPLAY") or not ts.ENABLE_VISUALISATION:
        import matplotlib
        matplotlib.use("agg")
    else:
        import matplotlib
        matplotlib.use(ts.MPL_BACKEND)
    import matplotlib.pyplot as plt

    # Create figure with photo and control points
    fig = plt.figure()
    ax = fig.add_subplot(111)
    order = photo.order
    photo.order = VOXEL_MAJOR
    if photo.tshape == ():
        ax.imshow(to_img(photo.data), cmap='gray', zorder=1)
    else:
        ax.imshow(to_img(photo.data), zorder=1)
    photo.order = order
    y, x = cpoints.T
    ax.scatter(x, y, c="magenta", zorder=2)

    # Save figure
    dot_file = os.path.join(filename)
    try:
        fig.savefig(dot_file)
    except Exception as exc:
        logger.error(exc.args)
        logger.info("Control point image could not be saved due to an error.")
    else:
        logger.info(f"Saved control point figure: {filename}")


def create_warp_from_points(img, points, t, cnf):
    """
    Creates empty warp field based on the voxel coordinates of the support
    points on the fixed image domain.

    """
    p = AttrMap(cnf)
    dim = len(t.vectorder)
    oldwarp = img.domain.chain["warp"]
    newdomain = Domain(points, internal=img.domain.internal,
                       external=oldwarp.domain.external)
    field = TField(domain=newdomain, tshape=(dim,), order=TENSOR_MAJOR,
                   dtype=ts.DEFAULT_FLOAT_TYPE, fill_value=0)

    if t.export.control_points:
        timestamp = datetime.now().strftime("%Y%m%d_%H%M%S_%f")
        fn = os.path.join(p.general.outputdir,
                          f"control_points_{timestamp}.png")
        np.savetxt(fn.replace(".png", ".txt"), points)
        save_control_point_image(img, points, fn, p)

    # Define bounds
    if t.vectorder == "xyz":
        lbxy = float(t.lower_dxy) * np.ones((2, field.domain.numel))
        lbz = float(t.lower_dz) * np.ones((1, field.domain.numel))
        ubxy = float(t.upper_dxy) * np.ones((2, field.domain.numel))
        ubz = float(t.upper_dz) * np.ones((1, field.domain.numel))
        lb = -np.concatenate((lbxy, lbz), axis=0).ravel()
        ub = np.concatenate((ubxy, ubz), axis=0).ravel()
    elif t.vectorder == "xy":
        lb = -float(t.lower_dxy) * np.ones((2, field.domain.numel))
        ub = float(t.upper_dxy) * np.ones((2, field.domain.numel))
    elif t.vectorder == "z":
        lb = -float(t.lower_dz) * np.ones(field.domain.numel)
        ub = float(t.upper_dz) * np.ones(field.domain.numel)
    else:
        raise ValueError(f"Invalid vector order: {t.vectorder}")

    # Create warp
    tx_warp = TxRbfDisplacementField(
        field, bounds=(lb, ub), name="warp", invertible=False,
        mode=NL_ABS, vectorder=t.vectorder, dense_shape=img.domain.shape)
    tx_warp.interpolator.model = t.model

    return tx_warp


def stage4(img, vol, cnf):
    """
    Stage 4 - performs regularised free-form deformation in all three spatial
    dimensions to achieve an optimal alignment with the 3D image.

    :param img: 2D image with registration chain
    :type img: TImage
    :param vol: 3D image
    :type vol: TImage
    :param cnf: all configurations
    :type cnf: dict or AttrMap

    :returns: 2D image with optimised off-plane curvatures.
    :rtype: TImage

    """
    p = AttrMap(cnf)
    t = p.regparams.stage_4
    logger = logging.getLogger(p.logger)

    # Smoothing iteration
    stage_cost = []

    states = tuple(zip(t.slice_scaling, t.volume_scaling,
                       t.slice_smoothing, t.volume_smoothing))

    for sm_no, (ssc, vsc, ssm, vsm) in enumerate(states):
        logger.info(f"Smoothing: ssc={ssc}, ssm={ssm}px, vsc={vsc}, "
                    f"vsm={vsm}vx.")
        img.rescale(1. / ssc, copy=False)
        img.storage = MEM
        img_smooth = img.smooth(ssm, copy=True)
        vol.rescale(1. / vsc, copy=False)
        vol.storage = MEM
        if vsm != 0:
            vol_smooth = vol.smooth(vsm, copy=True)
        else:
            vol_smooth = vol
        cost = create_cost_object(vol_smooth, img_smooth, p)
        logger.info(f"Initial cost: {cost()}")

        # Obtain nonlinear transformation
        warp = img_smooth.domain.chain["warp"]

        # How many support points?
        # Read from configuration file.
        if isinstance(t.support.points, int):
            n_points = t.support.points
            calculate_points = True
        # Read from external file
        elif isinstance(t.support.points, str) and \
                os.path.isfile(t.support.points):
            xi = np.loadtxt(t.support.points)
            n_points = xi.shape[0]
            calculate_points = False
        # Invalid input
        else:
            raise ValueError("Invalid support point specification.")

        # How many points should be optimised at once?
        optsize = min(int(t.optsize), n_points)

        # Optimise all points for any subsequent iteration, as little benefit
        # is anticipated from repeating the whole optimisation sequence.
        if sm_no > 0:
            optsize = n_points

        # ---> POINT OPTIMISATION SEQUENCE
        for n_old in range(0, n_points, optsize):

            # --------------------- CREATE NEW WARP -------------------- #

            # Create new incremental point set
            n_new = min(n_points - n_old, optsize)
            if calculate_points:
                if str(t.support.mode).lower() == "halton":
                    xi_new = calc_halton_points(n_new, n_old, cost, t, p)
                elif str(t.support.mode).lower() == "km":
                    xi_new = calc_km_control_points(n_new, n_old, img, t, p)
                elif str(t.support.mode).lower() == "cost_adaptive":
                    xi_new = calc_adaptive_control_points(
                        n_new, n_old, cost, t, p)
                else:
                    raise ValueError(f"Unrecognised support point scheme: "
                                     f"{t.support.mode}")
            else:
                xi_new = xi[n_old:n_old + n_new]

            # Concatenate old and new points
            if n_old > 0:
                xi_old = warp.domain.base_coordinates()
                xi_current = np.concatenate((xi_old, xi_new), axis=0)
            else:
                xi_current = xi_new

            # Create new transformation with the old and the new points
            newwarp = create_warp_from_points(img, xi_current, t, p)

            # If a transformation was previously optimised, fit the parameters
            # of the new transformation to recreate the displacement field.
            # This carries forward the information from previous optimisations.
            if warp is not None:
                # Note: std() is necessary, as we potentially change from a
                # unidirectional field to a 3D vector field here.
                newwarp.fit(warp.std(3).absolute().field())

            warp = newwarp

            # Update transformation parameter bounds
            params = warp.parameters[:]
            params = params.reshape(len(t.vectorder), -1)
            lb = params.copy()
            ub = params.copy()
            if t.vectorder == "xyz":
                lb[:2] -= float(t.lower_dxy)
                lb[2] -= float(t.lower_dz)
                ub[:2] += float(t.upper_dxy)
                ub[2] += float(t.upper_dz)
            elif t.vectorder == "xy":
                lb[:] -= float(t.lower_dxy)
                ub[:] += float(t.upper_dxy)
            elif t.vectorder == "z":
                lb[:] -= float(t.lower_dz)
                ub[:] += float(t.upper_dz)

            if NORMALISE:
                initial_step = t.opt_step
            else:
                initial_step = absolute_step(lb.ravel(), ub.ravel(), t.opt_step)
            warp.parameters.set_bounds(lb.ravel(), ub.ravel())
            warp.parameters.unlock()

            # Add warp to the chain
            _, ix = img_smooth.domain.get_transformation("warp", index=True)
            img_smooth.domain.chain[ix] = warp

            # Recreate cost object, because the chain was modified
            cost = create_cost_object(vol_smooth, img_smooth, p)

            # --------------------- OPTIMISE NEW WARP -------------------- #

            # Regularisation
            if t.regweight > 0:
                # reg = RegBendingEnergy(warp, weight=t.regweight)
                reg = NormTxRegulariser(warp, order=2, weight=t.regweight)
                cost_items = [cost, reg]
            else:
                cost_items = [cost]

            # Optimisation
            logger.debug(f"Optimising the following control points:"
                         f"\n{warp.domain.get_voxel_coordinates()}")
            OptNL(warp, *cost_items, method=OptMethods.LN_BOBYQA,
                  step=initial_step, xtol_abs=t.xtol_abs, logger=logger,
                  visualise=t.visualise, normalised=NORMALISE)()

        # <--- POINT OPTIMISATION SEQUENCE

        # Record the final cost at the current smoothing level
        if sm_no + 1 == len(states):
            stage_cost.append(cost())
            logger.info(f"Final cost @ [ssm={ssm}px, vsm={vsm}vx]: "
                        f"{stage_cost[-1]}")

        # Copy optimised transformations before creating a new image from
        # the original. [This may no longer be necessary.]
        img.domain = img_smooth.domain

    img.rescale(1)
    vol.rescale(1)

    return stage_cost[-1]


def histology_to_volume(args):
    """
    Main program code. Controls program flow, handles command-line arguments.

    """
    cnf = args.config
    if os.path.isfile(cnf):
        with open(cnf, "r") as fp:
            cnf = dict(json.load(fp))
    else:
        raise FileNotFoundError(f"The provided configuration file "
                                f"does not exist: {args.config}")

    # Override histology file paths in the configuration file with
    # those provided on the command line.
    if args.histo and os.path.isfile(args.histo):
        cnf["histology"]["file"] = args.histo
    if args.volume and os.path.isfile(args.volume):
        cnf["volume"]["file"] = args.volume
    if args.out:
        cnf["general"]["outputdir"] = args.out

    # Override verbose option in configurations
    cnf["general"]["verbose"] = args.verbose

    # Run registration script
    run(cnf)


def create_cli(parser):
    """
    Sets up the CLI argument parser instance.

    """
    parser.add_argument("--histo", metavar="image",
                        help="Histology slide", default=None, type=str,
                        required=False)
    parser.add_argument("--volume", metavar="image",
                        help="Volumetric image", default=None, type=str,
                        required=False)
    parser.add_argument("--out", metavar="dir",
                        help="Output directory", default=None, type=str,
                        required=False)
    parser.add_argument("--config", metavar="cnf_file.json",
                        help="configuration file", default=None, type=str,
                        required=True)
    parser.add_argument("-v", "--verbose", default=False, action="store_true",
                        help="Print status messages to the command line",
                        required=False)

    return parser


def main(*args):
    """ Main program code. """

    parser = argparse.ArgumentParser(
        prog="histology_to_volume",
        description="Fine-tunes the alignment of a registered histology slide "
                    "in a volume.")
    parser = create_cli(parser)

    if args:
        histology_to_volume(parser.parse_args(args))
    else:
        parser.print_help()


if __name__ == "__main__":
    main(*sys.argv[1:])
