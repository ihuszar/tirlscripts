#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
from numbers import Integral
from tirl.domain import Domain
from tirl.transformations.embedding import TxEmbed
from tirl.transformations.euler import TxEulerAngles
from tirl.transformations.rotation import TxRotation2D
from tirl.transformations.translation import TxTranslation


class MultiDomain(object):
    """
    MultiDomain is a set of indexed Domains.

    """
    def __init__(self, *domains, indices=None):
        if not domains:
            raise ValueError("There must be at least one Domain specified.")
        if indices is None:
            indices = np.arange(len(domains))
        else:
            indices = np.asarray(indices).ravel()
        assert indices.size == len(domains), "Indices must match the Domains."
        self._domains = dict()
        for ix, d in zip(indices, domains):
            ix = int(ix)
            if not hasattr(d, "__domain__"):
                raise TypeError(f"Invalid Domain instance at index {ix}: {d}")
            if not isinstance(ix, Integral):
                raise TypeError(f"Expected an integer index, got {ix}")
            self._domains.update({ix: d})

    def __repr__(self):
        cls = self.__class__.__name__
        n = self.n
        x, y, z = self.centre
        centre = f"({x:.02f}, {y:.02f}, {z:.02f})"
        return f"{cls}[{n} slices, middle_slice={self.middle}, centre={centre}]"

    def __getitem__(self, item):
        return self.domains.__getitem__(item)

    def __setitem__(self, key, value):
        self.domains.__setitem__(key, value)

    def __iter__(self):
        return self.domains.__iter__()

    @property
    def middle(self):
        return int(np.asarray(self.indices)[self.n // 2])

    @property
    def centre(self):
        return self.domains[self.middle].pcentre()

    @property
    def domains(self):
        return self._domains

    @property
    def n(self):
        return len(self.domains.keys())

    @property
    def indices(self):
        return tuple(np.sort(tuple(self.domains.keys())))

    def apply(self, timg):
        """
        Evaluates a TImage on the sorted domains of the MultiDomain.

        :param timg: TImage instance to be evaluated
        :type timg: TImage

        :returns: evaluated TImage for each domain
        :rtype: tuple[TImage] or TImage

        """
        indices = sorted(tuple(self.domains.keys()))
        result = []
        for ix in indices:
            result.append(timg.evaluate(self.domains[ix]))
        else:
            if len(result) > 1:
                return tuple(result)
            else:
                return result[0]


class Grating(MultiDomain):
    """
    Grating is a parametric subclass of MultiDomain that defines a set of
    indexed parallel Domains based on offset, slice gap, and rotations.
    The Domains are planar and are embedded in 3D space.

    """
    def __init__(self, n, gap, shape, offset=(0, 0, 0), rotation=(0, 0, 0),
                 indices=None):
        """
        Initialisation of Grating.

        :param n: number of slices in the grating
        :type n: int
        :param gap: distance between consecutive slices
        :type gap: float
        :param shape: shape of the slices; defined globally or individually
        :type shape: tuple or list or np.ndarray
        :param offset: centre of the 0th slice
        :type offset: tuple or list or np.ndarray
        :param rotation: uniform x-y-z rotations of the slices (in degrees)
        :type rotation: tuple or list or np.ndarray
        :param indices:
            slice indices (starting from 0) if the slices are discontinuous
        :type indices: tuple or list or np.ndarray or None

        """
        assert isinstance(n, Integral) and n > 0, \
            "The number of slices must be a positive integer."
        shape = np.atleast_2d(shape)
        assert np.all(shape > 0), "Invalid shape encountered."
        assert shape.shape[0] in (1, n), "Invalid shape specification."

        # Create grating offset transformation
        self._transformations = dict()
        offset = np.asarray(offset).ravel()
        assert offset.size == 3
        self.transformations["offset"] = TxTranslation(
            *offset, name="grating-offset")
        # Create 3D rotation
        rotation = np.asarray(rotation).ravel()
        self.transformations["rotation"] = TxEulerAngles(
            *rotation, order="xyz", mode="deg", name="grating-rotation")
        # Prepare slice indices
        if indices is None:
            indices = np.arange(n)
        else:
            indices = np.asarray(indices).ravel()
        assert np.all(indices) >= 0, "Slice indices must be non-negative."
        # Calculate slice offsets
        self.gap = gap
        slice_offsets = indices.reshape(-1, 1) * self.gap * self.normal

        # Create domains
        domains = []
        for i in range(n):
            if shape.shape[0] > 1:
                current_shape = shape[i]
            else:
                current_shape = shape[0]
            domain = Domain(*current_shape, name=f"{i}")
            pc = np.asarray(domain.pcentre())
            domain.external = [
                TxTranslation(*(-pc), name="centralise"),
                TxRotation2D(0, mode="deg", name="slice-rotation"),  # in-plane
                TxEmbed(1, name="embed"),
                self.transformations["rotation"],            # grating rotation
                self.transformations["offset"],                # grating offset
                TxTranslation(*slice_offsets[i], name="slice-offset")
            ]
            domains.append(domain)

        # Invoke superclass initialisation
        super(Grating, self).__init__(*domains, indices=indices)

    @property
    def transformations(self):
        return self._transformations

    @property
    def offset(self):
        return self.transformations["offset"].parameters[:]

    @offset.setter
    def offset(self, d0):
        d0 = np.asarray(d0).ravel()
        assert d0.size == 3
        self.transformations["offset"].parameters[:] = d0

    @property
    def rotation(self):
        return self.transformations["rotation"].angles

    @rotation.setter
    def rotation(self, angles):
        angles = np.asarray(angles)
        assert angles.size == 3, "Expected 3 values for XYZ rotation angles."
        self.transformations["rotation"].angles = angles

    @property
    def gap(self):
        return self._gap

    @gap.setter
    def gap(self, d):
        d = float(d)
        assert d >= 0, "The slice gap must be non-negative."
        self._gap = d
        # Update the slice offsets
        if hasattr(self, "domains"):
            for ix, d in self.domains.items():
                d.external["slice-offset"].parameters[:] = \
                    ix * self._gap * self.normal

    @property
    def normal(self):
        return self.transformations["rotation"].map([0, 0, 1])

    def __repr__(self):
        cls = self.__class__.__name__
        x, y, z = tuple(self.offset)
        offset = f"({x:.02f}, {y:.02f}, {z:.02f})"
        rx, ry, rz = self.rotation
        angles = f"({rx:.02f}, {ry:.02f}, {rz:.02f})"
        cx, cy, cz = self.centre
        centre = f"({cx:.02f}, {cy:.02f}, {cz:.02f})"
        return f"{cls}[{self.n} parallel slices, gap={self.gap:.03f}, " \
               f"offset={offset}, rotation={angles}, middle={self.middle}, " \
               f"centre={centre}]"
