#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2021 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import re
import sys
import json
import logging
import pathlib
import argparse
import numpy as np
import pandas as pd
from attrdict import AttrMap
import multiprocessing as mp
import matplotlib.pyplot as plt
from scipy.stats import linregress
from collections import namedtuple
from sklearn.decomposition import PCA


# TIRL IMPORTS

import tirl
import tirl.settings as ts
from tirl.constants import *
from tirl.chain import Chain
from tirl.timage import TImage
from multidomain import Grating
from composite_slice import CompositeSlice
from tirl.transformations.scale import TxScale
from tirl.transformations.affine import TxAffine
from tirl.transformations.embedding import TxEmbed
from tirl.transformations.axisangle import TxAxisAngle
from tirl.transformations.rotation import TxRotation3D
from tirl.transformations.translation import TxTranslation
from tirl.interpolation.scipyinterpolator import ScipyNearestNeighbours


# DEFINITIONS

np.set_printoptions(suppress=True, precision=4)
Init = namedtuple("Init", ["gap", "offset", "robust_normal", "normals",
                           "robust_rotation", "rotations", "cog"])
LABELS = {
    "right": 1,
    "left": 2,
    "whole": (1, 2)
}


# IMPLEMENTATION

def main(cnf):
    """
    Main program code.

    :param cnf: main program configurations
    :type cnf: dict

    """
    # Prepare output directory
    prepare_output_dir(cnf)

    # Prepare logger
    global logger
    logger = create_logger(cnf)
    cmd = " ".join(sys.argv)
    logger.info(f"The program started with the command: {cmd}")

    # Collate inputs
    inputs = get_inputs(cnf)

    # Load the images and create composite slices
    n_cpu = int(cnf["n_cpu"])
    n_cpu = n_cpu % mp.cpu_count() + 1 if n_cpu < 0 else n_cpu % mp.cpu_count()
    load_all_to_composite(inputs, n_cpu=n_cpu)
    reduce_composite_chains(inputs)
    initialise_composites_opt(inputs)

    # Do the jobs
    g = initialise_grating(inputs, cnf)
    print(g)
    mrislices = g.apply(inputs["volume"])
    for i, s in enumerate(mrislices):
        outname = os.path.join(cnf["outputdir"], f"mrislice-{i:02d}")
        s.save(outname + ".timg", True)
        s.snapshot(outname + ".png", True)
    exit()
    optimise_grating(g)
    finetune_slices(g)


def initialise_grating(inputs, cnf):
    """
    Creates a multi-slice object (a grating) with the following parameters
    and metaparameters:

    Parameters:
        grating offset
        grating rotation
        interslice gap

    Metaparameters:
        number of slices
        slice indices

    """
    ins = AttrMap(inputs)

    logger.info("Initialising the grating...")
    select = Ellipsis
    n = len(ins.composites)
    shapes = [c.domain.shape for c in ins.composites]
    init = get_initial_estimates(inputs, select)
    g = Grating(
        n, gap=init.gap, shape=shapes, offset=init.offset,
        rotation=init.robust_rotation, indices=ins.slices.Slice.unique())

    # Set in-plane slice rotations relative to a reference slice
    slice_rotations, index = get_slice_rotations(inputs, index=None)
    for sr, si in zip(slice_rotations, g.indices):
        g[si].external["slice-rotation"].angle = sr
    logger.info(f"In-plane rotation of the slices w.r.t. slice {index}:"
                f"\n{slice_rotations}")

    return g


def get_initial_estimates(inputs, select=Ellipsis):
    """
    Based on a select subset of the registered composite slices, this method
    estimates the parameters of a Grating, which will be used as initial values
    for the optimisation.

    """
    ins = AttrMap(inputs)
    composites = np.asarray(ins.composites)[select]
    indices = ins.slices.Slice.unique()[select]
    z0 = np.asarray([0, 0, -1])
    rotations = []
    normals = []
    for c in composites:
        rot = c.posttx["rotation_3d"]
        rotations.append(rot.euler(mode="deg", order="xyz"))
        normals.append(rot.map(z0))
    else:
        rotations = np.vstack(rotations)
        normals = np.vstack(normals)

    # Estimate the robust orientation iteratively by rejecting outliers
    select = np.full(shape=normals.shape[0], fill_value=True)
    robust_normal = z0
    for i in range(10):
        robust_normal = np.mean(normals[select], axis=0).ravel()
        robust_normal /= np.linalg.norm(robust_normal)
        print(i, robust_normal)
        diff = np.linalg.norm(normals - robust_normal, axis=-1)
        select[diff > 2 * np.median(diff[select])] = False
    else:
        pass
        # fig = plt.figure()
        # ax = fig.add_subplot(projection='3d')
        # ax.scatter(*normals.T, c="blue", marker="o")
        # ax.scatter(*n, c="red", marker="x")
        # plt.show()

    # Find the rotation angles corresponding to the robust normal
    angle = np.arccos(np.dot(z0, robust_normal.reshape(-1, 1)))
    axis = np.cross(robust_normal, z0)
    axis /= np.linalg.norm(axis)
    robust_rotation = TxAxisAngle(angle, axis, mode="rad")\
        .euler(mode="deg", order="xyz")

    # Centre of gravity
    centres = [np.asarray(c.posttx.map(c.domain.pcentre())) for c in composites]
    cog = np.mean(np.vstack(centres), axis=0)
    print(f"Centre of gravity: {cog}")
    # Slice gap
    x = np.subtract(indices, 1)
    y = [float(np.dot(robust_normal, c.reshape(-1, 1))) for c in centres]
    cog_pos = np.dot(robust_normal, np.asarray(cog).reshape(-1, 1))
    result = linregress(x, y)
    gap = result.slope
    grating_offset = \
        np.add(cog_pos, np.subtract(result.intercept, cog_pos) * robust_normal)

    # Keep slicing gap non-negative
    if gap < 0:
        gap *= -1
        robust_normal *= -1
        normals *= -1
    print(f"Effective slice gap: {gap:.04} +/- {result.stderr:.04} mm")
    print(f"Correlation coefficient: {result.rvalue:.04}")

    print(f"0th slice centre: {grating_offset} mm")

    return Init(gap, grating_offset, robust_normal, normals,
                robust_rotation, rotations, cog)


def estimate_slicing_gap(inputs, normal, cog, select=Ellipsis):
    """
    Based on a select subset of the registered slices, this method calculates
    the empirical interslice gap.

    :param normal: robust estimate of the slices normal
    :type normal: np.ndarray or list or tuple
    :param select: subset of the slices to consider during the estimation
    :type select: slice or Ellipsis or tuple or np.ndarray

    :returns: estimated interslice gap
    :rtype: float

    """
    ins = AttrMap(inputs)
    normal = np.asarray(normal).ravel()
    composites = np.asarray(ins.composites)[select]
    indices = ins.slices.Slice.unique()[select]

    x = np.subtract(indices, 1)
    centres = [np.asarray(c.posttx.map(c.domain.pcentre())) for c in composites]
    y = [float(np.dot(normal, c.reshape(-1, 1))) for c in centres]
    result = linregress(x, y)
    gap = np.abs(result.slope)
    print(f"Effective slice gap: {gap:.04} +/- {result.stderr:.04} mm")
    print(f"Correlation coefficient: {result.rvalue:.04}")
    cog_pos = np.dot(normal, np.asarray(cog).reshape(-1, 1))
    grating_offset = \
        np.add(cog_pos, np.subtract(result.intercept, cog_pos) * normal)
    print(f"0th slice centre: {grating_offset} mm")
    return gap, grating_offset


def optimise_grating(g):
    """
    Optimises the parameters of a Grating instance with respect to a volume.

    :param g: Grating instance
    :type g: Grating

    """
    pass


def finetune_slices(g):
    """
    Fine-tunes the alignment of the slices in a Grating instance with respect
    to a volume.

    :param g: Grating instance
    :type g: Grating

    """
    pass


def reduce_composite_chains(inputs):
    """
    Reduces the original stage-3 transformation chain into
    a simpler but identical rigid mapping.

    The function has no return value, the chains are updated in-place.

    """
    ins = AttrMap(inputs)
    logger.info("Reducing chains...")

    for cmp in ins.composites:
        if not cmp.posttx:
            continue
        vc = cmp.domain.vcentre()
        chain = Chain(cmp.posttx)
        embed = TxEmbed(1, name="embed")
        rot2d = np.eye(3)
        rot2d[:2, :2] = chain["rotation_2d"].matrix
        rot3d = chain["rotation_3d"].matrix
        rot3d = TxRotation3D(rot3d @ rot2d, name="rotation_3d")
        p0 = np.asarray(cmp.domain.pcentre())
        centralise = TxTranslation(*(-p0), name="centralise")
        b0 = np.eye(3)
        b1 = chain.map_vector(b0, coords=[vc, vc, vc])[:3, :3].T
        scale = TxScale(*np.linalg.norm(b1, axis=0)[:2], name="scale")
        trans3d = TxTranslation(*chain.map(p0), name="translation_3d")
        if np.linalg.det(b1) < 0:
            flipmat = np.asarray([[1, 0, 0], [0, -1, 0]])
            flip = TxAffine(flipmat, name="flip_y")
            cmp.posttx = Chain(centralise, flip, scale, embed, rot3d, trans3d)
        else:
            cmp.posttx = Chain(centralise, scale, embed, rot3d, trans3d)


def initialise_composites_opt(inputs):
    """
    Initialises the internal transformation parameters of all composite slices.

    """
    ins = AttrMap(inputs)
    from tirl.cost.mind import CostMSD
    from tirl.optimisation.optnl import OptNL
    from tirl.optimisation.optgroup import OptimisationGroup

    for i, cmp in enumerate(ins.composites):
        # No need to optimise anything if there is only a single component
        if len(cmp.images) == 1:
            cmp.parameters[:] = 0
            return
        # Optimise the internal position and orientation of each component
        capture = ins.slices.Capture.unique()[i]
        logger.info(f"Assembling composite {capture}...")
        cmpdomain = cmp.domain[:]
        cmpdomain.chain = [tx for tx in cmp.posttx if tx.name != "flip_y"]
        vlabels = ins.vmask.evaluate(cmpdomain)
        # vlabels.preview()
        vlabels.domain.reset(internal=True)
        coords = cmpdomain.get_voxel_coordinates()
        for ix, spart in enumerate(cmp.images):
            vlabel = LABELS[spart.name.lower()]
            vlc = coords[np.in1d(vlabels.data.ravel(), vlabel)]
            vcentre = np.mean(vlc, axis=0)
            offset = np.subtract(vcentre, cmpdomain.vcentre())
            vpart = vlabels.copy()
            vpart.storage = MEM
            vpart.data[vpart.data != vlabel] = 0
            smask = TImage(spart.tmask())
            smask.storage = MEM
            rot = smask.domain.chain["cmp-rotation2d"]
            trans = smask.domain.chain["cmp-translation2d"]
            rot.parameters[:] = 0
            rot.parameters.set_bounds(*np.deg2rad((-45, 45)))
            trans.parameters[:] += offset
            trans_low = trans.parameters[:] - 10
            trans_high = trans.parameters[:] + 10
            trans.parameters.set_lower_bounds(trans_low)
            trans.parameters.set_upper_bounds(trans_high)
            og = OptimisationGroup(trans, rot)
            cost = CostMSD(vpart, smask)
            OptNL(og, cost, method="LN_BOBYQA", xtol_abs=[0.01, 0.1, 0.1],
                  step=0.5, visualise=False)()
            cmp.parameters[ix, :] = og.parameters
        # else:
        #     cmp().preview()


def initialise_composites_pca(inputs, init):
    """
    This method used to estimate the internal transformation parameters of the
    composites based on the centre of gravity and principal axes. The method
    was largely inaccurate where one or more of the components were not very
    well aligned to MRI, hence the method was superseded by
    initialise_composites_opt, which is using a constrained 2D rigid
    optimisation to find the best internal parameters.

    """
    ins = AttrMap(inputs)

    for cmp in ins.composites:
        cmpimg = cmp()
        # cmpimg.preview()
        cmpdomain = cmpimg.domain
        # Discard slice flip
        cmpdomain.chain = [tx for tx in cmpdomain.chain if tx.name != "flip_y"]
        vlabels = ins.vmask.evaluate(cmpdomain)
        vlabels.preview()
        coords = cmpdomain.get_voxel_coordinates()
        pca = PCA(n_components=2, random_state=0)

        for ix, part in enumerate(cmp.images):
            print(part.name)
            part.tmask().preview()
            vlabel = LABELS[part.name.lower()]
            vlc = coords[np.in1d(vlabels.data.ravel(), vlabel)]
            vcentre = np.mean(vlc, axis=0)
            slc = coords[part.mask.ravel() > 0.5]
            scentre = np.mean(slc, axis=0)
            offset = np.subtract(vcentre, cmpdomain.vcentre())
            pca.fit(vlc - vcentre)
            vbasis = pca.components_.T
            print(vbasis)
            pca.fit(slc - scentre)
            sbasis = pca.components_.T
            print(sbasis)
            sbasis = vbasis / np.abs(vbasis) * np.abs(sbasis)  # orientation
            rotation = vbasis @ np.linalg.inv(sbasis)
            angle = np.angle(rotation[0, 0] + 1j * rotation[1, 0], deg=True)
            cmp.parameters[ix] = [*offset, angle]
        else:
            print(cmp.parameters)
            cmp().preview()


def load_all_to_composite(inputs, n_cpu=None):
    """
    Load all input images and create composite slices.

    """
    ins = AttrMap(inputs)

    # Load input images into composite slices
    logger.info("Creating composite slices from the input images...")
    slice_indices = np.atleast_1d(ins.slices.Slice.unique())
    if n_cpu is None:
        n_cpu = int(min(mp.cpu_count(), slice_indices.size))
    n_cpu = int(min(n_cpu, mp.cpu_count(), slice_indices.size))
    if n_cpu > 1:
        from functools import partial
        loader = partial(load_from_record, ins.slices)
        with mp.Pool(processes=n_cpu) as pool:
            inputs["composites"] = pool.map(loader, slice_indices)
            pool.close()
            pool.join()
    else:
        inputs["composites"] = \
            [load_from_record(ins.slices, ix) for ix in slice_indices]


def get_slice_centres(inputs):
    """
    Returns the voxel coordinates of the slice centres (estimated from
    binary masks).

    """
    logger.info("Calculating slice centres...")
    ins = AttrMap(inputs)
    scentres = dict()
    for im in ins.composites:
        coords = im().domain.get_voxel_coordinates()[im.mask.ravel() > 0]
        scentres.append(np.mean(coords, axis=0))
    else:
        return scentres


def get_volume_centre(inputs):
    """
    Returns the physical centre of the image volume.

    """
    logger.info("Calculating the centre of the volume...")
    return inputs["volume"].domain.pcentre()


def get_slice_rotations(inputs, index=None):
    """
    Determines the in-plane rotation of the slices relative the the slice with
    specified index. If the index is not give, the median index will be used by
    default.

    """
    logger.info("Measuring the in-plane rotations of the slices...")
    ins = AttrMap(inputs)
    slice_indices = ins.slices.Slice.unique()
    index = median_element(slice_indices) if index is None else index
    assert index in slice_indices, f"No slice was found with the index {index}."

    gammas = []
    for ix, sl in zip(slice_indices, ins.composites):
        rx, ry, rz = sl.posttx["rotation_3d"].euler(order="xyz", mode="deg")
        gammas.append(rz)
    else:
        ref = gammas[np.where(slice_indices == index)[0][0]]
        gammas = np.mod(np.subtract(gammas, ref), 360)
        gammas = np.where(gammas > 180, gammas - 360, gammas)

    return gammas, index


def median_element(arr):
    arr = np.sort(np.asarray(arr))
    return arr.flat[np.abs(arr - np.median(arr)).argmin()]


def prepare_output_dir(cnf):
    """
    Creates the full path to the output directory if it does not exist yet.
    If the output directory is not specified, the current directory will be
    used.

    :param cnf: main program configurations
    :type cnf: dict

    """
    outdir = cnf["outputdir"]
    if not outdir:
        outdir = os.getcwd()
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
    cnf["outputdir"] = outdir


def create_logger(cnf):
    """
    Creates a logger that sends log messages to the standard output and
    into a file.

    """
    p = AttrMap(cnf)
    logger_name = "tirl.scripts.oxdbb.zoo.multislice_to_volume"
    logger = logging.getLogger(logger_name)
    logger.handlers = []  # delete any existing handlers to avoid duplicate logs
    logger.setLevel(1)
    formatter = logging.Formatter(
        fmt='%(asctime)s Process-%(process)d %(levelname)s (%(lineno)d) '
            '- %(message)s',
        datefmt='[%Y-%m-%d %H:%M:%S]')

    # Make sure that the logfile can be created
    logfile = cnf["logfile"]
    if not logfile:
        logfile = os.path.join(p.outputdir, "logfile.log")
    else:
        logdir = os.path.dirname(logfile)
        if not os.path.isdir(logdir):
            os.makedirs(logdir)

    # Redirect all logs of interest to the logfile
    fh = logging.FileHandler(logfile, mode="a", delay=False)
    if isinstance(p.loglevel, str):
        level = getattr(logging, p.loglevel.upper())
    elif isinstance(p.loglevel, (int, float)):
        level = int(p.loglevel)
    elif not p.loglevel:
        level = logging.INFO
    else:
        raise TypeError(f"Invalid logging level: {p.loglevel}")
    fh.setLevel(level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Redirect only warnings/errors to the standard output, unless "verbose"
    ch = logging.StreamHandler()
    if p.verbose:
        ch.setLevel(level)
    else:
        ch.setLevel(logging.WARNING)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


def get_inputs(cnf):
    """
    Manages the inputs of the program.

    :param cnf: main program configurations
    :type cnf: dict

    """
    p = AttrMap(cnf)

    # Load slices
    logger.info("Loading slices...")
    slices = choose_data(pd.read_csv(p.slices))
    # Save the filtered inputs as a CSV file
    datafile = os.path.join(p.outputdir, "filtered_input_data.csv")
    slices.to_csv(datafile)
    logger.info(f"Exported the filtered input data to: {datafile}")

    # Load volume
    logger.info("Loading the volume...")
    volume = TImage(p.volume, external="sform")
    logger.info(f"Loaded the volume: {volume}")
    # Load volume labels (enforce nearest-neighbour interpolation)
    logger.info("Loading the volume mask...")
    vmask = TImage(p.vmask, external="sform")
    logger.info(f"Loaded the volume mask: {vmask}")
    vmask.interpolator = ScipyNearestNeighbours()

    return dict(slices=slices, volume=volume, vmask=vmask)


def choose_data(table):
    """
    Export a subset of the input table that will be used in the registration.
    Modify this function to choose different inputs.

    :param table: input table (slice database)
    :type table: pd.DataFrame

    :returns: reduced table
    :rtype: pd.DataFrame

    """
    # Some slices were captured more than once, and even for the same capture
    # there can be multiple registration attempts. Furthermore, several slices
    # were devided into left and right parts, and registered separately.

    # The following rules are applied to choose a single instance of each slice:
    # 1/ latest capture
    # 2/ latest registration
    # 3/ from left/right the one with the lower normalised registration cost

    def mtime(x):
        return pathlib.Path(x).stat().st_mtime

    def regcost(x):
        regdir = os.path.dirname(x)
        with open(os.path.join(regdir, "logfile.log"), "r") as fp:
            logs = "".join(line for line in fp)
            try:
                cost = float(re.search("(STAGE 1 cost: )(.+)", logs).group(2))
            except Exception as exc:
                cost = np.inf
        return cost

    filtered = []
    for slice_index, slice_group in table.groupby("Slice"):
        latest = slice_group.SliceOriginal.apply(mtime).argmax()
        latest_capture = slice_group.iloc[latest].Capture
        capture_group = slice_group[slice_group.Capture == latest_capture]
        best = capture_group.SliceRegistered.apply(regcost).argmin()
        best_registration = capture_group.iloc[best].Registration
        parts = table[(table.Slice == slice_index) &
                      (table.Capture == latest_capture) &
                      (table.Registration == best_registration)]
        filtered.append(parts)
    else:
        filtered = pd.concat(filtered)
        costs = filtered.SliceRegistered.apply(regcost)
        filtered["PreRegistrationCost"] = costs
        return filtered


def load_from_record(table, slice_index):
    """
    Loads an image file or a TImage file, depending on the extension,
    and returns a TImage object.

    :param table: input data sheet
    :type table: pd.DataFrame
    :param slice_index: slice index
    :type slice_index: int

    :returns: image as TImage
    :rtype: TImage

    """
    # Sort components by the registration cost (lowest to highest)
    components = table[table["Slice"] == slice_index]
    components = components.sort_values("PreRegistrationCost")
    source = components.SliceOriginal.unique().tolist()
    source = "\n".join(f for f in source).rstrip("\n")
    logger.info(f"Loading slice No. {slice_index} "
                f"from the following input(s):\n{source}")

    reg = tirl.load(components.SliceRegistered.iloc[0])
    chain = reg.domain.all_tx()

    images = []
    quickdir = cnf["quickload"]
    if quickdir:
        for index, cmp in components.iterrows():
            fname = cmp.Capture + "_" + cmp.Registration + "_" + cmp.Part
            img = tirl.load(os.path.join(quickdir, fname + ".timg"))
            img.name = cmp.Part
            images.append(img)
    else:
        for index, cmp in components.iterrows():
            img = TImage(cmp.SliceOriginal, storage=HDD, name=cmp.Part)
            img.mask = TImage(cmp.SliceMask, storage=HDD)
            img.resize(*reg.vshape)
            img.name = cmp.Part  # needed because the resizing
            images.append(img)

    return CompositeSlice(
        *images, mode="blend", params=None,
        domain=images[0].domain[:0, :0], transformations=chain)


def create_parser():
    """
    Creates argument parser object.

    """
    parser = argparse.ArgumentParser(
        prog="multislice_to_volume",
        description="Registers parallel sections to a volume.")
    parser.add_argument(
        "--config", metavar="<*.json>", type=str, required=True,
        help="Configuration file.")
    parser.add_argument(
        "--verbose", action="store_true", default=False, required=False,
        help="Logs to STDOUT.")
    return parser


if __name__ == "__main__":
    parser = create_parser()
    if len(sys.argv) > 1:
        args = parser.parse_args()
        with open(args.config, "r") as fp:
            cnf = dict(json.load(fp))
        if args.verbose:
            cnf["verbose"] = args.verbose
        main(cnf)
    else:
        parser.print_help()
