#!/usr/bin/env python

"""
Fine-tuning the slice alignment for BigMac

The first-pass single-slice registrations are visually accurate, but a few
neighbouring slices overlap with each other or leave gaps inbetween.
An adjustment is needed to ensure that all brain voxels have histology/PLI
mapped onto them.

The proposed algorithm for fine-tuning is the following:

1/  Take the current transformation parameter estimates as a starting point.
2/  Measure the distribution of slice orientation based on all slices.
3/  Fix the orientation of the slices to the expected value of the orientations.
4/  Starting from the "known" slice gap, measure the effective constant slice
    gap that best fits the current estimates.
5/  Define the expected position and orientation for each slice.
6/  Define a regularisation object for each slice based on L2 distance from the
    expected values.
7/  Determine the regularisation weight empirically on a few slices.
8/  Re-register the slices starting from the initial estimates, using the
    regularisation objects.
9/  If needed, iterate the algorithm until gaps/overlaps are observed.
10/ For quality check, report the distribution of slice gaps and plot the
    orientation as a function of slice number.

"""


import os
import re
import tirl
import numpy as np
import scipy.stats
import matplotlib
matplotlib.use("qt5agg")
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from tirl.transformations.axisangle import TxAxisAngle


np.set_printoptions(suppress=True, precision=4)
inc = slice(30, None)


def main(slices, indices=None, visualise=True):
    """
    Main program code.

    :param slices: list of registered slices ()
    :type slices: tuple or list
    :param indices: list of slice indices
    :type indices: tuple or list or NoneType

    """
    # Load registered slices
    imgs = [tirl.load(sl) for sl in slices]
    n_imgs = len(imgs)
    indices = tuple(range(n_imgs)) if indices is None else indices

    # Obtain robust estimate of the slice normal
    n = robust_normal(imgs)
    expected_angles = get_euler_angles(n, [0, 0, -1], order="xyz")
    print(f"Robust slice rotation ({n_imgs} slices): {expected_angles} [deg]")
    print(f"Robust slice normal: {n}")

    # Determine the expected slice positions
    gap, offset = slice_gap(imgs, indices, normal=n)
    # gap = 0.35
    expected_positions = []
    for i in range(len(indices)):
        centre = imgs[i].domain.pcentre()
        proj = np.dot(centre, n)
        dist = offset + (indices[i] - 1) * gap - proj
        epos = centre + dist * n
        expected_positions.append(epos)
        print(indices[i], dist)
    else:
        expected_positions = np.vstack(expected_positions)
    print("Expected slice positions:")
    for ix, pos in zip(indices, expected_positions):
        print(ix, pos)

    if visualise:
        # Slice position plot (to visualise slice gap trends)
        slice_position_plot(imgs, indices, n, expected_positions)
        # Slice orientation plot (to visualise rotation trends)
        slice_orientation_plot(imgs, indices, expected_angles)


def get_rotation(domain):
    rot2d = np.eye(3)
    rot2d[:2, :2] = domain.chain["rotation_2d"].matrix
    rot3d = domain.chain["rotation_3d"].matrix
    rot = rot3d @ rot2d
    rot[:, 1] *= -1
    return rot


def not_so_robust_normal(imgs):
    imgs = imgs[inc]
    z0 = np.asarray([0, 0, 1]).reshape(-1, 1)
    normals = np.stack(
        [(get_rotation(img.domain) @ z0).ravel() for img in imgs], axis=0)
    normals = np.transpose(normals.T / np.linalg.norm(normals.T, axis=0))
    n = np.mean(normals, axis=0).ravel()
    n /= np.linalg.norm(n)
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    ax.scatter(*normals.T, c="blue", marker="o")
    ax.scatter(*n, c="red", marker="x")
    plt.show()
    return n


def robust_normal(imgs):
    imgs = imgs[inc]
    z0 = np.asarray([0, 0, 1]).reshape(-1, 1)
    normals = np.stack(
        [(get_rotation(img.domain) @ z0).ravel() for img in imgs], axis=0)
    normals = np.transpose(normals.T / np.linalg.norm(normals.T, axis=0))
    select = np.full(shape=normals.shape[0], fill_value=True)
    n = 0
    for i in range(10):
        n = np.mean(normals[select], axis=0).ravel()
        n /= np.linalg.norm(n)
        print(i, n)
        diff = np.linalg.norm(normals - n, axis=-1)
        select[diff > 2 * np.median(diff[select])] = False
    else:
        fig = plt.figure()
        ax = fig.add_subplot(projection='3d')
        ax.scatter(*normals.T, c="blue", marker="o")
        ax.scatter(*n, c="red", marker="x")
        plt.show()
        return n


def slice_gap(imgs, indices, normal):
    """
    Returns the empirical slice gap, considering a predefined number of
    slices and actual registered slices.

    :param imgs: registered slices
    :type imgs: tuple or list
    :param indices: indices of the registered slices
    :type indices: tuple[int] or list[int] or np.ndarray
    :param normal: robust estimate of the slices normal
    :type normal: np.ndarray or list or tuple

    :returns: estimated slice gap
    :rtype: float

    """
    x = np.subtract(indices, 1)
    centres = [np.asarray(img.domain.pcentre()) for img in imgs]
    y = [float(np.dot(c.reshape(1, -1), normal.reshape(-1, 1)))
         for c in centres]
    result = scipy.stats.linregress(x[inc], y[inc])
    print(f"0th slice offset: "
          f"{result.intercept:.04} +/- {result.intercept_stderr:.04} mm")
    print(f"Effective slice gap: {result.slope:.04} +/- {result.stderr:.04} mm")
    print(f"Correlation coefficient: {result.rvalue:.04}")
    return result.slope, result.intercept


def get_euler_angles(v1, v2, mode="deg", order="xyz"):
    v1 = np.asarray(v1).ravel()
    v1 = v1 / np.linalg.norm(v1)
    v2 = np.asarray(v2).ravel()
    v2 = v2 / np.linalg.norm(v2)
    axis = np.cross(v1, v2)
    sine = np.linalg.norm(axis)
    cosine = np.dot(v1.reshape(1, -1), v2.reshape(-1, 1))
    tx = TxAxisAngle(float(np.arctan2(sine, cosine)), axis, mode="rad")
    return tx.euler(mode=mode, order=order)


def slice_position_plot(imgs, indices, normal, expected_positions):
    """
    Plots slice positions along the robust slice normal as a function of
    slice number. The trend line shows the expected slice positions.

    Use this plot to identify trends in interslice gaps.

    :param imgs: registered slices
    :type imgs: tuple or list
    :param indices: indices of the registered slices
    :type indices: tuple[int] or list[int]
    :param normal: robust slice normal
    :type normal: np.ndarray

    """
    fig, axes = plt.subplots(1, 2)
    fig.suptitle("Slice positions along the robust slice normal")
    axes = axes.ravel()

    # Plot 1: Slice position vs slice index + expected positions
    axes[0].set_xlabel("Slice index")
    axes[0].set_ylabel("Slice position [mm]")
    centres = [np.asarray(img.domain.pcentre()) for img in imgs]
    y = [float(np.dot(c.reshape(1, -1), normal.reshape(-1, 1)))
         for c in centres]
    axes[0].plot(indices, y, "o", label="measured")
    epos = [float(np.dot(pos.reshape(1, -1), normal.reshape(-1, 1)))
            for pos in expected_positions]
    axes[0].plot(indices, epos, "x", label="expected")
    axes[0].xaxis.set_major_locator(MaxNLocator(integer=True))
    axes[0].legend()

    # Plot 2: Error in slice position vs slice index (relative to expectation)
    axes[1].bar(indices, height=np.subtract(y, epos))
    axes[1].set_xlabel("Slice index")
    axes[1].set_ylabel("Offset from expectation [mm]")
    axes[0].xaxis.set_major_locator(MaxNLocator(integer=True))

    # Show plots
    fig.tight_layout()
    fig.canvas.draw()
    plt.show()


def slice_orientation_plot(imgs, indices, expected_angles):
    """
    Plots the three intrinsic rotation angles (x, y, z) as a function of slice
    index.

    Use this plot to identify trends in slice rotations and to spot outliers.

    """

    fig, axes = plt.subplots(3, 2)
    fig.suptitle("Intrinsic slice rotations (relative to coronal) "
                 "as a function of slice index")
    axes = axes.T
    expected_angles = np.asarray(expected_angles)
    expected_angles[0] -= 90
    z0 = np.asarray([0, 0, 1]).reshape(-1, 1)
    normals = np.stack(
        [(get_rotation(img.domain) @ z0).ravel() for img in imgs], axis=0)
    angles = np.stack(
        [get_euler_angles(normal, [0, -1, 0]) for normal in normals], axis=0)

    # Generate plots
    for rotax, label in zip(range(3), "xyz"):
        # Euler angle plots
        axes[0, rotax].plot(
            indices, angles[:, rotax], "o", label="measured")
        axes[0, rotax].plot(
            indices, [expected_angles[rotax]] * len(indices),
            "x", label="expected")
        axes[0, rotax].legend()
        axes[0, rotax].set_xlabel("Slice index")
        axes[0, rotax].set_ylabel(f"Rotation about {label} [deg]")
        axes[0, rotax].xaxis.set_major_locator(MaxNLocator(integer=True))
        # Error charts
        errors = np.subtract(angles[:, rotax], expected_angles[rotax])
        vmax = np.max(np.abs(errors)) * 1.25
        axes[1, rotax].bar(indices, errors)
        axes[1, rotax].set_ylim(-vmax, vmax)

    fig.canvas.draw()
    plt.show()


if __name__ == "__main__":

    # Get slices
    slice_descriptor = "/mnt/iris/BigMac/testslices2.txt"
    with open(slice_descriptor, "r") as f:
        slices = f.readlines()
        slices = [sl.rstrip("\n") for sl in slices]
    indices = [int(re.findall(r"(?<=P)\d{3}", sl, re.DOTALL)[0])
               for sl in slices]
    print("Slices:")
    for ix, sl in zip(indices, slices):
        print(f"{ix}, {sl}")

    # Run the script
    main(slices, indices=indices, visualise=True)
