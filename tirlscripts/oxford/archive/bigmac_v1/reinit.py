#!/usr/bin/env python

import os
import numpy as np
import matplotlib
matplotlib.use("qt5agg")
import matplotlib.pyplot as plt


import tirl
from tirl.timage import TImage
from tirl.transformations.scale import TxScale
from tirl.transformations.affine import TxAffine
from tirl.transformations.embedding import TxEmbed
from tirl.transformations.euler import TxEulerAngles
from tirl.transformations.axisangle import TxAxisAngle
from tirl.transformations.rotation import TxRotation3D
from tirl.transformations.translation import TxTranslation
from tirl.transformations.rbf_displacement import TxRbfDisplacementField


np.set_printoptions(suppress=True, precision=4)
EXPECTED_POSITION = \
    [40.5436, 40.5615, 40.1217]  # mm
EXPECTED_ROTATION = \
    [86.93596598243211, -1.478503301989157, 1.4014856620212701]  # deg


def main(sl, mfile, outdir):
    """ Main program code. """

    img = tirl.load(sl)
    mri = TImage(mfile, external="sform")

    domain = reduce_chain(img.domain)
    img.snapshot(os.path.join(outdir, "pli.png"), True)
    mri.evaluate(domain).snapshot(os.path.join(outdir, "affine.png"), True)

    # Initialise the slice with the expected position and orientations
    dpos = np.subtract(EXPECTED_POSITION, domain[:-1].pcentre())
    domain.chain["translation_3d"].parameters[:] += dpos
    assert np.allclose(
        domain[:-1].pcentre(), tuple(EXPECTED_POSITION), atol=1e-3)
    x, y, z = domain.chain["rotation_3d"].euler(order="xyz", mode="deg")
    print(x, y, z)
    ex, ey, ez = EXPECTED_ROTATION
    domain.chain[-3] = TxEulerAngles(ex, ey, z, mode="deg", order="xyz")
    res = mri.evaluate(domain[:-1])
    res.snapshot(os.path.join(outdir, "expected_rigid.png"), True)

    print("Done.")


def reduce_chain(domain):

    newdomain = domain[:, :0]
    # Resolution scaling
    embed, ix = domain.get_external_transformation("embed", index=True)
    newdomain.chain = [tx for tx in domain.chain[:ix + 1] if tx.name
                       not in ("rotation_2d", "translation_2d")]
    # 3D rotation
    rot2d = np.eye(3)
    rot2d[:2, :2] = domain.chain["rotation_2d"].matrix
    rot3d = domain.chain["rotation_3d"].matrix
    newdomain.chain.append(TxRotation3D(rot3d @ rot2d, name="rotation_3d"))
    # 3D translation
    cog = np.subtract(domain[:-1].pcentre(), newdomain.pcentre())
    newdomain.chain.append(TxTranslation(*cog, name="translation_3d"))
    # 3D affine
    newdomain.chain.append(domain.chain["affine_3d"].copy())

    return newdomain


if __name__ == "__main__":
    outdir = "/mnt/knox/bigmac/realign/P072x"
    sl = "/mnt/iris/BigMac/Microscopy/PLI/Anterior/P072x/Reg2MRI/2_stage2.timg"
    mfile = "/mnt/iris/BigMac/MRI/Postmortem/struct/data/" \
            "struct_bias_restore.nii.gz"
    main(sl, mfile, outdir)
