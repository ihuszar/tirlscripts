#!/usr/bin/env python

import os
import vtk
import tirl
import numpy as np
from glob import glob
from mayavi import mlab
from tirl.timage import TImage
from scipy.spatial import Delaunay
from vtk.util.numpy_support import numpy_to_vtk


def main():
    """ Main program code. """

    # Visualise straight slices
    show_scene("original")
    show_scene("parallel")


def draw_slice(dom, mri=None):

    if hasattr(dom, "__timage__"):
        dom = dom.domain

    vc = dom.get_voxel_coordinates()
    tri = Delaunay(vc[:, [1, 0]])
    pc = dom.get_physical_coordinates()
    if mri is not None:
        pc = mri.domain.map_physical_coordinates(pc)  # MRI voxel coordinates
    tmesh = mlab.triangular_mesh(*pc.T, tri.simplices, colormap="gray")

    tmesh.mlab_source.scalars = np.zeros(dom.shape).ravel()
    return tmesh

    # grid = vtk.vtkImageData()
    # grid.SetDimensions(*dom.shape[::-1], 1)
    # vtkarr = numpy_to_vtk(
    #     np.flip(data.swapaxes(0, 1), axis=(0, 1))
    #         .reshape((-1, 3), order='F'))
    # vtkarr.SetName('Image')
    # grid.GetPointData().AddArray(vtkarr)
    # grid.GetPointData().SetActiveScalars('Image')
    #
    # vtex = vtk.vtkTexture()
    # vtex.SetInputDataObject(grid)
    # vtex.Update()
    #
    # tmesh.actor.mapper.scalar_visibility = False
    # tmesh.actor.enable_texture = True
    # tmesh.actor.tcoord_generator_mode = 'plane'
    # tmesh.actor.actor.texture = vtex
    #
    # return tmesh


def show_mri(mri):

    mri.normalise()
    src = mlab.pipeline.scalar_field(mri.data)
    src.update_image_data = True

    voi = mlab.pipeline.extract_grid(src)
    voi.trait_set()
    surf = mlab.pipeline.iso_surface(
        voi, contours=[0.1], color=(0.5, 0.5, 0.5))

    return surf


def show_scene(args):
    """ Visualising straight slices """

    if args == "original":
        with open("/mnt/knox/bigmac/multislice-ismrm/oldreglist.txt", "r") as fp:
            samples = fp.readlines()
    elif args == "parallel":
        with open("/mnt/knox/bigmac/multislice-ismrm/newreglist.txt", "r") as fp:
            samples = fp.readlines()

    mlab.figure(1, bgcolor=(0.7, 0.7, 0.7), fgcolor=(0.5, 0.5, 0.5))
    mlab.plot3d([105, 105], [-20, 259], [88, 88], color=(1, 1, 0.5))

    # Show MRI isosurface
    mri = TImage("/Users/inhuszar/BigMac/MRI/Postmortem/struct/data/"
                 "struct_bias_restore.nii.gz", external="sform")
    show_mri(mri)

    # Display slices
    # for sample in samples[::5]:
    #     draw_slice(tirl.load(sample.rstrip("\n")), mri=mri)

    mlab.view(0, 0)
    mlab.show()


if __name__ == "__main__":
    main()
