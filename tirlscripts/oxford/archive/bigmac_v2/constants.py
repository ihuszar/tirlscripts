#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

from types import SimpleNamespace
from collections import namedtuple


# DEFINITIONS

ChainTemplate = namedtuple("ChainTemplate", [
    "c_cent", "c_isoscale", "c_scale", "c_rot", "c_offset", "c_affine",
    "s_cent", "s_isoscale", "s_scale", "s_rot", "s_offset", "s_affine",
    "g_embed", "s_warp", "s_distance", "g_rot", "g_trans", "g_warp"
])

STAGES = SimpleNamespace(**{
    "rigid2d": dict(
        script="rigid2d.py", tag="rigid2d", descr="2D rigid registration"),
    "rigid3d": dict(
        script="rigid3d.py", tag="rigid3d", descr="3D rigid registration"),
    "warp"   : dict(
        script="warp.py", tag="warp", descr="non-linear registration")
})

DEFAULT_SCHEDULER = None
DEFAULT_QUEUE = "short.q"

LOGFORMAT = "%(asctime)s Process-%(process)d %(levelname)s (%(lineno)d) " \
            "- %(message)s"

DATEFORMAT = "[%Y-%m-%d %H:%M:%S]"