#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import numpy as np
from itertools import count
from tirl.chain import Chain
from tirl.domain import Domain
from tirl.transformations.affine import TxAffine
from tirl.transformations.embedding import TxEmbed
from tirl.transformations.euler import TxEulerAngles
from tirl.transformations.rotation import TxRotation2D
from tirl.transformations.scale import TxScale, TxIsoScale
from tirl.transformations.translation import TxTranslation


# DEFINITIONS

STACK_SPECIFIC = 0
SLICE_SPECIFIC = 1
COMPONENT_SPECIFIC = 2
SHAREGROUPS = (STACK_SPECIFIC, SLICE_SPECIFIC, COMPONENT_SPECIFIC)


# IMPLEMENTATION

class MultiDomain(object):
    """
    Indexed set of Domain objects.

    """
    MultiDomainCount = count()

    def __new__(cls, *args, **kwargs):
        obj = super().__new__(cls)
        instance_no = next(cls.MultiDomainCount)
        obj.name = kwargs.get("name", f"{cls.__name__}-{instance_no}")
        return obj

    def __init__(self, *domains, indices=None, name=None):
        """
        Initialisation of MultiDomain.

        :param domains: Input Domain objects
        :type domains: Domain
        :param indices:
            Indices of the input domains. If None (default), indices are
            automatically assigned as 0..N, where N is the number of input
            domains. Compound indexing is supported.
        :type indices: None or Iterable[int]

        """
        super(MultiDomain, self).__init__()
        self._domains = dict()

        if indices is None:
            iterator = enumerate(domains)
        else:
            l_ind = len(indices)
            l_dom = len(domains)
            if l_ind == l_dom:
                if len(indices) == len(set(indices)):
                    iterator = zip(indices, domains)
                else:
                    raise AssertionError("Indices must be unique.")
            else:
                raise AssertionError(
                    f"Invalid number of inputs: "
                    f"indices: {l_ind} vs. domains {l_dom}")

        for ix, dom in iterator:
            if hasattr(dom, "__domain__"):
                self._domains.update({ix: dom})
            else:
                raise TypeError(f"Input No. {ix} is not a valid domain: {dom}")

        # Replace default name if something is specified in the inputs
        if name is not None:
            self.name = name

    @property
    def n(self):
        return len(self._domains.items())

    @property
    def domains(self):
        """ Read-only property. """
        return self._domains

    def apply(self, timg):
        result = dict()
        for ix, dom in self.domains.items():
            result.update({ix: timg.evaluate(dom)})
        else:
            return result

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, namestr):
        self._name = str(namestr)


class MCSliceStack(MultiDomain):
    """
    MCStack: Multi-component slice stack.

    An indexed collection of 2D slices, where each slice may consist of one or
    more domains of variable shape. Individual domains in a MCSliceStack
    are referred to as "components". This class defines a shared hierarchical
    transformation chain for its components:
        - component-specific transformations (different for each component)
        - slice-specific transformations (shared between the components of a
          single slice)
        - stack-specific transformations (shared between all components of all
          slices)

    The prototypical use case is the registration of serial histological
    sections to volumetric imaging data, where the individual sections may have
    topological inconsistencies relative to the volumetric data.

    For example, in a series of brain sections, some hemispherical sections may
    be separated by either a large gap, or have overlapping segments. In both
    cases, hemispheres are individually consistent with the volumetric data,
    hence they can be registered accurately without large local displacements,
    but the slice as a whole cannot. A slice with partially independent
    components can eliminate the need for such large local displacements,
    allowing the use of a simpler non-linear transformation model that is
    easier and therefore quicker to optimise.

    """

    def __new__(cls, *args, **kwargs):
        obj = super().__new__(cls, *args, **kwargs)
        return obj

    def __init__(self, images, slice_indices=None, slice_metadata=None,
                 chain=None, sharegroups=0, name=None, copy=True):
        """
        :param images:
            Input TImages, TFields, or Domains. All must be 2D. If a template
            chain is specified, all external transformations of the input
            domains will be replaced by copies of this chain.
        :type images: Iterable[TImage,TField,Domain]
        :param slice_indices:
            Integer slice index for each input Domain. If None, Domains will
            be assigned 0..N in the input order, where N is the number of input
            domains - 1. Negative indices are also supported.
        :type slice_indices: Iterable[int] or None
        :param slice_metadata: a dictionary of additional slice metadata that
            need to be indexed.
        :type slice_metadata: Iterable[dict]
        :param chain:
            Chain template for the domains. All transformations in the template
            are copied before they are assigned to the domains in the stack.
        :type chain: Chain or None
        :param sharegroups:
            Share group definitions for each transformation in the chain
            template. Definitions may be given collectively for all
            transformations in the chain in the form of a single integer input,
            or individually for each transformation in the form of an Iterable.

            Share groups:
                0: stack-specific (shared across all domains)
                1: slice-specific (shared across the same slice)
                2: component-specific (specific to the individual domains)
        :type sharegroups: int or Iterable[int]
        :param name: Name of the stack.
        :type name: str or None
        :param copy:
            If True (default), domains are copied internally, so that the input
            is not modified. If False, the inputs are passed by reference, thus
            their transformations may be replaced when the template chain is
            assigned to the slices of the stack.
        :type copy: bool

        """
        # Verify input arguments
        images, domains = self._verify_image_input(images, copy)
        slice_indices = self._verify_slice_indices(slice_indices, domains)
        slice_metadata = self._verify_metadata(slice_metadata, domains)

        # Generate compound indices: (slice_index, component_index)
        compound_indices = self._generate_compound_indices(slice_indices)

        # Establish dict of domains with compound indexing
        super(MCSliceStack, self).__init__(
            *domains, indices=compound_indices, name=name)

        # Assign individualised transformation chains to the domains if an only
        # if a template chain was specified. Preserve original domain
        # transformations if no chain was specified.
        if chain is not None:
            self.assign(chain=chain, sharegroups=sharegroups)
        else:
            self._chain = None
            self._sharegroups = None

        # Reassign the updated domains to the images
        for image, domain in zip(images, domains):
            if hasattr(image, "domain"):
                image.domain = domain

        # Store images with compound indexing
        self._images = dict(zip(compound_indices, images))
        # Store metadata with compound indexing
        self._metadata = dict(zip(compound_indices, slice_metadata))

    @staticmethod
    def _verify_image_input(images, copy):
        # Singular input for images
        if not hasattr(images, "__iter__"):
            images = (images,)
        # Verify input domains
        _images = []
        _domains = []
        for image in images:
            # Image input
            if hasattr(image, "domain"):
                if copy:
                    # Images are unlinked from their domains here for safety,
                    # but will be relinked later.
                    _images.append(image.copy())
                    _domains.append(image.domain.copy())
                else:
                    _images.append(image)
                    _domains.append(image.domain)
            # Domain-only input
            elif hasattr(image, "__domain__"):
                _images.append(None)
                if copy:
                    _domains.append(image.copy())
                else:
                    _domains.append(image)
            else:
                raise TypeError(f"Unrecognised input type: "
                                f"{image.__class__.__name__}")
        else:
            images = _images
            domains = _domains

        return images, domains

    @staticmethod
    def _verify_slice_indices(slice_indices, domains):

        # No input for slice indices --> auto-assign indices
        if slice_indices is None:
            return np.arange(0, len(domains))

        # Singular input for slice indices
        if not hasattr(slice_indices, "__iter__"):
            slice_indices = (slice_indices,)
        # Verify input slice indices
        slice_indices = np.asarray(slice_indices)
        if not np.can_cast(slice_indices, np.integer):
            raise TypeError("Floating-point input in slice indices.")
        slice_indices = np.asarray(slice_indices, dtype=np.intp)
        # Check if the indices correspond to the input
        n_indices = len(slice_indices)
        n_domains = len(domains)
        if n_indices != n_domains:
            raise ValueError(f"Input mismatch: {n_domains} Domains "
                             f"<> {n_indices} slice indices.")
        return slice_indices

    @staticmethod
    def _verify_metadata(slice_metadata, domains):
        if slice_metadata is None:
            slice_metadata = (dict(),) * len(domains)
        if not hasattr(slice_metadata, "__iter__"):
            slice_metadata = (slice_metadata,)
        for s_meta in slice_metadata:
            if not isinstance(s_meta, dict):
                raise TypeError(f"Expected dict for metadata, "
                                f"got {s_meta.__class__.__name__} instead.")
        else:
            return slice_metadata

    @staticmethod
    def _generate_compound_indices(slice_indices):
        component_counters = dict()
        compound_indices = []
        for s_index in slice_indices:
            if s_index not in component_counters.keys():
                component_counters[s_index] = count(0)
            c_ix = (s_index, next(component_counters[s_index]))
            compound_indices.append(c_ix)
        else:
            return compound_indices

    @staticmethod
    def _verify_sharegroups(sharegroups, chain):
        if chain is None:
            return None

        # Create share groups
        if not hasattr(sharegroups, "__iter__"):
            sharegroups = (sharegroups,) * len(chain)
        # Verify share groups
        n_sg = len(sharegroups)
        n_chain = len(chain)
        if n_sg != n_chain:
            raise ValueError(f"Mismatch: {n_chain} chained transformation(s) "
                             f"<> {n_sg} share group specifications")
        for sg in sharegroups:
            if sg not in SHAREGROUPS:
                raise ValueError(f"Unrecognised share group: {sg}. "
                                 f"Expected: {SHAREGROUPS}")
        else:
            return sharegroups

    def assign(self, chain, sharegroups=0):
        """
        Assigns individualised instances of the specified transformation chain
        template to the domains within the stack.

        :param chain:
            Chain template for the domains. All transformations in the template
            are copied before they are assigned to the domains in the stack.
            If None, the original chains will be preserved.
        :type chain: None or Chain or Iterable
        :param sharegroups:
            Share group definitions for each transformation in the chain
            template. Definitions may be given collectively for all
            transformations in the chain in the form of a single integer input,
            or individually for each transformation in the form of an Iterable.

            Share groups:
                0: stack-specific (shared across all domains)
                1: slice-specific (shared across the same slice)
                2: component-specific (specific to the individual domains)
        :type sharegroups: int or Iterable[int]

        """
        self._sharegroups = self._verify_sharegroups(sharegroups, chain)

        # Create stack-specific transformations
        # and unlink stack-level transformations from the input
        chain = Chain(chain).copy()
        for ix, tx in enumerate(chain):
            if self.sharegroups[ix] == STACK_SPECIFIC:
                tx.name = f"stack-{self.name}_{tx.name}"

        # Assign transformations
        slice_indices = sorted([s_ix for s_ix, c_ix in self.domains.keys()])
        for s_index in slice_indices:
            # Generate slice-specific chain
            s_chain = chain[:]  # New Chain instance with shared transformations
            for ix, tx in enumerate(s_chain):
                if self.sharegroups[ix] == SLICE_SPECIFIC:
                    new_tx = tx.copy()
                    new_tx.name = f"{self.name}_slice-{s_index}_{new_tx.name}"
                    s_chain[ix] = new_tx

            s_components = {key[1]: domain for (key, domain) in
                            self.domains.items() if key[0] == s_index}
            for c_index, domain in s_components.items():
                # Generate component-specific chain
                c_chain = s_chain[:]
                for ix, tx in enumerate(c_chain):
                    if self.sharegroups[ix] == COMPONENT_SPECIFIC:
                        new_tx = tx.copy()
                        new_tx.name = f"{self.name}_slice-{s_index}" \
                                      f"-cmp-{c_index}_{new_tx.name}"
                        c_chain[ix] = tx.copy()
                domain.external = c_chain

        # Sync the new domain with th inactive layers as well
        for c_ix, image in self.images.items():
            if image is not None:
                image.domain = self.domains[c_ix]
                image.resmgr.sync()
        else:
            self._chain = chain

    @property
    def images(self):
        """ Read-only property. """
        return self._images

    @property
    def metadata(self):
        """ Read-only property. """
        return self._metadata

    @property
    def chain(self):
        """ Read-only property. """
        return self._chain

    @property
    def sharegroups(self):
        """ Read-only property. """
        return self._sharegroups

    def select(self, indices, copy=True):
        domains = []
        images = []
        metadata = []
        for ix in indices:
            domains.append(self.domains[ix])
            images.append(self.images[ix])
            metadata.append(self.metadata[ix])

        # If image data is missing for certain slices, ensure that domains
        # are passed on nevertheless. (This is not an elegant solution, because
        # it creates inhomogeneous input, but it is quick enough and leads to
        # identical stack instances.)
        for ix, im in enumerate(images):
            if im is None:
                images[ix] = domains[ix]

        # Reduce compound indices
        indices = [c_ix[0] for c_ix in indices]

        return type(self)(
            images, slice_indices=indices, slice_metadata=metadata,
            chain=self.chain, sharegroups=self.sharegroups, name=None,
            copy=copy
        )

    def __add__(self, other):
        """
        The sum of two MCSliceStacks is defined as a new MCSliceStack instance,
        that has the same domains as the union of the two inputs registered
        under their respective slice numbers. Component numbers are however
        reassigned. The domains, images, and metadata are all passed by
        copy (metadata is a shallow copy). The chains of the original domains
        are preserved.

        """
        # Check type compatibility
        if not isinstance(other, type(self)):
            raise NotImplementedError(
                f"Addition is not supported between {self.__class__.__name__} "
                f"and {other.__class__.__name__}")

        self_indices, self_domains = zip(*self.domains.items())
        other_indices, other_domains = zip(*other.domains.items())
        indices = [s_ix for s_ix, c_ix in self_indices] + \
                  [s_ix for s_ix, c_ix in other_indices]
        domains = self_domains + other_domains
        images = []
        metadata = []
        for c_ix in self_indices:
            images.append(self.images[c_ix])
            metadata.append(self.metadata[c_ix].copy())
        for c_ix in other_indices:
            images.append(other.images[c_ix])
            metadata.append(other.metadata[c_ix].copy())

        for ix, (image, domain) in enumerate(zip(images, domains)):
            if image is None:
                images[ix] = domain

        return type(self)(
            images, slice_indices=indices, slice_metadata=metadata,
            chain=None, sharegroups=2, name=None, copy=True)


class Grating(MultiDomain):

    def __init__(self, gap, offset, rotation, shapes, resolutions,
                 indices=None, internals=None, flips=None):
        """
        Initialisation of Grating.

        :param gap: Slicing gap [mm]
        :type gap: float
        :param offset: 3D offset of the grating [mm]
        :type offset: Iterable[float]
        :param rotation: 3D rotation of the grating [deg]
        :type rotation: Iterable[float]
        :param shapes: Shape of the slices [pixels]
        :type shapes: Iterable[int] or Iterable[Iterable[int]]
        :param resolutions: Pixel sizes of the individual slices.
        :type resolutions: Iterable[float] or Iterable[Iterable[float]]
        :param indices:
            Slices to be generated. If None (default), indices are
            automatically assigned to each input shape: 0..n, where n is
            the number of shape inputs.
        :type indices: Iterable[int]
        :param internals: internal transformations for each domain
        :type internals: Iterable

        """
        # Verify inputs
        self.gap = float(gap)
        offset = np.asarray(offset, dtype=float)
        assert offset.size == 3, f"Invalid offset: {offset}"
        rotation = np.asarray(rotation, dtype=float)
        assert rotation.size == 3, f"Invalid rotation: {rotation}"
        shapes = np.atleast_2d(shapes)
        n, dim = shapes.shape
        assert n >= 1, f"Invalid number of slices: {n}"
        assert dim == 2, f"Slices must be 2-dimensional"
        resolutions = np.atleast_2d(resolutions)
        rn, rdim = resolutions.shape
        assert rdim in (1, dim), \
            f"Invalid dimensions in resolution specification: {rdim}"
        if rn == 1:
            resolutions = np.tile(resolutions, (n, 1))
        assert resolutions.shape[0] == n
        self.resolutions = resolutions
        if indices is None:
            indices = np.arange(n)
        else:
            indices = np.asarray(indices, dtype=int)
        assert (internals is None) or (len(internals) == n), \
            "Invalid number of internal transformations."
        self.internals = internals
        self.flips = flips or (None,) * shapes.shape[0]

        # Create grating transformations (these are shared by the slices)
        self.offset = TxTranslation(*offset, name="grating-offset")
        self.rotation = TxEulerAngles(*rotation, mode="deg", order="xyz",
                                      name="grating-rotation")

        # Create slice domains with transformations
        domains = []
        for ix in range(n):
            # Define slice-specific transformations
            res = self.resolutions[ix]
            if res.size > 1:
                s_res = TxScale(*res, name="slice-resolution")
            else:
                s_res = TxIsoScale(*res, name="slice-resolution")
            cy, cx = \
                Chain(*internals[ix], s_res).map(np.divide(shapes[ix], 2.))[0]
            s_centralise = TxTranslation(-cy, -cx, name="slice-centralise")
            s_rotation = TxRotation2D(90, mode="deg", name="slice-rotation")
            s_nudge = TxTranslation(0, 0, name="slice-nudge")
            s_embed = TxEmbed(1, name="slice-embed")
            s_offset = \
                TxTranslation(0, 0, indices[ix] * self.gap, name="slice-offset")
            s_flip = self.flips[ix] or TxAffine(np.ones(6), name="flip")
            chain = [s_flip, s_res, s_centralise, s_rotation, s_nudge, s_embed,
                     s_offset, self.rotation, self.offset]

            # Combine slice and grating txs and store new slice domain
            # resolution -> centralise -> in-plane rotation -> in-plain nudge ->
            # -> 3D embedding -> slice offset -> grating rotation -> grating
            # offset
            slice_no = indices[ix]
            domain = Domain(*shapes[ix], internal=internals[ix],
                            chain=chain, name=f"slice-{slice_no}")
            domains.append(domain)

        # Call the parent-class initialiser
        super(Grating, self).__init__(*domains, indices=indices)
