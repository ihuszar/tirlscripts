#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

"""

"""


__tirlscript__ = "bigmac.ms2v"


# DEPENDENCIES

import os
import re
import shutil
import sys
import dill
import yaml
import logging
import pathlib
import argparse
import subprocess
import numpy as np
import pandas as pd
from attrdict import AttrMap
import multiprocessing as mp
from functools import partial
from scipy.stats import linregress


# TIRL IMPORTS

import tirl
import tirl.utils as tu
import tirl.settings as ts
from tirl.constants import *
from tirl.chain import Chain
from tirl.tfield import TField
from tirl.timage import TImage

from tirl.transformations.affine import TxAffine
from tirl.transformations.embedding import TxEmbed
from tirl.transformations.identity import TxIdentity
from tirl.transformations.euler import TxEulerAngles
from tirl.transformations.rotation import TxRotation2D
from tirl.transformations.rotation import TxRotation3D
from tirl.transformations.axisangle import TxAxisAngle
from tirl.transformations.scale import TxScale, TxIsoScale
from tirl.transformations.translation import TxTranslation


# TIRLSCRIPT IMPORTS

from tirlscripts import srchash
from tirlscripts.oxford.bigmac.constants import *
from tirlscripts.oxford.bigmac import __version__
from tirlscripts.oxford.bigmac.multidomain import MCSliceStack


# GLOBAL DEFINITIONS

np.set_printoptions(precision=4, suppress=True)
LOGGER = "tirlscripts.oxford.bigmac.multislice_to_volume"


# IMPLEMENTATION

def main():
    """ Main program code. """

    # Prepare output directory
    prepare_output_dir()

    # Copy the input configurations to a file in the target directory
    with open(os.path.join(cnf["outputdir"], "configuration.yml"), "w") as fp:
        yaml.safe_dump(cnf, fp, sort_keys=False, indent=2,
                       default_flow_style=None)

    cmd = " ".join(sys.argv)
    logger.info(f"The program started with the command: {cmd}")

    # Logging detailed version information
    logger.debug(f"tirlscripts bigmac library version: {__version__}")
    ms2vpy = __file__
    rigid2dpy = os.path.join(os.path.dirname(__file__), "rigid2d.py")
    rigid3dpy = os.path.join(os.path.dirname(__file__), "rigid3d.py")
    warppy = os.path.join(os.path.dirname(__file__), "warp.py")
    logger.debug(f"multislice_to_volume.py SHA1: {srchash(ms2vpy)}")
    logger.debug(f"rigid2d.py SHA1: {srchash(rigid2dpy)}")
    logger.debug(f"rigid3d.py SHA1: {srchash(rigid3dpy)}")
    logger.debug(f"warp.py SHA1: {srchash(warppy)}")

    # Input data
    inputtable = pd.read_csv(cnf["slices"])
    # inputtable = filter_input(pd.read_csv(cnf["slices"]))
    images, slice_indices, slice_metadata = load_images(inputtable)

    # Disable slice masks if desired
    if cnf["use-slice-masks"] is False:
        for image in images:
            image.mask = None

    # Create raw slice stack from the (filtered) input images
    stack = MCSliceStack(
        images, slice_indices=slice_indices, slice_metadata=slice_metadata,
        chain=None, sharegroups=2, name="stack", copy=False
    )

    # Save reference image
    volume = TImage(cnf["volume"], external="affine", name="mri", storage=MEM)
    if cnf["vmask"] is not None:
        volume.mask = TField(cnf["vmask"], storage=MEM)
    refdir = os.path.join(cnf["outputdir"], "reference")
    if not os.path.exists(refdir):
        try:
            os.makedirs(refdir)
        except Exception as exc:
            logger.exception(
                f"ERROR while creating output directory: {refdir}")
            raise exc
    reference = os.path.join(refdir, "reference.timg")
    volume.save(reference, overwrite=True)
    logger.info(f"The 3D reference image was saved to {reference}")

    # Save snapshots of the components
    for c_ix, image in stack.images.items():
        slice_no, c_no = c_ix
        slicedir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")
        if not os.path.exists(slicedir):
            try:
                os.makedirs(slicedir)
            except Exception as exc:
                logger.exception(
                    f"ERROR while creating output directory: {slicedir}")
                raise exc
        part = stack.metadata[c_ix]["Part"]
        fname = f"slice-{slice_no:04d}_part-{part}_microscopy.png"
        thumb = os.path.join(slicedir, fname)
        image.snapshot(thumb, overwrite=True)
        logger.info(f"Created snapshot of the microscopy image: {thumb}")
        fname = f"slice-{slice_no:04d}_part-{part}_init.png"
        initsnap = os.path.join(slicedir, fname)
        mri_init = volume.evaluate(image.domain)
        mri_init.snapshot(initsnap, overwrite=True)
        logger.info(f"Created snapshot of initial alignment: {initsnap}")

    # Linear multislice optimisation
    try:
        g, err = linear_registration(stack, reference, cnf)
    except Exception as exc:
        logger.error(
            "The linear registration process terminated with an error.")
        raise exc

    # Save grating parameters
    with open(os.path.join(cnf["outputdir"], "best_grating.txt"), "w") as fp:
        c_ix, domains = zip(*g.domains.items())
        chain = ChainTemplate(*domains[0].external)
        fp.write(f"Offset (0th slice, mm): {chain.g_trans.parameters[:]}\n")
        rotation_angles = chain.g_rot.euler(mode="deg", order="xyz")
        fp.write(f"Rotation (deg): {rotation_angles}\n")
        gap = np.abs(g.metadata[c_ix[0]]["gap"])  # mm
        stderr = np.abs(g.metadata[c_ix[0]]["stderr"])  # mm
        fp.write(f"Slicing gap (mm): {gap} ± {stderr}\n")
        rvalue = np.abs(g.metadata[c_ix[0]]["rvalue"])
        fp.write(f"r-value: {rvalue}")

    # In-plane warping
    try:
        err += run_stage(STAGES.warp, g, reference, cnf)
    except Exception as exc:
        logger.error("The warping process terminated with an error.")
        raise exc

    if err == 0:
        logger.fatal("The multi-slice registration was successfully completed "
                     "for all input slices.")
    else:
        logger.fatal(f"Multi-slice registration complete. "
                     f"Number of slices excluded because of error: {err}")


def _get_cmd_prefix(scheduler, **kwargs):
    scheduler = str(scheduler).lower() if scheduler else None
    if scheduler == "sge":
        queue = kwargs.get("queue", DEFAULT_QUEUE)
        prefix = [f"fsl_sub", f"-q {queue}", f"--extra=\"-sync y\"", "tirl"]
    elif scheduler == "slurm":
        prefix = [f"sbatch", "-W", "tirl"]
    elif scheduler is None:
        prefix = [f"tirl"]
    else:
        raise NameError(f"Unrecognised task scheduler: {scheduler}")
    return prefix


def prepare_output_dir():
    """
    Creates the full path to the output directory if it does not exist yet.
    If the output directory is not specified, the current directory will be
    used.

    """
    outdir = cnf.get("outputdir", os.path.join(os.getcwd(), "output"))
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    cnf["outputdir"] = outdir


def create_logger(loggername):
    """
    Creates a logger that sends log messages to the standard output and
    into a file.

    """
    p = AttrMap(cnf)
    global logger
    logger = logging.getLogger(loggername)
    logger.handlers = []  # delete any existing handlers to avoid duplicate logs
    logger.setLevel(1)
    formatter = logging.Formatter(
        fmt='%(asctime)s Process-%(process)d %(levelname)s (%(lineno)d) '
            '- %(message)s',
        datefmt='[%Y-%m-%d %H:%M:%S]')

    # Make sure that the logfile can be created
    logfile = cnf["logfile"]
    if not logfile:
        logfile = os.path.join(p.outputdir, "logfile.log")
    else:
        logdir = os.path.dirname(logfile)
        if not os.path.isdir(logdir):
            os.makedirs(logdir)

    # Redirect all logs of interest to the logfile
    fh = logging.FileHandler(logfile, mode="a", delay=False)
    if isinstance(p.loglevel, str):
        level = getattr(logging, p.loglevel.upper())
    elif isinstance(p.loglevel, (int, float)):
        level = int(p.loglevel)
    elif not p.loglevel:
        level = logging.INFO
    else:
        raise TypeError(f"Invalid logging level: {p.loglevel}")
    fh.setLevel(level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Redirect only warnings/errors to the standard output, unless "verbose"
    ch = logging.StreamHandler()
    if p.verbose:
        ch.setLevel(level)
    else:
        ch.setLevel(logging.WARNING)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


def filter_input(csv_input):
    """
    Cleans the input data, and exports a new database of inputs to a CSV file.

    """
    table = choose_data(csv_input)

    # Save the filtered inputs as a CSV file
    db_input = os.path.join(cnf["outputdir"], "input_data.csv")
    try:
        table.to_csv(db_input)
    except Exception as exc:
        logger.error(f"ERROR while saving input file database: {db_input}")
    else:
        logger.info(f"Saved input file database: {db_input}")

    return table


def choose_data(table):
    """
    Exports a subset of the input table that will be used in the registration.
    Modify this function to choose different inputs.

    :param table: input table (slice database)
    :type table: pd.DataFrame

    :returns: reduced table
    :rtype: pd.DataFrame

    """
    # Some slices were captured more than once, and even for the same capture
    # there can be multiple registration attempts. Furthermore, several slices
    # were divided into left and right parts, and registered separately.

    # The following rules are applied to choose the input data:
    # 1/ the latest capture of each slice or hemisphere
    # 2/ for identical inputs, the one with the least registration cost

    def mtime(x):
        return pathlib.Path(x).stat().st_mtime

    def regcost(x):
        regdir = os.path.dirname(x)
        with open(os.path.join(regdir, "logfile.log"), "r") as fp:
            logs = "".join(line for line in fp)
            try:
                cost = float(re.search("(STAGE 1 cost: )(.+)", logs).group(2))
            except Exception as exc:
                cost = np.inf
        return cost

    filtered = []
    for slice_index, slice_group in table.groupby("Slice"):
        latest = slice_group.SliceOriginal.apply(mtime).argmax()
        latest_capture = slice_group.iloc[latest].Capture
        capture_group = slice_group[slice_group.Capture == latest_capture]
        for pindex, part_group in capture_group.groupby("Part"):
            best = part_group.SliceRegistered.apply(regcost).argmin()
            best_registration = part_group.iloc[best].Registration
            part = part_group.iloc[0].Part
            parts = table[(table.Slice == slice_index) &
                          (table.Capture == latest_capture) &
                          (table.Part == part) &
                          (table.Registration == best_registration)]
            assert len(parts) in (0, 1), f"{parts}"
            filtered.append(parts)

    # Finalise input descriptor table
    filtered = pd.concat(filtered)
    costs = filtered.SliceRegistered.apply(regcost)
    filtered["PreRegistrationCost"] = costs
    filtered.sort_values("PreRegistrationCost", inplace=True)
    return filtered


def load_images(table):
    """
    Returns TImages with their respective slice indices.

    """
    logger.info("Loading inputs...")

    # Set primary resource
    resource = str(cnf["primary-resource"]).lower()
    if resource == "memory":
        resource = MEM
    elif resource == "hdd":
        resource = HDD
    else:
        raise ValueError(f"Unrecognised primary resource: {resource}")

    inputs = []
    for ix, record in table.iterrows():
        imfile = record.ImageFile
        img0 = tirl.load(imfile)
        logger.info(f"Loaded TImage from {imfile}: {img0}")
        resolution = float(cnf["snapshot-resolution"])
        img = img0.resample(resolution, copy=True)
        del img0
        img.storage = resource
        logger.info(f"Set image resolution to {resolution} mm/px: {img}")
        if img.tsize > 1:
            for ix, layer in enumerate(img.resmgr.layers):
                img.resmgr.layers[ix] = layer.tensors.reduce()
            logger.info(f"Reduced image tensors: {img}")
        metadata = dict(
            PreRegistrationCost=img.header["metadata"]["PreRegistrationCost"],
            Part=img.header["metadata"]["Part"]
        )
        inputs.append((int(record.Slice), img, metadata))
    else:
        slice_indices, images, metadata = zip(*inputs)

    return images, slice_indices, metadata


def generate_chain_template():
    """
    Returns a chain template for the multislice stack.

    """

    # Component-specific transformations
    c_isoscale = TxIsoScale(1, dim=2, name="c_isoscale2D")
    c_scale = TxScale(1, 1, name="c_scale2D")
    c_cent = TxTranslation(0, 0, name="c_centralise2D")
    c_rot = TxRotation2D(0, mode="deg", name="c_rotation2D")
    c_offset = TxTranslation(0, 0, name="c_offset2D")
    c_affine = TxAffine(1, 0, 0, 0, 1, 0, name="c_affine2D")

    # Slice-specific transformations
    s_cent = TxTranslation(0, 0, name="s_centralise2D")
    s_isoscale = TxIsoScale(1, dim=2, name="s_isoscale2D")
    s_scale = TxScale(1, 1, name="s_scale2D")
    s_rot = TxRotation2D(0, mode="deg", name="s_rotation2D")
    s_offset = TxTranslation(0, 0, name="s_offset2D")
    s_affine = TxAffine(1, 0, 0, 0, 1, 0, name="s_affine2D")

    # Stack-specific transformations
    g_embed = TxEmbed(1, name="g_embed")
    s_warp = TxIdentity(0, name="s_warp3D")  # non-linear placeholder
    s_distance = TxTranslation(0, 0, 0, name="s_distance")
    g_rot = TxEulerAngles(0, 0, 0, mode="deg", order="xyz", name="g_rotation3D")
    g_trans = TxTranslation(0, 0, 0, name="g_offset3D")
    g_warp = TxIdentity(0, name="g_warp3D")  # non-linear placeholder

    chain = ChainTemplate(
        c_cent, c_isoscale, c_scale, c_rot, c_offset, c_affine,
        s_cent, s_isoscale, s_scale, s_rot, s_offset, s_affine,
        g_embed, s_warp, s_distance, g_rot, g_trans, g_warp
    )
    sharegroups = (
        2, 2, 2, 2, 2, 2,
        2, 1, 1, 1, 1, 1,
        0, 1, 1, 0, 0, 0
    )

    return chain, sharegroups


def linear_registration(stack, reference, cnf):
    """
    Linear multi-slice registration algorithm.

    :param stack: a stack of all input images
    :type stack: MCSliceStack
    :param reference: path to 3D TImage file
    :type reference: str
    :param cnf: Script configurations
    :type cnf: dict

    """
    err = 0

    # Define the order of slice recruitment
    recruitment = str(cnf["regparams"]["recruitment"]).lower()
    # BEST: from the most trustworthy to the least trustworthy
    if recruitment == "best":
        slice_indices = [c_ix for (c_ix, meta) in sorted(
            stack.metadata.items(), key=lambda t: t[1]["PreRegistrationCost"])]

    # RANDOM(seed): random permutation
    elif recruitment.startswith("random"):
        seed = re.findall(r"\d+", recruitment)
        seed = int(seed[0]) if seed is not None else 0
        ix = np.arange(len(stack.images))
        ix_rand = np.random.RandomState(seed).permutation(ix)
        slice_indices = np.asarray([*stack.metadata.keys()])[ix_rand]
        slice_indices = tuple(tuple(c_ix) for c_ix in slice_indices)

    elif recruitment == "sequential":
        slice_indices = np.asarray([*stack.metadata.keys()])
        slice_indices = tuple(tuple(c_ix) for c_ix in slice_indices)

    else:
        raise ValueError(f"Unrecognised recruitment pattern: {recruitment}")

    # Keep recruiting new slices until the best grating is assumed
    batchsize = int(min(cnf["regparams"].get("batchsize", stack.n), stack.n))
    n = batchsize
    while True:
        # Create new grating from estimated parameters
        g = stack.select(slice_indices[:n], copy=False)
        # Save domains to ensure that detrimental optimisations can be reverted
        save_trusted_domains(g, cnf)
        # Fit grating
        g = fit_grating(g)
        save_grating_parameters(g)
        # 2D rigid (in-plane) optimisation of the grating elements
        logger.info("Starting 2D rigid (in-plane) optimisation...")
        err += run_stage(STAGES.rigid2d, g, reference, cnf)
        # 3D rigid optimisation of the grating elements
        logger.info("Starting 3D rigid optimisation...")
        err += run_stage(STAGES.rigid3d, g, reference, cnf)
        # Update the slice iterator
        if n < stack.n:
            n = int(min(n + batchsize, stack.n))
        else:
            break

    # Fix the orientation and the position of the grating slices
    g = fit_grating(g)
    save_grating_parameters(g)
    logger.info("Starting 2D rigid (in-plane) optimisation...")
    err += run_stage(STAGES.rigid2d, g, reference, cnf)

    return g, err


def fit_grating(stack=None):
    """
    Estimates the slice gap, orientation, and offset of a regular grating that
    best matches the position of the slices in the specified stacks.

    :param stack: Input slice stack.
    :type stack: MCSliceStack

    """
    if stack is None:
        return stack

    # Estimate grating parameters

    z0 = [0, 0, 1]
    vcentres = dict()
    centres = dict()
    normals = dict()
    weights = dict()
    resolutions = dict()

    for c_ix, image in stack.images.items():
        slice_no = c_ix[0]
        domain = image.domain

        # Calculate the normal vector of the current component
        normal = get_3D_rotation(domain).map(z0)
        # if np.dot(normal, [0, -1, 0]) >= 0:
        #     normal *= -1
        logger.log(5, f"Slice {slice_no} normal: {normal}")
        normals.update({c_ix: normal / np.linalg.norm(normal)})

        # Calculate the weighted centre of the current component
        if image.mask is not None:
            vcog = image.mask.ravel() * domain.get_voxel_coordinates().T
            vcog = np.sum(vcog, axis=1).ravel()
            weight = np.sum(image.mask)
            vcog = vcog / weight
        else:
            vcog = domain.vcentre()
            weight = domain.numel

        vcentres.update({c_ix: vcog})
        centres.update({c_ix: domain.map_voxel_coordinates(vcog).ravel()})

        # Store component weights, which will be used to determine the
        # weighted centre of the slice that the components belong to.
        # Ignore this, because if the image resolution is not defined
        # correctly, this may lead to errors. If the multiplication by the
        # image resolution is omitted, we essentially assume that all
        # components are defined at the same resolution.
        # weights.update({c_ix: weight * image.resolution ** 2})
        weights.update({c_ix: weight})

        # Calculate the actual image resolutions
        resolution = np.multiply(image.resolution, get_scaling(domain))
        resolutions.update({c_ix: resolution})

    # Calculate weighted slice centres and weighted slice normals
    slice_indices = sorted(set([c_ix[0] for c_ix in stack.domains.keys()]))
    slice_normals = []
    slice_centres = []
    for slice_no in slice_indices:
        # Find all components that belong to the current slice
        c_indices = sorted(
            [c_ix for c_ix in stack.domains.keys() if c_ix[0] == slice_no],
            key=lambda t: t[1]
        )
        wscentre = 0
        wtotal = 0
        wnormal = 0
        for c_ix in c_indices:
            wscentre = wscentre + weights[c_ix] * centres[c_ix]
            wnormal = wnormal + weights[c_ix] * normals[c_ix]
            wtotal = wtotal + weights[c_ix]
        else:
            wscentre = wscentre / wtotal
            wnormal = wnormal / wtotal
        slice_centres.append(wscentre)
        slice_normals.append(wnormal / np.linalg.norm(wnormal))

    # Create arrays
    slice_normals = np.vstack(slice_normals)
    slice_centres = np.vstack(slice_centres)

    init_normal = np.mean(slice_normals, axis=0)
    logger.info(f"Normal vector estimate (initial): {init_normal}")
    def costfunc(x):
        x = np.divide(x, np.linalg.norm(x))
        y = [float(np.dot(x, c.reshape(-1, 1))) for c in slice_centres]
        result = linregress(slice_indices, y)
        return -(result.slope ** 2)
    from scipy.optimize import fmin
    opt = fmin(costfunc, init_normal, disp=True)
    robust_normal = opt
    logger.info(f"Normal vector estimate (robust): {robust_normal}")

    # Robust orientation estimation by iterative rejecting outliers
    # select = np.full(shape=slice_normals.shape[0], fill_value=True)
    # robust_normal = np.mean(slice_normals, axis=0).ravel()
    # for i in range(cnf["regparams"]["outlier-iterations"]):
    #     robust_normal = np.mean(slice_normals[select], axis=0).ravel()
    #     robust_normal /= np.linalg.norm(robust_normal)
    #     logger.log(5, f"Outlier removal iteration {i + 1}, robust normal: "
    #                   f"{robust_normal}")
    #     diff = np.linalg.norm(slice_normals - robust_normal, axis=-1)
    #     select[diff > 2 * np.median(diff[select])] = False
    # else:
    #     pass
    #     # fig = plt.figure()
    #     # ax = fig.add_subplot(projection='3d')
    #     # ax.scatter(*slice_normals.T, c="blue", marker="o")
    #     # ax.scatter(*robust_normal, c="red", marker="x")
    #     # plt.show()

    # Find the rotation angles corresponding to the robust normal
    angle = np.arccos(np.dot(z0, robust_normal.reshape(-1, 1)))
    axis = np.cross(z0, robust_normal)
    axis /= np.linalg.norm(axis)
    g_rotation = TxAxisAngle(angle, axis, mode="rad")
    euler_angles = g_rotation.euler(mode="deg", order="xyz")
    logger.info(f"Grating rotation (robust): {euler_angles} [deg]")

    # Centre of gravity
    # Slices should either carry an equal weight or be weighted by their
    # inverse registration cost to take trustworthiness into account.
    # Weighting by area is discouraged here as large but misaligned slices
    # could bias the result.
    cog = np.mean(slice_centres, axis=0)
    cog_pos = np.dot(robust_normal, np.asarray(cog).reshape(-1, 1))
    logger.info(f"Centre of gravity: {cog} [mm]")

    # Slicing gap
    y = [float(np.dot(robust_normal, c.reshape(-1, 1))) for c in slice_centres]
    result = linregress(slice_indices, y)
    gap = result.slope
    stderr = result.stderr
    rvalue = result.rvalue

    # Grating offset = centre of the 0th slice
    g_offset = np.add(
        cog, np.subtract(result.intercept, cog_pos) * robust_normal)

    logger.info(f"Effective slice gap: {gap:.04} +/- {stderr:.04} mm")
    logger.info(f"Correlation coefficient: {rvalue:.04}")
    logger.info(f"1st slice centre: {g_offset} mm")

    # Define new grating for the input slices using the estimated parameters
    chain_template, sharegroups = generate_chain_template()
    chain_template.s_rot.parameters[:] = -np.pi / 2
    chain_template.g_rot.parameters[:] = np.radians(euler_angles)
    chain_template.g_trans.parameters[:] = g_offset
    old_domains = \
        {c_ix: domain.copy() for (c_ix, domain) in stack.domains.items()}
    stack.assign(chain_template, sharegroups)

    # Initialise the component-specific chains, preparing them for fine-tuning
    for c_ix, image in stack.images.items():
        slice_no, c_no = c_ix
        stack.metadata[c_ix]["gap"] = gap
        stack.metadata[c_ix]["stderr"] = stderr
        stack.metadata[c_ix]["rvalue"] = rvalue
        chain = ChainTemplate(*image.domain.external)  # syntactic candy

        # Set the resolution
        image.resolution = resolutions[c_ix]

        # Calculate the distance of the current slice from the first slice in mm
        s_distance = slice_no * gap
        chain.s_distance.parameters[:] = np.asarray([0, 0, s_distance])

        # Calculate 2D slice offset from stack axis intersection in mm
        slice_cog = slice_centres[slice_indices.index(slice_no)]
        axis_intersection = g_offset + s_distance * robust_normal
        s_offset3D = planproj(axis_intersection, slice_cog, robust_normal)
        inv = get_subchain(chain, chain.s_offset).inverse()
        s_offset = inv.map_vector(s_offset3D, rule="fs").ravel()
        chain.s_offset.parameters[:] = np.asarray(s_offset[:2])

        # Calculate 2D component offset from slice COG in mm
        c_offset3D = planproj(slice_cog, centres[c_ix], robust_normal)
        inv = get_subchain(chain, chain.s_cent).inverse()
        s_cent = inv.map_vector(c_offset3D, rule="fs").ravel()
        chain.s_cent.parameters[:] = np.asarray(s_cent[:2])

        # Calculate the weighted centre of the component in mm
        c_cent = image.domain.internal.map(vcentres[c_ix]).ravel()
        chain.c_cent.parameters[:] = -np.asarray(c_cent)

        # Apply base vector flips if necessary
        i_base = image.domain.map_voxel_vectors([1, 0], rule="fs")
        j_base = image.domain.map_voxel_vectors([0, 1], rule="fs")
        iprime_base = old_domains[c_ix].map_voxel_vectors([1, 0], rule="fs")
        jprime_base = old_domains[c_ix].map_voxel_vectors([0, 1], rule="fs")
        flip_i = np.sign(np.dot(i_base, iprime_base.T)).ravel()
        flip_j = np.sign(np.dot(j_base, jprime_base.T)).ravel()
        chain.c_scale.parameters[:] *= np.asarray([flip_i, flip_j]).ravel()

    return stack


def save_grating_parameters(g):
    i = 0
    while True:
        fname = os.path.join(cnf["outputdir"], f"grating-{i:02d}.txt")
        if not os.path.exists(fname):
            break
        else:
            i += 1

    with open(fname, "w") as fp:
        c_ix, domains = zip(*g.domains.items())
        chain = ChainTemplate(*domains[0].external)
        fp.write(f"Offset (0th slice, mm): {chain.g_trans.parameters[:]}\n")
        rotation_angles = chain.g_rot.euler(mode="deg", order="xyz")
        fp.write(f"Rotation (deg): {rotation_angles}\n")
        gap = np.abs(g.metadata[c_ix[0]]["gap"])  # mm
        stderr = np.abs(g.metadata[c_ix[0]]["stderr"])  # mm
        fp.write(f"Slicing gap (mm): {gap} ± {stderr}\n")
        rvalue = np.abs(g.metadata[c_ix[0]]["rvalue"])
        fp.write(f"r-value: {rvalue}")


def save_trusted_domains(stack, cnf):
    outdir = cnf["outputdir"]
    ext = ts.EXTENSIONS["Domain"]
    for c_ix, image in stack.images.items():
        slice_no = c_ix[0]
        part = image.header["metadata"]["Part"]
        slicedir = os.path.join(outdir, f"{slice_no:04d}")
        fname = f"slice-{slice_no:04d}_part-{part}_trusted_domain.{ext}"
        dfile = os.path.join(slicedir, fname)
        image.domain.save(dfile, overwrite=True)
        logger.info(f"Saved trusted domain: {dfile}")


def get_subchain(chain, fromtx):
    for ix, tx in enumerate(chain):
        if tx is fromtx:
            return Chain(chain[ix:])


def planproj(o, p, normal):
    normal = normal / np.linalg.norm(normal)
    op = p - o
    return op - np.dot(op, normal) * normal


def run_stage(stage, g, reference, cnf):
    """
    Runs a specific registration stage.

    :param g: input image stack
    :type g: MCSliceStack
    :param cnf: script configutations
    :type cnf: dict

    """
    n_cpu = get_cpu_count(cnf["n_cpu"])
    worker = partial(canonical_worker, **stage)

    if n_cpu > 1:
        with mp.pool.ThreadPool(processes=n_cpu) as pool:
            result = pool.map(worker, get_jobs(g, reference, cnf))
    else:
        result = []
        for job in get_jobs(g, reference, cnf):
            result.append(worker(job))

    # Assign new domain to the grating
    err = 0
    for c_ix, d_file in result:
        try:
            domain = tirl.load(d_file)
        except FileNotFoundError:
            err += 1
            logger.error(f"File not found: {d_file}")
            logger.info(f"Component {c_ix} was removed from the stack.")
            del g.domains[c_ix]
            del g.images[c_ix]
            del g.metadata[c_ix]
        else:
            g.domains[c_ix] = domain
            g.images[c_ix].domain = domain
            g.images[c_ix].resmgr.sync()

    return err


def get_jobs(g, reference, cnf):

    for c_ix, image in g.images.items():
        slice_no, c_no = c_ix
        metadata = g.metadata[c_ix]

        # Create output directory
        outdir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")
        part = g.metadata[c_ix]["Part"]
        fname = f"slice-{slice_no:04d}_part-{part}.timg"
        if not os.path.exists(outdir):
            try:
                os.makedirs(outdir)
            except Exception as exc:
                logger.exception(
                    f"ERROR while creating output directory: {outdir}")
                continue

        # Define inputs on disk
        imfile = os.path.join(outdir, fname)
        if not os.path.isfile(imfile):
            image.save(imfile, overwrite=True)
            logger.info(f"Saved a working copy of the image data to: {imfile}")

        # Save the latest image domain next to the image data
        # in consecutive iterations, only this gets updated, because the image
        # data is not changing.
        ext = ts.EXTENSIONS["Domain"]
        dname = f"slice-{slice_no:04d}_part-{part}_latest_domain.{ext}"
        dname = os.path.join(outdir, dname)
        image.domain.save(dname, overwrite=True)
        logger.info(f"Updated image domain: {dname}")

        yield c_ix, imfile, reference, metadata, cnf


def canonical_worker(job, script, tag, descr):

    # Dump the job details to disk
    c_ix, imfile, reference, metadata, cnf = job
    slice_no, c_no = c_ix
    part = metadata["Part"]
    slicedir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")

    # Dump the job description to a file
    scriptdir = os.path.dirname(os.path.abspath(__file__))
    fullscript = os.path.join(scriptdir, script)
    fd, jobfile = tu.tmpfile(prefix=f"{tag}_", dir=slicedir, suffix=".job")
    with open(jobfile, "wb") as fp:
        dill.dump(job, fp)

    # Launch a new task and wait until it's completed
    logfile = os.path.join(
        slicedir, f"slice-{slice_no:04d}_part-{part}_tx-{tag}.log""")
    outputfile = os.path.join(
        slicedir, f"slice-{slice_no:04d}_part-{part}_tx-{tag}_domain.dom")
    cmd = _get_cmd_prefix(scheduler=cnf["scheduler"], queue=cnf["queue"]) + \
          [f"{fullscript}", f"{jobfile}", f"{outputfile}", f"{logfile}"]
    if cnf.get("debug", False):
        try:
            externalmodule = __import__(script.replace(".py", ""))
            externalmodule.main(jobfile, outputfile, logfile)
        except Exception as exc:
            returncode = 1
            raise exc
        else:
            returncode = 0
    else:
        proc = subprocess.run(cmd, env=os.environ.copy())
        returncode = proc.returncode

    # Close OS file handle
    try:
        os.close(fd)
    except Exception:
        pass

    # Remove the temporary jobfile
    if cnf["cleanup"]:
        try:
            os.remove(jobfile)
        except Exception:
            pass

    if returncode == 0:
        logger.info(f"Successfully completed \"{descr}\" stage "
                    f"for [Slice {slice_no}, Part {part}].")
    else:
        logger.error(f"There was an error while performing \"{descr}\" stage "
                     f"on [Slice {slice_no}, Part {part}].")

    # if os.path.isfile(outputfile):
    #     ext = ts.EXTENSIONS["Domain"]
    #     latest = os.path.join(
    #         slicedir, f"slice-{slice_no:04d}_part-{part}_latest_domain.{ext}")
    #     if os.path.exists(latest):
    #         os.remove(latest)
    #     os.symlink(outputfile, latest)

    # Return the output of the child process
    return c_ix, outputfile


def get_cpu_count(n_cpu):
    if int(n_cpu) < 0:
        return int(n_cpu) % (mp.cpu_count() + 1)
    else:
        return int(n_cpu)


def get_3D_rotation(domain):
    rotations = []
    for tx in domain.external:
        if isinstance(tx, TxRotation3D):
            rotations.append(tx)
    else:
        return Chain(rotations)


def get_scaling(domain):
    vdim = domain.ndim
    origin = np.zeros(vdim)
    b1 = domain.external.map_vector(np.eye(vdim), np.tile(origin, (vdim, 1)))
    return np.linalg.norm(b1, axis=-1).ravel()


def create_parser():
    """
    Creates argument parser object.

    """
    parser = argparse.ArgumentParser(
        prog="multislice_to_volume",
        description="Registers parallel sections to a volume.")
    parser.add_argument(
        "--config", metavar="<*.yml>", type=str, required=True,
        help="Configuration file.")
    parser.add_argument(
        "--verbose", action="store_true", default=False, required=False,
        help="Logs to STDOUT.")
    return parser


# PROGRAM FLOW CONTROL

if __name__ == "__main__":

    parser = create_parser()

    if len(sys.argv) > 1:

        # Load command-line arguments and configurations
        args = parser.parse_args()
        with open(args.config, "r") as fp:
            cnf = dict(yaml.safe_load(fp))

        # Override verbose setting from the CLI
        if args.verbose:
            cnf["verbose"] = args.verbose

        # Configure logger
        logger = create_logger(LOGGER)
        main()

    else:
        parser.print_help()
