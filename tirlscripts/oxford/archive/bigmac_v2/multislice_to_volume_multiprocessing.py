#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

"""

"""


# DEPENDENCIES

import os
import re
import sys
import yaml
import logging
import pathlib
import argparse
import numpy as np
import pandas as pd
from attrdict import AttrMap
import multiprocessing as mp
from datetime import datetime
from math import radians as rad
from collections import namedtuple
from scipy.stats import linregress


# TIRL IMPORTS

import tirl
import tirl.settings as ts
from tirl.constants import *
from tirl.chain import Chain
from tirl.domain import Domain
from tirl.tfield import TField
from tirl.timage import TImage
from tirl.cost.mi import CostMI
from tirl.cost.mind import CostMIND

from tirl.optimisation.optnl import OptNL, OptMethods
from tirl.optimisation.optgroup import OptimisationGroup
from tirl.regularisation.bending_energy import RegBendingEnergy

from tirl.transformations.affine import TxAffine
from tirl.transformations.embedding import TxEmbed
from tirl.transformations.identity import TxIdentity
from tirl.transformations.euler import TxEulerAngles
from tirl.transformations.rotation import TxRotation2D
from tirl.transformations.rotation import TxRotation3D
from tirl.transformations.axisangle import TxAxisAngle
from tirl.transformations.scale import TxScale, TxIsoScale
from tirl.transformations.translation import TxTranslation
from tirl.transformations.rbf_displacement import TxRbfDisplacementField


# TIRLSCRIPT IMPORTS

from tirlscripts.oxford.bigmac.multidomain import MCSliceStack


# GLOBAL DEFINITIONS

cnf = dict()
NORMALISE = True  # optimise parameters in the [-1, 1] normalised range
np.set_printoptions(precision=4, suppress=True)
LOGGER = "tirlscripts.oxford.bigmac.multislice_to_volume"
SAMPLE = 5

ChainTemplate = namedtuple("ChainTemplate", [
    "c_cent", "c_isoscale", "c_scale", "c_rot", "c_offset", "c_affine",
    "s_cent", "s_isoscale", "s_scale", "s_rot", "s_offset", "s_affine",
    "g_embed", "s_warp", "s_distance", "g_rot", "g_trans", "g_warp"
])


# IMPLEMENTATION

def main():
    """ Main program code. """

    # Prepare output directory
    prepare_output_dir()

    # Prepare logger
    # global logger
    # logger = create_logger(LOGGER)
    cmd = " ".join(sys.argv)
    logger.info(f"The program started with the command: {cmd}")

    # Input filtering ("cleaning the data")
    inputtable = filter_input(pd.read_csv(cnf["slices"]))  # [:SAMPLE]
    cnf["input-data"] = inputtable  # global variable for parallel processes

    # Create raw slice stack from the (filtered) input images
    images, slice_indices, slice_metadata = load_images(inputtable)
    stack = MCSliceStack(
        images, slice_indices=slice_indices, slice_metadata=slice_metadata,
        chain=None, sharegroups=2, name="stack", copy=False
    )

    # Save reference image
    volume = TImage(cnf["volume"], external="sform", name="mri", storage=MEM)
    volume.mask = TField(cnf["vmask"], storage=MEM)
    refdir = os.path.join(cnf["outputdir"], "reference")
    if not os.path.exists(refdir):
        try:
            os.makedirs(refdir)
        except Exception as exc:
            logger.exception(
                f"ERROR while creating output directory: {refdir}")
            raise exc
    r_file = os.path.join(refdir, "reference.timg")
    volume.save(r_file, overwrite=True)
    logger.info(f"The 3D reference image was saved to {r_file}")

    # Save snapshots of the components
    for c_ix, image in stack.images.items():
        slice_no, c_no = c_ix
        slicedir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")
        if not os.path.exists(slicedir):
            try:
                os.makedirs(slicedir)
            except Exception as exc:
                logger.exception(
                    f"ERROR while creating output directory: {slicedir}")
                raise exc
        part = stack.metadata[c_ix]["Part"]
        fname = f"slice-{slice_no:04d}_part-{part}_microscopy.png"
        image.snapshot(os.path.join(slicedir, fname), overwrite=True)

    # Linear multislice optimisation
    n_cpu = int(cnf["n_cpu"]) % (mp.cpu_count() + 1)
    try:
        g = linear_registration(stack, r_file, cnf["regparams"], n_cpu)
    except Exception as exc:
        logger.error(
            "The linear registration process terminated with an error.")
        raise exc

    # Save grating parameters
    with open(os.path.join(cnf["outputdir"], "best_grating.txt"), "w") as fp:
        c_ix, domains = zip(*g.domains.items())
        chain = ChainTemplate(*domains[0].external)
        fp.write(f"Offset (0th slice, mm): {chain.g_trans.parameters[:]}\n")
        rotation_angles = chain.g_rot.euler(mode="deg", order="xyz")
        fp.write(f"Rotation (deg): {rotation_angles}\n")
        gap = np.abs(g.metadata[c_ix[0]]["gap"])  # mm
        stderr = np.abs(g.metadata[c_ix[0]]["stderr"])  # mm
        fp.write(f"Slicing gap (mm): {gap} ± {stderr}\n")
        rvalue = np.abs(g.metadata[c_ix[0]]["rvalue"])
        fp.write(f"r-value: {rvalue}")

    # In-plane warping
    try:
        warp_parallel(g, r_file, cnf["regparams"]["warp"], n_cpu)
    except Exception as exc:
        logger.error("The warping process terminated with an error.")
        raise exc


def prepare_output_dir():
    """
    Creates the full path to the output directory if it does not exist yet.
    If the output directory is not specified, the current directory will be
    used.

    """
    outdir = cnf["outputdir"]
    if not outdir:
        outdir = os.getcwd()
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
    cnf["outputdir"] = outdir


def create_logger(loggername):
    """
    Creates a logger that sends log messages to the standard output and
    into a file.

    """
    p = AttrMap(cnf)
    global logger
    logger = logging.getLogger(loggername)
    logger.handlers = []  # delete any existing handlers to avoid duplicate logs
    logger.setLevel(1)
    formatter = logging.Formatter(
        fmt='%(asctime)s Process-%(process)d %(levelname)s (%(lineno)d) '
            '- %(message)s',
        datefmt='[%Y-%m-%d %H:%M:%S]')

    # Make sure that the logfile can be created
    logfile = cnf["logfile"]
    if not logfile:
        logfile = os.path.join(p.outputdir, "logfile.log")
    else:
        logdir = os.path.dirname(logfile)
        if not os.path.isdir(logdir):
            os.makedirs(logdir)

    # Redirect all logs of interest to the logfile
    fh = logging.FileHandler(logfile, mode="a", delay=False)
    if isinstance(p.loglevel, str):
        level = getattr(logging, p.loglevel.upper())
    elif isinstance(p.loglevel, (int, float)):
        level = int(p.loglevel)
    elif not p.loglevel:
        level = logging.INFO
    else:
        raise TypeError(f"Invalid logging level: {p.loglevel}")
    fh.setLevel(level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Redirect only warnings/errors to the standard output, unless "verbose"
    ch = logging.StreamHandler()
    if p.verbose:
        ch.setLevel(level)
    else:
        ch.setLevel(logging.WARNING)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


def filter_input(csv_input):
    """
    Cleans the input data, and exports a new database of inputs to a CSV file.

    """
    table = choose_data(csv_input)

    # Save the filtered inputs as a CSV file
    db_input = os.path.join(cnf["outputdir"], "input_data.csv")
    try:
        table.to_csv(db_input)
    except Exception as exc:
        logger.error(f"ERROR while saving input file database: {db_input}")
    else:
        logger.info(f"Saved input file database: {db_input}")

    return table


def choose_data(table):
    """
    Exports a subset of the input table that will be used in the registration.
    Modify this function to choose different inputs.

    :param table: input table (slice database)
    :type table: pd.DataFrame

    :returns: reduced table
    :rtype: pd.DataFrame

    """
    # Some slices were captured more than once, and even for the same capture
    # there can be multiple registration attempts. Furthermore, several slices
    # were divided into left and right parts, and registered separately.

    # The following rules are applied to choose the input data:
    # 1/ the latest capture of each slice or hemisphere
    # 2/ for identical inputs, the one with the least registration cost

    def mtime(x):
        return pathlib.Path(x).stat().st_mtime

    def regcost(x):
        regdir = os.path.dirname(x)
        with open(os.path.join(regdir, "logfile.log"), "r") as fp:
            logs = "".join(line for line in fp)
            try:
                cost = float(re.search("(STAGE 1 cost: )(.+)", logs).group(2))
            except Exception as exc:
                cost = np.inf
        return cost

    filtered = []
    for slice_index, slice_group in table.groupby("Slice"):
        latest = slice_group.SliceOriginal.apply(mtime).argmax()
        latest_capture = slice_group.iloc[latest].Capture
        capture_group = slice_group[slice_group.Capture == latest_capture]
        for pindex, part_group in capture_group.groupby("Part"):
            best = part_group.SliceRegistered.apply(regcost).argmin()
            best_registration = part_group.iloc[best].Registration
            part = part_group.iloc[0].Part
            parts = table[(table.Slice == slice_index) &
                          (table.Capture == latest_capture) &
                          (table.Part == part) &
                          (table.Registration == best_registration)]
            assert len(parts) in (0, 1), f"{parts}"
            filtered.append(parts)

    # Finalise input descriptor table
    filtered = pd.concat(filtered)
    costs = filtered.SliceRegistered.apply(regcost)
    filtered["PreRegistrationCost"] = costs
    filtered.sort_values("PreRegistrationCost", inplace=True)
    return filtered


def load_images(table):
    """
    Returns TImages with their respective slice indices.

    """
    global cnf
    logger.info("Creating a stack from the input slices...")

    # Load the microscopy files into an indexed stack
    inputs = []
    for ix, sl in table.iterrows():
        tpath = os.path.dirname(sl.SliceOriginal)
        tfile = os.path.join(
            tpath, f"{sl.Capture}-{sl.Part}-{sl.Registration}.timg")
        img = tirl.load(tfile)
        # img.storage = SHMEM
        logger.info(f"Loaded TImage from {tfile}")
        metadata = dict(
            PreRegistrationCost=sl.PreRegistrationCost,
            Part=sl.Part,
            Resolution=sl.Resolution
        )
        inputs.append((int(sl.Slice), img, metadata))
    else:
        slice_indices, images, metadata = zip(*inputs)

    return images, slice_indices, metadata


def generate_chain_template():
    """
    Returns a chain template for the multislice stack.

    """

    # Component-specific transformations
    c_isoscale = TxIsoScale(1, dim=2, name="c_isoscale2D")
    c_scale = TxScale(1, 1, name="c_scale2D")
    c_cent = TxTranslation(0, 0, name="c_centralise2D")
    c_rot = TxRotation2D(0, mode="deg", name="c_rotation2D")
    c_offset = TxTranslation(0, 0, name="c_offset2D")
    c_affine = TxAffine(1, 0, 0, 0, 1, 0, name="c_affine2D")

    # Slice-specific transformations
    s_cent = TxTranslation(0, 0, name="s_centralise2D")
    s_isoscale = TxIsoScale(1, dim=2, name="s_isoscale2D")
    s_scale = TxScale(1, 1, name="s_scale2D")
    s_rot = TxRotation2D(0, mode="deg", name="s_rotation2D")
    s_offset = TxTranslation(0, 0, name="s_offset2D")
    s_affine = TxAffine(1, 0, 0, 0, 1, 0, name="s_affine2D")

    # Stack-specific transformations
    g_embed = TxEmbed(1, name="g_embed")
    s_warp = TxIdentity(0, name="s_warp3D")  # non-linear placeholder
    s_distance = TxTranslation(0, 0, 0, name="s_distance")
    g_rot = TxEulerAngles(0, 0, 0, mode="deg", order="xyz", name="g_rotation3D")
    g_trans = TxTranslation(0, 0, 0, name="g_offset3D")
    g_warp = TxIdentity(0, name="g_warp3D")  # non-linear placeholder

    chain = ChainTemplate(
        c_cent, c_isoscale, c_scale, c_rot, c_offset, c_affine,
        s_cent, s_isoscale, s_scale, s_rot, s_offset, s_affine,
        g_embed, s_warp, s_distance, g_rot, g_trans, g_warp
    )
    sharegroups = (
        2, 2, 2, 2, 2, 2,
        2, 1, 1, 1, 1, 1,
        0, 1, 1, 0, 0, 0
    )

    return chain, sharegroups


def linear_registration(stack, reference, regparams, n_cpu=1):
    """
    Linear multi-slice registration algorithm.

    :param stack: a stack of all input images
    :type stack: MCSliceStack
    :param reference: path to 3D TImage file
    :type reference: str
    :param regparams: Registration parameters.
    :type regparams: dict

    """
    # Define the order of slice recruitment
    # (from the most trustworthy to the least trustworthy)
    slice_indices = [c_ix for (c_ix, meta) in sorted(
        stack.metadata.items(), key=lambda t: t[1]["PreRegistrationCost"])]

    # Keep recruiting new slices until the best grating is assumed
    batchsize = int(min(regparams.get("batchsize", stack.n), stack.n))
    n = batchsize
    while True:
        # Create new grating from estimated parameters
        g = fit_grating(stack.select(slice_indices[:n], copy=False))
        save_grating_parameters(g)
        # 2D rigid (in-plane) optimisation of the grating elements
        optimise_rigid2d_parallel(g, reference, regparams, n_cpu)
        # 3D rigid optimisation of the grating elements
        optimise_rigid3d_parallel(g, reference, regparams, n_cpu)
        # Update the slice iterator
        if n < stack.n:
            n = int(min(n + batchsize, stack.n))
        else:
            break

    # Fix the orientation and the position of the grating slices
    g = fit_grating(g)
    save_grating_parameters(g)
    optimise_rigid2d_parallel(g, reference, regparams, n_cpu)

    return g


def fit_grating(stack=None):
    """
    Estimates the slice gap, orientation, and offset of a regular grating that
    best matches the position of the slices in the specified stacks.

    :param stack: Input slice stack.
    :type stack: MCSliceStack

    """
    if stack is None:
        return stack

    # Estimate grating parameters

    z0 = [0, 0, 1]
    vcentres = dict()
    centres = dict()
    normals = dict()
    weights = dict()

    for c_ix, image in stack.images.items():
        slice_no = c_ix[0]
        domain = image.domain

        # Calculate the normal vector of the current component
        normal = get_3D_rotation(domain).map(z0)
        # if np.dot(normal, [0, -1, 0]) >= 0:
        #     normal *= -1
        logger.log(5, f"Slice {slice_no} normal: {normal}")
        normals.update({c_ix: normal / np.linalg.norm(normal)})

        # Calculate the weighted centre of the current component
        if image.mask is not None:
            vcog = image.mask.ravel() * domain.get_voxel_coordinates().T
            vcog = np.sum(vcog, axis=1).ravel()
            weight = np.sum(image.mask)
            vcog = vcog / weight
        else:
            vcog = domain.vcentre()
            weight = domain.numel

        vcentres.update({c_ix: vcog})
        centres.update({c_ix: domain.map_voxel_coordinates(vcog).ravel()})

        # Store component weights, which will be used to determine the
        # weighted centre of the slice that the components belong to.
        # Ignore this, because if the image resolution is not defined
        # correctly, this may lead to errors. If the multiplication by the
        # image resolution is omitted, we essentially assume that all
        # components are defined at the same resolution.
        # weights.update({c_ix: weight * image.resolution ** 2})
        weights.update({c_ix: weight})

    # Calculate weighted slice centres and weighted slice normals
    slice_indices = sorted(set([c_ix[0] for c_ix in stack.domains.keys()]))
    slice_normals = []
    slice_centres = []
    for slice_no in slice_indices:
        # Find all components that belong to the current slice
        c_indices = sorted(
            [c_ix for c_ix in stack.domains.keys() if c_ix[0] == slice_no],
            key=lambda t: t[1]
        )
        wscentre = 0
        wtotal = 0
        wnormal = 0
        for c_ix in c_indices:
            wscentre = wscentre + weights[c_ix] * centres[c_ix]
            wnormal = wnormal + weights[c_ix] * normals[c_ix]
            wtotal = wtotal + weights[c_ix]
        else:
            wscentre = wscentre / wtotal
            wnormal = wnormal / wtotal
        slice_centres.append(wscentre)
        slice_normals.append(wnormal / np.linalg.norm(wnormal))

    # Create arrays
    slice_normals = np.vstack(slice_normals)
    slice_centres = np.vstack(slice_centres)

    # Robust orientation estimation by iterative rejecting outliers
    select = np.full(shape=slice_normals.shape[0], fill_value=True)
    robust_normal = np.mean(slice_normals, axis=0).ravel()
    for i in range(cnf["regparams"]["outlier-iterations"]):
        robust_normal = np.mean(slice_normals[select], axis=0).ravel()
        robust_normal /= np.linalg.norm(robust_normal)
        logger.log(5, f"Outlier removal iteration {i + 1}, robust normal: "
                      f"{robust_normal}")
        diff = np.linalg.norm(slice_normals - robust_normal, axis=-1)
        select[diff > 2 * np.median(diff[select])] = False
    else:
        pass
        # fig = plt.figure()
        # ax = fig.add_subplot(projection='3d')
        # ax.scatter(*slice_normals.T, c="blue", marker="o")
        # ax.scatter(*robust_normal, c="red", marker="x")
        # plt.show()

    # Find the rotation angles corresponding to the robust normal
    angle = np.arccos(np.dot(z0, robust_normal.reshape(-1, 1)))
    axis = np.cross(z0, robust_normal)
    axis /= np.linalg.norm(axis)
    g_rotation = TxAxisAngle(angle, axis, mode="rad")
    euler_angles = g_rotation.euler(mode="deg", order="xyz")
    logger.info(f"Grating rotation (robust): {euler_angles} [deg]")

    # Centre of gravity
    # Slices should either carry an equal weight or be weighted by their
    # inverse registration cost to take trustworthiness into account.
    # Weighting by area is discouraged here as large but misaligned slices
    # could bias the result.
    cog = np.mean(slice_centres, axis=0)
    cog_pos = np.dot(robust_normal, np.asarray(cog).reshape(-1, 1))
    logger.info(f"Centre of gravity: {cog} [mm]")

    # Slicing gap
    y = [float(np.dot(robust_normal, c.reshape(-1, 1))) for c in slice_centres]
    result = linregress(slice_indices, y)
    gap = result.slope
    stderr = result.stderr
    rvalue = result.rvalue

    # Grating offset = centre of the 0th slice
    g_offset = np.add(
        cog, np.subtract(result.intercept, cog_pos) * robust_normal)

    logger.info(f"Effective slice gap: {gap:.04} +/- {stderr:.04} mm")
    logger.info(f"Correlation coefficient: {rvalue:.04}")
    logger.info(f"1st slice centre: {g_offset} mm")

    # Define new grating for the input slices using the estimated parameters
    chain_template, sharegroups = generate_chain_template()
    chain_template.s_rot.parameters[:] = -np.pi / 2
    chain_template.g_rot.parameters[:] = np.radians(euler_angles)
    chain_template.g_trans.parameters[:] = g_offset
    old_domains = \
        {c_ix: domain.copy() for (c_ix, domain) in stack.domains.items()}
    stack.assign(chain_template, sharegroups)

    # Initialise the component-specific chains, preparing them for fine-tuning
    for c_ix, image in stack.images.items():
        slice_no, c_no = c_ix
        stack.metadata[c_ix]["gap"] = gap
        stack.metadata[c_ix]["stderr"] = stderr
        stack.metadata[c_ix]["rvalue"] = rvalue
        factors = np.divide(image.layers[0].vshape, image.layers[1].vshape)
        image.resolution = factors * float(stack.metadata[c_ix]["Resolution"])
        chain = ChainTemplate(*image.domain.external)  # syntactic candy

        # Calculate the distance of the current slice from the first slice in mm
        s_distance = slice_no * gap
        chain.s_distance.parameters[:] = np.asarray([0, 0, s_distance])

        # Calculate 2D slice offset from stack axis intersection in mm
        slice_cog = slice_centres[slice_indices.index(slice_no)]
        axis_intersection = g_offset + s_distance * robust_normal
        s_offset3D = planproj(axis_intersection, slice_cog, robust_normal)
        inv = get_subchain(chain, chain.s_offset).inverse()
        s_offset = inv.map_vector(s_offset3D, rule="fs").ravel()
        chain.s_offset.parameters[:] = np.asarray(s_offset[:2])

        # Calculate 2D component offset from slice COG in mm
        c_offset3D = planproj(slice_cog, centres[c_ix], robust_normal)
        inv = get_subchain(chain, chain.s_cent).inverse()
        s_cent = inv.map_vector(c_offset3D, rule="fs").ravel()
        chain.s_cent.parameters[:] = np.asarray(s_cent[:2])

        # Calculate the weighted centre of the component in mm
        c_cent = image.domain.internal.map(vcentres[c_ix]).ravel()
        chain.c_cent.parameters[:] = -np.asarray(c_cent)

        # Apply base vector flips if necessary
        i_base = image.domain.map_voxel_vectors([1, 0], rule="fs")
        j_base = image.domain.map_voxel_vectors([0, 1], rule="fs")
        iprime_base = old_domains[c_ix].map_voxel_vectors([1, 0], rule="fs")
        jprime_base = old_domains[c_ix].map_voxel_vectors([0, 1], rule="fs")
        flip_i = np.sign(np.dot(i_base, iprime_base.T)).ravel()
        flip_j = np.sign(np.dot(j_base, jprime_base.T)).ravel()
        chain.c_scale.parameters[:] *= np.asarray([flip_i, flip_j]).ravel()

    return stack


def save_grating_parameters(g):
    i = 0
    while True:
        fname = os.path.join(cnf["outputdir"], f"grating-{i:02d}.txt")
        if not os.path.exists(fname):
            break
        else:
            i += 1

    with open(fname, "w") as fp:
        c_ix, domains = zip(*g.domains.items())
        chain = ChainTemplate(*domains[0].external)
        fp.write(f"Offset (0th slice, mm): {chain.g_trans.parameters[:]}\n")
        rotation_angles = chain.g_rot.euler(mode="deg", order="xyz")
        fp.write(f"Rotation (deg): {rotation_angles}\n")
        gap = np.abs(g.metadata[c_ix[0]]["gap"])  # mm
        stderr = np.abs(g.metadata[c_ix[0]]["stderr"])  # mm
        fp.write(f"Slicing gap (mm): {gap} ± {stderr}\n")
        rvalue = np.abs(g.metadata[c_ix[0]]["rvalue"])
        fp.write(f"r-value: {rvalue}")


def get_subchain(chain, fromtx):
    for ix, tx in enumerate(chain):
        if tx is fromtx:
            return Chain(chain[ix:])


def planproj(o, p, normal):
    normal = normal / np.linalg.norm(normal)
    op = p - o
    return op - np.dot(op, normal) * normal


def optimise_rigid2d_parallel(g, reference, regparams, n_cpu=1):
    """
    Fine-tunes the in-plane position and orientation of each slice component
    using the 3D image as a reference.

    :param g: input image stack
    :type g: MCSliceStack
    :param regparams: registration parameters
    :type regparams: dict

    """
    logger.info("Starting 2D rigid (in-plane) optimisation...")

    # Optimise the slice components
    if n_cpu > 1:
        with mp.Pool(processes=n_cpu) as pool:
            result = pool.map(rigid2d_worker_parallel,
                              rigid2d_jobs(g, reference, regparams))
    else:
        result = []
        for job in rigid2d_jobs(g, reference, regparams):
            result.append(rigid2d_worker_parallel(job))

    # Assign new domain to the grating
    for c_ix, d_file in result:
        domain = tirl.load(d_file)
        g.domains[c_ix] = domain
        g.images[c_ix].domain = domain
        g.images[c_ix].resmgr.sync()

    return 0


def rigid2d_jobs(g, reference, regparams):

    for c_ix, image in g.images.items():
        slice_no, c_no = c_ix
        metadata = g.metadata[c_ix]

        # Create output directory
        outdir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")
        part = g.metadata[c_ix]["Part"]
        fname = f"slice-{slice_no:04d}_part-{part}.timg"
        if not os.path.exists(outdir):
            try:
                os.makedirs(outdir)
            except Exception as exc:
                logger.exception(
                    f"ERROR while creating output directory: {outdir}")
                continue

        # Define inputs on disk
        imfile = os.path.join(outdir, fname)
        image.save(imfile, overwrite=True)

        yield c_ix, imfile, reference, regparams, metadata


def rigid2d_worker_parallel(job):
    """
    Optimises the component-specific rotation and in-plane offset.

    """
    c_ix, imfile, r_file, regparams, metadata = job

    # Load image pair
    image = tirl.load(imfile)
    reference = tirl.load(r_file)
    img_layer = image.resmgr.active
    ref_layer = reference.resmgr.active
    image.storage = MEM
    reference.storage = MEM

    # Load parameters
    params = regparams["rigid2d"]
    scales = params["scaling"]
    smoothing = params["smoothing"]
    ldx = np.asarray(params["ldx"], dtype=float)
    ldx[0] = rad(ldx[0])
    udx = np.asarray(params["udx"], dtype=float)
    udx[0] = rad(udx[0])

    # Scaling-smoothing iteration
    for sc, sm in zip(scales, smoothing):
        image.rescale(1 / sc, copy=False)
        if sm != 0:
            sm_img = image.smooth(sm, copy=True)
            sm_img.domain = image.domain
        else:
            sm_img = image
        chain = ChainTemplate(*image.domain.external)
        chain.c_offset.parameters.set_bounds(None)  # bugfix
        chain.c_rot.parameters.set_bounds(None)  # bugfix
        og = OptimisationGroup(chain.c_rot, chain.c_offset)
        lb = np.subtract(og.parameters[:], ldx)
        ub = np.add(og.parameters[:], udx)
        og.set_lower_bounds(lb)
        og.set_upper_bounds(ub)
        sm_img.storage = MEM
        try:
            cost = CostMIND(reference, sm_img)
        except:
            pass
        else:
            try:
                OptNL(og, cost, method="LN_BOBYQA", normalised=True,
                      xtol_abs=[0.001, 0.01, 0.01], step=0.1,
                      visualise=params["visualise"], logger=logger)()
            except:
                pass

        image.domain.external = sm_img.domain.external
        image.resmgr.sync()

    # Restore the input resolution
    image.resmgr.set(img_layer)
    reference.resmgr.set(ref_layer)

    # Generate output
    # (optimised domain)
    slice_no, c_no = c_ix
    slicedir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")
    part = metadata["Part"]
    d_file = os.path.join(
        slicedir, f"slice-{slice_no:04d}_part-{part}_tx-rigid2d_domain.dom")
    image.domain.save(d_file, overwrite=True)

    # (reference image resampled onto the optimised domain)
    vs = reference.evaluate(image.domain)
    vs.normalise()
    s_file = os.path.join(
        slicedir, f"slice-{slice_no:04d}_part-{part}_tx-rigid2d_mri.png")
    vs.snapshot(s_file, overwrite=True)

    return c_ix, d_file


def optimise_rigid3d_parallel(g, reference, regparams, n_cpu=1):
    """
    Fine-tunes the 3D position and orientation of each slice component
    using the 3D image as a reference.

    :param g: input image stack
    :type g: MCSliceStack
    :param regparams: registration parameters
    :type regparams: dict

    """
    logger.info("Starting 3D rigid optimisation...")

    if n_cpu > 1:
        with mp.Pool(processes=n_cpu) as pool:
            result = pool.map(rigid3d_worker_parallel,
                              rigid3d_jobs(g, reference, regparams))
    else:
        result = []
        for job in rigid3d_jobs(g, reference, regparams):
            result.append(rigid3d_worker_parallel(job))

    # Assign the new image to the grating
    for c_ix, d_file in result:
        domain = tirl.load(d_file)
        g.domains[c_ix] = domain
        g.images[c_ix].domain = domain
        g.images[c_ix].resmgr.sync()

    return 0


def rigid3d_jobs(g, reference, regparams):

    for c_ix, image in g.images.items():
        slice_no, c_no = c_ix
        metadata = g.metadata[c_ix]

        # Create output directory
        outdir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")
        part = g.metadata[c_ix]["Part"]
        fname = f"slice-{slice_no:04d}_part-{part}.timg"
        if not os.path.exists(outdir):
            try:
                os.makedirs(outdir)
            except Exception as exc:
                logger.exception(
                    f"ERROR while creating output directory: {outdir}")
                continue

        # Define inputs on disk
        imfile = os.path.join(outdir, fname)
        image.save(imfile, overwrite=True)

        yield c_ix, imfile, reference, regparams, metadata


def rigid3d_worker_parallel(job):
    """
    Optimises the component-specific 3D position and orientation.

    """
    c_ix, imfile, r_file, regparams, metadata = job

    # Load image pair
    image = tirl.load(imfile)
    reference = tirl.load(r_file)
    img_layer = image.resmgr.active
    ref_layer = reference.resmgr.active
    image.storage = MEM
    reference.storage = MEM

    # Load parameters
    params = regparams["rigid3d"]
    scales = params["scaling"]
    smoothing = params["smoothing"]
    ldx = np.asarray(params["ldx"], dtype=float)
    udx = np.asarray(params["udx"], dtype=float)

    # Add 3D rigid adjustment
    centre = image.centre(weighted=True)
    offset = TxTranslation(*centre)
    rotation = TxEulerAngles(0, 0, 0, order="xyz", mode="deg")
    image.domain.external += [-offset, rotation, offset]

    # Scaling-smoothing iteration
    for sc, sm in zip(scales, smoothing):
        image.rescale(1 / sc)
        if sm != 0:
            sm_newimg = image.smooth(sm, copy=True)
            sm_newimg.domain = image.domain
        else:
            sm_newimg = image
        sm_newimg.storage = MEM
        rotation, offset = sm_newimg.domain.external[-2:]
        og = OptimisationGroup(rotation, offset)
        logger.log(5, str(og.parameters[:]))
        l_rot = np.subtract(rotation.parameters[:], np.radians(ldx[:3]))
        u_rot = np.add(rotation.parameters[:], np.radians(udx[:3]))
        l_offset = np.subtract(offset.parameters[:], [*ldx[3:]])
        u_offset = np.add(offset.parameters[:], [*udx[3:]])
        og.set_lower_bounds(np.asarray([*l_rot, *l_offset]))
        og.set_upper_bounds(np.asarray([*u_rot, *u_offset]))
        cost = CostMIND(reference, sm_newimg)
        OptNL(og, cost, method="LN_BOBYQA", normalised=True,
              xtol_abs=[0.01, 0.01, 0.01, 0.01, 0.01, 0.01],
              step=0.5, logger=logger, visualise=params["visualise"])()

        image.domain.external = sm_newimg.domain.external
        image.resmgr.sync()

    # Restore the input resolution
    image.resmgr.set(img_layer)
    reference.resmgr.set(ref_layer)

    # Generate output
    # (optimised domain)
    slice_no, c_no = c_ix
    slicedir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")
    part = metadata["Part"]
    d_file = os.path.join(
        slicedir, f"slice-{slice_no:04d}_part-{part}_tx-rigid3d_domain.dom")
    image.domain.save(d_file, overwrite=True)

    # (reference image resampled onto the optimised domain)
    vs = reference.evaluate(image.domain)
    vs.normalise()
    s_file = os.path.join(
        slicedir, f"slice-{slice_no:04d}_part-{part}_tx-rigid3d_mri.png")
    vs.snapshot(s_file, overwrite=True)

    return c_ix, d_file


def get_3D_rotation(domain):
    rotations = []
    for tx in domain.external:
        if isinstance(tx, TxRotation3D):
            rotations.append(tx)
    else:
        return Chain(rotations)


def warp_parallel(g, reference, regparams, n_cpu=1):
    """
    Performs in-plane non-linear adjustment on a stack of pre-registered slices.

    :param g: Stack of pre-registered slices.
    :type g: MCSliceStack
    :param regparams: Registration parameters.
    :type regparams: dict
    :param n_cpu: number of parallel processing cores
    :type n_cpu: int

    """
    logger.info("Starting in-plane warping...")

    # Warp the slices
    if n_cpu > 1:
        with mp.Pool(processes=n_cpu, maxtasksperchild=1) as pool:
            result = pool.map(warp_worker, warp_jobs(g, reference, regparams))
    else:
        result = []
        for job in warp_jobs(g, reference, regparams):
            result.append(warp_worker(job))

    # Assign newly optimised domains
    for c_ix, d_file in result:
        domain = tirl.load(d_file)
        g.domains[c_ix] = domain
        g.images[c_ix].domain = domain
        g.images[c_ix].resmgr.sync()

    return 0


def warp_jobs(g, reference, regparams):
    for c_ix, image in g.images.items():
        slice_no, c_no = c_ix
        metadata = g.metadata[c_ix]
        outdir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")
        part = g.metadata[c_ix]["Part"]
        fname = f"slice-{slice_no:04d}_part-{part}.timg"
        if not os.path.exists(outdir):
            try:
                os.makedirs(outdir)
            except Exception as exc:
                logger.exception(
                    f"ERROR while creating output directory: {outdir}")
                continue
        imfile = os.path.join(outdir, fname)
        image.save(imfile, overwrite=True)
        yield c_ix, imfile, reference, regparams, metadata


def warp_worker(job):
    """
    Warps a slice/component within the slicing plane.

    """
    c_ix, imfile, r_file, params, metadata = job
    slice_no, c_no = c_ix
    part = metadata["Part"]
    t = AttrMap(params)
    slicedir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")

    # Load images from disk
    img = tirl.load(imfile)
    vol = tirl.load(r_file)
    img_layer = img.resmgr.active
    vol_layer = vol.resmgr.active

    # Smoothing iteration
    stage_cost = []

    # Scaling-smoothing iterations
    warp = None
    states = tuple(zip(t.slice_scaling, t.volume_scaling,
                       t.slice_smoothing, t.volume_smoothing))

    for sm_no, (ssc, vsc, ssm, vsm) in enumerate(states):
        logger.info(f"Smoothing: ssc={ssc}, ssm={ssm}px, "
                    f"vsc={vsc}, vsm={vsm}vx.")
        img.rescale(1. / ssc, copy=False)
        img.storage = MEM
        img_smooth = img.smooth(ssm, copy=True)
        vol.rescale(1. / vsc, copy=False)
        vol.storage = MEM
        if vsm != 0:
            vol_smooth = vol.smooth(vsm, copy=True)
        else:
            vol_smooth = vol
        cost = create_cost_object(vol_smooth, img_smooth, cnf)
        logger.info(f"Initial cost: {cost()}")

        # How many support points?
        # Read from configuration file.
        if isinstance(t.support.points, int):
            n_points = t.support.points
            calculate_points = True
            points_iter = None
        elif isinstance(t.support.points, (list, tuple)):
            n_points = len(t.support.points)
            points_iter = t.support.points
            calculate_points = True
        # Read from external file
        elif isinstance(t.support.points, str) and \
                os.path.isfile(t.support.points):
            xi = np.loadtxt(t.support.points)
            n_points = xi.shape[0]
            calculate_points = False
            points_iter = None
        # Invalid input
        else:
            raise ValueError("Invalid support point specification.")

        # How many points should be optimised at once?
        # Optimise all points for any subsequent iteration, as little benefit
        # is anticipated from repeating the whole optimisation sequence.
        if sm_no == 0:
            optsize = int(min(int(t.optsize), n_points))
            points_iter = points_iter or \
                (*range(optsize, n_points, optsize), n_points)
        else:
            points_iter = (n_points,)

        # ---> POINT OPTIMISATION SEQUENCE
        for n_current in points_iter:

            # --------------------- CREATE NEW WARP -------------------- #

            # Create new point set
            if calculate_points:
                xi_current = calc_km_control_points(n_current, img)
            else:
                xi_current = xi[:n_current]

            # Create new transformation with the old and the new points
            newwarp = create_warp_from_points(img, xi_current, t, slicedir)

            # If a transformation was previously optimised, fit the parameters
            # of the new transformation to recreate the displacement field.
            # This carries forward the information from previous optimisations.
            if warp is not None:
                newwarp.fit(warp.std(3).absolute().field())

            warp = newwarp

            # Update transformation parameter bounds
            params = warp.parameters[:]
            params = params.reshape(len(t.vectorder), -1)
            lb = params.copy()
            ub = params.copy()
            if t.vectorder == "xyz":
                lb[:2] -= float(t.lower_dxy)
                lb[2] -= float(t.lower_dz)
                ub[:2] += float(t.upper_dxy)
                ub[2] += float(t.upper_dz)
            elif t.vectorder == "xy":
                lb[:] -= float(t.lower_dxy)
                ub[:] += float(t.upper_dxy)
            elif t.vectorder == "z":
                lb[:] -= float(t.lower_dz)
                ub[:] += float(t.upper_dz)

            if NORMALISE:
                initial_step = t.opt_step
            else:
                initial_step = absolute_step(lb.ravel(), ub.ravel(), t.opt_step)
            warp.parameters.set_bounds(lb.ravel(), ub.ravel())
            warp.parameters.unlock()

            # Add warp to the chain
            chain = ChainTemplate(*img_smooth.domain.external)
            img_smooth.domain.chain[chain.index(chain.s_warp)] = warp
            # Recreate cost object, because the chain was modified
            cost = create_cost_object(vol_smooth, img_smooth, cnf)

            # --------------------- OPTIMISE NEW WARP -------------------- #

            # Regularisation
            if t.regweight > 0:
                reg = RegBendingEnergy(warp, weight=t.regweight, logger=logger)
                # reg = NormTxRegulariser(warp, order=2, weight=t.regweight)
                cost_items = [cost, reg]
            else:
                cost_items = [cost]

            # Optimisation
            logger.debug(f"Optimising the following control points:"
                         f"\n{warp.domain.get_voxel_coordinates()}")
            OptNL(warp, *cost_items, method=OptMethods.LN_BOBYQA,
                  step=initial_step, xtol_abs=t.xtol_abs, logger=logger,
                  visualise=t.visualise, normalised=NORMALISE)()

            # Create snapshot
            timestamp = datetime.now().strftime("%Y%m%d_%H%M%S_%f")
            fn = os.path.join(
                slicedir,
                f"slice-{slice_no:04d}_part-{part}_tx-warp_"
                f"{n_current}pts_{timestamp}."
                f"{ts.TIMAGE_DEFAULT_SNAPSHOT_EXT}")
            vol_smooth.evaluate(img_smooth.domain).snapshot(fn, overwrite=True)

        # <--- POINT OPTIMISATION SEQUENCE

        # Record the final cost at the current smoothing level
        if sm_no + 1 == len(states):
            stage_cost.append(cost())
            logger.info(f"Final cost @ [ssm={ssm}px vsm={vsm}vx]: "
                        f"{stage_cost[-1]}")

        # Copy optimised transformations before creating a new image from
        # the original. [This may no longer be necessary.]
        img.domain = img_smooth.domain
        img.resmgr.sync()

    # Restore the input resolution
    img.resmgr.set(img_layer)
    vol.resmgr.set(vol_layer)

    # Generate output
    # (optimised domain)
    d_file = os.path.join(
        slicedir, f"slice-{slice_no:04d}-part-{part}_tx-warp_domain.dom")
    img.domain.save(d_file, overwrite=True)

    # (reference image resampled onto the optimised domain)
    vs = vol.evaluate(img.domain)
    vs.normalise()
    s_file = os.path.join(
        slicedir, f"slice-{slice_no:04d}-part-{part}_tx-warp_mri.png")
    vs.snapshot(s_file, overwrite=True)

    return c_ix, d_file


def create_cost_object(source, target, cnf):
    """
    Creates a script-wide uniform cost object based on the general cost
    function type setting.

    """
    p = AttrMap(cnf)

    if str(p.cost).lower() == "mind":
        return CostMIND(source, target, normalise=True, sigma=1.0,
                        truncate=1.5, kernel=MK_STAR)
    elif str(p.cost).lower() == "mi":
        return CostMI(source, target, normalise=True, bins=32)
    else:
        raise ValueError(f"Unrecognised cost function specification: "
                         f"{p.general.cost}")


def absolute_step(lb, ub, step):
    di = ub - lb
    di[~np.isfinite(di)] = 2
    if np.any(di <= 0):
        raise AssertionError("No space for optimisation.")
    return np.multiply(0.5 * di, step)


def calc_km_control_points(n_new, img):
    """
    Uses k-means clustering to generate an exact number of tiles,
    distributed evenly and compactly across the ROI of the target image, as
    defined by a mask.

    :param n_new: number of new points
    :type n_new: int
    :param img: target image
    :type img: TImage

    :returns: (n_points, 2) voxel coordinate array of control points
    :rtype: np.ndarray

    """
    from sklearn.cluster import KMeans
    from tirlscripts.oxford.scriptutils.image import get_mask

    logger = logging.getLogger(LOGGER)

    # Define ROI for the cost map
    tmp = img.copy().tensors.reduce()
    tmp.normalise()
    mask = get_mask(tmp, scope=globals())
    mask = TImage(mask).resize(*tmp.domain.shape, copy=True).data
    coords = np.vstack(np.where(mask > 0.5)).T
    n_points = n_new
    km = KMeans(n_clusters=n_points, random_state=0)
    km.fit(coords)
    xi = km.cluster_centers_
    logger.info(f"Created {xi.shape[0]} new control points: \n{xi}")

    # Measure the average distance between segment centroids
    edges = np.max(xi, axis=0) - np.min(xi, axis=0)
    edges = edges[np.nonzero(edges)]
    mean_distance = np.power(np.prod(edges) / xi.shape[0], 1. / edges.size)
    logger.info(f"Mean control point distance: {mean_distance} px")

    # Return the voxel coordinates of the sorted control points
    return xi


def create_warp_from_points(img, points, t, outdir):
    """
    Creates empty warp field based on the voxel coordinates of the support
    points on the fixed image domain.

    """
    dim = len(t.vectorder)
    chain = ChainTemplate(*img.domain.external)
    ix = chain.index(chain.g_embed)
    assert chain[ix] is chain.g_embed
    domain = Domain(points, transformations=img.domain.chain[:ix + 1],
                    offset=img.domain.offset)
    field = TField(domain=domain, tshape=(dim,), order=TENSOR_MAJOR,
                   dtype=ts.DEFAULT_FLOAT_TYPE, fill_value=0)

    if t.export.control_points:
        timestamp = datetime.now().strftime("%Y%m%d_%H%M%S_%f")
        fn = os.path.join(outdir, f"control_points_{timestamp}.png")
        np.savetxt(fn.replace(".png", ".txt"), points)
        save_control_point_image(img, points, fn)

    # Define bounds
    if t.vectorder == "xyz":
        lbxy = float(t.lower_dxy) * np.ones((2, field.domain.numel))
        lbz = float(t.lower_dz) * np.ones((1, field.domain.numel))
        ubxy = float(t.upper_dxy) * np.ones((2, field.domain.numel))
        ubz = float(t.upper_dz) * np.ones((1, field.domain.numel))
        lb = -np.concatenate((lbxy, lbz), axis=0).ravel()
        ub = np.concatenate((ubxy, ubz), axis=0).ravel()
    elif t.vectorder == "xy":
        lb = -float(t.lower_dxy) * np.ones((2, field.domain.numel))
        ub = float(t.upper_dxy) * np.ones((2, field.domain.numel))
    elif t.vectorder == "z":
        lb = -float(t.lower_dz) * np.ones(field.domain.numel)
        ub = float(t.upper_dz) * np.ones(field.domain.numel)
    else:
        raise ValueError(f"Invalid vector order: {t.vectorder}")

    # Create warp
    tx_warp = TxRbfDisplacementField(
        field, bounds=(lb, ub), name="warp", invertible=False,
        mode=NL_ABS, vectorder=t.vectorder, dense_shape=img.domain.shape)
    tx_warp.interpolator.model = t.model

    return tx_warp


def save_control_point_image(photo, cpoints, filename):
    logger = logging.getLogger(LOGGER)
    # Creating the figure requires a window management system, so check this
    # first, and change the backend accordingly if it cannot be found.
    from tirl.utils import to_img
    if not os.getenv("DISPLAY") or not ts.ENABLE_VISUALISATION:
        import matplotlib
        matplotlib.use("agg")
    else:
        import matplotlib
        matplotlib.use(ts.MPL_BACKEND)
    import matplotlib.pyplot as plt

    # Create figure with photo and control points
    fig = plt.figure()
    ax = fig.add_subplot(111)
    order = photo.order
    photo.order = VOXEL_MAJOR
    if photo.tshape == ():
        ax.imshow(to_img(photo.data), cmap='gray', zorder=1)
    else:
        ax.imshow(to_img(photo.data), zorder=1)
    photo.order = order
    y, x = cpoints.T
    ax.scatter(x, y, c="magenta", zorder=2)

    # Save figure
    dot_file = os.path.join(filename)
    try:
        fig.savefig(dot_file)
    except Exception as exc:
        logger.error(exc.args)
        logger.info("Control point image could not be saved due to an error.")
    else:
        logger.info(f"Saved control point figure: {filename}")


def create_parser():
    """
    Creates argument parser object.

    """
    parser = argparse.ArgumentParser(
        prog="multislice_to_volume",
        description="Registers parallel sections to a volume.")
    parser.add_argument(
        "--config", metavar="<*.yml>", type=str, required=True,
        help="Configuration file.")
    parser.add_argument(
        "--verbose", action="store_true", default=False, required=False,
        help="Logs to STDOUT.")
    return parser


# PROGRAM FLOW CONTROL

parser = create_parser()

if len(sys.argv) > 1:

    # Load command-line arguments and configurations
    args = parser.parse_args()
    with open(args.config, "r") as fp:
        cnf = dict(yaml.safe_load(fp))

    # Override verbose setting from the CLI
    if args.verbose:
        cnf["verbose"] = args.verbose

    logger = create_logger(LOGGER)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main()
    else:
        parser.print_help()
