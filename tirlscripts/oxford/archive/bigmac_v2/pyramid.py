#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

"""
pyramid.py --

Creates a pyramidal TImage file from an input image, an image mask, and
precomputed slice-to-volume transformation chain.

"""


__tirlscript__ = "bigmac.pyramid"


# DEPENDENCIES

import os
import sys
import yaml
import argparse
from base64 import b64decode


# TIRL IMPORTS

import tirl
import tirl.settings as ts
from tirl.timage import TImage


# DEFINITIONS

from tirl.constants import *


# IMPLEMENTATION

def main(args):
    """ Main program code. """

    if args.code:
        code = str(args.code).strip("\"\'")
        code = b64decode(code.encode())
        job = yaml.safe_load(code)
    elif args.columns and args.record:
        if len(args.columns) != len(args.record):
            raise AssertionError(
                "The input record does not match the header specification.")
        job = dict(zip(args.columns, args.record))
    else:
        raise AssertionError("Invalid arguments.")

    scales = args.rescale if args.rescale else ()
    index, imfile = process(job, scales, args.output, args.compressed)
    print(f"{index}, {imfile}")


def process(record, scales, output, compressed):

    image_name = "-".join([
        record["Capture"],
        record["Part"],
        record["Registration"]])

    reg = tirl.load(record["SliceRegistered"])
    img = TImage(record["SliceOriginal"], storage=MEM, name=image_name)
    img.resolution = float(record["Resolution"])
    img.mask = TImage(record["SliceMask"], dtype="u8")
    for scale in sorted(scales):
        img.rescale(1 / scale, copy=False)
    else:
        img.resmgr.sync()
        img.resmgr.set(0)
        img.maskmgr.set(0)
    #img.resize(*reg.vshape, copy=False)
    img.domain.external = reg.domain.external

    # Account for TIRL 2.1 vs TIRL>=3.1 differences (specific to BigMad)
    adjust_chain(img)

    # Set metadata
    img.header["metadata"] = record

    # Save pyramidal image to file
    ext = "." + ts.EXTENSIONS["TImage"]
    if not output:
        outdir = os.path.dirname(record["SliceRegistered"])
        outfile = os.path.join(outdir, image_name + ext)
    else:
        output = str(output).strip("\"\'").rstrip(os.sep)
        if os.path.isdir(output):
            outfile = os.path.join(output, image_name + ext)
        else:
            outfile = output

    outdir = os.path.dirname(outfile)
    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    # Make sure that the file extensions are correct if the output is meant to
    # be saved as a compressed TImage.
    if compressed:
        if not str(outfile).lower().endswith(".gz"):
            outfile = outfile + ".gz"

    img.save(outfile, overwrite=True)
    # print(f"Saved: {outfile}", record["Slice"], outfile)

    return record["Slice"], outfile#


def adjust_chain(img):
    """
    This is to accommodate the difference between TIRL 2.1 and TIRL>=3. With
    the former, the image resolution was usually set in the external part of
    the chain, and if any transformation preceded it (e.g. flipping), it is not
    possible to represent this without recomputing the transformations. So to
    maintain compatibility, this function undoes the resolution setting at the
    beginning of the external chain.

    """
    res = img.domain.internal["resolution"]
    img.domain.external.prepend(res.inverse())


def create_parser():

    parser = argparse.ArgumentParser(prog="pyramid.py")
    parser.add_argument("--record", nargs="+", type=str, required=False,
                        help="One row of the input table.")
    parser.add_argument("--columns", nargs="+", type=str, required=False,
                        help="Header of the input table.")
    parser.add_argument("--rescale", metavar="factor", nargs="+", type=float,
                        help="Inverse scaling factors, i.e. 2 -> 1/2",
                        required=False)
    parser.add_argument("--code", metavar="<str>", type=str,
                        help="Base-64 encoded input (for scripting)",
                        required=False)
    parser.add_argument("--output", metavar="<output.timg>", type=str,
                        help="Output file", required=False)
    parser.add_argument("--compressed", action="store_true", default=False,
                        help="Create .timg.gz compressed output.")
    return parser


if __name__ == "__main__":

    parser = create_parser()
    if len(sys.argv) > 1:
        main(parser.parse_args())
    else:
        parser.print_help()
