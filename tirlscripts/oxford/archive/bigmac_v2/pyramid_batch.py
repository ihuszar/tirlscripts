#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

"""
pyramid_batch.py --

Creates pyramidal TImage files from a table of original images, image masks,
and precomputed slice-to-volume transformations. Exports an indexed list of the
new pyramidal TImage files, which can be used as input for the
multislice_to_volume registration script.

"""


__tirlscript__ = "bigmac.pyramid_batch"


# DEVELOPMENT NOTES
import numpy as np

"""
2022-Oct-9 INH:
Redeveloped to add a command-line interface.

2021-Jul-20 INH:
This script precomputes the low-resolution version of the masked PLI slides, so
that the multislice registration does not need to spend time on this. The main
motivation is development and debugging, as it currently takes too long to run
the multislice script.

"""


# DEPENDENCIES

import re
import os
import sys
import yaml
import argparse
import subprocess
import numpy as np
import pandas as pd
from base64 import b64encode
import multiprocessing as mp
from functools import partial
import multiprocessing.dummy as mt


# TIRL IMPORTS


# DEFINITIONS

from tirlscripts.oxford.bigmac.constants import *
PYRAMID_SCRIPT = os.path.join(os.path.dirname(__file__), "pyramid.py")


# IMPLEMENTATION

def main(args):
    """ Main program code. """

    table = pd.read_csv(args.table)
    n_cpu = int(args.cpu) % mp.cpu_count()
    jobs = get_jobs(
        table,
        scheduler=args.scheduler,
        queue=args.queue,
        outputdir=args.output,
        scales=args.rescale,
        compressed=args.compressed
    )

    sys.stdout.write("Slice,ImageFile\n")
    with mt.Pool(processes=n_cpu) as pool:
        exitcodes = pool.map(partial(worker, scheduler=args.scheduler), jobs)

    err = np.count_nonzero(np.asarray(exitcodes))
    if err == 0:
        sys.stderr.write("The program completed successfully.\n")
    else:
        sys.stderr.write(f"The program completed with {err} error(s).\n")


def get_jobs(table, scheduler=DEFAULT_SCHEDULER, queue=DEFAULT_QUEUE,
             outputdir=None, scales=None, compressed=False):
    prefix = get_cmd_prefix(scheduler=scheduler, queue=queue)
    scalesstr = [str(sc) for sc in scales]
    if outputdir is not None:
        if not os.path.isdir(outputdir):
            os.makedirs(outputdir)
    for ix, record in table.iterrows():
        codestr = b64encode(yaml.safe_dump(dict(record)).encode()).decode()
        cmd = prefix + ["tirl", PYRAMID_SCRIPT, f"--code=\"{codestr}\""]
        if outputdir is not None:
            cmd.extend(["--output", f"\"{outputdir}\""])
        if scales is not None:
            cmd.extend(["--rescale", *scalesstr])
        if compressed:
            cmd.extend(["--compressed"])
        yield cmd


def worker(cmd, scheduler):
    import re
    import os
    scheduler = str(scheduler).lower() if scheduler else None
    proc = subprocess.run(cmd, env=os.environ.copy(), capture_output=True)
    if proc.returncode == 0:
        if scheduler == "slurm":
            jobid = re.findall(r"\d+", proc.stdout.decode(), re.MULTILINE)[0]
            with open(f"slurm-{jobid}.out", "r") as fp:
                stdout = fp.read()
        elif scheduler == "sge":
            # Find the latest file with this subjobid.
            # While this is error prone, there is no way for this process
            # to find out its own SGE job ID.
            subjobid = re.findall(r"\d+", proc.stdout.decode())[0]
            pattern = f"tirl.o\d+\-{subjobid}"
            jobfiles = [f for f in os.listdir() if re.match(pattern, f)]
            outfile = sorted(jobfiles)[-1]
            with open(outfile, "r") as fp:
                stdout = fp.read()
        else:
            stdout = proc.stdout.decode()

        # Generate output if the script did not terminate with an error
        try:
            index, pyramid_file = stdout.split(",")
        except ValueError:
            sys.stderr.write(f"Unrecognised output from worker: {stdout}")
        else:
            sys.stdout.write(f"{index.strip()},{pyramid_file.strip()}\n")
            return 0

    # Generate output if the script terminated with an error
    cmdstr = " ".join(cmd)
    sys.stderr.write(f"Command failed: {cmdstr}\n")
    return int(proc.returncode)


def get_cmd_prefix(scheduler, **kwargs):
    scheduler = str(scheduler).lower() if scheduler else None
    if scheduler == "sge":
        queue = kwargs.get("queue", DEFAULT_QUEUE)
        prefix = [f"fsl_sub", f"-q {queue}", f"--extra=\"-sync y\""]
    elif scheduler == "slurm":
        prefix = [
            f"sbatch",
            "-W",
            "-o", r"slurm-%A.out",
            "-e", r"slurm-%A.err"
        ]
    elif scheduler is None:
        prefix = [f"python"]
    else:
        raise NameError(f"Unrecognised task scheduler: {scheduler}")
    return prefix


def create_parser():
    parser = argparse.ArgumentParser(prog="pyramid_batch.py")
    parser.add_argument("--table", metavar="<input_table.csv>", type=str,
                        help="Table of input slices.")
    parser.add_argument("--rescale", metavar="factor", nargs="+", type=float,
                        help="Inverse scaling factors, i.e. 2 -> 1/2",
                        required=False)
    parser.add_argument("--cpu", metavar="<int>", default=1, type=int,
                        help="Number of CPU cores to use.")
    parser.add_argument("--scheduler", metavar="<str>",
                        default=DEFAULT_SCHEDULER,
                        help="Task scheduler: sge, slurm, or leave empty.")
    parser.add_argument("--queue", metavar="<str>", type=str,
                        default=DEFAULT_QUEUE,
                        help="Name of the SGE job submission queue.")
    parser.add_argument("--output", metavar="<dir>", default=None,
                        help="Output directory")
    parser.add_argument("--compressed", action="store_true", default=False,
                        help="Create .timg.gz compressed output.")

    return parser


if __name__ == "__main__":

    parser = create_parser()

    if len(sys.argv) > 1:
        main(parser.parse_args())
    else:
        parser.print_help()
