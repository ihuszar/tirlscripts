#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

"""

"""


__tirlscript__ = "bigmac.recon"


# DEPENDENCIES

import os
import re
import numpy as np
import nibabel as nib


# TIRL IMPORTS

import tirl
from tirl.timage import TImage
from scipy.interpolate import griddata


# DEFINITIONS

MRI = "/Users/inhuszar/BigMac/MRI/Postmortem/struct/data/" \
      "struct_bias_restore.nii.gz"
ROOTDIR = "/Users/inhuszar/BigMac/Microscopy/PLI/Posterior/multislice-reg"


# IMPLEMENTATION


def main():
    vol = TImage(MRI, external="sform", name="mri")
    mm2vox = vol.domain.chain.inverse()

    # Locate registered domains
    domainfiles = []
    for root, dirs, files in os.walk(ROOTDIR):
        for f in files:
            if f.endswith(".dom") and ("warp" in f):
                domainfiles.append(os.path.join(root, f))

    known_points = []
    data = []

    # Collect available microscopy data from the registered components
    for d_file in domainfiles:
        fpath, fname = os.path.split(d_file)
        slice_no = int(re.findall(r"(?<=slice-)\d+", fname)[0])
        c_no = int(re.findall(r"(?<=cmp-)\d+", fname)[0])
        imfile = os.path.join(fpath, f"slice-{slice_no:04d}_cmp-{c_no}.timg")
        image = tirl.load(imfile)
        domain = tirl.load(d_file)
        image.resize(*domain.shape)
        image.domain = domain
        image.resmgr.sync()
        print(f"Slice {slice_no}, Component {c_no}: Domain: {domain.shape}")
        mask = image.mask
        if mask is not None:
            mask = image.mask.ravel() > 0
        vc = mm2vox.map(domain.get_physical_coordinates())[mask, :]
        known_points.append(vc)
        data.append(image.data.ravel()[mask])

    x, y, z = np.vstack(known_points).T
    data = np.concatenate(data)
    xi, yi, zi = np.meshgrid(*tuple(range(dim) for dim in vol.vshape),
                             indexing="ij")

    # Do the interpolation
    interpolants = griddata((x, y, z), data, (xi, yi, zi), fill_value=0)
    pli = interpolants.reshape(vol.data.shape)
    mri_nifti = nib.load(MRI)
    hdr = mri_nifti.header
    pli_nifti = nib.Nifti1Image(pli, affine=hdr.get_best_affine(), header=hdr)
    nib.save(pli_nifti, os.path.join(ROOTDIR, "pli_posterior_recon3D.nii.gz"))


if __name__ == "__main__":
    main()
