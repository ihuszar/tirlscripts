#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2021 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Updated: 7 October 2022


# SHBASECOPYRIGHT


# DESCRIPTION

"""
rigid2d.py --

This is a satellite script for multislice_to_volume.py that performs the
2D rigid registration step on a single slice. The satellite script allows the
parallel execution of this registration step on a computing cluster, where
jobs are managed by a scheduler.

"""


# DEPENDENCIES

import os
import sys
import dill
import logging
import numpy as np
import multiprocessing as mp
from math import radians as rad


# TIRL IMPORTS

import tirl
import tirl.settings as ts
from tirl.constants import *
from tirl.cost.mind import CostMIND
from tirl.optimisation.optnl import OptNL, OptMethods
from tirl.optimisation.optgroup import OptimisationGroup


# TIRSCRIPT IMPORTS

from tirlscripts.oxford.bigmac.constants import *


# DEFINITIONS

np.set_printoptions(precision=4, suppress=True)
LOGGER = None
logger = logging.getLogger(LOGGER)


# IMPLEMENTATION

def create_logger(loggername, logfile, cnf):
    """
    Creates a logger that sends log messages to the standard output and
    into a file.

    """
    global logger
    logger = logging.getLogger(loggername)
    logger.handlers = []  # delete any existing handlers to avoid duplicate logs
    logger.setLevel(1)
    formatter = logging.Formatter(fmt=LOGFORMAT, datefmt=DATEFORMAT)

    # Make sure that the logfile can be created
    logdir = os.path.dirname(os.path.abspath(logfile))
    if not os.path.isdir(logdir):
        os.makedirs(logdir)

    # Redirect all logs of interest to the logfile
    loglevel = cnf["loglevel"]
    fh = logging.FileHandler(logfile, mode="a", delay=False)
    if isinstance(loglevel, str):
        level = getattr(logging, loglevel.upper())
    elif isinstance(loglevel, (int, float)):
        level = int(loglevel)
    elif not loglevel:
        level = logging.INFO
    else:
        raise TypeError(f"Invalid logging level: {loglevel}")
    fh.setLevel(level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Redirect only warnings/errors to the standard output, unless "verbose"
    ch = logging.StreamHandler()
    if cnf["verbose"]:
        ch.setLevel(level)
    else:
        ch.setLevel(logging.WARNING)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


def load_image(imfile):
    image = tirl.load(imfile)
    slice_no = image.header["metadata"]["Slice"]
    part = image.header["metadata"]["Part"]
    ext = ts.EXTENSIONS["Domain"]
    dname = f"slice-{slice_no:04d}_part-{part}_latest_domain.{ext}"
    latest = os.path.join(os.path.dirname(imfile), dname)
    image.domain = tirl.load(latest)
    image.resmgr.sync()
    return image


def rigid2d_worker(job, outputfile):
    """
    Optimises the component-specific rotation and in-plane offset.

    """
    c_ix, imfile, r_file, metadata, cnf = job

    # Load image pair
    image = load_image(imfile)
    reference = tirl.load(r_file)
    img_layer = image.resmgr.active
    ref_layer = reference.resmgr.active
    # reference.storage = MEM

    # Load parameters
    params = cnf["regparams"]["rigid2d"]
    resolutions = params["resolution"]
    smoothing = params["smoothing"]
    ldx = np.asarray(params["ldx"], dtype=float)
    ldx[0] = rad(ldx[0])
    udx = np.asarray(params["udx"], dtype=float)
    udx[0] = rad(udx[0])

    # Scaling-smoothing iteration
    for resolution, sm in zip(resolutions, smoothing):
        resolution = image.resolution if resolution is None else resolution
        scaling_factors = np.divide(image.layers[0].resolution, resolution)
        sc = np.abs(np.mean(scaling_factors))
        logger.debug(f"Slice scaling factor: 1 : {1 / sc}")
        image.rescale(sc, copy=False)
        image.storage = MEM
        if sm != 0:
            sm_img = image.smooth(sm, copy=True)
            sm_img.domain = image.domain
        else:
            sm_img = image
        chain = ChainTemplate(*image.domain.external)
        chain.c_offset.parameters.set_bounds(None)  # bugfix
        chain.c_rot.parameters.set_bounds(None)  # bugfix
        chain.c_scale.parameters.set_bounds(None)
        og = OptimisationGroup(chain.c_scale, chain.c_rot, chain.c_offset)
        lb = np.subtract(og.parameters[:], ldx)
        ub = np.add(og.parameters[:], udx)
        og.set_lower_bounds(lb)
        og.set_upper_bounds(ub)
        sm_img.storage = MEM
        try:
            cost = CostMIND(reference, sm_img)
        except Exception:
            pass
        else:
            try:
                OptNL(og, cost, method=OptMethods.LN_BOBYQA, normalised=True,
                      xtol_abs=[0.001, 0.001, 0.001, 0.01, 0.01], step=0.1,
                      visualise=params["visualise"], logger=logger)()
            except Exception:
                pass

        image.domain.external = sm_img.domain.external
        image.resmgr.sync()

    # Restore the input resolution
    image.resmgr.set(img_layer)
    reference.resmgr.set(ref_layer)

    # Generate output
    # (optimised domain)
    slice_no, c_no = c_ix
    slicedir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")
    part = metadata["Part"]
    image.domain.save(outputfile, overwrite=True)

    # (reference image resampled onto the optimised domain)
    vs = reference.evaluate(image.domain)
    vs.normalise()
    s_file = os.path.join(
        slicedir, f"slice-{slice_no:04d}_part-{part}_tx-rigid2d_mri.png")
    vs.snapshot(s_file, overwrite=True)

    return c_ix, outputfile


def main(jobfile, outputfile, logfile):
    """ Main program code. """

    with open(jobfile, "rb") as fp:
        job = dill.load(fp)

    c_ix, imfile, reference, metadata, cnf = job
    slice_no, c_no = c_ix
    part = metadata["Part"]
    pid = mp.current_process().pid

    global LOGGER
    LOGGER = f"slice-{slice_no}_part-{part}_tx-rigid2d_proc-{pid}"
    create_logger(LOGGER, logfile, cnf)
    cmd = " ".join(sys.argv)
    logger.info(f"The program started with the following command: {cmd}")
    rigid2d_worker(job, outputfile)


if __name__ == "__main__":
    main(*sys.argv[1:])
