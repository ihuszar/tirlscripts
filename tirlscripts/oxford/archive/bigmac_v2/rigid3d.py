#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2021 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Updated: 7 October 2022


# SHBASECOPYRIGHT


# DESCRIPTION

"""
rigid3d.py --

This is a satellite script for multislice_to_volume.py that performs the
3D rigid registration step on a single slice. The satellite script allows the
parallel execution of this registration step on a computing cluster, where
jobs are managed by a scheduler.

"""


# DEPENDENCIES

import os
import sys
import dill
import logging
import numpy as np
import multiprocessing as mp


# TIRL IMPORTS

import tirl
import tirl.settings as ts
from tirl.constants import *
from tirl.cost.mind import CostMIND
from tirl.transformations.euler import TxEulerAngles
from tirl.optimisation.optnl import OptNL, OptMethods
from tirl.optimisation.optgroup import OptimisationGroup
from tirl.transformations.translation import TxTranslation


# TIRSCRIPT IMPORTS

from tirlscripts.oxford.bigmac.constants import *


# DEFINITIONS

np.set_printoptions(precision=4, suppress=True)
LOGGER = None
logger = logging.getLogger(LOGGER)


# IMPLEMENTATION

def create_logger(loggername, logfile, cnf):
    """
    Creates a logger that sends log messages to the standard output and
    into a file.

    """
    global logger
    logger = logging.getLogger(loggername)
    logger.handlers = []  # delete any existing handlers to avoid duplicate logs
    logger.setLevel(1)
    formatter = logging.Formatter(fmt=LOGFORMAT, datefmt=DATEFORMAT)

    # Make sure that the logfile can be created
    logdir = os.path.dirname(os.path.abspath(logfile))
    if not os.path.isdir(logdir):
        os.makedirs(logdir)

    # Redirect all logs of interest to the logfile
    loglevel = cnf["loglevel"]
    fh = logging.FileHandler(logfile, mode="a", delay=False)
    if isinstance(loglevel, str):
        level = getattr(logging, loglevel.upper())
    elif isinstance(loglevel, (int, float)):
        level = int(loglevel)
    elif not loglevel:
        level = logging.INFO
    else:
        raise TypeError(f"Invalid logging level: {loglevel}")
    fh.setLevel(level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Redirect only warnings/errors to the standard output, unless "verbose"
    ch = logging.StreamHandler()
    if cnf["verbose"]:
        ch.setLevel(level)
    else:
        ch.setLevel(logging.WARNING)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


def load_image(imfile):
    image = tirl.load(imfile)
    slice_no = image.header["metadata"]["Slice"]
    part = image.header["metadata"]["Part"]
    ext = ts.EXTENSIONS["Domain"]
    dname = f"slice-{slice_no:04d}_part-{part}_latest_domain.{ext}"
    latest = os.path.join(os.path.dirname(imfile), dname)
    image.domain = tirl.load(latest)
    image.resmgr.sync()
    return image


def rigid3d_worker(job, outputfile):
    """
    Optimises the component-specific 3D position and orientation.

    """
    c_ix, imfile, r_file, metadata, cnf = job

    # Load image pair
    image = load_image(imfile)
    reference = tirl.load(r_file)
    img_layer = image.resmgr.active
    ref_layer = reference.resmgr.active
    # reference.storage = MEM

    # Load parameters
    params = cnf["regparams"]["rigid3d"]
    resolutions = params["resolution"]
    smoothing = params["smoothing"]
    ldx = np.asarray(params["ldx"], dtype=float)
    udx = np.asarray(params["udx"], dtype=float)

    # Add 3D rigid adjustment
    centre = image.centre(weighted=True)
    offset = TxTranslation(*centre)
    rotation = TxEulerAngles(0, 0, 0, order="xyz", mode="deg")
    image.domain.external += [-offset, rotation, offset]

    # Scaling-smoothing iteration
    for resolution, sm in zip(resolutions, smoothing):
        resolution = image.resolution if resolution is None else resolution
        scaling_factors = np.divide(image.layers[0].resolution, resolution)
        sc = np.abs(np.mean(scaling_factors))
        logger.debug(f"Slice scaling factor: 1 : {1 / sc}")
        image.rescale(sc, copy=False)
        image.storage = MEM
        if sm != 0:
            sm_newimg = image.smooth(sm, copy=True)
            sm_newimg.domain = image.domain
        else:
            sm_newimg = image
        rotation, offset = sm_newimg.domain.external[-2:]
        og = OptimisationGroup(rotation, offset)
        logger.log(ts.PARAMETER_UPDATE_LOG_LEVEL, str(og.parameters[:]))
        l_rot = np.subtract(rotation.parameters[:], np.radians(ldx[:3]))
        u_rot = np.add(rotation.parameters[:], np.radians(udx[:3]))
        l_offset = np.subtract(offset.parameters[:], [*ldx[3:]])
        u_offset = np.add(offset.parameters[:], [*udx[3:]])
        og.set_lower_bounds(np.asarray([*l_rot, *l_offset]))
        og.set_upper_bounds(np.asarray([*u_rot, *u_offset]))
        cost = CostMIND(reference, sm_newimg)
        OptNL(og, cost, method=OptMethods.LN_BOBYQA, normalised=True,
              xtol_abs=[0.01, 0.01, 0.01, 0.01, 0.01, 0.01],
              step=0.5, logger=logger, visualise=params["visualise"])()

        image.domain.external = sm_newimg.domain.external
        image.resmgr.sync()

    # Restore the input resolution
    image.resmgr.set(img_layer)
    reference.resmgr.set(ref_layer)

    # Revert parameters to the last trusted state if the optimisation
    # was detrimental
    slice_no, c_no = c_ix
    slicedir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")
    part = metadata["Part"]
    ext = ts.EXTENSIONS["Domain"]
    dname = f"slice-{slice_no:04d}_part-{part}_trusted_domain.{ext}"
    trusted_domain = tirl.load(os.path.join(slicedir, dname))
    logger.info("Verifying the optimisation result...")
    prevent_loss(image, reference, trusted_domain)

    # Generate output (optimised or trusted domain)
    image.domain.save(outputfile, overwrite=True)

    # (reference image resampled onto the optimised domain)
    vs = reference.evaluate(image.domain)
    vs.normalise()
    s_file = os.path.join(
        slicedir, f"slice-{slice_no:04d}_part-{part}_tx-rigid3d_mri.png")
    vs.snapshot(s_file, overwrite=True)

    return c_ix, outputfile


def prevent_loss(image, reference, trusted_domain):
    """
    Revert to a trusted domain if the optimisation resulted in the increase
    of the cost function value.

    """
    censored = 0
    newimg = reference.evaluate(image.domain)
    newimg.domain.reset(internal=True)
    oldimg = reference.evaluate(trusted_domain)
    oldimg.domain.reset(internal=True)
    voxelimage = image.copy()
    voxelimage.domain.reset(internal=True)
    voxelimage.storage = MEM
    newimg.storage = MEM
    oldimg.storage = MEM

    old_cost = CostMIND(oldimg, voxelimage, normalise=True)()
    logger.debug(f"Cost on trusted domain: {old_cost}")
    try:
        new_cost = CostMIND(newimg, voxelimage, normalise=True)()
    except AssertionError:
        new_cost = np.inf
    logger.debug(f"Cost on newly optimised domain: {new_cost}")

    if new_cost > old_cost:
        slice_no = int(image.header["metadata"]["Slice"])
        part = image.header["metadata"]["Part"]
        image.domain = trusted_domain
        image.resmgr.sync()
        censored += 1
        logger.info(f"Censored transformations for "
                    f"[Slice {slice_no} Part {part}]")
    else:
        logger.info("The optimisation result is valid.")


def main(jobfile, outputfile, logfile):
    """ Main program code. """

    with open(jobfile, "rb") as fp:
        job = dill.load(fp)

    c_ix, imfile, reference, metadata, cnf = job
    slice_no, c_no = c_ix
    part = metadata["Part"]
    pid = mp.current_process().pid

    global LOGGER
    LOGGER = f"slice-{slice_no}_part-{part}_tx-rigid3d_proc-{pid}"
    create_logger(LOGGER, logfile, cnf)
    cmd = " ".join(sys.argv)
    logger.info(f"The program started with the following command: {cmd}")
    rigid3d_worker(job, outputfile)


if __name__ == "__main__":
    main(*sys.argv[1:])
