#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2021 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Updated: 7 October 2022


# SHBASECOPYRIGHT


# DESCRIPTION

"""
warp.py --

This is a satellite script for multislice_to_volume.py that performs the
non-linear registration step on a single slice. The satellite script allows the
parallel execution of this registration step on a computing cluster, where
jobs are managed by a scheduler.

"""


# DEPENDENCIES

import os
import sys
import dill
import logging
import numpy as np
import multiprocessing as mp
from attrdict import AttrMap
from datetime import datetime


# TIRL IMPORTS

import tirl
import tirl.settings as ts
from tirl.constants import *
from tirl.domain import Domain
from tirl.tfield import TField
from tirl.timage import TImage
from tirl.cost.mi import CostMI
from tirl.cost.mind import CostMIND
from tirl.optimisation.optnl import OptNL, OptMethods
from tirl.regularisation.bending_energy import RegBendingEnergy
from tirl.transformations.rbf_displacement import TxRbfDisplacementField


# TIRSCRIPT IMPORTS

from tirlscripts.oxford.bigmac.constants import *


# DEFINITIONS

np.set_printoptions(precision=4, suppress=True)
NORMALISE = True
LOGGER = None
logger = logging.getLogger(LOGGER)


# IMPLEMENTATION


def create_logger(loggername, logfile, cnf):
    """
    Creates a logger that sends log messages to the standard output and
    into a file.

    """
    global logger
    logger = logging.getLogger(loggername)
    logger.handlers = []  # delete any existing handlers to avoid duplicate logs
    logger.setLevel(1)
    formatter = logging.Formatter(fmt=LOGFORMAT, datefmt=DATEFORMAT)

    # Make sure that the logfile can be created
    logdir = os.path.dirname(os.path.abspath(logfile))
    if not os.path.isdir(logdir):
        os.makedirs(logdir)

    # Redirect all logs of interest to the logfile
    loglevel = cnf["loglevel"]
    fh = logging.FileHandler(logfile, mode="a", delay=False)
    if isinstance(loglevel, str):
        level = getattr(logging, loglevel.upper())
    elif isinstance(loglevel, (int, float)):
        level = int(loglevel)
    elif not loglevel:
        level = logging.INFO
    else:
        raise TypeError(f"Invalid logging level: {loglevel}")
    fh.setLevel(level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Redirect only warnings/errors to the standard output, unless "verbose"
    ch = logging.StreamHandler()
    if cnf["verbose"]:
        ch.setLevel(level)
    else:
        ch.setLevel(logging.WARNING)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


def create_cost_object(source, target, cnf):
    """
    Creates a script-wide uniform cost object based on the general cost
    function type setting.

    """
    p = AttrMap(cnf)

    if str(p.cost).lower() == "mind":
        return CostMIND(source, target, normalise=True, sigma=1.0,
                        truncate=1.5, kernel=MK_STAR)
    elif str(p.cost).lower() == "mi":
        return CostMI(source, target, normalise=True, bins=32)
    else:
        raise ValueError(f"Unrecognised cost function specification: "
                         f"{p.cost}")


def absolute_step(lb, ub, step):
    di = ub - lb
    di[~np.isfinite(di)] = 2
    if np.any(di <= 0):
        raise AssertionError("No space for optimisation.")
    return np.multiply(0.5 * di, step)


def calc_km_control_points(n_new, img):
    """
    Uses k-means clustering to generate an exact number of tiles,
    distributed evenly and compactly across the ROI of the target image, as
    defined by a mask.

    :param n_new: number of new points
    :type n_new: int
    :param img: target image
    :type img: TImage

    :returns: (n_points, 2) voxel coordinate array of control points
    :rtype: np.ndarray

    """
    from sklearn.cluster import KMeans
    from tirlscripts.oxford.scriptutils.image import get_mask

    # Define ROI for the cost map
    tmp = img.copy().tensors.reduce()
    tmp.normalise()
    mask = get_mask(tmp, scope=globals())
    if mask is not None:
        mask = TImage(mask).resize(*tmp.domain.shape, copy=True).data
        coords = np.vstack(np.where(mask > 0.5)).T
    else:
        coords = tmp.domain.get_voxel_coordinates()
    n_points = n_new
    km = KMeans(n_clusters=n_points, random_state=0)
    km.fit(coords)
    xi = km.cluster_centers_
    logger.info(f"Created {xi.shape[0]} new control points: \n{xi}")

    # Measure the average distance between segment centroids
    edges = np.max(xi, axis=0) - np.min(xi, axis=0)
    edges = edges[np.nonzero(edges)]
    mean_distance = np.power(np.prod(edges) / xi.shape[0], 1. / edges.size)
    logger.info(f"Mean control point distance: {mean_distance} px")

    # Return the voxel coordinates of the sorted control points
    return xi


def create_warp_from_points(img, points, t, outdir):
    """
    Creates empty warp field based on the voxel coordinates of the support
    points on the fixed image domain.

    """
    dim = len(t.vectorder)
    chain = ChainTemplate(*img.domain.external)
    ix = chain.index(chain.g_embed)
    assert chain[ix] is chain.g_embed
    domain = Domain(points, transformations=img.domain.chain[:ix + 1],
                    offset=img.domain.offset)
    field = TField(domain=domain, tshape=(dim,), order=TENSOR_MAJOR,
                   dtype=ts.DEFAULT_FLOAT_TYPE, fill_value=0)

    if t.export.control_points:
        timestamp = datetime.now().strftime("%Y%m%d_%H%M%S_%f")
        fn = os.path.join(outdir, f"control_points_{timestamp}.png")
        np.savetxt(fn.replace(".png", ".txt"), points)
        save_control_point_image(img, points, fn)

    # Define bounds
    if t.vectorder == "xyz":
        lbxy = float(t.lower_dxy) * np.ones((2, field.domain.numel))
        lbz = float(t.lower_dz) * np.ones((1, field.domain.numel))
        ubxy = float(t.upper_dxy) * np.ones((2, field.domain.numel))
        ubz = float(t.upper_dz) * np.ones((1, field.domain.numel))
        lb = -np.concatenate((lbxy, lbz), axis=0).ravel()
        ub = np.concatenate((ubxy, ubz), axis=0).ravel()
    elif t.vectorder == "xy":
        lb = -float(t.lower_dxy) * np.ones((2, field.domain.numel))
        ub = float(t.upper_dxy) * np.ones((2, field.domain.numel))
    elif t.vectorder == "z":
        lb = -float(t.lower_dz) * np.ones(field.domain.numel)
        ub = float(t.upper_dz) * np.ones(field.domain.numel)
    else:
        raise ValueError(f"Invalid vector order: {t.vectorder}")

    # Create warp
    tx_warp = TxRbfDisplacementField(
        field, bounds=(lb, ub), name="warp", invertible=False,
        mode=NL_ABS, vectorder=t.vectorder, dense_shape=img.domain.shape)
    tx_warp.interpolator.model = t.model

    return tx_warp


def save_control_point_image(photo, cpoints, filename):

    # Creating the figure requires a window management system, so check this
    # first, and change the backend accordingly if it cannot be found.
    from tirl.utils import to_img
    if not os.getenv("DISPLAY") or not ts.ENABLE_VISUALISATION:
        import matplotlib
        matplotlib.use("agg")
    else:
        import matplotlib
        matplotlib.use(ts.MPL_BACKEND)
    import matplotlib.pyplot as plt

    # Create figure with photo and control points
    fig = plt.figure()
    ax = fig.add_subplot(111)
    order = photo.order
    photo.order = VOXEL_MAJOR
    if photo.tshape == ():
        ax.imshow(to_img(photo.data), cmap='gray', zorder=1)
    else:
        ax.imshow(to_img(photo.data), zorder=1)
    photo.order = order
    y, x = cpoints.T
    ax.scatter(x, y, c="magenta", zorder=2)

    # Save figure
    dot_file = os.path.join(filename)
    try:
        fig.savefig(dot_file)
    except Exception as exc:
        logger.error(exc.args)
        logger.info("Control point image could not be saved due to an error.")
    else:
        logger.info(f"Saved control point figure: {filename}")


def load_image(imfile):
    image = tirl.load(imfile)
    slice_no = image.header["metadata"]["Slice"]
    part = image.header["metadata"]["Part"]
    ext = ts.EXTENSIONS["Domain"]
    dname = f"slice-{slice_no:04d}_part-{part}_latest_domain.{ext}"
    latest = os.path.join(os.path.dirname(imfile), dname)
    image.domain = tirl.load(latest)
    image.resmgr.sync()
    return image


def warp_worker(job, outputfile):
    """
    Warps a slice/component within the slicing plane.

    """
    slicedir = os.path.dirname(outputfile)
    c_ix, imfile, r_file, metadata, cnf = job
    slice_no, c_no = c_ix
    part = metadata["Part"]
    t = AttrMap(cnf["regparams"]["warp"])

    # Load images from disk
    img = load_image(imfile)
    vol = tirl.load(r_file)
    img_layer = img.resmgr.active
    vol_layer = vol.resmgr.active

    # Smoothing iteration
    stage_cost = []

    # Scaling-smoothing iterations
    warp = None
    states = tuple(zip(t.slice_resolution, t.volume_resolution,
                       t.slice_smoothing, t.volume_smoothing))

    for sm_no, (sres, vres, ssm, vsm) in enumerate(states):
        sres = img.resolution if sres is None else sres
        ssc = np.abs(np.mean(np.divide(img.layers[0].resolution, sres)))
        logger.info(f"Smoothing: sres={sres}, ssm={ssm}px, "
                    f"vres={vres}, vsm={vsm}vx.")
        vres = vol.resolution if vres is None else vres
        vsc = np.abs(np.mean(np.divide(vol.layers[0].resolution, vres)))
        logger.debug(f"Slice scaling factor: 1 : {1 / ssc}")
        img.rescale(ssc, copy=False)
        img.storage = MEM
        img_smooth = img.smooth(ssm, copy=True)
        logger.debug(f"Volume scaling factor: 1 : {1 / vsc}")
        vol.rescale(vsc, copy=False)
        # vol.storage = MEM
        if vsm != 0:
            vol_smooth = vol.smooth(vsm, copy=True)
        else:
            vol_smooth = vol
        cost = create_cost_object(vol_smooth, img_smooth, cnf)
        logger.info(f"Initial cost: {cost()}")

        # How many support points?
        # Read from configuration file.
        if isinstance(t.support.points, int):
            n_points = t.support.points
            calculate_points = True
            points_iter = None
        elif isinstance(t.support.points, (list, tuple)):
            n_points = len(t.support.points)
            points_iter = t.support.points
            calculate_points = True
        # Read from external file
        elif isinstance(t.support.points, str) and \
                os.path.isfile(t.support.points):
            xi = np.loadtxt(t.support.points)
            n_points = xi.shape[0]
            calculate_points = False
            points_iter = None
        # Invalid input
        else:
            raise ValueError("Invalid support point specification.")

        # How many points should be optimised at once?
        # Optimise all points for any subsequent iteration, as little benefit
        # is anticipated from repeating the whole optimisation sequence.
        if sm_no == 0:
            optsize = int(min(int(t.optsize), n_points))
            points_iter = points_iter or \
                (*range(optsize, n_points, optsize), n_points)
        else:
            points_iter = (n_points,)

        # ---> POINT OPTIMISATION SEQUENCE
        for n_current in points_iter:

            # --------------------- CREATE NEW WARP -------------------- #

            # Create new point set
            if calculate_points:
                xi_current = calc_km_control_points(n_current, img)
            else:
                xi_current = xi[:n_current]

            # Create new transformation with the old and the new points
            newwarp = create_warp_from_points(img, xi_current, t, slicedir)

            # If a transformation was previously optimised, fit the parameters
            # of the new transformation to recreate the displacement field.
            # This carries forward the information from previous optimisations.
            if warp is not None:
                newwarp.fit(warp.std(3).absolute().field())

            warp = newwarp

            # Update transformation parameter bounds
            params = warp.parameters[:]
            params = params.reshape(len(t.vectorder), -1)
            lb = params.copy()
            ub = params.copy()
            if t.vectorder == "xyz":
                lb[:2] -= float(t.lower_dxy)
                lb[2] -= float(t.lower_dz)
                ub[:2] += float(t.upper_dxy)
                ub[2] += float(t.upper_dz)
            elif t.vectorder == "xy":
                lb[:] -= float(t.lower_dxy)
                ub[:] += float(t.upper_dxy)
            elif t.vectorder == "z":
                lb[:] -= float(t.lower_dz)
                ub[:] += float(t.upper_dz)

            if NORMALISE:
                initial_step = t.opt_step
            else:
                initial_step = absolute_step(lb.ravel(), ub.ravel(), t.opt_step)
            warp.parameters.set_bounds(lb.ravel(), ub.ravel())
            warp.parameters.unlock()

            # Add warp to the chain
            chain = ChainTemplate(*img_smooth.domain.external)
            img_smooth.domain.chain[chain.index(chain.s_warp)] = warp
            # Recreate cost object, because the chain was modified
            cost = create_cost_object(vol_smooth, img_smooth, cnf)

            # --------------------- OPTIMISE NEW WARP -------------------- #

            # Regularisation
            if t.regweight > 0:
                reg = RegBendingEnergy(warp, weight=t.regweight, logger=logger)
                # reg = NormTxRegulariser(warp, order=2, weight=t.regweight)
                cost_items = [cost, reg]
            else:
                cost_items = [cost]

            # Optimisation
            logger.debug(f"Optimising the following control points:"
                         f"\n{warp.domain.get_voxel_coordinates()}")
            OptNL(warp, *cost_items, method=OptMethods.LN_BOBYQA,
                  step=initial_step, xtol_abs=t.xtol_abs, logger=logger,
                  visualise=t.visualise, normalised=NORMALISE)()

            # Create snapshot
            timestamp = datetime.now().strftime("%Y%m%d_%H%M%S_%f")
            fn = os.path.join(
                slicedir,
                f"slice-{slice_no:04d}_part-{part}_tx-warp_"
                f"{n_current}pts_{timestamp}."
                f"{ts.TIMAGE_DEFAULT_SNAPSHOT_EXT}")
            vol_smooth.evaluate(img_smooth.domain).snapshot(fn, overwrite=True)

        # <--- POINT OPTIMISATION SEQUENCE

        # Record the final cost at the current smoothing level
        if sm_no + 1 == len(states):
            stage_cost.append(cost())
            logger.info(f"Final cost @ [ssm={ssm}px vsm={vsm}vx]: "
                        f"{stage_cost[-1]}")

        # Copy optimised transformations before creating a new image from
        # the original. [This may no longer be necessary.]
        img.domain = img_smooth.domain
        img.resmgr.sync()

    # Restore the input resolution
    img.resmgr.set(img_layer)
    vol.resmgr.set(vol_layer)

    # Generate output
    # (optimised domain)
    img.domain.save(outputfile, overwrite=True)

    # (reference image resampled onto the optimised domain)
    vs = vol.evaluate(img.domain)
    vs.normalise()
    s_file = os.path.join(
        slicedir, f"slice-{slice_no:04d}_part-{part}_tx-warp_mri.png")
    vs.snapshot(s_file, overwrite=True)

    return 0


def main(jobfile, outputfile, logfile):
    """ Main program code. """

    with open(jobfile, "rb") as fp:
        job = dill.load(fp)

    c_ix, imfile, reference, metadata, cnf = job
    slice_no, c_no = c_ix
    part = metadata["Part"]
    pid = mp.current_process().pid

    global LOGGER
    LOGGER = f"slice-{slice_no}_part-{part}_tx-warp_proc-{pid}"
    create_logger(LOGGER, logfile, cnf)
    cmd = " ".join(sys.argv)
    logger.info(f"The program started with the following command: {cmd}")
    warp_worker(job, outputfile)


if __name__ == "__main__":
    main(*sys.argv[1:])
