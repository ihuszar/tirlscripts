#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2021 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Date: 10 June 2020


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import sys
import argparse


# TIRL IMPORTS

import tirl
import tirl.settings as ts


def combine(args):
    """ Main program code. """

    histo = tirl.load(args.histo)
    block = tirl.load(args.block)
    slice_ = tirl.load(args.slice)
    mri = tirl.load(args.vol)
    histo.domain.chain += block.domain.chain[2:] + slice_.domain.chain[1:]
    result = mri.evaluate(histo.domain, rule=None)

    # Save output
    outputdir = os.path.dirname(args.out)
    if not os.path.isdir(outputdir):
        os.makedirs(outputdir)
    ext = ts.EXTENSIONS["TImage"]
    result.save(args.out + f".{ext}")
    ext = ts.TIMAGE_DEFAULT_SNAPSHOT_EXT
    result.snapshot(args.out + f".{ext}")


def create_cli():
    """
    Creates command-line interface.

    """
    prog = "combine"
    usage = "combine --histo <timg> --block <timg> --slice <timg> --vol <timg>"
    descr = "Combines the transformations from stages 1-3 of the MND " \
            "registration pipeline to achieve an end-to-end mapping between " \
            "histology and MRI coordinates."
    parser = argparse.ArgumentParser(prog, usage, description=descr)
    parser.add_argument("--histo", metavar="timg", type=str, required=True,
                        default="", help="Registered histology TImage.")
    parser.add_argument("--block", metavar="timg", type=str, required=True,
                        default="", help="Registered tissue block TImage.")
    parser.add_argument("--slice", metavar="timg", type=str, required=True,
                        default="", help="Registered brain slice TImage.")
    parser.add_argument("--vol", metavar="timg", type=str, required=True,
                        default="", help="MRI volume as TImage.")
    parser.add_argument("--out", metavar="basename", type=str, required=True,
                        default="", help="Basename for output.")

    return parser


def main(*args):
    """ Main program code. """

    parser = create_cli()
    if args:
        args = parser.parse_args(args)
        combine(args)
    else:
        parser.print_help()


if __name__ == "__main__":
    main(*sys.argv[1:])
