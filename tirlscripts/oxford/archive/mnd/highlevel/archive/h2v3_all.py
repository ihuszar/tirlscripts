#!/usr/bin/env python

import os
import json
import tirl
from glob import glob
from tirl.timage import TImage
from scripts.oxdbb.pathology.highlevel import histology_to_volume3 as h2v

INDEX_HMOD = "PLP"
INDEX_MMOD = "TRUFI"
SUBJECTS = [
    "CTL10",
    "CTL12",
    "MND12",
    "MND13",
    "MND14",
    "MND15",
    "MND16",
    "MND17",
    "MND18",
    "MND19",
    "MND20",
    "MND21",
    "MND22",
    "MND23"
]
rootdir = "/mnt/nvme/mnd"
cnftemplate = "/home/inhuszar/apps/tirl/scripts/oxdbb/pathology/highlevel/" \
              "histology_to_volume3.json"


def main():
    """ Main program code. """

    for subject in SUBJECTS:
        subjdir = os.path.join(rootdir, subject)
        blocks = os.listdir(os.path.join(subjdir, "blocks"))
        blocks = [b for b in blocks
                  if os.path.isdir(os.path.join(subjdir, "blocks", b))]
        # blocks = ["13Ra", "20a"]
        mfile = os.path.join(subjdir, "mri", INDEX_MMOD, "trufi_comb.nii.gz")

        for block in blocks:
            print(f"Processing: subject {subject}, block {block}")
            with open(cnftemplate, "r") as f:
                cnf = dict(json.load(f))
            hfile = glob(os.path.join(
                subjdir, "blocks", block, "histology",
                INDEX_HMOD, "*.svs"))[0]
            histo = TImage(hfile, loaderkwargs=dict(pages=3))
            cfile = os.path.join(
                subjdir, "blocks", block, "histology", INDEX_HMOD,
                "to_" + INDEX_MMOD, "rawcombine.timg")
            histo.domain.chain = tirl.load(cfile).domain.chain
            cout = cfile.replace("rawcombine.timg", "historc.timg")
            histo.save(cout, overwrite=True)
            outdir = os.path.join(subjdir, "blocks", block, "histology",
                                  INDEX_HMOD, "to_" + INDEX_MMOD,
                                  "histology2volume")
            if not os.path.isdir(outdir):
                os.makedirs(outdir)

            # Set basic configurations
            cnf["histology"]["file"] = cout
            cnf["volume"]["file"] = mfile
            cnf["general"]["outputdir"] = outdir
            cnf["general"]["logfile"] = os.path.join(outdir, "logfile.log")
            cnf["general"]["paramlogfile"] = os.path.join(outdir, "params.log")
            cnf["regparams"]["stage_1"]["n_cpu"] = -1

            # Run the histology-to-volume optimisation
            h2v.run(cnf)


if __name__ == "__main__":
    main()
