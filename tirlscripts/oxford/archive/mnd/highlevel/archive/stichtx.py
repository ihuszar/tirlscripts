#!/usr/bin/env python

"""
Stiches left- and right-hemisphere optimised slice-to-volume registrations into
a single warped image.

The script takes a less trivial approach to avoid the definition of a
non-linear transformation on a non-linearly deformed 3D embedded 2D domain.
This allows the stitch transformation to be computed quickly.

"""

import os
import numpy as np

import tirl
from tirl.tfield import TField
from tirl.timage import TImage
from tirl.transformations.displacement import TxDisplacementField
from tirl.interpolation.scipyinterpolator import ScipyInterpolator

SUBJECTS = [
    "CTL10",
    "CTL12",
    "MND12",
    "MND13",
    "MND14",
    "MND15",
    "MND16",
    "MND17",
    "MND18",
    "MND19",
    "MND20",
    "MND21",
    "MND22",
    "MND23"
]

for subject in SUBJECTS:
    slicesdir = f"/mnt/alpha/mnd/{subject}/slices/"
    wfile = f"/mnt/alpha/mnd/{subject}/mri/Segmentation/warpweights.nii.gz"
    trufi = f"/mnt/alpha/mnd/{subject}/mri/TRUFI/trufi_comb.nii.gz"
    trufi = TImage(trufi, external="sform")
    linpol = ScipyInterpolator(order=1, prefilter=False)
    weights = TImage(wfile, external="sform", interpolator=linpol)

    for root, dirs, files in os.walk(slicesdir):
        try:
            if not root.endswith(".left"):
                continue
            print(f"Processing {root}...")
            leftdir = root
            rightdir = root.replace(".left", ".right")
            left = tirl.load(os.path.join(leftdir, "4_stage4.timg"))
            right = tirl.load(os.path.join(rightdir, "4_stage4.timg"))
            lc = left.domain.get_physical_coordinates()
            lw = weights.tensors[0].evaluate(left.domain).data.ravel()
            rc = right.domain.get_physical_coordinates()
            rw = weights.tensors[1].evaluate(right.domain).data.ravel()
            hc = ((lc.T * lw + rc.T * rw) / (lw + rw)).T
            leftzone = np.isclose(lw, 1, 1e-2)
            hc[leftzone, :] = lc[leftzone, :]
            rightzone = np.isclose(rw, 1, 1e-2)
            hc[rightzone, :] = rc[rightzone, :]
            nans = ~np.isfinite(hc)
            hc[nans] = (lc[nans] + rc[nans]) / 2

            hybrid = left.copy()
            h, w = hybrid.domain.shape
            warp, ix = hybrid.domain.get_external_transformation(
                "warp", index=True)
            newdomain = hybrid.domain[:, :ix]
            x0 = newdomain.get_physical_coordinates()
            diff = (hc - x0).T.reshape(-1, h, w)
            field = TField(diff, tshape=3, vshape=(h, w),
                           order="T", domain=newdomain[:])
            linpol = ScipyInterpolator(order=1, prefilter=False)
            warp = TxDisplacementField(field, interpolator=linpol, mode="abs")
            newdomain.chain.append(warp)

            res = trufi.evaluate(newdomain)
            outdir = leftdir.replace(".left", ".hybrid")
            if not os.path.isdir(outdir):
                os.makedirs(outdir)
            res.save(os.path.join(outdir, "slice.timg"), overwrite=True)
            res.snapshot(os.path.join(outdir, "resampled_mri.png"),
                         overwrite=True)
        except Exception as exc:
            print(exc.args)
            continue
