08-Feb-2022, Istvan N Huszar
A coarse guide to creating the MND dataset



1. Create master directory tree to store the following inputs in a standard layout:
	<subject_ID>
		- mri: preprocessed MRI modalities with transformations from Ben's pipeline
		- blocks/<block_ID>:
			- histology/<stain>: raw histology data (.svs) files + XML annotations + create a TIFF version too
			- photo: raw multi-block photograph with captions + cropped, background-segmented block photograph (strip-bg.py)
		- slices
			- <slice_number>/{A,P}: raw + cropped and segmented photo series (strip-bg.py)

2. Add Stage 1-3 registration results
	- Stage 1: histology_to_block.py output directory --> blocks/<block_ID>/histology/PLP/to_block
	- Stage 2: block_to_slice.py output directory --> blocks/<block_ID>/photo/to_slice
	- Stage 3: slice_to_volume.py output directory --> slices/<slice_number>/{A,P}/to_vol.{left,right,bilateral}

3. Create MRI table and registration table for each subject (create_sheets.py)
	- the MRI sheet contains the transformations between different MRI spaces
	- the Registration sheet is a joblist, linking all files for each job

4. Perform histology-to-histology registrations (stage0/register_all.py and histology_to_histology.py)
	- blocks/<block_ID>/<stain>/to_PLP (except for PLP)
	- create animations from the results to observe the quality and fine-tune if necessary (qc_h2h.py)

5. Derive hybrid slice-to-volume mappings from separate left/right Stage-3 results
	- compute warpweights based on 3D hemisphere masks (mriedt.py)
	- compute warpweights from 2D hand-drawn hemisphere masks for the slices if MRI segmentation didn't work
	- create hybrid slice-to-volume transformations (stitchtx2.py) --> slice/<slice_number>/{A,P}/to_vol.hybrid

6. Combine Stage 1-3 transformations
	- use left-optimised Stage 3 result where the block is from the left hemisphere
	- use right-optimised Stage 3 result where the block is from the right hemisphere 
	- use hybrid or bilateral Stage 3 result where the block has an intersection with the median sagittal plane
	- evaluate the ISO image with the combined chain --> blocks/<block_ID>/histology/PLP/to_TRUFI/rawcombine.{timg,png}
	- save the PLP image with the same combined chain --> blocks/<block_ID>/histology/PLP/to_TRUFI/historc.timg

7. Peform histology-to-volume registration using the historc.timg and the ISO volumes.
	

8. Verify the histology-to-volume registrations
	- Evaluate the ISO volume and overlap the result with the native PLP image (.tif/.png)
	- If non-linear deformations are exaggerated in Stage 4, use the Stage4/Stage1 output or add regularisation and rerun

9. Create reduced database and create cross-modality evaluations using the appropriate Stage-4 output (scraper.py)

10. Calculate the SHA1 checksum for each file in the reduced dataset to ensure that future changes can be detected and tracked.
