for f in /mnt/alpha/mnd/config/*.conf; do sbatch --ntasks=8 --output=${f/.conf/.out} --wrap="/sapps/anaconda3/envs/t3env/bin/python /home/inhuszar/apps/tirl/scripts/oxdbb/pathology/highlevel/histology_to_histology.py --config=$f --verbose"; done


for f in `find /mnt/alpha/mnd/ -name h2v_config.conf | sort`; do sbatch --ntasks=3 --output=${f/.conf/.out} --wrap="/sapps/anaconda3/envs/t3env/bin/python /home/inhuszar/apps/tirl/scripts/oxdbb/pathology/highlevel/histology_to_volume3.py --config=$f --verbose"; done




/sapps/anaconda3/envs/t3env/bin/python /home/inhuszar/apps/tirl/scripts/oxdbb/pathology/highlevel/histology_to_volume3.py --config=/mnt/alpha/mnd/CTL10/blocks/18a/histology/PLP/to_TRUFI/histology2volume/h2v_config.conf --verbose






for f in `cat /mnt/alpha/mnd/scripts/hybrid_resubmit.txt | sort | xargs`; do sbatch --ntasks=24 --output=${f/.conf/.out} --wrap="/sapps/anaconda3/envs/t3env/bin/python /home/inhuszar/apps/tirl/scripts/oxdbb/pathology/highlevel/histology_to_volume3.py --config=$f --verbose"; done


for f in `cat /mnt/alpha/mnd/scripts/parallel_jobs.txt | sort | xargs`; do sbatch --ntasks=3 --output=${f/.conf/.out} --wrap="/sapps/anaconda3/envs/t3env/bin/python /home/inhuszar/apps/tirl/scripts/oxdbb/pathology/highlevel/histology_to_volume3.py --config=$f --verbose"; done



/sapps/anaconda3/envs/t3env/bin/python /home/inhuszar/apps/tirl/scripts/oxdbb/pathology/highlevel/histology_to_volume3b.py --config=/mnt/alpha/mnd/MND16/blocks/13La/histology/PLP/to_TRUFI/histology2volume.b/h2v_config.conf --verbose



for f in `ls /mnt/alpha/mnd/*/blocks/*/histology/PLP/to_TRUFI/histology2volume.mind/h2v_config.conf | sort`; do sbatch --ntasks=3 --output=${f/.conf/.out} --wrap="/sapps/anaconda3/envs/t3env/bin/python /home/inhuszar/apps/tirl/scripts/oxdbb/pathology/highlevel/histology_to_volume3b.py --config=$f --verbose"; done





rsync -rltDPv --relative --update --exclude="*/to_vol.hybrid*" --exclude="*histology/*" --exclude="stage1_temp" --exclude="*.bak" --exclude="*/report/*" --exclude="reproducibility/*" --exclude="config/*" --exclude="*.old" /mnt/alpha/mnd/./ ihuszar@jalapeno.fmrib.ox.ac.uk:/vols/Data/km/ihuszar/mnd/./





for f in `cat rerun-stage3-list.txt`; do sed -i 's/\/mnt\/alpha\/mnd\//\/vols\/Data\/km\/ihuszar\/mnd\//g' $f; done

\/vols\/Data\/km\/ihuszar\/mnd\/



for f in `cat rerun-stage3-list.txt`; do fsl_sub -q long.q -l $(dirname $f) -n stage3 /home/fs0/ihuszar/.conda/envs/testenv/bin/python /vols/Data/km/ihuszar/tirl/scripts/oxdbb/pathology/highlevel/slice_to_volume.py --config=$f --verbose; done



for f in /vols/Data/km/ihuszar/mnd/*/blocks/*/histology/PLP/to_TRUFI/histology2volume.mind/h2v_config.conf; do fsl_sub -q long.q -l $(dirname $f) ~/.conda/envs/testenv/bin/python /vols/Data/km/ihuszar/tirl/scripts/oxdbb/pathology/highlevel/histology_to_volume3b.py --verbose --config=$f; done
for f in /vols/Data/km/ihuszar/mnd/*/blocks/*/histology/PLP/to_TRUFI/histology2volume.mi/h2v_config.conf; do fsl_sub -q long.q -l $(dirname $f) ~/.conda/envs/testenv/bin/python /vols/Data/km/ihuszar/tirl/scripts/oxdbb/pathology/highlevel/histology_to_volume3b.py --verbose --config=$f; done


for f in /vols/Data/km/ihuszar/mnd/*/blocks/*/histology/PLP/to_TRUFI/histology2volume.mi*/h2v_config.conf; do sed -i 's/\/mnt\/alpha\/mnd\//\/vols\/Data\/km\/ihuszar\/mnd\//g' $f; done

rsync -ravPn --relative --include='historc.timg' --include='rawcombine.*' --exclude='*.*' /mnt/alpha/mnd/ ihuszar@jalapeno.fmrib.ox.ac.uk:/vols/Data/km/ihuszar/mnd/

for x in `qstat | grep -Po '\d{6}'`; do echo $x; tail `find /vols/Data/km/ihuszar/mnd/ -name python.e${x}`; printf "\n"; done


TRGDIR=/vols/Data/km/ihuszar/mnd/MND14/blocks/09La/histology/PLP/to_TRUFI/histology2volume.mind; fsl_sub -q long.q -l $TRGDIR ~/.conda/envs/testenv/bin/python /vols/Data/km/ihuszar/tirl/scripts/oxdbb/pathology/highlevel/histology_to_volume3b.py --verbose --config=$TRGDIR/h2v_config.conf


sbatch --ntasks=3 --output=${f/.json/.out} --wrap="/sapps/anaconda3/envs/testenv/bin/python /home/inhuszar/apps/tirl/scripts/oxdbb/pathology/highlevel/slice_to_volume.py --config=$f --verbose"

for f in ./*/mri/Segmentation/trufi_comb_iso.mat; do convert_xfm -omat ${f/.mat/_inv.mat} -inverse $f; done


for subj in CTL10 CTL12 MND14 MND15 MND16 MND17 MND18 MND19 MND20 MND21 MND22 MND23; do echo sbatch --ntasks=2 --output=/mnt/alpha/mnd-reduced/${subj}/scraper.out --wrap="python scraper.py ${subj}"; done


rsync -ravP --relative --delete /mnt/nvme/mnd-reduced/./{CTL10,CTL12,MND14,MND15,MND16,MND17,MND18,MND19,MND20,MND21,MND22,MND23} ihuszar@jalapeno.fmrib.ox.ac.uk:/vols/Data/km/ihuszar/mnd/./


for d in {CTL*,MND*}; do sbatch --wrap="for f in `find . -name '*' -type f | sort`; do echo $f $(sha1sum $f); done > ${d}/inventory.txt"; done