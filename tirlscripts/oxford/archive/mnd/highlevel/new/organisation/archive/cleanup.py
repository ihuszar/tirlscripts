#!/usr/bin/env python
import os
import sys
import tirl

TAGS = ("_rotation", "_rigid", "_affine", "_nonlinear")


def main(rootdir):
    """ Main program code. """
    for root, dirs, files in os.walk(rootdir):
        for f in files:
            fname, ext = os.path.splitext(f)
            if ext == ".timg":
                if fname.endswith(TAGS):
                    img = tirl.load(os.path.join(root, f))
                    domain = img.domain
                    mov = tirl.load(os.path.join(root, "moving.timg"))
                    domain.chain += mov.domain.chain.inverse()
                    domain.save(os.path.join(root, fname + ".dom"), overwrite=True)
                    # os.remove(os.path.join(root, f))
                    print(os.path.join(root, f))
                elif f == "block.timg":
                    print(os.path.join(root, f))
                elif fname.endswith(("_stage1", "_stage3", "_stage4")):
                    img = tirl.load(os.path.join(root, f))
                    domain = img.domain
                    domain.save(os.path.join(root, fname + ".dom"), overwrite=True)
                    # os.remove(os.path.join(root, f))
                    print(os.path.join(root, f))
        else:
            try:
                # os.remove(os.path.join(root, "fixed.timg"))
                # os.remove(os.path.join(root, "moving.timg"))
                fp = os.path.join(root, "fixed.timg")
                mp = os.path.join(root, "moving.timg")
                if os.path.isfile(fp):
                    print(fp)
                if os.path.isfile(mp):
                    print(mp)
            except:
                continue

        for d in dirs:
            if d == "stage1_temp":
                print(os.path.join(root, d))
                # os.removedirs(os.path.join(root, d))


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        print("Usage: ./cleanup.py <rootdir>")
