#!/usr/bin/env python

import os
import sys
import tirl
import numpy as np
from glob import glob
import nibabel as nib
import pandas as pd
from tirl.timage import TImage

TARGET_SCALE = 2


def main(subjdir):
    """ Main program code. """

    subject = os.path.basename(subjdir)
    rs = glob(os.path.join(subjdir, subject + "_registration_sheet.csv"))[0]
    regsheet = pd.read_csv(rs)

    for root, dirs, files in os.walk(subjdir):
        for f in files:
            if f.endswith(".svs"):
                hfile = os.path.join(root, f)
                print(hfile)
                img = TImage(hfile, loaderkwargs=dict(pages=3))
                img.rescale(1 / TARGET_SCALE, copy=False)
                hfile_rel = os.path.relpath(hfile, os.path.dirname(subjdir))
                record = regsheet[regsheet.HistologyFile == hfile_rel]
                resolution = float(record.HistologyResolutionUM) / 1000 * 16
                affine = np.eye(4)
                affine[:2, :2] *= resolution * TARGET_SCALE
                fname, ext = os.path.splitext(f)
                rgb_dtype = np.dtype([('R', 'u1'), ('G', 'u1'), ('B', 'u1')])
                nifti = nib.Nifti1Image(
                    img.data.view(rgb_dtype), affine)
                nib.save(nifti, os.path.join(root, fname + ".nii.gz"))

            elif f == "to_PLP.png":
                hfile = os.path.join(root, f)
                print(hfile)
                img = TImage(hfile)
                img.rescale(1 / TARGET_SCALE, copy=False)
                svsfile = glob(os.path.join(root, "..", "..", "PLP", "*.svs"))[0]
                svsfile_rel = os.path.relpath(svsfile, os.path.dirname(subjdir))
                record = regsheet[regsheet.HistologyFile == svsfile_rel]
                resolution = float(record.HistologyResolutionUM) / 1000 * 16
                affine = np.eye(4)
                affine[:2, :2] *= resolution * TARGET_SCALE
                fname, ext = os.path.splitext(f)
                rgb_dtype = np.dtype([('R', 'u1'), ('G', 'u1'), ('B', 'u1')])
                nifti = nib.Nifti1Image(
                    img.data.view(rgb_dtype), affine)
                nib.save(nifti, os.path.join(root, fname + ".nii.gz"))


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        print("Usage: ./create_histology_nifti.py <subject_dir>")
