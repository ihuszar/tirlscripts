#!/usr/bin/env python

import os
import sys

import tirl
import nibabel
import numpy as np
import pandas as pd
from glob import glob
import nibabel as nib
from tirl.fsl import load_mat
from tirl.timage import TImage
from tirl.interpolation.scipyinterpolator import ScipyNearestNeighbours

TARGET_RESOLUTION = 0.5  # mm/px
SCALAR_MODALITIES = ("to_AD", "to_F1", "to_F2", "to_FA", "to_MD", "to_P1",
                     "to_P2", "to_QSM", "to_R1", "to_R2s", "to_RD", "to_T1",
                     "to_T2", "to_T2s", "to_TRUFI")


def main(subjdir):
    """ Main program code. """

    subject = os.path.basename(subjdir)
    d1file = os.path.join(subjdir, "mri", "B&S", "dyads1.nii.gz")
    trufifile = os.path.join(subjdir, "mri", "TRUFI", "trufi_comb.nii.gz")
    sform = TImage(trufifile, external="sform").domain.chain[-1]
    trufi2diff = load_mat(os.path.join(subjdir, "mri", "TRUFI", "trufi_to_diff.mat"),
                          input=trufifile, reference=d1file)
    chain = sform.inverse() + trufi2diff
    d1mri = TImage(os.path.join(subjdir, "mri", "B&S", "dyads1.nii.gz"))
    d1mri.interpolator = ScipyNearestNeighbours
    d1mri.data[..., 0] *= -1
    d2mri = TImage(os.path.join(subjdir, "mri", "B&S", "dyads2.nii.gz"))
    d2mri.interpolator = ScipyNearestNeighbours
    d2mri.data[..., 0] *= -1
    regsheet = glob(os.path.join(subjdir, subject + "_registration_sheet.csv"))[0]
    regsheet = pd.read_csv(regsheet)
    for root, dirs, files in os.walk(subjdir):
        if root.endswith(("to_D1", "to_D2")):
            print(root)
            hdir = os.path.dirname(root)
            hfile = glob(os.path.join(hdir, "*.svs"))[0]
            hfile_rel = os.path.relpath(hfile, os.path.dirname(subjdir))
            record = regsheet[regsheet.HistologyFile == hfile_rel]
            resolution = float(record.HistologyResolutionUM) / 1000 * 16
            factor = resolution / TARGET_RESOLUTION
            imfile = glob(os.path.join(root, "D?_nn.timg"))[0]
            fname, ext = os.path.splitext(os.path.basename(imfile))
            img = tirl.load(imfile)
            img.domain.chain += chain
            img.interpolator = ScipyNearestNeighbours
            img.rescale(factor, copy=False)
            if root.endswith("to_D1"):
                img = d1mri.evaluate(img.domain, rule="fs")
            else:
                img = d2mri.evaluate(img.domain, rule="fs")
            # img.preview("hsv")
            aff = np.eye(4)
            aff[:2, :2] *= TARGET_RESOLUTION
            hdr = nibabel.Nifti1Header()
            nifti = nibabel.Nifti1Image(img.data[:, :, np.newaxis, :], aff, hdr)
            nib.save(nifti, os.path.join(root, fname + f"_{TARGET_RESOLUTION}mm" + ".nii.gz"))
        elif root.endswith(SCALAR_MODALITIES):
            print(root)
            hdir = os.path.dirname(root)
            hfile = glob(os.path.join(hdir, "*.svs"))[0]
            hfile_rel = os.path.relpath(hfile, os.path.dirname(subjdir))
            record = regsheet[regsheet.HistologyFile == hfile_rel]
            resolution = float(record.HistologyResolutionUM) / 1000 * 16
            for imfile in glob(os.path.join(root, "*.timg")):
                fname, ext = os.path.splitext(os.path.basename(imfile))
                img = tirl.load(imfile)
                img.resolution = resolution
                aff = np.eye(4)
                aff[:2, :2] *= resolution
                hdr = nibabel.Nifti1Header()
                nifti = nibabel.Nifti1Image(img.data, aff, hdr)
                nib.save(nifti, os.path.join(root, fname + ".nii.gz"))
        else:
            continue


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        print("Usage: ./create_vector_nifti.py <subject_dir>")
