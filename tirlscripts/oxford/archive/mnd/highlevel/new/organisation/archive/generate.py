#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2021 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

"""
This script uses the piecewise estimated transformations to bridge the gap
between all pairs of histology and MRI modalities. Images are generated at
8 um/px resolution, using both linear and nearest-neighbour interpolation.
Hence the total number of outputs is n(histo) x n(mri) x 2.

"""


# DEPENDENCIES

import os
import sys
import pandas as pd

import tirl
import numpy as np
from tirl import fsl
from glob import glob
from tirl.constants import *
import multiprocessing as mp
from functools import partial
from itertools import product
from tirl.timage import TImage
import tirl.interpolation.scipyinterpolator as tip


# DEFINITIONS

INDEX_HMOD = "PLP"
N_CPU = 12
EXCLUDE_BLOCKS = ()

MFILES = dict(
    T1="T1" + os.sep + "T1map.nii.gz",
    R1="T1" + os.sep + "R1map.nii.gz",
    T2="T2" + os.sep + "T2map.nii.gz",
    R2="T2" + os.sep + "R2map.nii.gz",
    T2SE="T2" + os.sep + "T2map_SE.nii.gz",
    T2s="T2s" + os.sep + "T2s.nii.gz",
    R2s="T2s" + os.sep + "R2s.nii.gz",
    QSM="T2s" + os.sep + "QSM.nii.gz",
    FA="DTI" + os.sep + "dti_FA.nii.gz",
    MD="DTI" + os.sep + "dti_MD.nii.gz",
    AD="DTI" + os.sep + "dti_L1.nii.gz",
    RD="DTI" + os.sep + "dti_rad.nii.gz",
    D1="B&S" + os.sep + "dyads1.nii.gz",
    D2="B&S" + os.sep + "dyads2.nii.gz",
    F1="B&S" + os.sep + "mean_f1samples.nii.gz",
    F2="B&S" + os.sep + "mean_f2samples.nii.gz",
    P1="B&S" + os.sep + "dyads1_dispersion.nii.gz",
    P2="B&S" + os.sep + "dyads2_dispersion.nii.gz",
    TRUFI="TRUFI" + os.sep + "trufi_comb.nii.gz",

)
TX2INDEX = dict(
    T1=("T1" + os.sep + "T1_to_diff.mat",
        "TRUFI" + os.sep + "diff_to_trufi.mat"),
    R1=("T1" + os.sep + "T1_to_diff.mat",
        "TRUFI" + os.sep + "diff_to_trufi.mat"),
    T2=("T2" + os.sep + "T2_to_diff.mat",
        "TRUFI" + os.sep + "diff_to_trufi.mat"),
    R2=("T2" + os.sep + "T2_to_diff.mat",
        "TRUFI" + os.sep + "diff_to_trufi.mat"),
    T2SE=("T2" + os.sep + "T2_to_diff.mat",
          "TRUFI" + os.sep + "diff_to_trufi.mat"),
    T2s=("T2s" + os.sep + "T2s_to_diff.mat",
         "TRUFI" + os.sep + "diff_to_trufi.mat"),
    R2s=("T2s" + os.sep + "T2s_to_diff.mat",
         "TRUFI" + os.sep + "diff_to_trufi.mat"),
    QSM=("T2s" + os.sep + "T2s_to_diff.mat",
         "TRUFI" + os.sep + "diff_to_trufi.mat"),
    FA=("TRUFI" + os.sep + "diff_to_trufi.mat",),
    MD=("TRUFI" + os.sep + "diff_to_trufi.mat",),
    AD=("TRUFI" + os.sep + "diff_to_trufi.mat",),
    RD=("TRUFI" + os.sep + "diff_to_trufi.mat",),
    D1=("TRUFI" + os.sep + "diff_to_trufi.mat",),
    D2=("TRUFI" + os.sep + "diff_to_trufi.mat",),
    F1=("TRUFI" + os.sep + "diff_to_trufi.mat",),
    F2=("TRUFI" + os.sep + "diff_to_trufi.mat",),
    P1=("TRUFI" + os.sep + "diff_to_trufi.mat",),
    P2=("TRUFI" + os.sep + "diff_to_trufi.mat",),
    TRUFI=()
)


# IMPLEMENTATION

def main(subjdir, blocks=None):
    """ Main program code """

    if not blocks:
        blocks = sorted(os.listdir(os.path.join(subjdir, "blocks")))
    for block in [b for b in blocks if b not in EXCLUDE_BLOCKS]:
        combine_block(subjdir, block)


def combine_block(subjdir, block):

    h2m = tirl.load(os.path.join(
        subjdir, "blocks", block, "histology", INDEX_HMOD,
        "to_TRUFI", "histology2volume", "3_stage4.dom"))

    # Perform all combination of evaluations
    hmods = ["PLP"]
    mmods = ["TRUFI", "T1", "R1", "T2", "T2s",
             "R2s", "QSM", "FA", "MD", "RD", "AD",
             "D1", "F1", "P1", "D2", "F2", "P2"]
    interpolators = (tip.ScipyInterpolator(), tip.ScipyNearestNeighbours())
    jobs = product(hmods, mmods, interpolators)
    workerfunc = partial(process_safe, subjdir, block, h2m.chain)
    if N_CPU > 1:
        ctx = mp.get_context("spawn")
        with ctx.Pool(processes=N_CPU, maxtasksperchild=1) as pool:
            pool.map(workerfunc, jobs)
    else:
        for job in jobs:
            workerfunc(job)


def process_safe(subjdir, block, to_mri, job):
    subject = os.path.basename(subjdir.rstrip(os.sep))
    try:
        process(subjdir, block, to_mri, job)
    except Exception as exc:
        print(f"Failed to process: subject {subject}, block {block}")
        print(f"Reason: {exc.args}")
    else:
        print(f"Successfully processed {subject}, block {block}")


def process(subjdir, block, to_mri, job):
    """
    Creates a single image given the histology and the MRI modality and the
    type of the interpolator.

    """
    hmod, mmod, ipol = job
    print(f"Processing {block}: {hmod} --> {mmod} with "
          f"{ipol.__class__.__name__}...")

    # Load histology image at 8 um/px resolution
    hfile = glob(
        os.path.join(subjdir, "blocks", block, "histology", hmod, "*.svs"))[0]
    histo = TImage(hfile, loaderkwargs=dict(pages=3))

    # Save a TIFF version of the histology file, but only once!
    hsnap = hfile.replace(".svs", ".tif")
    if not os.path.isfile(hsnap):
        histo.snapshot(hsnap, overwrite=True)

    # Load transformation to the index histology image if necessary
    # And apply chain on the RGB histology image
    if hmod != INDEX_HMOD:
        chain_h2h = tirl.load(
            os.path.join(subjdir, "blocks", block, "histology", hmod,
                         "to_" + INDEX_HMOD, "fixed4_nonlinear.dom")).all_tx()
        svsfile = glob(os.path.join(subjdir, "blocks", block, "histology",
                                    INDEX_HMOD, "*.svs"))[0]
        movimg = TImage(svsfile, loaderkwargs=dict(pages=3))
        subject = os.path.basename(subjdir.rstrip(os.sep))
        regsheet = os.path.join(subjdir, subject + "_registration_sheet.csv")
        regsheet = pd.read_csv(regsheet)
        svsfile_rel = os.path.relpath(svsfile, os.path.dirname(subjdir))
        record = regsheet[regsheet["HistologyFile"] == svsfile_rel]
        resolution = float(record.HistologyResolutionUM) / 1000 * 16
        moving.resolution = resolution
        movimg.centralise(weighted=True)
        h2pixel = movimg.all_tx().inverse()
        histo.domain.chain = chain_h2h + h2pixel + to_mri
    else:
        histo.domain.chain = to_mri

    # Load MRI volume with FLIRT transformations to TRUFI physical space
    mri = get_mri(subjdir, mmod)

    # Set interpolator type
    mri.interpolator = ipol

    # Select evaluation rule
    if mmod in ("D1", "D2"):
        rule = RULE_FS
    else:
        rule = None

    # Evaluate MRI on the index histology domain and save the output
    resampled_mri = mri.evaluate(histo.domain, rule=rule)
    outdir = os.path.join(
        subjdir, "blocks", block, "histology", hmod, "to_" + mmod)
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
    if isinstance(ipol, tip.ScipyNearestNeighbours):
        tag = "nn"
    else:
        tag = "sp"
    resampled_mri.save(
        os.path.join(outdir, mmod + "_" + tag + ".timg"), overwrite=True)
    resampled_mri.snapshot(
        os.path.join(outdir, mmod + "_" + tag + ".tif"), overwrite=True)
    print(f"Exported registration result for {block}: {hmod} --> {mmod}.")


def get_mri(subjdir, mmod):

    txs = tuple(os.path.join(subjdir, "mri", tx) for tx in TX2INDEX[mmod])
    mfile = os.path.join(subjdir, "mri", MFILES[mmod])

    # Load MRI volume with transformations to physical TRUFI space
    # Case: TRUFI
    if mmod == "TRUFI":
        return TImage(mfile, external="sform", storage="mem")

    # Case: Diffusion
    elif mmod in ("FA", "MD", "AD", "RD", "D1", "D2", "F1", "F2", "P1", "P2"):
        trufifile = os.path.join(subjdir, "mri", MFILES["TRUFI"])
        trufi = TImage(trufifile, external="sform")
        mri = TImage(mfile, storage="mem")
        flirtchain = fsl.load_mat(txs[0], reference=trufifile, input=mfile)
        mri.domain.chain = flirtchain
        mri.domain.chain.append(trufi.domain.chain["sform"])
        # Convert vectors to neurological convention
        if mmod in ("D1", "D2"):
            mri.data[..., 0] *= -1
        return mri

    # Case: Relaxometry, QSM, etc.
    else:
        trufifile = os.path.join(subjdir, "mri", MFILES["TRUFI"])
        trufi = TImage(trufifile, external="sform")
        fafile = os.path.join(subjdir, "mri", "DTI", "dti_FA.nii.gz")
        mri = TImage(mfile, storage="mem")
        fc1 = fsl.load_mat(txs[0], reference=fafile, input=mfile)
        fc2 = fsl.load_mat(txs[1], reference=trufifile, input=fafile)
        mri.domain.chain = fc1 + fc2
        mri.domain.chain.append(trufi.domain.chain["sform"])
        return mri


if __name__ == "__main__":
    if len(sys.argv) > 1:
        subjdir, *blocks = sys.argv[1:]
        main(subjdir, blocks=blocks)
    else:
        print("Usage: ./generate.py <subject_dir> [blocks ...]")
