#!/usr/bin/env python

import os
import glob
import re
import shutil
import pandas as pd
from distutils.dir_util import copy_tree

SRCROOT = "/mnt/rio/mnd"
TRGROOT = "/mnt/alpha/mnd"
SUBJECTS = [
    "CTL10",
    "CTL12",
    "MND12",
    "MND13",
    "MND14",
    "MND15",
    "MND16",
    "MND17",
    "MND18",
    "MND19",
    "MND20",
    "MND21",
    "MND22",
    "MND23"
]


def main():
    """ Main program code."""

    scans_file = "/mnt/alpha/mnd/db/scans.csv"
    acqids, npids = get_id_maps(scans_file)

    for subject in SUBJECTS:
        print(f"Subject: {subject}")
        try:
            copy_subject_data(subject, acqids)
        except:
            continue


def get_id_maps(scans_file):
    table = pd.read_csv(scans_file)
    np_orig = table["np_id"].apply(lambda s: "NP" + s[-3:] + "_" + s[2:4])
    table["np_id"] = np_orig
    acqids = dict()
    npids = dict()
    for ix, item in table[["acq_id", "np_id"]].iterrows():
        acqids.update({item.acq_id: item.np_id})
        npids.update({item.np_id: item.acq_id})
    else:
        return acqids, npids


def copy_subject_data(subject, acqids):
    blocksdir = os.path.join(SRCROOT, acqids[subject], "blocks", "out")
    dirs = [d for d in os.listdir(blocksdir)
            if os.path.isdir(os.path.join(blocksdir, d))]
    for d in dirs:
        trgdir = os.path.join(TRGROOT, subject, "blocks", d, "photo")
        if not os.path.exists(trgdir):
            os.makedirs(trgdir)
        print(f"Dir: {trgdir}")

        src_file = os.path.join(blocksdir, d[:3] + ".tif")
        trg_file = os.path.join(trgdir, d[:3] + ".tif")
        try:
            shutil.copy2(src_file, trg_file)
        except:
            print(f"Could not copy: {trg_file}")
            continue

        src_file = os.path.join(blocksdir, d[:7] + ".tif")
        trg_file = os.path.join(trgdir, d[:3] + "_crop_seg.tif")
        try:
            shutil.copy2(src_file, trg_file)
        except:
            print(f"Could not copy: {trg_file}")

        src_dir = os.path.join(blocksdir, d)
        trg_dir = os.path.join(trgdir, "to_slice")
        if not os.path.exists(trg_dir):
            os.makedirs(trg_dir)
        try:
            copy_tree(src_dir, trg_dir)
        except:
            print(f"Could not copy: {trg_dir}")
            continue


if __name__ == "__main__":
    main()
