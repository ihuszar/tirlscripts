#!/usr/bin/env python

import os
import glob
import re
import shutil

import openslide
import pandas as pd
from distutils.dir_util import copy_tree

SRCROOT = "/mnt/rio/mnd/histology"
TRGROOT = "/mnt/alpha/mnd"
SUBJECTS = [
    "CTL10",
    "CTL12",
    "MND12",
    "MND13",
    "MND14",
    "MND15",
    "MND16",
    "MND17",
    "MND18",
    "MND19",
    "MND20",
    "MND21",
    "MND22",
    "MND23"
]


def main():
    """ Main program code."""

    scans_file = "/mnt/alpha/mnd/db/scans.csv"
    acqids, npids = get_id_maps(scans_file)

    svsfiles = []
    for root, dirs, files in os.walk(os.path.join(SRCROOT)):
        svsfiles.extend(
            [os.path.join(root, f) for f in files if f.endswith(".svs")]
        )
    else:
        svsfiles = sorted(svsfiles)

    for subject in SUBJECTS:
        print(f"Subject: {subject}")
        try:
            copy_subject_data(subject, acqids, svsfiles)
        except:
            continue


def get_id_maps(scans_file):
    table = pd.read_csv(scans_file)
    np_orig = table["np_id"].apply(lambda s: "NP" + s[-3:] + "_" + s[2:4])
    table["np_id"] = np_orig
    acqids = dict()
    npids = dict()
    for ix, item in table[["acq_id", "np_id"]].iterrows():
        acqids.update({item.acq_id: item.np_id})
        npids.update({item.np_id: item.acq_id})
    else:
        return acqids, npids


def copy_subject_data(subject, acqids, svsfiles):
    # Which blocks are we looking for?
    blocks = sorted(os.listdir(os.path.join(TRGROOT, subject, "blocks")))
    blocks = list(filter(lambda x: re.search(r"\d{2}[LR]?a", x), blocks))

    # Copy the raw and the registered histology data for each block
    for bid in blocks:
        try:
            copy_block_data(bid, subject, acqids, svsfiles)
        except AssertionError:
            continue


def copy_block_data(bid, subject, acqids, svsfiles):
    sid = acqids[subject].replace("_", "-")
    svs = list(filter(lambda x: (sid in x) and (bid in x), svsfiles))
    assert len(svs) == 1, f"SVS file is not known for: {sid}, {bid}"
    svs = svs[0]
    sname = os.path.basename(svs)
    trgdir = \
        os.path.join(TRGROOT, subject, "blocks", bid, "histology", "PLP")
    if not os.path.isdir(trgdir):
        os.makedirs(trgdir)

    # Copy the SVS file
    trg_file = os.path.join(trgdir, sname)
    try:
        shutil.copy2(svs, trg_file)
    except:
        print(f"Could not copy: {trg_file}")

    # Create a TIFF file from the SVS
    src_file = trg_file
    trg_file = trg_file.replace(".svs", ".tif")
    sl = openslide.OpenSlide(src_file)
    im = sl.get_thumbnail(sl.level_dimensions[2])
    with open(trg_file, "wb") as fp:
        im.save(fp)

    # Copy the XML file
    trg_file = os.path.join(trgdir, sname.replace(".svs", ".xml"))
    try:
        shutil.copy2(svs.replace(".svs", ".xml"), trg_file)
    except:
        print(f"Could not copy: {trg_file}")

    # Copy the Stage 1 registration directory
    src_dir = svs.replace(".svs", "")
    trg_dir = os.path.join(trgdir, "to_block")
    try:
        copy_tree(src_dir, trg_dir)
    except:
        print(f"Could not copy: {trg_dir}")


if __name__ == "__main__":
    main()
