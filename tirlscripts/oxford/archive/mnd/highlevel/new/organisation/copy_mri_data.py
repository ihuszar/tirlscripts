#!/usr/bin/env python

import os
import shutil
from glob import glob


SRCROOT = "/mnt/alpha/DBB"
TRGROOT = "/mnt/alpha/mnd"
SUBJECTS = [
	"CTL10",
	"CTL12",
	"MND12",
	"MND13",
	"MND14",
	"MND15",
	"MND16",
	"MND17",
	"MND18",
	"MND19",
	"MND20",
	"MND21",
	"MND22",
	"MND23"
]


def copy_data(files, srctag, trgtag, subject):
	for f in files:
		try:
			src_file = os.path.join(SRCROOT, subject, srctag, f)
			trg_file = os.path.join(TRGROOT, subject, "mri", trgtag, f)
			print(trg_file)
			shutil.copy2(src_file, trg_file)
		except:
			print(f"Could not copy: {trg_file}")

def copy_subject(subject):
	""" Main program code."""
		
	# B&S
	print("B&S/")
	copy_data(
		["dyads1.nii.gz",
		"dyads1_dispersion.nii.gz",
		"dyads2.nii.gz",
		"dyads2_dispersion.nii.gz",
		"mean_f1samples.nii.gz",
		"mean_f2samples.nii.gz"], 
		srctag="for_model.Ball_2_Sticks_2_Flips_Weighted_Noisefloor_SSFP",
		trgtag="B&S",
		subject=subject
	)

	# DTI
	print("DTI/")
	copy_data(
		["dti_FA.nii.gz",
		"dti_L1.nii.gz",
		"dti_L2.nii.gz",
		"dti_L3.nii.gz",
		"dti_MD.nii.gz",
		"dti_rad.nii.gz",
		"dti_V1.nii.gz",
		"dti_V2.nii.gz",
		"dti_V3.nii.gz"
		],
		srctag="for_model.DiffusionTensor_2_Flips_Weighted_NoiseFloor_SSFP",
		trgtag="DTI",
		subject=subject
	)

	# MNI
	print("MNI/")
	copy_data(
		["coreg0GenericAffine.mat",
		"coreg1InverseWarp.nii.gz",
		"coreg1Warp.nii.gz",
		"coreg_InverseWarped.nii.gz",
		"coreg_Warped.nii.gz",
		"input_bc.nii.gz",
		],
		srctag="Coregistration_MNI",
		trgtag="MNI",
		subject=subject
	)
	try:
		trg_file = os.path.join(TRGROOT, subject, "mri", "MNI", "MNI152_T1_1mm.nii.gz")
		shutil.copy2("/mnt/knox/dataset/CTL11/mri/MNI/MNI152_T1_1mm.nii.gz", trg_file)
	except:
		print(f"Could not copy: {trg_file}")
	try:
		trg_file = os.path.join(TRGROOT, subject, "mri", "MNI", "MNI152_T1_1mm_brain.nii.gz")
		shutil.copy2("/mnt/knox/dataset/CTL11/mri/MNI/MNI152_T1_1mm_brain.nii.gz", trg_file)
	except:
		print(f"Could not copy: {trg_file}")

	# Segmentation
	print("Segmentation/")
	segfiles = glob(f"/mnt/nemo/mnd/MRI/{subject}/trufi_025mm/*.*")
	for sf in segfiles:
		try:
			sfname = os.path.basename(sf)
			trg_file = os.path.join(TRGROOT, subject, "mri", "Segmentation", sfname)
			print(trg_file)
			shutil.copy2(sf, trg_file)
		except:
			print(f"Could not copy: {trg_file}")

	# T1
	print("T1/")
	copy_data([
		"diff_to_T1.mat",
		"R1map.nii.gz",
		"T1map.nii.gz",
		"T1_to_diff.mat"
		],
		srctag="T1",
		trgtag="T1",
		subject=subject
	)

	# T2
	print("T2/")
	copy_data([
		"diff_to_T2.mat",
		"R2map.nii.gz",
		"T2map.nii.gz",
		"T2map_SE.nii.gz",
		"T2_to_diff.mat"
		],
		srctag="T2",
		trgtag="T2",
		subject=subject
	)

	# T2s
	print("T2s/")
	copy_data([
		"QSM.nii.gz",
		"R2s.nii.gz",
		"T2s.nii.gz",
		"T2s_to_diff.mat"
		],
		srctag="SWI",
		trgtag="T2s",
		subject=subject
	)

	# TRUFI
	print("TRUFI/")
	copy_data([
		"diff_to_trufi.mat",
		"trufi_comb.nii.gz",
		"trufi_to_diff.mat"
		],
		srctag="trufi",
		trgtag="TRUFI",
		subject=subject
	)


def main():

	for subject in SUBJECTS:
		print(f"Subject: {subject}")
		copy_subject(subject)


if __name__ == "__main__":
	main()
