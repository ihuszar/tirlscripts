#!/usr/bin/env python

import os
import re
import shutil
from distutils.dir_util import copy_tree

import pandas as pd

SRCROOT = "/mnt/nemo/mnd"
TRGROOT = "/mnt/alpha/mnd"
SUBJECTS = [
    "CTL10",
    "CTL12",
    "MND12",
    "MND13",
    "MND14",
    "MND15",
    "MND16",
    "MND17",
    "MND18",
    "MND19",
    "MND20",
    "MND21",
    "MND22",
    "MND23"
]


def main():
    """ Main program code."""

    scans_file = "/mnt/alpha/mnd/db/scans.csv"
    acqids, npids = get_id_maps(scans_file)

    for subject in SUBJECTS:
        print(f"Subject: {subject}")
        try:
            copy_subject_data(subject, acqids)
        except:
            continue


def get_id_maps(scans_file):
    table = pd.read_csv(scans_file)
    np_orig = table["np_id"].apply(lambda s: "NP" + s[-3:] + "_" + s[2:4])
    table["np_id"] = np_orig
    acqids = dict()
    npids = dict()
    for ix, item in table[["acq_id", "np_id"]].iterrows():
        acqids.update({item.acq_id: item.np_id})
        npids.update({item.np_id: item.acq_id})
    else:
        return acqids, npids


def copy_subject_data(subject, acqids):
    slicesdir = os.path.join(SRCROOT, acqids[subject], "slices")
    if not os.path.isdir(slicesdir):
        print(f"Could not open {slicesdir}")
        raise

    try:
        slices = sorted(list(filter(
            lambda d: re.search(r"\d{2}[AB]", d),
            os.listdir(slicesdir)
        )))
    except:
        print(f"No slices at: {slicesdir}")
        raise
    else:
        print(slices)

    for sl in slices:
        print(f"Slice: {sl}")
        sl_no = sl[:2]
        sl_side = sl[2]
        sl_side = "A" if sl_side == "A" else "P"
        trgdir = os.path.join(TRGROOT, subject, "slices", sl_no, sl_side)
        if not os.path.exists(trgdir):
            os.makedirs(trgdir)
        srcdir = os.path.join(slicesdir, sl)
        if not os.path.exists(srcdir):
            print(f"Could not open {srcdir}")
            continue

        for obj in os.listdir(srcdir):
            if obj.endswith(".bilateral"):
                print(obj)
                copy_tree(os.path.join(srcdir, obj),
                          os.path.join(trgdir, "to_vol.bilateral"), )
            elif obj.endswith(".left"):
                print(obj)
                copy_tree(os.path.join(srcdir, obj),
                          os.path.join(trgdir, "to_vol.left"))
            elif obj.endswith(".right"):
                print(obj)
                copy_tree(os.path.join(srcdir, obj),
                          os.path.join(trgdir, "to_vol.right"))
            elif os.path.isfile(os.path.join(srcdir, obj)):
                print(obj)
                shutil.copy2(os.path.join(srcdir, obj),
                             os.path.join(trgdir, obj))
            else:
                print(f"Omitted: {obj}")


if __name__ == "__main__":
    main()
