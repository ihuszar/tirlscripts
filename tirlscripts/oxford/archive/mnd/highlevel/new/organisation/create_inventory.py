#!/usr/bin/env python

import os
import sys
import json
import hashlib
import multiprocessing as mp

ROOT = "/mnt/nvme/mnd-reduced/"
N_CPU = 24
SUBJECTS = [
    "CTL10", "CTL11", "CTL12", "MND14", "MND15", "MND16", "MND17", "MND18",
    "MND19", "MND20", "MND21", "MND22", "MND23", "MND24"
]
BUF_SIZE = 100 * 1024 ^ 2  # bytes
db_hashes = dict()


def main(subjects):
    """ Main program code. """

    for subject in subjects:
        process_subject(subject)


def process_subject(subject):
    print(f"Processing subject {subject}...")
    subjdir = os.path.join(ROOT, subject)
    all_files = []
    for root, dirs, files in os.walk(subjdir):
        files = filter(lambda s: not s.startswith("."), files)
        files = [os.path.join(root, f) for f in files]
        all_files += list(files)
    else:
        all_files = sorted(all_files)

    if N_CPU > 1:
        with mp.Pool(processes=N_CPU) as pool:
            hashes = pool.map(get_hash, all_files)
    else:
        hashes = []
        for f in all_files:
            hashes.append(get_hash(f))

    # Create inventory with paths relative to the subject directory
    all_files = [os.path.relpath(f, subjdir) for f in all_files]
    inventory = dict(zip(all_files, hashes))
    inventory_file = os.path.join(subjdir, f"sub-{subject}_inventory.json")
    with open(inventory_file, "w") as f:
        json.dump(inventory, f, sort_keys=True, indent=4)


def get_hash(f):
    try:
        h = db_hashes[f]
    except KeyError:
        hasher = hashlib.sha1()
        with open(f, "rb") as fp:
            while True:
                data = fp.read(BUF_SIZE)
                if not data:
                    break
                else:
                    hasher.update(data)
        h = hasher.hexdigest()
        db_hashes[f] = h
    finally:
        return h


if __name__ == "__main__":
    main(SUBJECTS)
    exit()
    if len(sys.argv) > 1:
        main(sys.argv[1:])
    else:
        print("Usage: ./create_inventory.py <subject>")