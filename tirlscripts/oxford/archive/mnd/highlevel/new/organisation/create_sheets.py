#!/usr/bin/env python

import os
import glob
import re
import shutil
import openslide
import pandas as pd
from glob import glob
from hashlib import sha1
import multiprocessing as mp
from functools import partial
from collections import namedtuple
from distutils.dir_util import copy_tree

ROOTDIR = "/mnt/alpha/mnd"
SUBJECTS = [
    "CTL10",
    "CTL12",
    "MND12",
    "MND13",
    "MND14",
    "MND15",
    "MND16",
    "MND17",
    "MND18",
    "MND19",
    "MND20",
    "MND21",
    "MND22",
    "MND23"
]
COLUMNS = [
    "HistologySHA1",
    "HistologyFile",
    "HistologyResolutionUM",
    "NPSubjectID",
    "FMRIBSubjectID",
    "BlockID",
    "BlockSurface",
    "BlockFile",
    "BlockSHA1",
    "BlockResolutionUM",
    "BlockRegion",
    "HistologyStain",
    "HistologyBatchID",
    "HistologyMagnification",
    "SliceID",
    "SliceSurface",
    "SliceFile",
    "SliceSHA1",
    "SliceResolutionUM",
    "MRIVoxelSpace",
    "MRIVoxelSpaceSHA1"
]
Record = namedtuple("Record",  COLUMNS)
STAINS = ["CD68", "Iba1", "pTDP43", "SMI312", "PLP"]
BUF_SIZE = 100 * 1024 ^ 2  # bytes
RESOLUTIONS = {
    "cd68":    0.5019,
    "iba1":    0.5019,
    "plp":     0.5019,
    "smi312":  0.5019,
    "ptdp43":  0.252,
    "ptdp-43": 0.252
}
MAGNIFICATIONS = {
    "cd68":    20,
    "iba1":    20,
    "plp":     20,
    "smi312":  20,
    "ptdp43":  40,
    "ptdp-43": 40
}
N_CPU = 10
db_regions = "/mnt/alpha/mnd/db/regions.csv"
db_scans = "/mnt/alpha/mnd/db/scans.csv"
db_hashes = dict()


def main():
    """ Main program code."""

    scans_file = "/mnt/alpha/mnd/db/scans.csv"
    acqids, npids = get_id_maps(scans_file)

    # create_mri_sheets(acqids)
    create_registration_sheets(acqids)


def get_id_maps(scans_file):
    table = pd.read_csv(scans_file)
    np_orig = table["np_id"].apply(lambda s: "NP" + s[-3:] + "_" + s[2:4])
    table["np_id"] = np_orig
    acqids = dict()
    npids = dict()
    for ix, item in table[["acq_id", "np_id"]].iterrows():
        acqids.update({item.acq_id: item.np_id})
        npids.update({item.np_id: item.acq_id})
    else:
        return acqids, npids


def create_mri_sheets(acqids):
    src = "/mnt/alpha/mnd/MND24/MND24_MRI_sheet.csv"
    sname = os.path.basename(src)
    for subject in SUBJECTS:
        trgdir = os.path.join(TRGROOT, subject)
        tname = sname.replace("MND24", subject)
        trg_file = os.path.join(trgdir, tname)
        with open(src, "r") as fp:
            content = fp.read()
        content = content.replace("MND24", subject)
        content = content.replace("NP029_16", acqids[subject])
        with open(trg_file, "w") as fp:
            fp.write(content)


def create_registration_sheets(acqids):
    wfunc = partial(process_subject, acqids=acqids)
    with mp.Pool(N_CPU, maxtasksperchild=1) as pool:
        pool.map(wfunc, SUBJECTS)


def process_subject(subject, acqids):
    proc_id = mp.current_process().pid
    print(f"Process {proc_id}: Subject {subject}")
    try:
        records = create_subject_sheet(subject, acqids)
        table = pd.DataFrame(records, columns=COLUMNS)
        rs_file = os.path.join(
            ROOTDIR, subject, subject + "_registration_sheet.csv")
        table.to_csv(rs_file, index=False)
    except:
        print(f"Could not create registration sheet for subject {subject}.")


def get_hash(f):
    try:
        h = db_hashes[f]
    except KeyError:
        hasher = sha1()
        with open(f, "rb") as fp:
            while True:
                data = fp.read(BUF_SIZE)
                if not data:
                    break
                else:
                    hasher.update(data)
        h = hasher.hexdigest()
        db_hashes[f] = h
    finally:
        return h


def create_subject_sheet(subject, acqids):

    # Find all histology files for a given subject
    blockdir = os.path.join(ROOTDIR, subject, "blocks")
    svsfiles = []
    for root, dirs, files in os.walk(blockdir):
        svsfiles.extend([
            os.path.join(root, f) for f in files if f.endswith(".svs")
        ])
    else:
        svsfiles = sorted(svsfiles)

    regions = pd.read_csv(db_regions)
    scans = pd.read_csv(db_scans)

    # Create registration entry for each histology image
    records = []

    for svs in svsfiles:
        print(f"Creating registration entry for {svs}...")
        record = dict()
        record["NPSubjectID"] = acqids[subject]
        record["FMRIBSubjectID"] = subject

        # Histology
        try:
            get_histology_info(svs, record)
        except:
            print(f"Could not get histology info for {svs}.")

        # Block photo
        try:
            block_id = get_block_info(svs, scans, regions, subject, record)
        except:
            print(f"Could not get block info for {svs}.")
            raise AssertionError

        # Slice photo
        get_slice_info(block_id, scans, regions, subject, record)

        # MRI volume
        try:
            get_mri_info(subject, record)
        except:
            print(f"Could not get MRI info for {subject}.")

        records.append(record)

    return records


def get_histology_info(svs, record):

    record["HistologySHA1"] = get_hash(svs)
    record["HistologyFile"] = os.path.relpath(svs, ROOTDIR)
    stain = None
    for stain in STAINS:
        if stain in os.path.basename(svs):
            break
    else:
        print("Unrecognised stain.")
        record["HistologyStain"] = ""
    record["HistologyStain"] = stain.replace("-", "")
    record["HistologyMagnification"] = MAGNIFICATIONS[stain.lower()]
    record["HistologyResolutionUM"] = RESOLUTIONS[stain.lower()]
    record["HistologyBatchID"] = 1


def get_block_info(svs, scans, regions, subject, record):

    block_id = re.search(r"\d\d?[LR]?[ab]", os.path.basename(svs)).group(0)
    record["BlockID"] = block_id[:-1]
    record["BlockSurface"] = block_id[-1]
    blockfile = os.path.join(subject, "blocks", block_id, "photo",
                             block_id + "_crop_seg.tif")
    record["BlockFile"] = blockfile
    record["BlockSHA1"] = get_hash(os.path.join(ROOTDIR, blockfile))
    record["BlockResolutionUM"] = 50
    np_id = scans.loc[scans["acq_id"] == subject].np_id.iloc[0]
    region = regions.loc[
        (regions["np_id"] == np_id) &
        (regions["label"] == "b" + block_id)
        ].iloc[0]
    record["BlockRegion"] = region["name"]
    return block_id


def get_slice_info(block_id, scans, regions, subject, record):
    np_id = scans.loc[scans["acq_id"] == subject].np_id.iloc[0]
    region = regions.loc[
        (regions["np_id"] == np_id) &
        (regions["label"] == "b" + block_id)
        ].iloc[0]
    slice_no = region.parent_label[1:-1]
    slice_surf = region.parent_label[-1]
    slice_surf = "P" if slice_surf == "B" else "A"
    slicedir = os.path.join(ROOTDIR, subject, "slices", slice_no,
                            slice_surf)
    slice_file = sorted(glob(os.path.join(slicedir, "*_crop*_seg.tif")))[0]
    record["SliceID"] = slice_no
    record["SliceSurface"] = slice_surf
    record["SliceFile"] = os.path.relpath(slice_file, ROOTDIR)
    record["SliceSHA1"] = get_hash(slice_file)
    record["SliceResolutionUM"] = 50


def get_mri_info(subject, record):
    mri_file = os.path.join(subject, "mri", "TRUFI", "trufi_comb.nii.gz")
    record["MRIVoxelSpace"] = mri_file
    record["MRIVoxelSpaceSHA1"] = get_hash(os.path.join(ROOTDIR, mri_file))


if __name__ == "__main__":
    main()
