#!/usr/bin/env python

import os
import sys
import json
from glob import glob


INDEX_HMOD = "PLP"


def main(subjdir):
    """ Main program code. """

    subject = os.path.basename(subjdir.rstrip(os.path.sep))
    niftistack = {subject: dict()}
    imgstack = {subject: dict()}
    blocks = os.listdir(os.path.join(subjdir, "blocks"))

    for block in blocks:
        niftistack[subject][block] = dict()
        imgstack[subject][block] = dict()
        stains = os.listdir(os.path.join(subjdir, "blocks", block, "histology"))
        for stain in stains:
            if stain == INDEX_HMOD:
                # Index stain
                niftifile = glob(os.path.join(
                    subjdir, "blocks", block, "histology", INDEX_HMOD,
                    "*.nii.gz"))[0]
                niftistack[subject][block][stain] = niftifile
                tiffile = glob(os.path.join(
                    subjdir, "blocks", block, "histology", INDEX_HMOD,
                    "*.tif"))[0]
                imgstack[subject][block][stain] = tiffile
                # MRI modalities
                mmods = glob(os.path.join(
                    subjdir, "blocks", block, "histology", INDEX_HMOD, "to_*"))
                mmods = [mm for mm in mmods if not mm.endswith("to_block")]
                for mm in sorted(mmods):
                    mdir = os.path.join(
                        subjdir, "blocks", block, "histology", INDEX_HMOD, mm)
                    tag = mm.split("_")[-1]
                    for ipol in ("sp", "nn"):
                        if tag in ("D1", "D2"):
                            ipol = "nn"
                            niftifile = glob(
                                os.path.join(mdir, f"*_{ipol}_0.5mm.nii.gz"))[0]
                        else:
                            niftifile = \
                                glob(os.path.join(mdir, f"*_{ipol}.nii.gz"))[0]
                        niftistack[subject][block][tag + "_" + ipol] = niftifile
                        imgfile = glob(os.path.join(mdir, f"*_{ipol}.tif"))[0]
                        imgstack[subject][block][tag + "_" + ipol] = imgfile
            # Other registered stains
            else:
                niftifile = os.path.join(
                    subjdir, "blocks", block, "histology", stain,
                    "from_" + INDEX_HMOD, "to_" + INDEX_HMOD + ".nii.gz")
                niftistack[subject][block][stain] = niftifile
                pngfile = os.path.join(
                    subjdir, "blocks", block, "histology", stain,
                    "from_" + INDEX_HMOD, "to_" + INDEX_HMOD + ".png")
                imgstack[subject][block][stain] = pngfile

    else:
        with open(os.path.join(subjdir, "niftistack.json"), "w") as f:
            json.dump(niftistack, f, sort_keys=True, indent=4)
        with open(os.path.join(subjdir, "imgstack.json"), "w") as f:
            json.dump(imgstack, f, sort_keys=True, indent=4)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        print("Usage: ./create_stack.py <subject_dir>")
