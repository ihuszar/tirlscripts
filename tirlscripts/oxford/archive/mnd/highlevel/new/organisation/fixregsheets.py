#!/usr/bin/env python

import os
import pandas as pd
from hashlib import sha1


ROOTDIR = "/mnt/alpha/mnd"
BUF_SIZE = 100 * 1024 ^ 2  # bytes
SUBJECTS = [
    "CTL10",
    "CTL12",
    "MND12",
    "MND13",
    "MND14",
    "MND15",
    "MND16",
    "MND17",
    "MND18",
    "MND19",
    "MND20",
    "MND21",
    "MND22",
    "MND23"
]
db_hashes = dict()


def main():
    for subject in SUBJECTS:
        print(f"Processing subject {subject}...")
        csvfile = os.path.join(
            ROOTDIR, subject, f"{subject}_registration_sheet.csv")
        table = pd.read_csv(csvfile)
        os.rename(csvfile, csvfile.replace(".csv", ".csv.bak"))
        mripath = os.path.join(
            subject, "mri", "Segmentation", "trufi_comb_iso.bse.nii.gz")
        table["MRIVoxelSpace"] = mripath
        table["MRIVoxelSpaceSHA1"] = get_hash(os.path.join(ROOTDIR, mripath))
        table.to_csv(csvfile)


def get_hash(f):
    try:
        h = db_hashes[f]
    except KeyError:
        hasher = sha1()
        with open(f, "rb") as fp:
            while True:
                data = fp.read(BUF_SIZE)
                if not data:
                    break
                else:
                    hasher.update(data)
        h = hasher.hexdigest()
        db_hashes[f] = h
    finally:
        return h


if __name__ == "__main__":
    main()

