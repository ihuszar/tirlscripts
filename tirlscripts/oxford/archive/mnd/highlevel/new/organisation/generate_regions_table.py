#!/usr/bin/env python

import os
import re
import uuid
import pandas as pd
from glob import glob


COLUMNS = ["region_uid", "np_id", "label", "parent_label", "type", "name"]


def main():
    """ Main program code. """

    sheets = glob("/mnt/rio/mnd/sampling_sheets/NP*.csv")
    records = []
    for sheet in sheets:
        records.extend(standardise(sheet))
    else:
        regions = pd.DataFrame(records, columns=COLUMNS)

    print(regions)
    regions.to_csv("/mnt/alpha/mnd/db/regions.csv", index=False)


def standardise(sheet):
    table = pd.read_csv(sheet, keep_default_na=False)
    records = []
    for ix, row in table.iterrows():
        # Make block IDs uniform
        block_id = row.Block
        if not block_id:
            continue
        block_no = re.search(r"\d+", block_id).group(0)
        block_surf = block_id.replace(block_no, "")
        if block_id[0].upper() == "C":
            block_id = f"b{block_surf}{int(block_no):02d}"
        else:
            block_id = f"b{int(block_no):02d}{block_surf}"

        # Make slice IDs uniform
        slice_id = str(row.Slice)
        if len(slice_id) > 4:
            print(f"Invalid slice ID: {slice_id}")
            slice_id = ""
        if slice_id:
            try:
                slice_no = re.search(r"\d+", slice_id).group(0)
                slice_surf = slice_id.replace(slice_no, "")
            except:
                print(f"Invalid slice ID: {slice_id}")
                slice_id = ""
            else:
                if slice_id[0].upper() == "L":
                    slice_id = f"s{slice_surf}{int(slice_no):02d}"
                else:
                    slice_id = f"s{int(slice_no):02d}{slice_surf}"

        # Put it all together
        np_id, _ = os.path.splitext(os.path.basename(sheet))
        np_id = "NP" + np_id[-2:] + "/" + np_id[2:5]
        record = {
            "region_uid": str(uuid.uuid4()),
            "np_id": np_id,
            "label": block_id,
            "parent_label": slice_id,
            "type": "block",
            "name": row.Region
        }
        records.append(record)

    return records


if __name__ == "__main__":
    main()
