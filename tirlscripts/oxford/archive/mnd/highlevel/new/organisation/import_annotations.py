#!/usr/bin/env python

import os
import re
import shutil
from glob import glob
import pandas as pd

SOURCE = "/mnt/elisa/proc/postmort/histology/"
TARGET = "/mnt/alpha/mnd/"
STAINS = ["CD68", "PLP", "Iba1", "pTDP43", "SMI312"]
SUBJECTS = ["CTL10", "CTL12", "MND14", "MND15", "MND16", "MND17", "MND18",
            "MND19", "MND20", "MND21", "MND22", "MND23"]


def main():
    """ Main program code. """

    # Get target directories
    dirs = glob(os.path.join(TARGET, "*", "blocks", "*", "histology", "*"))
    dirs = filter(lambda x: any(re.search(s, x) for s in STAINS), dirs)
    dirs = filter(lambda x: any(re.search(s, x) for s in SUBJECTS), dirs)
    dirs = sorted(list(dirs))
    print("\n".join(dirs))

    # Build inventory
    inventory = create_inventory(SOURCE)
    print(inventory)

    # Choose the input for each of the targets
    for d in dirs:
        copy_dir(d, inventory)


def create_inventory(source):
    inventory = []
    columns = ["np", "block", "stain", "path"]
    for root, dirs, files in os.walk(source):
        if "AnnotationLayer_00" in dirs:
            d = os.path.basename(root)
            np = re.search(r"NP\d+-\d{2}", d).group(0)
            stain = crossmatch(os.path.basename(root), STAINS)
            try:
                block = re.search(r"\d+[LR]?[ab]", d).group(0)
            except AttributeError:
                block = None
            inventory.append((np, block, stain, root))

    return pd.DataFrame(inventory, columns=columns)


def copy_dir(d, inventory):
    subject = crossmatch(d, SUBJECTS)
    stain = crossmatch(d, STAINS)
    block = re.search(r"(?<=blocks\/).+(?=\/histology)", d).group(0)
    np = get_np(subject)
    try:
        src = inventory[(inventory["np"] == np) &
                        (inventory["block"] == block) &
                        (inventory["stain"] == stain)].iloc[0].path
    except Exception as exc:
        block = block.lstrip("0")
        try:
            src = inventory[(inventory["np"] == np) &
                            (inventory["block"] == block) &
                            (inventory["stain"] == stain)].iloc[0].path
        except:
            print(subject, block, stain, np)
            print(None, "-->", d)
        else:
            trg = os.path.join(d, "annotation")
            os.makedirs(trg, exist_ok=True)
            try:
                shutil.copytree(src, trg, dirs_exist_ok=True)
            except shutil.Error:
                pass
    else:
        trg = os.path.join(d, "annotation")
        os.makedirs(trg, exist_ok=True)
        try:
            shutil.copytree(src, trg, dirs_exist_ok=True)
        except shutil.Error:
            pass


def get_np(subject):
    sheet = os.path.join(TARGET, subject, f"{subject}_registration_sheet.csv")
    table = pd.read_csv(sheet)
    return str(table.iloc[0].NPSubjectID).replace("_", "-")


def crossmatch(d, values):
    for s in values:
        res = re.search(s, d)
        if res:
            return res.group(0)
    else:
        return None


if __name__ == "__main__":
    main()
