#!/usr/bin/env python

import os
import sys
import tirl
import pandas as pd
from glob import glob
from tirl.timage import TImage


def main(subjdir):
    """ Main program code. """
    subject = os.path.basename(subjdir)
    regsheet = os.path.join(subjdir, subject + "_registration_sheet.csv")
    regsheet = pd.read_csv(regsheet)
    for root, dirs, files in os.walk(subjdir):
        if root.endswith("from_PLP"):
            print(root)
            domain = tirl.load(os.path.join(root, "fixed4_nonlinear.dom"))
            print(domain)
            hfile = glob(os.path.join(os.path.dirname(root), "*.svs"))[0]
            hfile_rel = os.path.relpath(hfile, os.path.dirname(subjdir))
            record = regsheet[regsheet.HistologyFile == hfile_rel]
            img = TImage(hfile, loaderkwargs=dict(pages=3))
            img.resolution = float(record.HistologyResolutionUM) / 1000 * 16
            img.evaluate(domain).snapshot(os.path.join(root, "to_PLP.png"))


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        print("Usage: ./resample_rgb_histology.py <subject_dir>")
