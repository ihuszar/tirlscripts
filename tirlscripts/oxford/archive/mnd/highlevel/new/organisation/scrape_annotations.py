#!/usr/bin/env python

import os
import shutil

SOURCE = "/mnt/alpha/mnd/"
TARGET = "/mnt/nvme/mnd-reduced/"


def main():
    """ Main program code. """

    inventory = []
    for root, dirs, files in os.walk(SOURCE):
        if os.path.basename(root) == "annotation":
            path = root.split(os.sep)
            subject = path[-6]
            block = path[-4]
            stain = path[-2]
            inventory.append((subject, block, stain, root))

    print("\n".join(str(t) for t in inventory))

    for subject, block, stain, srcdir in inventory:
        trgdir = os.path.join(TARGET, subject, "blocks", block,
                              "annotations", stain)
        print(srcdir, "-->", trgdir)
        os.makedirs(trgdir, exist_ok=True)
        shutil.copytree(srcdir, trgdir, dirs_exist_ok=True)


if __name__ == "__main__":
    main()
