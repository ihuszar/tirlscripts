#!/usr/bin/env python

# This program scrapes the MND folder to create a user-facing version
# of the registered MRI-histology image data.

# DEPENDENCIES

import os
import re
import sys
import json
import uuid
import shutil
import hashlib

import imageio
import logging
import openslide
import numpy as np
import pandas as pd
from glob import glob
import nibabel as nib
import multiprocessing as mp
from functools import partial
from datetime import datetime
from collections import ChainMap

import tirl
from tirl.chain import Chain
from tirl.timage import TImage
from tirl.fsl import FlirtMatrix
from tirl.interpolation.scipyinterpolator import ScipyInterpolator
from tirl.interpolation.scipyinterpolator import ScipyNearestNeighbours


# DEFINITIONS

SOURCE = "/mnt/alpha/mnd"
TARGET = "/mnt/nvme/mnd-reduced"
SUBJECTS = [
    "CTL10", "CTL12", "MND14", "MND15", "MND16", "MND17", "MND18",
    "MND19", "MND20", "MND21", "MND22", "MND23"
]
HMODS = [
    "CD68", "Iba1", "PLP", "pTDP43", "SMI312"
]
SCALAR_MMODS = {
    "TRUFI": "mri" + os.sep + "TRUFI" + os.sep + "trufi_comb.nii.gz",
    "T1":    "mri" + os.sep + "T1" + os.sep + "T1map.nii.gz",
    "R1":    "mri" + os.sep + "T1" + os.sep + "R1map.nii.gz",
    "T2":    "mri" + os.sep + "T2" + os.sep + "T2map.nii.gz",
    "T2SE": "mri" + os.sep + "T2" + os.sep + "T2map_SE.nii.gz",
    "T2s":   "mri" + os.sep + "T2s" + os.sep + "T2s.nii.gz",
    "R2s":   "mri" + os.sep + "T2s" + os.sep + "R2s.nii.gz",
    "QSM":   "mri" + os.sep + "T2s" + os.sep + "QSM.nii.gz",
    "AD":    "mri" + os.sep + "DTI" + os.sep + "dti_L1.nii.gz",
    "RD":    "mri" + os.sep + "DTI" + os.sep + "dti_rad.nii.gz",
    "MD":    "mri" + os.sep + "DTI" + os.sep + "dti_MD.nii.gz",
    "FA":    "mri" + os.sep + "DTI" + os.sep + "dti_FA.nii.gz",
}
VECTORLIKE_MMODS = {
    "V1": "mri" + os.sep + "DTI" + os.sep + "dti_V1.nii.gz",
    "V2": "mri" + os.sep + "DTI" + os.sep + "dti_V2.nii.gz",
    "V3": "mri" + os.sep + "DTI" + os.sep + "dti_V3.nii.gz",
    "L1": "mri" + os.sep + "DTI" + os.sep + "dti_L1.nii.gz",
    "L2": "mri" + os.sep + "DTI" + os.sep + "dti_L2.nii.gz",
    "L3": "mri" + os.sep + "DTI" + os.sep + "dti_L3.nii.gz",
    "D1": "mri" + os.sep + "B&S" + os.sep + "dyads1.nii.gz",
    "D2": "mri" + os.sep + "B&S" + os.sep + "dyads2.nii.gz",
    "F1": "mri" + os.sep + "B&S" + os.sep + "mean_f1samples.nii.gz",
    "F2": "mri" + os.sep + "B&S" + os.sep + "mean_f2samples.nii.gz",
    "P1": "mri" + os.sep + "B&S" + os.sep + "dyads1_dispersion.nii.gz",
    "P2": "mri" + os.sep + "B&S" + os.sep + "dyads2_dispersion.nii.gz"
}

diff_to_trufi = (
    FlirtMatrix(
        "mri/TRUFI/diff_to_trufi.mat", input=SCALAR_MMODS["FA"],
        reference=SCALAR_MMODS["TRUFI"]
    ),
)

t1_to_trufi = (
    FlirtMatrix(
        "mri/T1/T1_to_diff.mat", input=SCALAR_MMODS["T1"],
        reference=SCALAR_MMODS["FA"]
    ),
    *diff_to_trufi
)

t2_to_trufi = (
    FlirtMatrix(
        "mri/T2/T2_to_diff.mat", input=SCALAR_MMODS["T2"],
        reference=SCALAR_MMODS["FA"]
    ),
    *diff_to_trufi
)

t2s_to_trufi = (
    FlirtMatrix(
        "mri/T2s/T2s_to_diff.mat", input=SCALAR_MMODS["T2s"],
        reference=SCALAR_MMODS["FA"]
    ),
    *diff_to_trufi
)

XFM2TRUFI = {
    "T1": t1_to_trufi,
    "R1": t1_to_trufi,
    "T2": t2_to_trufi,
    "R2": t2_to_trufi,
    "T2SE": t2_to_trufi,
    "T2s": t2s_to_trufi,
    "R2s": t2s_to_trufi,
    "QSM": t2s_to_trufi,
    "FA": diff_to_trufi,
    "MD": diff_to_trufi,
    "AD": diff_to_trufi,
    "RD": diff_to_trufi,
    "V1": diff_to_trufi,
    "V2": diff_to_trufi,
    "V3": diff_to_trufi,
    "L1": diff_to_trufi,
    "L2": diff_to_trufi,
    "L3": diff_to_trufi,
    "D1": diff_to_trufi,
    "D2": diff_to_trufi,
    "F1": diff_to_trufi,
    "F2": diff_to_trufi,
    "P1": diff_to_trufi,
    "P2": diff_to_trufi,
    "TRUFI": ()
}

RESOLUTION = 8  # um/px
VECTOR_RESOLUTION = 500  # um/px
OVERWRITE = False
N_CPU = 3
BUF_SIZE = 100 * 1024 ^ 2  # bytes
LOGLEVEL = 1

db_hashes = dict()
mainlogger = logging.getLogger("scraper.main")
logger = mainlogger


# IMPLEMENTATION

def main(subject, blocks):

    # Make sure that the target directory tree exists
    os.makedirs(TARGET, exist_ok=True)

    # Configure main logger instance
    now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    rnd = uuid.uuid4()
    mainlogfile = os.path.join(TARGET, f"scraper-{now}-{rnd}.log")
    global mainlogger
    mainlogger = create_logger("scraper.main", mainlogfile, LOGLEVEL)

    # Scrape files from the specified subject
    inventory = scrape_subject(subject, blocks)
    # Create file inventory
    subjdir = os.path.join(TARGET, subject)
    inventory = {os.path.relpath(k, subjdir): v
                 for k, v in inventory.items()}
    invfile = os.path.join(TARGET, subject, "inventory.json")
    with open(invfile, "w") as fp:
        json.dump(inventory, fp, indent=4, sort_keys=True)


def create_logger(loggername, logfile, loglevel):

    logger = logging.getLogger(loggername)
    logger.handlers = []  # delete any existing handlers to avoid duplicate logs
    logger.setLevel(1)
    formatter = logging.Formatter(
        fmt="%(asctime)s Process-%(process)d %(levelname)s (%(lineno)d) "
            "- %(message)s",
        datefmt='[%Y-%m-%d %H:%M:%S]')

    # Redirect all logs of interest to the specified logfile
    fh = logging.FileHandler(logfile, mode="a", delay=False)
    fh.setLevel(loglevel)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Redirect only warnings/errors to the standard output, unless "verbose"
    ch = logging.StreamHandler()
    ch.setLevel(loglevel)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


def scrape_subject(subject, blocks=None):
    """ Main program code. """

    mainlogger.info(f"Processing subject {subject}...")
    inventory = dict()

    # Copy the registration sheet
    hashes = copy_registration_sheet(subject, OVERWRITE)
    inventory.update(hashes)

    # Copy MRI data
    hashes = copy_mri(subject, OVERWRITE)
    inventory.update(hashes)

    # Convert FSL transformations to TIRL chains
    tirlify_transformations(subject, OVERWRITE)

    # Define blocks for the current subject
    bdir = os.path.join(SOURCE, subject, "blocks")
    if not blocks:
        blocks = filter(lambda x: re.match(r"\d+[LR]?a", x), os.listdir(bdir))
    blocks = [d for d in blocks if os.path.isdir(os.path.join(bdir, d))]
    blocks = sorted(blocks)

    # Copy and create files for each block of the same subject (in parallel)
    workerfunc = partial(scrape_block, subject=subject)
    if N_CPU > 1:
        with mp.Pool(processes=N_CPU, maxtasksperchild=1) as pool:
            hashes = pool.map(workerfunc, blocks)
            inventory.update(dict(ChainMap(*hashes)))
    else:
        for block in sorted(blocks):
            hashes = scrape_block(block, subject)
            inventory.update(hashes)

    return inventory


def copy_registration_sheet(subject, overwrite):
    mainlogger.info(f"Copying the registration sheet for {subject}...")
    srcf = os.path.join(SOURCE, subject, f"{subject}_registration_sheet.csv")
    trgf = os.path.join(TARGET, subject, f"{subject}_registration_sheet.csv")
    code = safe_copy(srcf, trgf, overwrite)
    mainlogger.info(f"FINISHED copying the registration sheet for {subject}.")
    return code


def copy_mri(subject, overwrite):
    mainlogger.info(f"Copying MRI data...")
    mrilist = "/mnt/alpha/mnd/scripts/4_CleanUp/mrilist.txt"
    hashes = dict()
    with open(mrilist, "r") as fp:
        mrifiles = fp.readlines()
    for f in mrifiles:
        f = f.rstrip("\n")
        srcf = os.path.join(SOURCE, subject, "mri", f)
        trgf = os.path.join(TARGET, subject, "mri", f)
        if os.path.isfile(trgf) and not overwrite:
            code = get_hash(trgf)
            logger.info(f"File already exists ({code}): {trgf}")
            hashes.update({trgf: code})
            continue
        code = safe_copy(srcf, trgf, overwrite)
        hashes.update(code)

    mainlogger.info("FINISHED copying MRI data.")
    return hashes


def tirlify_transformations(subject, overwrite):

    subjdir = os.path.join(TARGET, subject)
    trufivol = os.path.join(subjdir, "mri", "TRUFI", "trufi_comb.nii.gz")
    diffvol = os.path.join(subjdir, "mri", "DTI", "dti_FA.nii.gz")
    t1vol = os.path.join(subjdir, "mri", "T1", "T1map.nii.gz")
    t2vol = os.path.join(subjdir, "mri", "T2", "T2map.nii.gz")
    t2svol = os.path.join(subjdir, "mri", "T2s", "T2s.nii.gz")
    isovol = os.path.join(
        subjdir, "mri", "Segmentation", "trufi_comb_iso.nii.gz")

    # Check if the reference volume files actually exist
    for vfile in (trufivol, diffvol, t1vol, t2vol, t2svol, isovol):
        if not os.path.isfile(vfile):
            logger.error(f"TIRL transformations cannot be created because of "
                         f"a missing MRI reference volume: {vfile}")
            return

    hashes = dict()

    # Diffusion to T1
    matfile = os.path.join(subjdir, "mri", "T1", "diff_to_T1.mat")
    chain = FlirtMatrix(matfile, input=diffvol, reference=t1vol)()
    txfile = matfile.replace(".mat", ".chain")
    code = safe_save(chain, "save", txfile, overwrite)
    hashes.update(code)

    # T1 to Diffusion
    matfile = os.path.join(subjdir, "mri", "T1", "T1_to_diff.mat")
    chain = FlirtMatrix(matfile, input=t1vol, reference=diffvol)()
    txfile = matfile.replace(".mat", ".chain")
    code = safe_save(chain, "save", txfile, overwrite)
    hashes.update(code)

    # Diffusion to T2
    matfile = os.path.join(subjdir, "mri", "T2", "diff_to_T2.mat")
    chain = FlirtMatrix(matfile, input=diffvol, reference=t2vol)()
    txfile = matfile.replace(".mat", ".chain")
    code = safe_save(chain, "save", txfile, overwrite)
    hashes.update(code)

    # T2 to Diffusion
    matfile = os.path.join(subjdir, "mri", "T2", "T2_to_diff.mat")
    chain = FlirtMatrix(matfile, input=t2vol, reference=diffvol)()
    txfile = matfile.replace(".mat", ".chain")
    code = safe_save(chain, "save", txfile, overwrite)
    hashes.update(code)

    # T2s to Diffusion
    matfile = os.path.join(subjdir, "mri", "T2s", "T2s_to_diff.mat")
    chain = FlirtMatrix(matfile, input=t2svol, reference=diffvol)()
    txfile = matfile.replace(".mat", ".chain")
    code = safe_save(chain, "save", txfile, overwrite)
    hashes.update(code)

    # Diffusion to TRUFI
    matfile = os.path.join(subjdir, "mri", "TRUFI", "diff_to_trufi.mat")
    chain = FlirtMatrix(matfile, input=diffvol, reference=trufivol)()
    txfile = matfile.replace(".mat", ".chain")
    code = safe_save(chain, "save", txfile, overwrite)
    hashes.update(code)

    # TRUFI to Diffusion
    matfile = os.path.join(subjdir, "mri", "TRUFI", "trufi_to_diff.mat")
    chain = FlirtMatrix(matfile, input=trufivol, reference=diffvol)()
    txfile = matfile.replace(".mat", ".chain")
    code = safe_save(chain, "save", txfile, overwrite)
    hashes.update(code)

    # ISO to TRUFI
    matfile = os.path.join(subjdir, "mri", "Segmentation", "trufi_comb_iso.mat")
    chain = FlirtMatrix(matfile, input=isovol, reference=trufivol)()
    txfile = matfile.replace(".mat", ".chain")
    code = safe_save(chain, "save", txfile, overwrite)
    hashes.update(code)

    return hashes


def scrape_block(block, subject):

    # Log
    mainlogger.info(f"Processing {subject}/{block}...")
    hashes = dict()

    # Create report dir
    reportdir = os.path.join(TARGET, subject, "blocks", block, "report")
    try:
        os.makedirs(reportdir, exist_ok=True)
        mainlogger.info(f"Created report directory for {subject}/{block}: "
                        f"{reportdir}")
    except Exception as exc:
        mainlogger.error(f"ERROR while creating report dir: {reportdir} "
                         f"- {exc.args}")

    # Create logger
    global logger
    loggername = f"scraper.{subject}.{block}"
    logfile = os.path.join(reportdir, f"sub-{subject}_block-{block}.log")
    logger = create_logger(loggername, logfile, LOGLEVEL)  # log everything
    mainlogger.info(f"Created block-specific logger for {subject}/{block}")

    # STEPS
    err = 0

    # Copy the SVS files (raw data)
    try:
        code = copy_raw_histology(block, subject, OVERWRITE)
        hashes.update(code)
    except Exception as exc:
        err += 1
        logger.error(f"ERROR while copying raw histology data "
                     f"for {subject}/{block}: {exc.args}")
    # Create JSON file with exact resolution value for the nominal resolution
    try:
        create_data_descriptor_file(block, subject, RESOLUTION)
    except Exception as exc:
        err += 1
        logger.error(f"ERROR while creating data descriptor file "
                     f"for {subject}/{block}: {exc.args}")
    # Create more easily readable histology image data at a given resolution
    try:
        code = create_png_and_nifti_histology(
            block, subject, RESOLUTION, OVERWRITE)
        hashes.update(code)
    except Exception as exc:
        err += 1
        logger.error(f"ERROR while creating PNG and NIfTI files "
                     f"from the raw histology data for {subject}/{block}: "
                     f"{exc.args}")
    # Retrieve the histology-to-histology transformations and their inverses
    try:
        code = copy_h2h_registrations(block, subject, RESOLUTION, OVERWRITE)
        hashes.update(code)
    except Exception as exc:
        err += 1
        logger.error(f"ERROR while retrieving the histology-to-histology "
                     f"transformation chains for {subject}/{block}: {exc.args}")
    # Create histology images that are warped onto the downsampled PLP domain
    try:
        code = create_h2h_images(block, subject, RESOLUTION, OVERWRITE)
        hashes.update(code)
    except Exception as exc:
        err += 1
        logger.error(f"ERROR while warping histology data onto "
                     f"the PLP domains for {subject}/{block}: {exc.args}")
    # Retrieve the PLP-to-TRUFI transformation chain (resolution agnostic)
    try:
        code = copy_h2v_registration(block, subject, OVERWRITE)
        hashes.update(code)
    except Exception as exc:
        err += 1
        logger.error(f"ERROR while retrieving the combined PLP-to-TRUFI "
                     f"transformation chains for {subject}/{block}: {exc.args}")
    # Scalar MRI evaluations
    try:
        code = perform_mri_evaluations(
            block, subject, SCALAR_MMODS, RESOLUTION, OVERWRITE)
        hashes.update(code)
    except Exception as exc:
        err += 1
        logger.error(f"ERROR while evaluating scalar MRI modalities on the "
                     f"registered histology domain for {subject}/{block}: "
                     f"{exc.args}")
    # Vector-like MRI evaluations
    try:
        code = perform_mri_evaluations(
            block, subject, VECTORLIKE_MMODS, VECTOR_RESOLUTION, OVERWRITE)
        hashes.update(code)
    except Exception as exc:
        err += 1
        logger.error(f"ERROR while evaluating vector-like MRI modalities on "
                     f"the registered histology domain for {subject}/{block}: "
                     f"{exc.args}")
    # Copy intermediate photographs (block and slice)
    try:
        code = copy_intermediaries(block, subject, OVERWRITE)
        hashes.update(code)
    except Exception as exc:
        err += 1
        logger.error(f"ERROR while copying photographic intermediaries "
                     f"for {subject}/{block}: {exc.args}")
    # Copy report files (to monitor stage-specific registration accuracy)
    try:
        code = copy_report_files(block, subject, OVERWRITE)
        hashes.update(code)
    except Exception as exc:
        err += 1
        logger.error(f"ERROR while copying stage-specific audit files "
                     f"for {subject}/{block}: {exc.args}")

    mainlogger.info(f"FINISHED processing {subject}/{block}.")
    if err:
        mainlogger.info(f"Total number of errors: {err}")

    return hashes


def copy_raw_histology(block, subject, overwrite):
    mainlogger.info(f"Copying raw histology data for {subject}/{block}...")
    logger.info(f"Copying raw histology data for {subject}/{block}...")
    hashes = dict()
    src = os.path.join(SOURCE, subject, "blocks", block, "histology")
    for root, dirs, files in os.walk(src):
        for f in files:
            if f.lower().endswith(".svs"):
                stain = get_stain(f)
                srcf = os.path.join(root, f)
                trgf = os.path.join(
                    TARGET, subject, "blocks", block,
                    f"sub-{subject}_block-{block}_stain-{stain}.svs")
                code = safe_copy(srcf, trgf, overwrite)
                hashes.update(code)
    logger.info(f"FINISHED copying raw histology data for {subject}/{block}.")
    mainlogger.info(
        f"FINISHED copying raw histology data for {subject}/{block}.")
    return hashes


def create_data_descriptor_file(block, subject, resolution):
    # Get PLP as a prototype
    plp = load_svs(subject, block, "PLP", resolution)
    sheet = os.path.join(TARGET, subject, f"{subject}_registration_sheet.csv")
    table = pd.read_csv(sheet)
    record = table[(table["BlockID"] == block[:-1]) &
                   (table["HistologyStain"] == "PLP")].iloc[0]
    # Create descriptor file
    descriptor = os.path.join(
        TARGET, subject, "blocks", block,
        f"sub-{subject}_block-{block}_res-{resolution}.json")
    desc = dict(
        FMRIBSubjectID=str(record.FMRIBSubjectID),
        NPSubjectID=str(record.NPSubjectID),
        Block=str(record.BlockID) + str(record.BlockSurface),
        Region=str(record.BlockRegion),
        Slice=str(record.SliceID) + str(record.SliceSurface),
        NativeResolution=record.HistologyResolutionUM / 1000,
        Resolution=tuple(plp.resolution),
        Unit="mm/px",
        HistologyModalities=tuple(HMODS),
        MRIModalities=dict(ChainMap(VECTORLIKE_MMODS, SCALAR_MMODS)))
    del plp

    # Save descriptor file
    with open(descriptor, "w") as fp:
        json.dump(desc, fp, indent=4, sort_keys=True)


def create_png_and_nifti_histology(block, subject, resolution, overwrite):
    mainlogger.info(f"Creating downsampled ({resolution} um/px) PNG and NIfTI "
                f"versions of the histology images for {subject}/{block}...")
    logger.info(f"Creating downsampled ({resolution} um/px) PNG and NIfTI "
                f"versions of the histology images for {subject}/{block}...")
    src = os.path.join(TARGET, subject, "blocks", block)
    svsfiles = sorted(glob(os.path.join(src, "*.svs")))
    hashes = dict()
    for svs in svsfiles:
        pngfile = svs.replace(".svs", f"_res-{resolution}.png")
        niftifile = svs.replace(".svs", f"_res-{resolution}.nii.gz")
        if os.path.isfile(pngfile) and \
                os.path.isfile(niftifile) and not overwrite:
            pcode = get_hash(pngfile)
            ncode = get_hash(niftifile)
            hashes.update({
                pngfile: pcode,
                niftifile: ncode
            })
            logger.info(f"File already exists ({pcode}): {pngfile}")
            logger.info(f"File already exists ({ncode}): {niftifile}")
            continue
        im = load_svs(subject, block, get_stain(svs), resolution)
        # Create PNG
        code = safe_save(im, "snapshot", pngfile, overwrite)
        hashes.update(code)
        # Create NIfTI
        affine = np.diag([*im.resolution, 1., 1.])
        rgb_dtype = np.dtype([('R', 'u1'), ('G', 'u1'), ('B', 'u1')])
        nifti = nib.Nifti1Image(im.data.view(rgb_dtype), affine)
        code = safe_save(nifti, "save", niftifile, overwrite)
        hashes.update(code)
        del im

    logger.info(f"FINISHED creating downsampled ({resolution} um/px) PNG and "
                f"NIfTI versions of the histology images for "
                f"{subject}/{block}...")
    mainlogger.info(f"FINISHED creating downsampled ({resolution} um/px) PNG "
                    f"and NIfTI versions of the histology images for "
                    f"{subject}/{block}...")
    return hashes


def copy_h2h_registrations(block, subject, resolution, overwrite):
    logger.info(f"Copying histology-to-histology transformation chains for "
                f"{subject}/{block}...")
    mainlogger.info(f"Copying histology-to-histology transformation chains "
                    f"for {subject}/{block}...")
    src = os.path.join(SOURCE, subject, "blocks", block, "histology")
    trg = os.path.join(TARGET, subject, "blocks", block)
    fimgs = glob(os.path.join(src, "*", "to_PLP", "fixed4_nonlinear.timg"))

    if not fimgs:
        # Compatibility with the CT11+MND24 small dataset
        fimgs = glob(os.path.join(src, "*", "to_PLP", "fixed4_nonlinear.dom"))

    hashes = dict()

    for fixed in sorted(fimgs):
        if not os.path.isfile(fixed):
            continue
        stain = get_stain(fixed)[0]
        fname = f"sub-{subject}_block-{block}_{stain}toPLP.chain"
        fname = os.path.join(trg, fname)
        iname = f"sub-{subject}_block-{block}_PLPto{stain}.chain"
        iname = os.path.join(trg, iname)
        if os.path.isfile(fname) and os.path.isfile(iname) and not overwrite:
            fcode = get_hash(fname)
            icode = get_hash(iname)
            hashes.update({
                fname: fcode,
                iname: icode
            })
            logger.info(f"File already exists ({fcode}): {fname}")
            logger.info(f"File already exists ({icode}): {iname}")
            continue
        fimg = tirl.load(fixed)

        if hasattr(fimg, "__domain__"):
            # Compatibility with the CT11+MND24 small dataset
            fimg = TImage(np.ones(fimg.shape), domain=fimg)

        # The warp field was optimised at 0.25x of the scale of the smallest
        # thumbnail. Since the fixed images were saved at this scale, it is
        # fine to rescale them and make the warp field coarser too.
        # While it is not ideal to interpolate the warps here, it saves a lot
        # of computation time.
        fimg.rescale(0.25, copy=False)  # regrid nonlinear tx
        fchain = get_subchain(fimg.domain.all_tx(), "resolution", None)
        moving = os.path.join(os.path.dirname(fixed), "moving.timg")

        try:
            mimg = tirl.load(moving)
        except FileNotFoundError as exc:
            # Compatibility with the CT11+MND24 small dataset
            cnf = os.path.join(os.path.dirname(fixed), "configuration.json")
            with open(cnf, "r") as fp:
                cnf = dict(json.load(fp))
            mimg = TImage(
                cnf["moving"]["file"],
                loaderkwargs={"pages": cnf["moving"]["pages"]}
            )
            mimg.resolution = cnf["moving"]["resolution"]
            # No centralisation, since the fixed chain already contains the
            # inverse of the centralisation in this previous dataset.
            # mimg.centralise()

        mchain = get_subchain(mimg.domain.all_tx(), "resolution", None)
        chain = fchain + mchain.inverse()
        chain = resolution_free(chain)
        code = safe_save(chain, "save", fname, overwrite)
        hashes.update(code)
        inv = chain.inverse()
        inv = resolution_free(inv)
        code = safe_save(inv, "save", iname, overwrite)
        hashes.update(code)

    logger.info(f"FINISHED copying histology-to-histology transformation "
                f"chains for {subject}/{block}.")
    mainlogger.info(f"FINISHED copying histology-to-histology transformation "
                    f"chains for {subject}/{block}.")
    return hashes


def resolution_free(chain):
    chain = Chain(*chain)
    # Trim resolution-related transformations
    if "resolution" in chain[0].name.lower():
        chain = chain[1:]
    if "resolution" in chain[-1].name.lower():
        chain = chain[:-1]
    return chain


def safe_save(obj, method, fpath, overwrite):
    if os.path.isfile(fpath):
        if overwrite:
            try:
                code = get_hash(fpath)
                os.remove(fpath)
                logger.info(f"Removed existing file ({code}): fpath")
            except Exception as exc:
                logger.error(f"ERROR while removing existing file: {fpath}")
                logger.error(exc.args)
                return dict()
            if isinstance(obj, nib.Nifti1Image):
                nib.save(obj, fpath)
                code = get_hash(fpath)
                logger.info(f"Created new file ({code}): {fpath}")
                return {fpath: code}
            else:
                getattr(obj, method)(fpath)
                code = get_hash(fpath)
                logger.info(f"Created new file ({code}): {fpath}")
                return {fpath: code}
        else:
            code = get_hash(fpath)
            logger.warning(f"File already exists ({code}): {fpath}")
            return {fpath: code}
    else:
        if isinstance(obj, nib.Nifti1Image):
            nib.save(obj, fpath)
            code = get_hash(fpath)
            logger.info(f"Created new file ({code}): {fpath}")
            return {fpath: code}
        else:
            getattr(obj, method)(fpath)
            code = get_hash(fpath)
            logger.info(f"Created new file ({code}): {fpath}")
            return {fpath: code}


def get_hash(f):
    try:
        h = db_hashes[f]
    except KeyError:
        hasher = hashlib.sha1()
        with open(f, "rb") as fp:
            while True:
                data = fp.read(BUF_SIZE)
                if not data:
                    break
                else:
                    hasher.update(data)
        h = hasher.hexdigest()
        db_hashes[f] = h
    finally:
        return h


def create_h2h_images(block, subject, resolution, overwrite):
    logger.info(f"Resampling histology images to the common histology domain "
                f"(PLP) for {subject}/{block}...")
    mainlogger.info(f"Resampling histology images to the common histology "
                    f"domain (PLP) for {subject}/{block}...")
    hashes = dict()
    trg = os.path.join(TARGET, subject, "blocks", block)
    cfiles = sorted(glob(os.path.join(trg, "*PLPto*.chain")))
    plp = load_svs(subject, block, "PLP", resolution)

    for cfile in cfiles:
        if not os.path.isfile(cfile):
            continue
        stain = get_stain(cfile)[-1]
        if stain not in HMODS:
            continue
        histo = load_svs(subject, block, stain, resolution)
        warpedfile = os.path.join(
            trg, f"sub-{subject}_block-{block}_stain-{stain}_res-{resolution}"
                 f"_toPLP.png")
        niftifile = os.path.join(
            trg, f"sub-{subject}_block-{block}_stain-{stain}_res-{resolution}"
                 f"_toPLP.nii.gz")
        if os.path.isfile(warpedfile) and \
            os.path.isfile(niftifile) and not overwrite:
            wcode = get_hash(warpedfile)
            ncode = get_hash(niftifile)
            hashes.update({
                warpedfile: wcode,
                niftifile: ncode
            })
            logger.info(f"File already exists ({wcode}): {warpedfile}")
            logger.info(f"File already exists ({ncode}): {niftifile}")
            continue
        plp.domain.chain = tirl.load(cfile)
        warped = histo.evaluate(plp.domain)
        del histo
        # Create PNG
        code = safe_save(warped, "snapshot", warpedfile, overwrite)
        hashes.update(code)
        # Create NIFTI
        affine = np.diag([*plp.resolution, 1., 1.])
        rgb_dtype = np.dtype([('R', 'u1'), ('G', 'u1'), ('B', 'u1')])
        nifti = nib.Nifti1Image(warped.data.view(rgb_dtype), affine)
        code = safe_save(nifti, "save", niftifile, overwrite)
        hashes.update(code)

    del plp

    logger.info(f"FINISHED resampling histology images to the common "
                f"histology domain (PLP) for {subject}/{block}.")
    mainlogger.info(f"FINISHED resampling histology images to the common "
                    f"histology domain (PLP) for {subject}/{block}.")
    return hashes


def load_svs(subject, block, stain, resolution=None):
    """
    Returns a TImage of a specific histology stain at the specified resolution.

    """
    svs = os.path.join(TARGET, subject, "blocks", block,
        f"sub-{subject}_block-{block}_stain-{stain}.svs")
    sheet = os.path.join(TARGET, subject, f"{subject}_registration_sheet.csv")
    table = pd.read_csv(sheet)
    record = table[(table["BlockID"] == block[:-1]) &
                   (table["HistologyStain"] == stain)].iloc[0]
    slide = openslide.OpenSlide(svs)
    nativedim = slide.level_dimensions[0]
    nativeres = float(record.HistologyResolutionUM)
    if resolution is not None:
        dimensions = np.round(np.multiply(slide.level_dimensions[0],
                     nativeres / resolution))
    else:
        dimensions = nativedim
    im = TImage(np.asarray(slide.get_thumbnail(dimensions)))
    im.resolution = np.divide(nativedim, dimensions)[::-1] * nativeres / 1000
    return im


def copy_h2v_registration(block, subject, overwrite):
    logger.info(f"Copying histology-to-MRI transformation chains for "
                f"{subject}/{block}...")
    mainlogger.info(f"Copying histology-to-MRI transformation chains for "
                f"{subject}/{block}...")
    hashes = dict()
    src = os.path.join(SOURCE, subject, "blocks", block, "histology", "PLP",
                       "to_TRUFI", "histology2volume.mind")

    try:
        hchain = tirl.load(os.path.join(src, "1_stage1.timg")).domain.all_tx()
    except FileNotFoundError as exc:
        # Compatibility with the CT11+MND24 small dataset
        hchain = tirl.load(os.path.join(src, "1_stage1.dom")).all_tx()

    hchain = get_subchain(hchain, "resolution", None)
    hchain = resolution_free(hchain)
    trufifile = os.path.join(
        SOURCE, subject, "mri", "TRUFI", "trufi_comb.nii.gz")
    trufivol = TImage(trufifile, external="sform")
    isofile = os.path.join(
        SOURCE, subject, "mri", "Segmentation", "trufi_comb_iso.nii.gz")
    isovol = TImage(isofile, external="sform")
    iso_to_trufi = FlirtMatrix(
        os.path.join(SOURCE, subject, "mri", "Segmentation",
        "trufi_comb_iso.mat"), reference=trufifile, input=isofile)()
    hchain += [isovol.domain.chain["sform"].inverse(),
               *iso_to_trufi,
               trufivol.domain.chain["sform"]]
    hcfile = os.path.join(TARGET, subject, "blocks", block,
                f"sub-{subject}_block-{block}_PLPtoTRUFI.chain")
    code = safe_save(hchain, "save", hcfile, overwrite)
    hashes.update(code)
    logger.info(f"FINISHED copying histology-to-MRI transformation chains for "
                f"{subject}/{block}...")
    mainlogger.info(f"FINISHED copying histology-to-MRI transformation chains "
                    f"for {subject}/{block}...")

    del trufivol
    del isovol

    return hashes


def perform_mri_evaluations(block, subject, modalities, resolution, overwrite):
    """
    Evaluates scalar and vector-valued volumes onto the registered
    histology surface.

    """
    logger.info(f"Evaluating MRI modalities ({tuple(modalities.keys())}) "
                f"on the PLP domain at {resolution} um/px "
                f"for {subject}/{block}...")
    mainlogger.info(f"Evaluating MRI modalities ({tuple(modalities.keys())}) "
                f"on the PLP domain at {resolution} um/px "
                f"for {subject}/{block}...")
    # Prepare PLP-based sampling domain at the specified resolution
    plp = load_svs(subject, block, "PLP", resolution)

    # Set transformations (PLP vox --> TRUFI vox)
    cfile = os.path.join(TARGET, subject, "blocks", block,
        f"sub-{subject}_block-{block}_PLPtoTRUFI.chain")
    plp.domain.chain = tirl.load(cfile)
    sform = plp.domain.chain.pop(-1)  # TRUFI sform
    base_chain = plp.domain.chain[:]

    # Set up interpolators
    linpol = ScipyInterpolator(order=1, prefilter=False)
    nnpol = ScipyNearestNeighbours()
    ipols = {
        "lin": linpol,
        "nn": nnpol
    }

    hashes = dict()

    # Evaluate all modalities
    for mmod, volfile in modalities.items():

        # Amend chain so that it points from PLP vox --> MOD vox
        tvx2mvx = Chain()
        for tx in XFM2TRUFI[mmod]:
            tx = tx.copy()
            tx.matfile = os.path.join(TARGET, subject, tx.matfile)
            tx.input = os.path.join(TARGET, subject, tx.input)
            tx.reference = os.path.join(TARGET, subject, tx.reference)
            tvx2mvx += tx()
        plp.domain.chain = base_chain + tvx2mvx.inverse()

        # Load the current modality
        volfile = os.path.join(TARGET, subject, volfile)
        if os.path.isfile(volfile):
            vol = TImage(volfile)
        else:
            continue

        # Transform vectors to NEUROLOGICAL orientation (TIRL convention)
        if vol.tsize > 1:
            vol.data[..., 0] *= -1  # Orientation: FSL RADIO, TIRL NEURO

        # Perform evaluation
        for tag, ipol in ipols.items():
            vol.interpolator = ipol

            # Save as TImage
            trgf = os.path.join(
                TARGET, subject, "blocks", block,
                f"sub-{subject}_block-{block}_mod-{mmod}"
                f"_res-{resolution}_ipol-{tag}_toPLP.timg")

            if os.path.isfile(trgf) and not overwrite:
                code = get_hash(trgf)
                logger.info(f"File already exists ({code}): {trgf}")
                hashes.update({trgf: code})
                im = tirl.load(trgf)
            else:
                im = vol.evaluate(plp.domain, rule="fs")  # finite strain
                code = safe_save(im, "save", trgf, overwrite)
                hashes.update(code)

            # Save as NIfTI
            affine = np.eye(4)
            affine[:2, :2] *= resolution / 1000  # mm
            if im.tsize > 1:
                nifti = nib.Nifti1Image(im.data[:, :, np.newaxis, :], affine)
            else:
                nifti = nib.Nifti1Image(im.data, affine)
            niftifile = trgf.replace(".timg", ".nii.gz")
            if os.path.isfile(niftifile) and not overwrite:
                code = get_hash(niftifile)
                logger.info(f"File already exists ({code}): {niftifile}")
                hashes.update({niftifile: code})
            else:
                code = safe_save(nifti, "save", niftifile, overwrite)
                hashes.update(code)

            # Save as PNG (only if the modality is scalar)
            if vol.tsize <= 1:
                pngfile = trgf.replace(".timg", ".png")
                if os.path.isfile(pngfile) and not overwrite:
                    code = get_hash(pngfile)
                    hashes.update({pngfile: code})
                    logger.info(f"File already exists ({code}): {pngfile}")
                else:
                    code = safe_save(im, "snapshot", pngfile, overwrite)
                    hashes.update(code)

        del vol
    del plp

    logger.info(f"FINISHED evaluating MRI modalities ({modalities.keys()}) "
                f"on the PLP domain at {resolution} um/px "
                f"for {subject}/{block}...")
    mainlogger.info(f"FINISHED evaluating MRI modalities ({modalities.keys()}) "
                f"on the PLP domain at {resolution} um/px "
                f"for {subject}/{block}...")
    return hashes


def copy_intermediaries(block, subject, overwrite):
    logger.info(f"Copying intermediary photographs for {subject}/{block}...")
    mainlogger.info(f"Copying intermediary photographs for "
                    f"{subject}/{block}...")
    hashes = dict()

    # Block face photograph (Intermediary 1)
    try:
        srcf = sorted(glob(os.path.join(
            SOURCE, subject, "blocks", block, "photo", "*_seg.tif")))[0]
    except IndexError:
        logger.error(f"No block face photograph (Intermediary 1) was "
                     f"found for {subject}/{block}.")
    else:
        trgf = os.path.join(
            TARGET, subject, "blocks", block,
            f"sub-{subject}_block-{block}_seg.png")
        im = TImage(np.asarray(imageio.imread(srcf)))
        code = safe_save(im, "snapshot", trgf, overwrite)
        hashes.update(code)
        del im

    # Slice photograph (Intermediary 2)
    sheet = os.path.join(SOURCE, subject, f"{subject}_registration_sheet.csv")
    table = pd.read_csv(sheet)
    record = table[(table["BlockID"] == block[:-1]) &
                   (table["HistologyStain"] == "PLP")].iloc[0]
    slice_id = str(record["SliceID"]) + str(record["SliceSurface"])
    srcf = os.path.join(SOURCE, str(record["SliceFile"]))
    trgf = os.path.join(TARGET, subject, "blocks", block,
                        f"sub-{subject}_slice-{slice_id}_seg.png")
    code = safe_copy(srcf, trgf, overwrite)
    hashes.update(code)

    logger.info(f"FINISHED copying intermediary photographs for "
                f"{subject}/{block}.")
    mainlogger.info(f"FINISHED copying intermediary photographs for "
                    f"{subject}/{block}.")
    return hashes


def copy_report_files(block, subject, overwrite):
    reportlist = "/mnt/alpha/mnd/scripts/4_CleanUp/reportlist.txt"
    logger.info(f"Copying the stage report files from Stages 1-4 for "
                f"{subject}/{block}...")
    mainlogger.info(f"Copying the stage report files from Stages 1-4 for "
                f"{subject}/{block}...")
    hashes = dict()
    with open(reportlist, "r") as fp:
        rfiles = fp.readlines()

    for rfile in rfiles:
        srcf = os.path.join(
            SOURCE, subject, "blocks", block, "report", rfile.rstrip("\n"))
        trgf = os.path.join(
            TARGET, subject, "blocks", block, "report", rfile.rstrip("\n"))
        code = safe_copy(srcf, trgf, overwrite)
        hashes.update(code)

    logger.info(f"FINISHED copying the stage report files from Stages 1-4 for "
                f"{subject}/{block}...")
    mainlogger.info(f"FINISHED copying the stage report files from Stages "
                    f"1-4 for {subject}/{block}...")
    return hashes


def safe_copy(src, trg, overwrite=False):
    exists = os.path.exists(trg)
    if exists:
        if overwrite:
            try:
                code = get_hash(trg)
                os.remove(trg)
                logger.info(f"Removed existing file ({code}): {trg}")
            except Exception as exc:
                logger.error(f"ERROR while removing existing file: {trg}")
                logger.error(exc.args)
                return dict()
        else:
            code = get_hash(trg)
            logger.warning(f"File already exists ({code}): {trg}")
            return {trg: code}

    trgdir = os.path.dirname(trg)
    try:
        os.makedirs(trgdir, exist_ok=True)
    except Exception as exc:
        logger.error(f"ERROR while creating directory: {trgdir}")
        logger.error(exc.args)
        return dict()

    try:
        shutil.copy2(src, trg)
    except Exception as exc:
        logger.error(f"ERROR while creating file: {trg}")
        logger.error(exc.args)
        return dict()
    else:
        code = get_hash(trg)
        logger.info(f"Copied new file ({code}) from {src} to {trg}")
        return {trg: code}


def get_stain(f, ignorecase=False):
    stain = []
    for st in HMODS:
        try:
            if ignorecase:
                pos = re.search(st.lower(), f.lower()).start()
            else:
                pos = re.search(st, f).start()
        except AttributeError:
            continue
        else:
            stain.append((st, pos))
    if not stain:
        return None
    elif len(stain) > 1:
        return [st for st, pos in sorted(stain, key=lambda p: p[1])]
    else:
        return stain[0][0]


def get_subchain(chain, start=None, stop=None, index=False):
    """
    Extracts the first subchain that starts and ends with transformations
    that have specific names.

    If index=True, the slicing indices are returned (start inc, stop exc)

    """
    newchain = Chain([tx for tx in chain])
    start_index = 0
    if start is not None:
        while newchain and newchain[0].name != start:
            newchain.pop(0)
            start_index += 1
        else:
            if not newchain:
                raise NameError(
                    f"Transformation '{start}' is not in the chain.")
    if stop is not None:
        for ix, tx in enumerate(newchain):
            stop_index = start_index + ix
            if tx.name == stop:
                newchain = newchain[:stop_index + 1]
                break
        else:
            raise NameError(f"Transformation '{stop}' is not in the chain.")
    else:
        stop_index = len(chain) - 1

    if index:
        return start_index, stop_index + 1
    else:
        return newchain


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Usage: ./scraper.py <subject> [blocks ...]")
    else:
        main(sys.argv[1], sys.argv[2:])
