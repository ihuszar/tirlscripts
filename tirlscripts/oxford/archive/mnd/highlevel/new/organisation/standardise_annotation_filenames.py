#!/usr/bin/env python

import os
import sys
import glob


def main(subjdir):
    """ Main program code. """

    subject = os.path.basename(subjdir.rstrip(os.sep))
    for root, dirs, files in os.walk(subjdir):
        if not root.endswith("annotation"):
            continue
        *rootpath, block, _, stain, _ = root.split(os.sep)
        for f in files:
            fname, ext = os.path.splitext(f)
            tag = fname.split("_")
            if len(tag) > 1:
                tag = "_" + tag[-1]
            else:
                tag = ""
            src = os.path.join(root, f)
            dst = os.path.join(root, f"sub-{subject}_block-{block}_stain-{stain}{tag}{ext}")
            print(src, "-->", dst)
            os.rename(src, dst)
        for d in dirs:
            for f in os.listdir(os.path.join(root, d)):
                fname, ext = os.path.splitext(f)
                tag = fname.split("_")
                if len(tag) > 1:
                    tag = "_" + tag[-1]
                else:
                    tag = ""
                num = int(d.split("_")[-1])
                src = os.path.join(root, d, f)
                dst = os.path.join(root, d,
                    f"sub-{subject}_block-{block}_stain-{stain}_layer-{num:02}"
                    f"{tag}{ext}")
                print(src, "-->", dst)
                os.rename(src, dst)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        print("Usage: ./standardise_annotation_filenames.py <subject_dir>")
