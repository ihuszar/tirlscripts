#!/usr/bin/env python

import sys
sys.path.append("/home/inhuszar/apps/tirl/src")
sys.path.append("/home/inhuszar/apps/tirl/scripts")

import os
import imageio
from glob import glob
import multiprocessing as mp
from tirl.timage import TImage

ROOTDIR = "/mnt/alpha/mnd"
SUBJECTS = [
	# "CTL10",
	"CTL11",
	# "CTL12",
	# "MND14",
	# "MND15",
	# "MND16",
	# "MND17",
	# "MND18",
	# "MND19",
	# "MND20",
	# "MND21",
	# "MND22",
	# "MND23",
	"MND24"
]
INTERVAL = 0.3
N_CPU = 8


def main():
	""" Main program code. """

	dirs = []
	for subject in SUBJECTS:
		dirs += glob(os.path.join(
			ROOTDIR, subject, "blocks", "*", "histology", "*", "to_PLP"))
	dirs = sorted(dirs)
	# print("\n".join(dirs))

	if N_CPU > 1:
		with mp.Pool(N_CPU, maxtasksperchild=1) as pool:
			pool.map(safe_process_dir, dirs)
	else:
		for d in dirs:
			safe_process_dir(d)


def safe_process_dir(d):
	try:
		process_dir(d)
	except Exception as exc:
		print(f"ERROR while processing directory: {d}")


def process_dir(d):
	
	images = [os.path.join(d, x) for x in
			  ("fixed.png", "moving4_nonlinear.png")]
	# images = [imageio.imread(im) for im in images]
	images = [TImage(im).rescale(0.1, copy=True).data for im in images]
	afile = os.path.join(d, "anim_nonlinear.gif")
	print(f"Saving {afile}...")
	if os.path.exists(afile):
		os.remove(afile)
	imageio.mimsave(afile, images, duration=INTERVAL)


if __name__ == "__main__":
	main()
