#!/usr/bin/env python

import os
import re
import tirl
import imageio
import numpy as np
import pandas as pd
import multiprocessing as mp
from functools import partial

from tirl.chain import Chain
from tirl.timage import TImage


ROOTDIR = "/mnt/alpha/mnd"
SUBJECTS = [
    # "CTL10",
    "CTL11",
    # "CTL12",
    # "MND12",
    # "MND13",
    # "MND14",
    # "MND15",
    # "MND16",
    # "MND17",
    # "MND18",
    # "MND19",
    # "MND20",
    # "MND21",
    # "MND22",
    # "MND23",
    "MND24"
]
ST4DIR = "histology2volume.mind"
DURATION = 0.3
N_CPU = 24


def main():
    """ Main program code. """

    # table = pd.read_csv("/mnt/alpha/mnd/MND16/MND16_registration_sheet.csv")
    # bdir = "/mnt/alpha/mnd/MND16/blocks/18a"
    # process_blockdir(bdir, table)
    # exit()

    process_subject("MND24", ["13La"])
    exit()

    for subject in SUBJECTS:
        try:
            process_subject(subject)
        except Exception as exc:
            print(f"Error while processing subject {subject}")
            print(exc.args)
            continue


def process_subject(subject, blocks=None):
    print(f"Processing subject {subject}...")

    # Retrieve the list of blocks
    subjdir = os.path.join(ROOTDIR, subject)
    blocksdir = os.path.join(subjdir, "blocks")

    blockdirs = []
    for b in (blocks or os.listdir(blocksdir)):
        if not re.match(r"\d+[LR]?a", b):
            continue
        bp = os.path.join(blocksdir, b)
        if not os.path.isdir(bp):
            continue
        blockdirs.append(bp)
    else:
        blockdirs = sorted(blockdirs)

    # Process blocks one by one
    sheet = os.path.join(subjdir, f"{subject}_registration_sheet.csv")
    table = pd.read_csv(sheet)
    workerfunc = partial(safe_process_blockdir, table=table)
    if N_CPU > 1:
        with mp.Pool(processes=N_CPU, maxtasksperchild=1) as pool:
            pool.map(workerfunc, blockdirs)
    else:
        for bdir in blockdirs:
            workerfunc(bdir)


def safe_process_blockdir(blockdir, table):
    try:
        process_blockdir(blockdir, table)
    except Exception as exc:
        print(f"Error while processing block directory: {blockdir}")
        print(exc.args)


def process_blockdir(blockdir, table):

    print(f"Processing {blockdir}...")
    dirs = blockdir.split(os.sep)
    subject, block = dirs[-3], dirs[-1]

    # Create report directory
    reportdir = os.path.join(blockdir, "report")
    if not os.path.isdir(reportdir):
        os.makedirs(reportdir)

    # Stage 1 result: histo + block
    st1(subject, block, table, reportdir)
    # Stage 2 result: block + slice
    st2(subject, block, table, reportdir)
    # Stage 1 + 2: histo + slice (locally)
    st12(subject, block, table, reportdir)
    # Stage 3 result: slice + volume
    st3(subject, block, table, reportdir)
    # Stage 2 + 3 result: block + volume (2D)
    st23(subject, block, table, reportdir)
    # Stage 1 + 2 + 3: histo + TRUFI (rawcombine)
    st123(subject, block, table, reportdir)

    for st4dir in ("histology2volume.mi", "histology2volume.mind"):
        chk = os.path.join(blockdir, "histology", "PLP", "to_TRUFI", st4dir)
        if not os.path.isdir(chk):
            print(f"Directory not found: {chk}")
            continue
        # Stage 4 linear initialisation: linear init (TRUFI) + histo
        st4_lin_init(subject, block, table, st4dir, reportdir)
        # Stage 4 non-linear initialisation: nonlinear init (TRUFI) + histo
        st4_nl_init(subject, block, table, st4dir, reportdir)
        # Stage 1-3 vs Stage 4 non-linear initialisation
        st123st4nlinit(subject, block, table, st4dir, reportdir)
        # Stage 4 - step 1 (rigid initialisation of nonlinear sample) + histo
        st4_linear(subject, block, table, st4dir, reportdir)
        # Stage 4 - step 2 (in-plane non-linear result) + histo
        st4_nonlinear(subject, block, table, st4dir, reportdir)
        # Stage 4 - step 3 (final result) + histo
        st4_nonlinear(subject, block, table, st4dir, reportdir)


def get_subchain(chain, start=None, stop=None, index=False):
    """
    Extracts the first subchain that starts and ends with transformations
    that have specific names.

    If index=True, the slicing indices are returned (start inc, stop exc)

    """
    newchain = Chain([tx for tx in chain])
    start_index = 0
    if start is not None:
        while newchain and newchain[0].name != start:
            newchain.pop(0)
            start_index += 1
        else:
            if not newchain:
                raise NameError(
                    f"Transformation '{start}' is not in the chain.")
    if stop is not None:
        for ix, tx in enumerate(newchain):
            stop_index = start_index + ix
            if tx.name == stop:
                newchain = newchain[:stop_index + 1]
                break
        else:
            raise NameError(f"Transformation '{stop}' is not in the chain.")
    else:
        stop_index = len(chain) - 1

    if index:
        return start_index, stop_index + 1
    else:
        return newchain


def divide_chain(chain, index):

    start1, stop1 = get_subchain(
        chain, "resolution", "resolution_inv", index=True)
    start2, stop2 = get_subchain(
        chain[stop1:], "resolution", "resolution_inv", index=True)
    start2 += stop1
    stop2 += stop1
    start3, stop3 = get_subchain(
        chain[stop2:], "resolution", None, index=True)
    start3 += stop2
    stop3 += stop2

    if index is True:
        return start1, start2, start3
    elif index == 1:
        return chain[start1:stop1]
    elif index == 2:
        return chain[start2:stop2]
    elif index == 3:
        return chain[start3:stop3]
    elif index is False:
        return chain[start1:stop1], chain[start2:stop2], chain[start3:stop3]
    else:
        return ValueError(f"Invalid index: {index}")


def st1(subject, block, table, reportdir):
    rcfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                          "PLP", "to_TRUFI", "historc.timg")
    histo = tirl.load(rcfile)
    chain = divide_chain(histo.domain.chain, 1).inverse()
    bfile = table[(table["BlockID"] == block[:-1]) &
                  (table["HistologyStain"] == "PLP")].BlockFile.iloc[0]
    bfile = os.path.join(ROOTDIR, bfile)
    blockimg = TImage(bfile, chain=chain)
    histo.domain.chain = []
    warped_histo = histo.evaluate(blockimg.domain)
    whfile = os.path.join(reportdir, "histology2block.png")
    warped_histo.snapshot(whfile, overwrite=True)
    im1 = imageio.imread(bfile)
    im2 = imageio.imread(whfile)
    afile = os.path.join(reportdir, "stage1.gif")
    if os.path.isfile(afile):
        os.remove(afile)
    imageio.mimsave(afile, [im1, im2], duration=DURATION)


def st2(subject, block, table, reportdir):
    rcfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                          "PLP", "to_TRUFI", "historc.timg")
    histo = tirl.load(rcfile)
    chain = divide_chain(histo.domain.chain, 2)
    record = table[(table["BlockID"] == block[:-1]) &
                   (table["HistologyStain"] == "PLP")]
    bfile = os.path.join(ROOTDIR, record.BlockFile.iloc[0])
    sfile = os.path.join(ROOTDIR, record.SliceFile.iloc[0])
    blockimg = TImage(bfile, chain=chain)
    sliceimg = TImage(sfile)
    warped_slice = sliceimg.evaluate(blockimg.domain)
    wsfile = os.path.join(reportdir, "slice2block.png")
    warped_slice.snapshot(wsfile, overwrite=True)
    im1 = imageio.imread(wsfile)
    im2 = imageio.imread(bfile)
    afile = os.path.join(reportdir, "stage2.gif")
    if os.path.isfile(afile):
        os.remove(afile)
    imageio.mimsave(afile, [im1, im2], duration=DURATION)


def st12(subject, block, table, reportdir):
    rcfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                          "PLP", "to_TRUFI", "historc.timg")
    histo = tirl.load(rcfile)
    record = table[(table["BlockID"] == block[:-1]) &
                   (table["HistologyStain"] == "PLP")]
    bfile = os.path.join(ROOTDIR, record.BlockFile.iloc[0])
    sfile = os.path.join(ROOTDIR, record.SliceFile.iloc[0])
    chain = histo.domain.chain
    blockimg = TImage(bfile)
    histo.domain.chain = divide_chain(chain, 1)
    sliceimg = TImage(sfile, chain=divide_chain(chain, 2).inverse())
    h2b = histo.evaluate(blockimg.domain)
    s2b = sliceimg.evaluate(blockimg.domain)
    afile = os.path.join(reportdir, "stage1&2.gif")
    if os.path.isfile(afile):
        os.remove(afile)
    imageio.mimsave(afile, [h2b.data, s2b.data], duration=DURATION)


def st3(subject, block, table, reportdir):
    rcfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                          "PLP", "to_TRUFI", "historc.timg")
    histo = tirl.load(rcfile)
    chain = divide_chain(histo.domain.chain, 3)
    record = table[(table["BlockID"] == block[:-1]) &
                   (table["HistologyStain"] == "PLP")]
    sfile = os.path.join(ROOTDIR, record.SliceFile.iloc[0])
    vfile = os.path.join(ROOTDIR, record.MRIVoxelSpace.iloc[0])
    sliceimg = TImage(sfile, chain=chain)
    volimg = TImage(vfile, chain="sform")
    warped_mri = volimg.evaluate(sliceimg.domain)
    wmfile = os.path.join(reportdir, "mri2slice.png")
    warped_mri.snapshot(wmfile, overwrite=True)
    im2 = imageio.imread(wmfile)
    afile = os.path.join(reportdir, "stage3.gif")
    if os.path.isfile(afile):
        os.remove(afile)
    imageio.mimsave(afile, [sliceimg.data, im2], duration=DURATION)


def st23(subject, block ,table, reportdir):
    rcfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                          "PLP", "to_TRUFI", "historc.timg")
    histo = tirl.load(rcfile)
    record = table[(table["BlockID"] == block[:-1]) &
                   (table["HistologyStain"] == "PLP")]
    bfile = os.path.join(ROOTDIR, record.BlockFile.iloc[0])
    vfile = os.path.join(ROOTDIR, record.MRIVoxelSpace.iloc[0])
    st2ix = divide_chain(histo.domain.chain, index=True)[1]
    blockimg = TImage(bfile, chain=histo.domain.chain[st2ix:])
    volimg = TImage(vfile, chain="sform")
    warped_mri = volimg.evaluate(blockimg.domain)
    wmfile = os.path.join(reportdir, "mri2block.png")
    warped_mri.snapshot(wmfile, overwrite=True)
    im2 = imageio.imread(wmfile)
    afile = os.path.join(reportdir, "stage2&3.gif")
    if os.path.isfile(afile):
        os.remove(afile)
    imageio.mimsave(afile, [blockimg.data, im2], duration=DURATION)


def st123(subject, block, table, reportdir):
    rcfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                          "PLP", "to_TRUFI", "historc.timg")
    histo = tirl.load(rcfile)
    histo.rescale(8/50, copy=False)  # blockface image resolution
    record = table[(table["BlockID"] == block[:-1]) &
                   (table["HistologyStain"] == "PLP")]
    vfile = os.path.join(ROOTDIR, record.MRIVoxelSpace.iloc[0])
    volimg = TImage(vfile, chain="sform")
    warped_mri = volimg.evaluate(histo.domain)
    wmfile = os.path.join(reportdir, "mri2histo.png")
    warped_mri.snapshot(wmfile, overwrite=True)
    im2 = imageio.imread(wmfile)
    afile = os.path.join(reportdir, "stage1&2&3.gif")
    if os.path.isfile(afile):
        os.remove(afile)
    imageio.mimsave(afile, [histo.data, im2], duration=DURATION)


def st4_lin_init(subject, block, table, st4dir, reportdir):
    cf = st4dir.split(".")[-1]
    rcfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                          "PLP", "to_TRUFI", "historc.timg")
    regfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                           "PLP", "to_TRUFI", st4dir, "1_stage1.timg")

    # Compatibility with the CTL11+MND24 dataset
    if not os.path.isfile(regfile):
        regfile = regfile.replace(".timg", ".dom")

    record = table[(table["BlockID"] == block[:-1]) &
                   (table["HistologyStain"] == "PLP")]
    vfile = os.path.join(ROOTDIR, record.MRIVoxelSpace.iloc[0])
    histo = tirl.load(rcfile)

    # Compatibility with the CTL11+MND24 dataset
    reg = tirl.load(regfile)
    if hasattr(reg, "__domain__"):
        chain = reg.chain
    else:
        chain = reg.domain.chain

    chain = Chain([tx for tx in chain
                   if tx.name not in ("warp", "rotation_3d", "translation_3d")])
    histo.domain.chain = chain
    histo.rescale(8/50, copy=False)  # blockface photo resolution
    volimg = TImage(vfile, chain="sform")
    warped_mri = volimg.evaluate(histo.domain)
    wmfile = os.path.join(reportdir, f"stage4_linear_init_{cf}.png")
    warped_mri.snapshot(wmfile, overwrite=True)
    im2 = imageio.imread(wmfile)
    afile = os.path.join(reportdir, f"stage4_linear_init_{cf}.gif")
    if os.path.isfile(afile):
        os.remove(afile)
    imageio.mimsave(afile, [histo.data, im2], duration=DURATION)


def st4_nl_init(subject, block, table, st4dir, reportdir):
    cf = st4dir.split(".")[-1]
    rcfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                          "PLP", "to_TRUFI", "historc.timg")
    regfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                          "PLP", "to_TRUFI", st4dir, "1_stage1.timg")

    # Compatibility with the CTL11+MND24 dataset
    if not os.path.isfile(regfile):
        regfile = regfile.replace(".timg", ".dom")

    record = table[(table["BlockID"] == block[:-1]) &
                   (table["HistologyStain"] == "PLP")]
    vfile = os.path.join(ROOTDIR, record.MRIVoxelSpace.iloc[0])
    histo = tirl.load(rcfile)

    # Compatibility with the CTL11+MND24 dataset
    reg = tirl.load(regfile)
    if hasattr(reg, "__domain__"):
        chain = reg.chain
    else:
        chain = reg.domain.chain

    chain = Chain([tx for tx in chain
                   if tx.name not in ("rotation_3d", "translation_3d")])
    histo.domain.chain = chain
    histo.rescale(8/50, copy=False)  # blockface photo resolution
    volimg = TImage(vfile, chain="sform")
    warped_mri = volimg.evaluate(histo.domain)
    wmfile = os.path.join(reportdir, f"stage4_nonlinear_init_{cf}.png")
    warped_mri.snapshot(wmfile, overwrite=True)
    im2 = imageio.imread(wmfile)
    afile = os.path.join(reportdir, f"stage4_nonlinear_init_{cf}.gif")
    if os.path.isfile(afile):
        os.remove(afile)
    imageio.mimsave(afile, [histo.data, im2], duration=DURATION)


def st123st4nlinit(subject, block, table, st4dir, reportdir):
    cf = st4dir.split(".")[-1]
    rcfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                          "PLP", "to_TRUFI", "historc.timg")
    regfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                           "PLP", "to_TRUFI", st4dir, "1_stage1.timg")

    # Compatibility with the CTL11+MND24 dataset
    if not os.path.isfile(regfile):
        regfile = regfile.replace(".timg", ".dom")

    record = table[(table["BlockID"] == block[:-1]) &
                   (table["HistologyStain"] == "PLP")]
    vfile = os.path.join(ROOTDIR, record.MRIVoxelSpace.iloc[0])
    histo = tirl.load(rcfile)

    # Compatibility with the CTL11+MND24 dataset
    reg = tirl.load(regfile)
    if hasattr(reg, "__domain__"):
        regchain = reg.chain
    else:
        regchain = reg.domain.chain

    chain = Chain([tx for tx in regchain
                   if tx.name not in ("rotation_3d", "translation_3d")])
    histo.domain.chain = chain
    histo.rescale(8 / 50, copy=False)  # blockface image resolution
    volimg = TImage(vfile, chain="sform")
    rc_mri = volimg.evaluate(histo.domain)
    histo.domain.chain = regchain
    st4_mri = volimg.evaluate(histo.domain)
    im1 = np.array(rc_mri.data / rc_mri.data.max() * 255, dtype=np.uint8)
    im2 = np.array(st4_mri.data / st4_mri.data.max() * 255, dtype=np.uint8)
    afile = os.path.join(reportdir, f"stage123_stage4_nl_init_{cf}.gif")
    if os.path.isfile(afile):
        os.remove(afile)
    imageio.mimsave(afile, [im1, im2], duration=DURATION)


def st4_linear(subject, block, table, st4dir, reportdir):
    cf = st4dir.split(".")[-1]
    rcfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                          "PLP", "to_TRUFI", "historc.timg")
    regfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                          "PLP", "to_TRUFI", st4dir, "1_stage1.timg")

    # Compatibility with the CTL11+MND24 dataset
    if not os.path.isfile(regfile):
        regfile = regfile.replace(".timg", ".dom")

    record = table[(table["BlockID"] == block[:-1]) &
                   (table["HistologyStain"] == "PLP")]
    vfile = os.path.join(ROOTDIR, record.MRIVoxelSpace.iloc[0])
    histo = tirl.load(rcfile)

    # Compatibility with the CTL11+MND24 dataset
    reg = tirl.load(regfile)
    if hasattr(reg, "__domain__"):
        chain = reg.chain
    else:
        chain = reg.domain.chain

    histo.domain.chain = chain
    histo.rescale(8/50, copy=False)  # blockface photo resolution
    volimg = TImage(vfile, chain="sform")
    warped_mri = volimg.evaluate(histo.domain)
    wmfile = os.path.join(reportdir, f"stage4_linear_{cf}.png")
    warped_mri.snapshot(wmfile, overwrite=True)
    im2 = imageio.imread(wmfile)
    afile = os.path.join(reportdir, f"stage4_linear_{cf}.gif")
    if os.path.isfile(afile):
        os.remove(afile)
    imageio.mimsave(afile, [histo.data, im2], duration=DURATION)


def st4_nonlinear(subject, block, table, st4dir, reportdir):
    cf = st4dir.split(".")[-1]
    rcfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                          "PLP", "to_TRUFI", "historc.timg")
    regfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                          "PLP", "to_TRUFI", st4dir, "2_stage3.timg")

    # Compatibility with the CTL11+MND24 dataset
    if not os.path.isfile(regfile):
        regfile = regfile.replace(".timg", ".dom")

    record = table[(table["BlockID"] == block[:-1]) &
                   (table["HistologyStain"] == "PLP")]
    vfile = os.path.join(ROOTDIR, record.MRIVoxelSpace.iloc[0])
    histo = tirl.load(rcfile)

    # Compatibility with the CTL11+MND24 dataset
    reg = tirl.load(regfile)
    if hasattr(reg, "__domain__"):
        chain = reg.chain
    else:
        chain = reg.domain.chain

    histo.domain.chain = chain
    histo.rescale(8/50, copy=False)  # blockface photo resolution
    volimg = TImage(vfile, chain="sform")
    warped_mri = volimg.evaluate(histo.domain)
    wmfile = os.path.join(reportdir, f"stage4_nonlinear_{cf}.png")
    warped_mri.snapshot(wmfile, overwrite=True)
    im2 = imageio.imread(wmfile)
    afile = os.path.join(reportdir, f"stage4_nonlinear_{cf}.gif")
    if os.path.isfile(afile):
        os.remove(afile)
    imageio.mimsave(afile, [histo.data, im2], duration=DURATION)


def st4_final(subject, block, table, st4dir, reportdir):
    cf = st4dir.split(".")[-1]
    rcfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                          "PLP", "to_TRUFI", "historc.timg")
    regfile = os.path.join(ROOTDIR, subject, "blocks", block, "histology",
                          "PLP", "to_TRUFI", st4dir, "3_stage4.timg")

    # Compatibility with the CTL11+MND24 dataset
    if not os.path.isfile(regfile):
        regfile = regfile.replace(".timg", ".dom")

    record = table[(table["BlockID"] == block[:-1]) &
                   (table["HistologyStain"] == "PLP")]
    vfile = os.path.join(ROOTDIR, record.MRIVoxelSpace.iloc[0])
    histo = tirl.load(rcfile)

    # Compatibility with the CTL11+MND24 dataset
    reg = tirl.load(regfile)
    if hasattr(reg, "__domain__"):
        chain = reg.chain
    else:
        chain = reg.domain.chain

    histo.domain.chain = chain
    histo.rescale(8/50, copy=False)  # blockface photo resolution
    volimg = TImage(vfile, chain="sform")
    warped_mri = volimg.evaluate(histo.domain)
    wmfile = os.path.join(reportdir, f"stage4_final_{cf}.png")
    warped_mri.snapshot(wmfile, overwrite=True)
    im2 = imageio.imread(wmfile)
    afile = os.path.join(reportdir, f"stage4_final_{cf}.gif")
    if os.path.isfile(afile):
        os.remove(afile)
    imageio.mimsave(afile, [histo.data, im2], duration=DURATION)


if __name__ == "__main__":
    main()
