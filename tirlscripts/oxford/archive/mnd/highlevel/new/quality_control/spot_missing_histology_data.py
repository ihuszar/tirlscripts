#/usr/bin/env python

import os
import re
from glob import glob


ROOTDIR = "/mnt/alpha/mnd"
STAINS = ["CD68", "Iba1", "PLP", "pTDP43", "SMI312"]


def main():
	""" Main program code. """

	hdirs = glob(os.path.join(ROOTDIR, "*/blocks/*a/histology"))
	hdirs = sorted(hdirs)

	for hdir in hdirs:
		stains = set(STAINS)
		for d in os.listdir(hdir):
			stains = stains.difference(set([d]))
		else:
			if stains:
				print(f"No {stains} directory in {hdir}")



if __name__ == "__main__":
	main()
