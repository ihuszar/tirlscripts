#!/usr/bin/env python

import os
import re
import json
from glob import glob


ROOTDIR = "/mnt/alpha/mnd/"


def main():
	""" Main program code. """

	# Find histology directories across the dataset that do not contain
	# histology-to-histology registration data
	histdirs = get_histdirs(ROOTDIR)
	print("\n".join(histdirs))

	# Create a library of configuration files
	lib = dict()
	cfiles = glob(os.path.join(ROOTDIR, "config", "*.conf"))
	for cf in cfiles:
		with open(cf, "r") as fp:
			cnf = dict(json.load(fp))
			lib.update({os.path.dirname(cnf["fixed"]["file"]): cf})

	# Filter the configuration files that need to be executed
	torun = [lib[hdir] for hdir in histdirs]
	print("\n".join(torun))


def get_histdirs(rootdir):

	histdirs = []
	for root, dirs, files in os.walk(rootdir):
		if any(filter(lambda x: x.lower().endswith(".svs"), files)):
			if not any(filter(lambda x: re.match(r"to_", x), dirs)):
				histdirs.append(root)
	else:
		return histdirs


if __name__ == "__main__":
	main()
