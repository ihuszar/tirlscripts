#!/usr/bin/env python

import os
import json
import shutil
import subprocess
from glob import glob


ROOTDIR = "/mnt/alpha/mnd"
CFILES = [
	# "/mnt/alpha/mnd/CTL12/blocks/23a/histology/CD68/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/CTL12/blocks/23a/histology/Iba1/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/CTL12/blocks/23a/histology/pTDP43/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/CTL12/blocks/23a/histology/SMI312/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/CTL12/blocks/26Ra/histology/CD68/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/MND16/blocks/22La/histology/SMI312/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/MND16/blocks/22Ra/histology/SMI312/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/MND18/blocks/14La/histology/CD68/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/MND18/blocks/14La/histology/Iba1/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/MND18/blocks/14La/histology/pTDP43/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/MND18/blocks/14Ra/histology/CD68/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/MND18/blocks/14Ra/histology/Iba1/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/MND18/blocks/14Ra/histology/pTDP43/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/MND19/blocks/14La/histology/pTDP43/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/MND19/blocks/25La/histology/SMI312/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/MND20/blocks/18a/histology/pTDP43/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/MND20/blocks/24Ra/histology/SMI312/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/MND21/blocks/21a/histology/SMI312/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/MND21/blocks/27La/histology/SMI312/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/MND21/blocks/27Ra/histology/SMI312/to_NP*/configuration.json",
	# "/mnt/alpha/mnd/MND23/blocks/24a/histology/SMI312/to_NP*/configuration.json",
	"/mnt/alpha/mnd/MND23/blocks/28La/histology/pTDP43/to_NP*/configuration.json",
	"/mnt/alpha/mnd/MND23/blocks/28La/histology/SMI312/to_NP*/configuration.json"
]
CFILES = [glob(f)[0] for f in CFILES]



def main():
	""" Main program code. """

	for cfile in CFILES:

		bdir = os.path.dirname(cfile)
		
		# Move registration files to archive directory
		files = [d for d in os.listdir(bdir) if d != "old"]
		archdir = os.path.join(bdir, "old")
		try:
			os.makedirs(archdir)
		except:
			pass
		for f in files:
			os.remove(os.path.join(bdir, f))
			# shutil.move(os.path.join(bdir, f), os.path.join(archdir, f))
		
		# Create new configuration file with amended parameters
		with open(os.path.join(archdir, "configuration.json"), "r") as fp:
			cnf = dict(json.load(fp))
		cnf["regparams"]["nonlinear"]["scaling"] = [32, 16, 8, 4]
		cnf["regparams"]["rotsearch"]["coarse"] = 1
		cnf["regparams"]["init"]["rotation"]["lb"] = -180
		cnf["regparams"]["init"]["rotation"]["ub"] = 180
		cnf["regparams"]["init"]["scale"]["lb"] = 0.9
		cnf["regparams"]["init"]["scale"]["ub"] = 1.1
		newcfile = os.path.join(bdir, "h2h_config.conf")
		with open(newcfile, "w") as fp:
			json.dump(cnf, fp, sort_keys=True, indent=4)

		# Start SLURM process
		outfile = newcfile.replace(".conf", ".out")
		script = "~/apps/tirl/scripts/oxdbb/pathology/highlevel/histology_to_histology.py"
		cmd = f'sbatch --output={outfile} --wrap="python {script} --verbose --config={newcfile}"'
		print(cmd, "\n")
		subprocess.call(cmd, shell=True)


if __name__ == "__main__":
	main()
