#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2021 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import json
import numpy as np
import pandas as pd
import multiprocessing as mp
from collections import namedtuple

import tirl
import tirl.fsl
from scripts.oxdbb.pathology.highlevel import histology_to_histology as h2hreg
from scripts.oxdbb.pathology.highlevel import histology_to_block as h2breg
from scripts.oxdbb.pathology.highlevel import block_to_slice as b2sreg
from scripts.oxdbb.pathology.highlevel import slice_to_volume as s2vreg


# DEFINITIONS

RegPlan = namedtuple("RegPlan", ["h2h", "h2b", "b2s", "s2v", "v2v"])
N_CPU = 12
SUBJECTS = [
    "CTL10",
    "CTL12",
    "MND12",
    "MND13",
    "MND14",
    "MND15",
    "MND16",
    "MND17",
    "MND18",
    "MND19",
    "MND20",
    "MND21",
    "MND22",
    "MND23"
]
REFERENCE_STAIN = "PLP"
SUBJECTS_DIR = "/mnt/alpha/mnd"
H2H_TEMPLATE = "/home/inhuszar/apps/tirl/scripts/oxdbb/pathology/highlevel/" \
               "histology_to_histology.json"
H2B_TEMPLATE = "/home/inhuszar/apps/tirl/scripts/oxdbb/pathology/highlevel/" \
               "histology_to_block.json"
S2V_TEMPLATE = "/home/inhuszar/apps/tirl/scripts/oxdbb/pathology/highlevel/" \
               "slice_to_volume.json"


# IMPLEMENTATION

def main(rsfile, mrifile=None):
    """
    Main program code.

    """
    # Read registration sheet and select entries for groupwise registration
    regsheet = pd.read_csv(rsfile)

    for subj in regsheet["FMRIBSubjectID"].unique().tolist():
        subsheet = regsheet[regsheet.FMRIBSubjectID == subj]

        # Plan and run the groupwise registration
        plan = RegPlan(
            get_h2h(subsheet),
            get_h2b(subsheet),
            get_b2s(subsheet),
            None, #get_s2v(subsheet),
            None
        )
        register(plan)

        # Get intermodal mappings for the MRI data
        if (mrifile is not None) and os.path.isfile(mrifile):
            mrisheet = pd.read_csv(mrifile)
            immaps = get_v2v(mrisheet)
        else:
            immaps = {}

        # Combine registration results (all histo vs all MRI)
        combine(plan, immaps)


def get_h2h(sheet, reference_stain=REFERENCE_STAIN):
    """
    Gathers all histology-to-histology registrations based on the current input.

    """
    blockgroup = sheet.groupby(by="BlockFile")
    stains_per_block = blockgroup.apply(lambda x: x["HistologyStain"].unique())
    columns = ["Fixed", "Moving", "FixedResolution", "MovingResolution", "Stain"]
    jobs = []
    for block, stains in stains_per_block.items():
        slides = sheet[sheet.BlockFile == block]
        stains = stains[stains != reference_stain]
        try:
            blockrefstain = slides[
                (slides.HistologyStain == reference_stain)].HistologyFile.iloc[0]
        except:
            raise
        for stain in stains:
            hfiles = slides[
                (slides.HistologyStain == stain)].HistologyFile.tolist()
            for hfile in hfiles:
                jobs.append([
                    os.path.join(SUBJECTS_DIR, hfile),
                    os.path.join(SUBJECTS_DIR, blockrefstain),
                    float(sheet[sheet.HistologyFile == hfile].HistologyResolutionUM) / 1000 * 16,
                    float(sheet[sheet.HistologyFile == blockrefstain].HistologyResolutionUM) / 1000 * 16,
                    stain
                ])
    else:
        jobs = pd.DataFrame(columns=columns, data=jobs)
        print(jobs)
        return jobs


def get_h2b(sheet, reference_stain=REFERENCE_STAIN):
    """
    Gathers all histology-to-block registrations based on the current input.

    """
    reduced = sheet[
        ["HistologyFile", "HistologyResolutionUM", "HistologyStain",
         "BlockFile", "BlockResolutionUM"]
    ]
    return reduced[
        ["HistologyFile", "HistologyResolutionUM",
         "BlockFile", "BlockResolutionUM"]
    ].loc[reduced.HistologyStain == reference_stain]


def get_b2s(sheet):
    """
    Gathers all block-to-slice registrations based on the current input.

    """
    return {r["BlockFile"]: r["SliceFile"] for ix, r in sheet.iterrows()}


def get_s2v(sheet):
    """
    Gathers all slice-to-volume registrations based on the current input.

    """
    volgroup = sheet.groupby(by="MRIVoxelSpace").apply(
        lambda x: x["SliceFile"].unique())
    columns = ["SliceFile", "SliceResolutionUM", "SlicePosition",
               "MRIVoxelSpace", "MRIPrincipalAxis"]
    jobs = []
    for vol, slices in volgroup.items():
        for sl in slices:
            jobs.append([
                sl,
                sheet[sheet.SliceFile == sl].iloc[0].SliceResolutionUM,
                sheet[sheet.SliceFile == sl].iloc[0].SlicePosition,
                vol,
                sheet[sheet.MRIVoxelSpace == vol].iloc[0].MRIPrincipalAxis
            ])
    return pd.DataFrame(columns=columns, data=jobs)


def get_v2v(sheet):
    """
    Gathers volume-to-volume registrations based on the current input.

    """
    from tirl.chain import Chain

    mappings = dict()
    for ix, m in sheet.iterrows():
        transformations = [tx for tx in m["Transformations":].tolist()
            if tx is not np.nan]
        source = os.path.join(SUBJECTS_DIR, m["MRIFile"])
        reference = os.path.join(SUBJECTS_DIR, m["MRIVoxelSpace"])
        chain = []
        for tx in transformations:
            tx = os.path.join(SUBJECTS_DIR, tx)
            if tx.lower().endswith(".mat"):
                chain.append(tirl.fsl.load_mat(tx, source, reference))
            elif tx.lower().endswith(".nii") or tx.lower().endswith(".nii.gz"):
                chain.append(tirl.fsl.load_warp(tx, reference))
            else:
                raise TypeError(f"Invalid transformation file: {tx}")

        # For concatenated transformations the intenal reference images are
        # invalid, however the last inverse sampling matrix and the following
        # sampling matrix always cancel each other, so these can be safely
        # eliminated.
        n_tx = len(chain)
        if n_tx == 1:
            mappings.update({source: chain[0]})
        else:
            reduced = []
            for ix, subchain in enumerate(chain):
                if ix != n_tx - 1:
                    subchain = subchain[:-1]
                if ix != 0:
                    subchain = subchain[1:]
                reduced.extend(subchain)
            mappings.update({source: Chain(reduced)})
    return mappings


def create_folders(plan):
    """
    Creates standardised folders for the registration results.

    """

    return


def find_insertion_sites(plan):
    """
    Identifies block insertion sites on all slices that are part of the
    registration plan.

    """
    return


def h2hreg_prepare(plan):
    # Histology to histology registration
    for ix, record in plan.h2h.iterrows():
        with open(H2H_TEMPLATE, "r") as fp:
            cnf = dict(json.load(fp))
        cnf["fixed"]["file"] = record.Fixed
        cnf["moving"]["file"] = record.Moving
        fn, ext = os.path.splitext(os.path.basename(cnf["fixed"]["file"]))
        mn, ext = os.path.splitext(os.path.basename(cnf["moving"]["file"]))
        outdir = os.path.join(os.path.dirname(record.Fixed), f"to_{mn}")
        cnf["general"]["outputdir"] = outdir
        cnf["general"]["logfile"] = os.path.join(outdir, "h2h.log")
        cnf["general"]["paramlogfile"] = os.path.join(outdir, "parameters.log")
        cnf["fixed"]["resolution"] = record.FixedResolution
        cnf["moving"]["resolution"] = record.MovingResolution
        yield cnf


def h2breg_prepare(plan):
    for ix, record in plan.h2b.iterrows():
        with open(H2B_TEMPLATE, "r") as fp:
            cnf = dict(json.load(fp))
        cnf["histology"]["file"] = os.path.join(
            SUBJECTS_DIR, str(record.HistologyFile))
        cnf["block"]["file"] = os.path.join(
            SUBJECTS_DIR, str(record.BlockFile))
        fn, ext = os.path.splitext(os.path.basename(cnf["histology"]["file"]))
        mn, ext = os.path.splitext(os.path.basename(cnf["block"]["file"]))
        outdir = os.path.join(os.path.dirname(cnf["histology"]["file"]),
                              f"{mn}_to_{fn}")
        cnf["general"]["outputdir"] = outdir
        cnf["general"]["logfile"] = os.path.join(outdir, "histology2block.log")
        cnf["general"]["paramlogfile"] = os.path.join(outdir, "parameters.log")
        cnf["histology"]["resolution"] = record.HistologyResolutionUM / 1000 * 16
        cnf["block"]["resolution"] = record.BlockResolutionUM / 1000
        yield cnf


def s2vreg_prepare(plan):
    for ix, record in plan.s2v.iterrows():
        with open(S2V_TEMPLATE, "r") as fp:
            cnf = dict(json.load(fp))
        cnf["slice"]["file"] = os.path.join(
            SUBJECTS_DIR, str(record.SliceFile))
        cnf["volume"]["file"] = os.path.join(
            SUBJECTS_DIR, str(record.MRIVoxelSpace))
        fn, ext = os.path.splitext(os.path.basename(cnf["slice"]["file"]))
        mn, ext = os.path.splitext(os.path.basename(cnf["volume"]["file"]))
        mn = mn.replace(".nii", "")
        outdir = os.path.join(
            os.path.dirname(cnf["slice"]["file"]), f"{fn}_to_{mn}")
        slicepos = [float(x) for x in str(record.SlicePosition).split(";")]
        paxis = [float(x) for x in str(record.MRIPrincipalAxis).split(";")]

        cnf["general"]["outputdir"] = outdir
        cnf["general"]["logfile"] = os.path.join(outdir, "slice2volume.log")
        cnf["general"]["paramlogfile"] = os.path.join(outdir, "parameters.log")
        cnf["slice"]["resolution"] = record.SliceResolutionUM / 1000
        cnf["regparams"]["stage_1"]["slab"]["centre"] = slicepos
        cnf["regparams"]["stage_1"]["slab"]["normal"] = paxis
        cnf["regparams"]["stage_1"]["slab"]["offset"] = 0

        yield cnf


def register(plan):
    """
    Run the registrations.

    """
    # ctx = mp.get_context("spawn")
    # with ctx.Pool(processes=N_CPU, maxtasksperchild=1) as pool:
    #     pool.map(h2hreg.run, h2hreg_prepare(plan))

    # with ctx.Pool(processes=10, maxtasksperchild=1) as pool:
    #     pool.map(h2breg.run, h2breg_prepare(plan))

    import uuid
    for cnf in h2hreg_prepare(plan):
        # Save the configuration file for the specific job
        configdir = "/mnt/alpha/mnd/config"
        fname = os.path.join(configdir, str(uuid.uuid4()) + ".conf")
        print(fname)
        with open(fname, "w") as fp:
            json.dump(cnf, fp, indent=4, sort_keys=True)

    # # Histology-to-histology registrations
    # for ix, record in plan.h2h.iterrows():
    #     with open(H2H_TEMPLATE, "r") as fp:
    #         cnf = dict(json.load(fp))
    #     cnf["fixed"]["file"] = record.Fixed
    #     cnf["moving"]["file"] = record.Moving
    #     fn, ext = os.path.splitext(os.path.basename(cnf["fixed"]["file"]))
    #     mn, ext = os.path.splitext(os.path.basename(cnf["moving"]["file"]))
    #     outdir = os.path.join(os.path.dirname(record.Fixed), f"{mn}_to_{fn}")
    #     cnf["general"]["outputdir"] = outdir
    #     cnf["general"]["logfile"] = os.path.join(outdir, "h2h.log")
    #     cnf["general"]["paramlogfile"] = os.path.join(outdir, "parameters.log")
    #     cnf["fixed"]["resolution"] = record.FixedResolution * 16
    #     cnf["moving"]["resolution"] = record.MovingResolution * 16
    #     h2hreg.run(cnf)
    #     from pprint import pprint
    #     # pprint(plan.h2h)
    #     # pprint(plan.h2b)
    #     # pprint(plan.b2s)
    #     # pprint(plan.s2v)
    #     # Create folder structure (optimal)
    #     # create_folders(plan)
    #     # Find insertion sites for all slices
    #     # find_insertion_sites(plan)
    #     # Verify hashes!
    #     pass


def h2h(source, target, config):

    pass


def combine(plan, immaps=None):
    """
    Combines the optimised transformation chains from the three stages to
    achieve histology-to-MRI registration.

    """
    print(immaps)
    return


if __name__ == "__main__":
    for fsid in SUBJECTS:
        mrifile = f"/mnt/alpha/mnd/{fsid}/{fsid}_MRI_sheet.csv"
        rsfile = f"/mnt/alpha/mnd/{fsid}/{fsid}_registration_sheet.csv"
        try:
            main(rsfile, mrifile)
        except Exception as exc:
            print(f"ERROR while registering the histology data for {fsid}: ",
                  exc.args)
    else:
        print("Fully done.")
