#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2021 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Date: 7 June 2020


# SHBASECOPYRIGHT


"""
Oxford Post-Mortem MND Biomarkers Histology-MRI Registration Pipeline, Stage 1:
Registers a histology slide to the corresponding tissue block photograph.

After the input images are centralised, the registration employs sequential
rigid, affine and non-linear alignment. Depending on the chosen direction of
the registration (default: histology->block), the following TIRL chain is
optimised:

+-------------+
| Fixed image |   (histology slide by default)
+-------------+
    offset:
        [centralisation]-->[resampling]
    chain:
        -->(2D anisotropic scale)-->(2D rigid rotation)-->(2D translation)
        -->(2D full affine)
        -->(2D full-resolution displacement field)

+--------------+
| Moving image |  (block photograph by default)
+--------------+
    offset:
        [centralisation]

The above chain maps the full-resolution 2D histology coordinates to the 2D
space of the tissue block photograph.

"""


# DEPENDENCIES

import os
import sys
import json
import logging
import argparse
import warnings
import numpy as np
from attrdict import AttrMap
from math import radians, degrees
from skimage.measure import label, regionprops


# TIRL IMPORTS

import tirl.settings as ts
from tirl.beta import beta_function

from tirl.chain import Chain
from tirl.tfield import TField
from tirl.timage import TImage
from tirl.cost.mind import CostMIND
from tirl.optimisation.optnl import OptNL
from tirl.optimisation.optgroup import OptimisationGroup
from tirl.optimisation.gnoptdiff import GNOptimiserDiffusion
from tirl.regularisation.diffusion import DiffusionRegulariser

from tirl.transformations.scale import TxScale, TxIsoScale
from tirl.transformations.rotation import TxRotation2D
from tirl.transformations.translation import TxTranslation
from tirl.transformations.affine import TxAffine
from tirl.transformations.displacement import TxDisplacementField

from scripts.oxdbb.pathology.midlevel import general, inout, image


# DEFINITIONS

from tirl.constants import *

# Rotation search: number of best initialisations to test
N_BEST = 3
# File extension of the snapshot images
SNAPSHOT_EXT = "png"
# NumPy print formatting
np.set_printoptions(precision=4)


# IMPLEMENTATION

def run(cnf=None, **options):
    """
    Runs TIRL histology-to-block registration (stage 1).

    :param cnf:
        Configuration file. If no file is specified (default), the default
        parameters will be used that are embedded in the source code file
        (see Definitions section above). Instead of a file, a dictionary with
        suitable content may also be specified.
    :type cnf: Union[str, dict, None]
    :param options:
        Overriding configuration parameters.
    :type options: Any

    """
    # Load script configuration
    if cnf is not None:
        if isinstance(cnf, dict):
            cnf = dict(cnf)
        elif isinstance(cnf, str):
            with open(cnf, "r") as configfile:
                cnf = dict(json.load(configfile))
        else:
            raise TypeError(
                f"Unrecognised configuration format: {cnf.__.class__.__name__}")
    cnf.update(options)
    p, logger = general.initialise_script(**cnf)
    p.logger = logger.name  # avoid globals

    # Load and configure input images
    if p.histology.export is True:
        ext = ts.EXTENSIONS["TImage"]
        p.histology.export = \
            os.path.join(p.general.outputdir, f"histology.{ext}")
    histo = inout.load_histology(scope=globals(), **p.histology)
    histo.rule = None
    if p.block.export is True:
        ext = ts.EXTENSIONS["TImage"]
        p.block.export = os.path.join(p.general.outputdir, f"block.{ext}")
    block = inout.load_image(scope=globals(), **p.block)
    block.rule = None

    # Having loaded both images, perform actions on the histology image prior
    # to registration, unless it was loaded from a TImage.
    # Actions are user-defined functions within the TIRL namespace. Actions
    # can be chain-loaded to perform preparatory analysis steps on the images
    # before registration begins.
    isalternative = p.histology.file.lower().endswith(
        (ts.EXTENSIONS["TImage"], ts.EXTENSIONS["TIRLObject"]))
    if not isalternative:
        histo = image.perform_image_operations(
            histo, *p.preprocessing.histology, scope=globals(), other=block,
            cnf=p)
        # Initialise registration frame
        histo.centralise(weighted=True)

    # Perform actions on the block image prior to registration, unless it was
    # loaded from a TImage file.
    isalternative = p.block.file.lower().endswith(
        (ts.EXTENSIONS["TImage"], ts.EXTENSIONS["TIRLObject"]))
    if not isalternative:
        block = image.perform_image_operations(
            block, *p.preprocessing.block, scope=globals(), other=histo, cnf=p)
        block.centralise(weighted=True)

    # Run the registration routine
    try:
        if str(p.general.direction).lower() == "h2b":
            register(histo, block, p)
        elif str(p.general.direction).lower() == "b2h":
            register(block, histo, p)
        else:
            raise ValueError(f"Invalid registration direction: "
                             f"{p.general.direction}")
    except Exception as exc:
        logger.error(exc.args)
        logger.fatal(f"The registration terminated with an exception.")
        raise exc
    else:
        logger.fatal("The registration was completed successfully.")


def labkmeans(histo, **kwargs):
    """
    Segments the foreground (tissue) in a histological slide and sets it as the
    TImage mask.

    This method was tested on slides that were stained with DAB + haematoxylin,
    and scanned with a bright white background. The method was also effectively
    removing the shadow effect close to the slide edge.

    """
    from skimage.color import rgb2lab
    from sklearn.cluster import KMeans
    from skimage.exposure import rescale_intensity
    orig_order = histo.order
    histo.order = VOXEL_MAJOR
    imdata = np.asarray(
        rescale_intensity(histo.data[..., :3], out_range=np.uint8),
        dtype=np.uint8)
    lab_a = rgb2lab(imdata)[..., 1]
    X = lab_a.reshape(-1, 1)
    km = KMeans(n_clusters=2, random_state=0).fit(X)
    kc = km.cluster_centers_
    mask = km.labels_.reshape(histo.vshape)
    if kc[0] > kc[1]:       # make sure that the lower intensity is labeled 0
        mask = 1 - mask
    histo.order = orig_order
    return mask


def initialise_transformations(fixed, p):
    """
    Create transformation chain that will be optimised.

    """
    q = p.regparams

    # Scale
    lb = np.asarray(q.init.scale.lb)
    ub = np.asarray(q.init.scale.ub)
    if p.general.isotropic:
        bounds = (float(lb), float(ub))
        tx_scale = TxIsoScale(
            float(q.init.scale.x0), dim=2, bounds=bounds, name="scale")
    else:
        tx_scale = TxScale(*q.init.scale.x0, bounds=(lb, ub), name="scale")

    # Rotation
    if str(q.init.rotation.mode).lower() == "deg":
        lb = radians(float(q.init.rotation.lb))
        ub = radians(float(q.init.rotation.ub))
    else:
        lb = float(q.init.rotation.lb)
        ub = float(q.init.rotation.ub)
    tx_rotation = TxRotation2D(
        float(q.init.rotation.x0), mode=q.init.rotation.mode,
        bounds=(lb, ub), name="rotation")

    # Translation
    lb = np.asarray(q.init.translation.lb)
    ub = np.asarray(q.init.translation.ub)
    tx_trans = TxTranslation(
        *q.init.translation.x0, bounds=(lb, ub), name="translation")

    # Affine
    x0 = np.asarray(q.init.affine.x0).reshape((2, 3))
    lb = np.asarray(q.init.affine.lb)
    ub = np.asarray(q.init.affine.ub)
    tx_affine = TxAffine(x0, bounds=(lb, ub), name="affine")

    # Append linear transformations to the domain of the fixed image
    linear_chain = Chain(tx_rotation, tx_scale, tx_trans, tx_affine)
    domain = fixed.domain[:]
    domain.chain.extend(linear_chain)

    # Nonlinear
    x0 = float(q.init.nonlinear.x0) * np.ones((2, *fixed.vshape))
    if q.init.nonlinear.lb is None:
        lb = None
    else:
        lb = float(q.init.nonlinear.lb) * np.ones_like(x0)
    if q.init.nonlinear.ub is None:
        ub = None
    else:
        ub = float(q.init.nonlinear.ub) * np.ones_like(x0)
    field = TField(
        x0, taxes=(0,), copy=False, domain=domain[:], order=TENSOR_MAJOR)
    tx_nonlinear = TxDisplacementField(
        field, bounds=(lb, ub), name="nonlinear", mode=NL_REL)

    # Return the full transformation chain
    return Chain(*linear_chain, tx_nonlinear)


def register(fixed, moving, cnf):
    """
    Runs the four registration stages: rotation search, rigid, affine, and
    non-linear. The function has no return value. The transformation chain is
    attached to the Domain of the fixed TImage object and is optimised in situ.

    :param fixed:
        fixed image (to which the chain is attached)
    :type fixed: TImage
    :param moving:
        moving image (that defines the coordinate space that the mapping
        is into)
    :type moving: TImage
    :param cnf: all configuration options
    :type cnf: dict or AttrMap

    """
    p = AttrMap(cnf)
    q = p.regparams
    logger = logging.getLogger(p.logger)

    # Create transformation chain (does not change the fixed image)
    # rotation -> scale -> translation -> affine -> nonlinear
    logger.info("Initialising transformation chain...")
    chain = initialise_transformations(fixed, p)
    logger.info("Transformation chain has been initialised.")

    # Generate output: initial alignment
    fixed.save(os.path.join(
        p.general.outputdir, "fixed.timg"), overwrite=True)
    fixed.snapshot(os.path.join(
        p.general.outputdir, f"fixed.{SNAPSHOT_EXT}"), overwrite=True)
    moving.save(os.path.join(
        p.general.outputdir, "moving.timg"), overwrite=True)
    moving.snapshot(os.path.join(
        p.general.outputdir, f"moving.{SNAPSHOT_EXT}"), overwrite=True)

    # Set the first part of the chain
    fixed.domain.chain.extend(chain[:-2])

    # Rotation search
    if "rotation" in p.general.stages:
        logger.info("Starting rotation search...")
        rotation_search2d(fixed, moving, p)
        logger.info("Completed rotation search.")
        # Generate output
        fixed.save(os.path.join(
            p.general.outputdir, "fixed1_rotation.timg"), overwrite=True)
        moving.evaluate(fixed.domain).snapshot(os.path.join(
            p.general.outputdir, f"moving1_rotation.{SNAPSHOT_EXT}"),
            overwrite=True)
    else:
        logger.info("Rotation search was skipped.")

    # Rigid registration
    if "rigid" in p.general.stages:
        logger.info("Starting rigid registration...")
        rigid2d(fixed, moving, p)
        logger.info("Completed rigid registration.")
        # Generate output
        fixed.save(os.path.join(
            p.general.outputdir, "fixed2_rigid.timg"), overwrite=True)
        moving.evaluate(fixed.domain).snapshot(os.path.join(
            p.general.outputdir, f"moving2_rigid.{SNAPSHOT_EXT}"),
            overwrite=True)
    else:
        logger.info("Rigid registration was skipped.")

    # Affine registration
    fixed.domain.chain.append(chain[-2])
    if "affine" in p.general.stages:
        logger.info("Starting affine registration...")
        affine2d(fixed, moving, p)
        logger.info("Completed affine registration.")
        # Generate output
        fixed.save(os.path.join(
            p.general.outputdir, "fixed3_affine.timg"), overwrite=True)
        moving.evaluate(fixed.domain).snapshot(os.path.join(
            p.general.outputdir, f"moving3_affine.{SNAPSHOT_EXT}"),
            overwrite=True)
    else:
        logger.info("Affine registration was skipped.")

    # Non-linear registration
    tx_nonlinear = chain[-1]
    tx_nonlinear.domain.chain = fixed.domain.chain[:]
    fixed.domain.chain.append(tx_nonlinear)
    if "nonlinear" in p.general.stages:
        logger.info("Starting non-linear registration...")
        diffreg2d(fixed, moving, p)
        logger.info("Completed non-linear registration.")
        # Generate output
        fixed.save(os.path.join(
            p.general.outputdir, "fixed4_nonlinear.timg"), overwrite=True)
        moving.evaluate(fixed.domain).snapshot(os.path.join(
            p.general.outputdir, f"moving4_nonlinear.{SNAPSHOT_EXT}"),
            overwrite=True)
    else:
        logger.info("Non-linear registration was skipped.")


def rotation_search2d(fixed, moving, cnf):
    """
    Finds the best relative orientation of the images.

    :param fixed: Fixed image.
    :type fixed: TImage
    :param moving: Moving image.
    :type moving: TImage
    :param cnf: all configuration options
    :type cnf: dict or AttrMap

    """
    p = AttrMap(cnf)
    q = p.regparams.rotsearch
    logger = logging.getLogger(p.logger)

    # Part 1: coarse search for N best orientations

    # Coarse search at a predefined scale
    fixed.rescale(float(q.scale), copy=False)
    moving.rescale(float(q.scale), copy=False)
    step = radians(q.coarse)
    tx_rotation = fixed.domain.chain["rotation"]
    lb, ub = tx_rotation.parameters.get_bounds()[0]
    rotations = np.arange(lb, ub, step)
    costvals = []
    for i, angle in enumerate(rotations):
        tx_rotation.parameters.parameters[0] = angle
        tx_rotation.parameters.set_lower_bounds(angle - step / 2)
        tx_rotation.parameters.set_upper_bounds(angle + step / 2)
        cost = CostMIND(moving, fixed, normalise=True, kernel=MK_FULL)()
        # cost = CostMI(moving, fixed, normalise=True, bins=32)()
        logger.debug(f"{degrees(angle)} deg: {cost}")
        costvals.append([cost, angle])
    else:
        costvals = np.asarray(costvals)

    # Test the best N initial rotations
    n_best = max(min(N_BEST, len(rotations)), 1)
    best_angles = costvals[np.argsort(costvals[:, 0]), 1][:n_best].ravel()
    logger.info(f"The {n_best} best initialisation angles: "
                f"{np.rad2deg(best_angles)} deg")

    # Part 2: fine-tune the rotation parameter by simultaneous additional
    # optimisation of translation and scaling, starting from the best three
    # orientations.
    tx_scale = fixed.domain.chain["scale"]
    tx_translation = fixed.domain.chain["translation"]
    og = OptimisationGroup(tx_rotation, tx_scale, tx_translation)
    scale_orig = tx_scale.parameters.parameters.copy()
    translation_orig = tx_translation.parameters.parameters.copy()
    costvals = []
    for angle in best_angles:
        tx_rotation.parameters.parameters[0] = angle
        tx_rotation.parameters.set_lower_bounds(angle - step / 2)
        tx_rotation.parameters.set_upper_bounds(angle + step / 2)
        tx_rotation.parameters.unlock()
        tx_scale.parameters.parameters[:] = scale_orig.copy()
        tx_translation.parameters.parameters[:] = translation_orig.copy()
        # Rescale to the same predefined resolution as above
        fixed.rescale(float(q.scale), copy=False)
        moving.rescale(float(q.scale), copy=False)
        # Set cost function
        cost = CostMIND(moving, fixed, maskmode="and", normalise=True)
        # cost = CostMI(moving, fixed, maskmode="and", normalise=True)
        # Start optimisation
        logger.info(f"Co-optimising scale and translation at "
                    f"{degrees(angle)} deg...")

        OptNL(og, cost, method="LN_BOBYQA", visualise=q.visualise,
              xtol_abs=q.xtol_abs, xtol_rel=q.xtol_rel, step=q.opt_step,
              logger=logger, normalised=True)()
        costvals.append((cost(), og.parameters[:]))

    # Find best initialisation based on cost
    best_params = min(costvals, key=lambda r: r[0])[1]
    logger.info(f"Best parameters after rotation search: {best_params}")
    og.set(best_params)
    tx_rotation.parameters.set_lower_bounds(best_params[0] - step / 2)
    tx_rotation.parameters.set_upper_bounds(best_params[0] + step / 2)
    tx_rotation.parameters.unlock()

    # Return to full resolution after the rotation search
    fixed.rescale(1, copy=False)
    moving.rescale(1, copy=False)


def rigid2d(fixed, moving, cnf):
    """
    Optimises rigid-body parameters and scaling.

    """
    p = AttrMap(cnf)
    q = p.regparams.rigid
    logger = logging.getLogger(p.logger)

    # Scaling-smoothing iteration
    for i, (sc, sm) in enumerate(zip(q.scaling, q.smoothing)):
        logger.debug(f"Scale: {sc}, smoothing: {sm} px...")
        # Prepare images for the current iteration
        fixed.rescale(1. / sc, copy=False)
        moving.rescale(1. / sc, copy=False)
        fixed_smooth = fixed.smooth(sm, copy=True)
        moving_smooth = moving.smooth(sm, copy=True)
        # Prepare co-optimised transformations
        tx_rotation = fixed_smooth.domain.chain["rotation"]
        tx_scale = fixed_smooth.domain.chain["scale"]
        tx_translation = fixed_smooth.domain.chain["translation"]
        og = OptimisationGroup(tx_rotation, tx_scale, tx_translation)
        lb, ub = og.get_bounds().T
        lb = lb - np.finfo(lb.dtype).eps
        ub = ub + np.finfo(lb.dtype).eps
        og.set_bounds(lb, ub)
        # Set cost function
        # cost = CostMI(moving_smooth, fixed_smooth, normalise=True)
        cost = CostMIND(moving_smooth, fixed_smooth, normalise=True)
        # Start optimisation
        OptNL(og, cost, method="LN_BOBYQA", visualise=q.visualise,
              xtol_abs=q.xtol_abs, xtol_rel=q.xtol_rel, step=q.opt_step,
              logger=logger, normalised=True)()
        # Transfer optimised transformations to the non-smoothed images
        fixed.domain = fixed_smooth.domain
        moving.domain = moving_smooth.domain
    else:
        # Restore full resolution of the images
        fixed.rescale(1, copy=False)
        moving.rescale(1, copy=False)


def affine2d(fixed, moving, cnf):
    """
    Optimises a 6-DOF affine transformation after rigid alignment has been
    fine-tuned.

    """
    p = AttrMap(cnf)
    q = p.regparams.affine
    logger = logging.getLogger(p.logger)

    # Scaling-smoothing iterations
    for i, (sc, sm) in enumerate(zip(q.scaling, q.smoothing)):
        logger.debug(f"Scale: {sc}, smoothing: {sm} px...")
        # Prepare images for the current iteration
        fixed.rescale(1. / sc, copy=False)
        moving.rescale(1. / sc, copy=False)
        fixed_smooth = fixed.smooth(sm, copy=True)
        moving_smooth = moving.smooth(sm, copy=True)
        # Prepare transformation to optimise
        tx_affine = fixed_smooth.domain.chain["affine"]
        # Set cost function
        # cost = CostMI(moving_smooth, fixed_smooth, normalise=True)
        cost = CostMIND(moving_smooth, fixed_smooth, normalise=True)
        # Start optimisation
        OptNL(tx_affine, cost, method="LN_BOBYQA",
              xtol_rel=q.xtol_rel, xtol_abs=q.xtol_abs,
              visualise=q.visualise, step=q.opt_step, logger=logger,
              normalised=True)()
        # Transfer optimised transformations to the non-smoothed images
        fixed.domain = fixed_smooth.domain
        moving.domain = moving_smooth.domain
    else:
        # Restore full resolution of the images
        fixed.rescale(1, copy=False)
        moving.rescale(1, copy=False)


def diffreg2d(fixed, moving, cnf):
    """
    Performs a non-linear registration. The transformation is parameterised as
    a dense displacement field. The cost is MIND, and diffusion regularisation
    is used to create smoothness in the deformation field.

    """
    p = AttrMap(cnf)
    q = p.regparams.nonlinear
    logger = logging.getLogger(p.logger)

    # Scaling-smoothing iteration
    for i, (sc, sm) in enumerate(zip(q.scaling, q.smoothing)):
        logger.debug(f"Scale: {sc}, smoothing: {sm} px...")
        # Prepare images for the current iteration
        fixed.rescale(1 / sc, copy=False)
        moving.rescale(1 / sc, copy=False)
        fixed_smooth = fixed.smooth(sm, copy=True)
        moving_smooth = moving.smooth(sm, copy=True)
        # Prepare transformation to optimise
        tx_nonlinear = fixed_smooth.domain.chain[-1]
        # Set cost and regulariser
        cost = CostMIND(moving_smooth, fixed_smooth, sigma=float(q.sigma),
                        truncate=float(q.truncate), kernel=MK_FULL)
        regularisation = DiffusionRegulariser(
            tx_nonlinear, weight=float(q.regweight))
        # Optimise the non-linear transformation
        GNOptimiserDiffusion(
            tx_nonlinear, cost, regularisation, maxiter=int(q.maxiter[i]),
            xtol_rel=q.xtol_rel, xtol_abs=q.xtol_abs, visualise=q.visualise,
            logger=logger)()
        # Transfer optimised transformations to the non-smoothed images
        fixed.domain = fixed_smooth.domain
        moving.domain = moving_smooth.domain
    else:
        # Restore the original resolution of the images
        fixed.rescale(1, copy=False)
        moving.rescale(1, copy=False)


                            # AUXILIARY FUNCTIONS

# These functions may be used within other functions of the script. Their usage
# outside this script (via importing from this module) is discouraged. They are
# included here to prevent too frequent code repetitions.

def rgb2hsv(x):
    assert x.shape[-1] == 3, "The input must be RGB."
    _x = x / 255
    _x = _x.reshape(-1, 3)
    c_min = np.min(_x, axis=-1)
    c_max = np.max(_x, axis=-1)
    delta = c_max - c_min
    r, g, b = _x.reshape(-1, 3).T
    r = r.ravel()
    g = g.ravel()
    b = b.ravel()
    i2 = c_max == r
    i3 = c_max == g
    i4 = c_max == b

    # Calculate Hue
    h = np.zeros(r.size)
    h[i2] = 60 * ((g[i2] - b[i2]) / delta[i2] % 6)
    h[i3] = 60 * ((b[i3] - r[i3]) / delta[i3] + 2)
    h[i4] = 60 * ((r[i4] - g[i4]) / delta[i4] + 4)

    # Calculate Saturation
    s = np.where(np.isclose(c_max, 0), 0, delta / c_max)

    # Calculate Value
    v = c_max

    x_hsv = np.stack((h.ravel(), s.ravel(), v.ravel()), axis=-1)
    return x_hsv


def match_block_resolution(img, **kwargs):
    """
    Resamples histology image to match the resolution of another 2D image.

    """
    p = AttrMap(kwargs.get("cnf"))
    logger = logging.getLogger(p.logger)
    other = kwargs.get("other")

    res = other.resolution
    factors = np.divide(img.resolution, res)
    logger.info(f"Resampling 2D image by factors {tuple(factors)}...")
    img = img.rescale(*factors, copy=True)
    img.resmgr.reset()
    img.storage = MEM
    img.domain.storage = MEM
    logger.info(f"Shape of the 2D image after resampling: {img.shape}.")

    # Save low-resolution image
    fp, fn = os.path.split(p.histology.file)
    fn, ext = os.path.splitext(fn)
    filename = os.path.join(p.general.outputdir, f"{fn}_lowres.{SNAPSHOT_EXT}")
    img.snapshot(filename, overwrite=True)
    logger.info(f"Saved low-resolution snapshot of the 2D image to: {filename}")

    return img


def histology_preprocessing(histo, **kwargs):
    """
    Preprocess the histology image.

    """
    # Convert to grayscale using the Y channel of the YIQ colour space.
    return image.rgb2yiq(histo.tensors[:3]).tensors[0]


def block_preprocessing(block, **kwargs):
    """
    Preprocess the block photograph.

    """
    # Convert to grayscale using the Y channel of the YIQ colour space.
    return image.rgb2yiq(block.tensors[:3]).tensors[0]


def hsv_sat(timg, **kwargs):
    from skimage.color import rgb2hsv
    hsv = rgb2hsv(timg.data)
    return hsv[..., 1]


def hsv_invsat(timg, **kwargs):
    from skimage.color import rgb2hsv
    hsv = rgb2hsv(timg.data)
    return 1 - hsv[..., 1]


def dilated_object_mask(timg, **kwargs):
    from skimage.color import rgb2hsv
    hsv = rgb2hsv(timg.data)
    objmask = hsv[..., 2] > 0.1
    from skimage.morphology import binary_dilation
    objmask = binary_dilation(objmask.astype(np.uint8), selem=np.ones((20, 20)))
    return objmask.astype(np.float32)


@beta_function
def mask_roi_defects(histo, block, p):

    # Highlight non-matching areas between affine-registered images
    tmp = histo.evaluate(block.domain, rule=None)
    tmp = np.where(tmp.data > 0.05, 1, 0)
    binary = np.where(block.data > 1, 1, 0)
    totalarea = np.count_nonzero(binary)
    # print(totalarea)
    area = p.area * totalarea if p.area < 1 else p.area
    # print(area)
    binary[tmp == 1] = 0
    del tmp

    # Discard small regions, that should actually drive non-linear registration

    # Area filter
    labels = label(binary, background=0, connectivity=binary.ndim)
    regions = regionprops(labels)
    # print(sorted([region["area"] for region in regions])[::-1])
    large_region_labels = [region["label"] for region in regions
                           if region["area"] > area]
    binary[...] = 0
    for value in large_region_labels:
        binary[labels == value] = 1

    labels = label(binary, background=0, connectivity=binary.ndim)
    n_defects = np.max(labels)
    TImage(labels).preview()

    # Warn the user about ROI defects
    if n_defects:
        warnings.warn("Number of ROI defects found: {}.".format(int(n_defects)))

    # Add the identified ROI defects to the block mask
    binary = 1 - binary
    bmask = block.mask.data
    # bmask[bmask == 0.1] = 0
    block.mask = bmask * binary


def pad(timg, **kwargs):
    """ Zero-pads the image by 1/5 of the image shape on both ends along each
    spatial dimension. """
    padding = []
    for dim in timg.vshape:
        padding.append((dim // 6, dim // 6))
    padding = tuple(padding)
    offset = TxTranslation([-pad[0] for pad in padding], name="padding")
    lpad = ((0, 0),) * timg.tdim
    if timg.order == TENSOR_MAJOR:
        padding = lpad + padding
    else:
        padding = padding + lpad
    imdata = np.pad(timg.data, padding, mode="maximum")
    offset = offset + timg.domain.offset
    chain = timg.domain.chain
    padded = TImage(imdata, taxes=timg.taxes)
    padded.domain.offset = offset
    padded.domain.chain = chain
    return padded


                            # PROGRAM FLOW CONTROL

def histology_to_block(args):
    """
    Main program code. Controls program flow, handles command-line arguments.

    """
    cnf = args.config
    if os.path.isfile(cnf):
        with open(cnf, "r") as fp:
            cnf = dict(json.load(fp))
    else:
        raise FileNotFoundError(f"The provided configuration file "
                                f"does not exist: {args.config}")

    # Override histology and block file paths in the configuration file with
    # those provided on the command line.
    if args.histo and os.path.isfile(args.histo):
        cnf["histology"]["file"] = args.histo
    if args.block and os.path.isfile(args.block):
        cnf["block"]["file"] = args.block
    if args.out:
        cnf["general"]["outputdir"] = args.out

    # Override verbose option in configurations
    cnf["general"]["verbose"] = args.verbose

    # Run registration script
    run(cnf)


def create_cli(parser):
    """
    Sets up the CLI argument parser instance.

    """
    parser.add_argument("--histo", metavar="image",
                        help="Histology slide", default=None, type=str,
                        required=False)
    parser.add_argument("--block", metavar="image",
                        help="Block photograph", default=None, type=str,
                        required=False)
    parser.add_argument("--out", metavar="dir",
                        help="Output directory", default=None, type=str,
                        required=False)
    parser.add_argument("--config", metavar="cnf_file.json",
                        help="configuration file", default=None, type=str,
                        required=True)
    parser.add_argument("-v", "--verbose", default=False, action="store_true",
                        help="Print status messages to the command line",
                        required=False)

    return parser


def main(*args):
    """ Main program code. """

    parser = argparse.ArgumentParser(
        prog="histology_to_block",
        description="Registers a histology slide to "
                    "a corresponding tissue block photograph.")
    parser = create_cli(parser)

    if args:
        histology_to_block(parser.parse_args(args))
    else:
        parser.print_help()


if __name__ == "__main__":
    main(*sys.argv[1:])
