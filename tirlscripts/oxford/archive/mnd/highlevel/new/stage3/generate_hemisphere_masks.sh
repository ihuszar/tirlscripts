#!/bin/sh

for subject in CTL12 MND19 MND21 MND22; do
	
	echo "Generating approximate hemisphere masks for subject ${subject}..."
	mridir=/mnt/alpha/mnd/${subject}/mri

	bet ${mridir}/Segmentation/trufi_comb_iso.nii.gz ${mridir}/Segmentation/trufi_comb_iso_brain.nii.gz -m -R -f 0.05

	echo "Right..."
	fslroi ${mridir}/MNI/MNI152_T1_1mm_brain.nii.gz ${mridir}/Segmentation/rh_mni.nii.gz 0 90 0 -1 0 -1
	fslmaths ${mridir}/Segmentation/rh_mni.nii.gz -add 1 -bin ${mridir}/Segmentation/rh_mni.nii.gz
	antsApplyTransforms -d 3 -e 0 -i ${mridir}/Segmentation/rh_mni.nii.gz -r ${mridir}/Segmentation/trufi_comb_iso_brain.nii.gz -o ${mridir}/Segmentation/rh.nii.gz -n Linear -t ${mridir}/MNI/coreg1InverseWarp.nii.gz ${mridir}/MNI/coreg0GenericAffine.mat -f 0 -v
	fslmaths ${mridir}/Segmentation/rh.nii.gz -mul ${mridir}/Segmentation/trufi_comb_iso_brain.nii.gz -bin ${mridir}/Segmentation/rh.nii.gz

	echo "Left..."
	fslroi ${mridir}/MNI/MNI152_T1_1mm_brain.nii.gz ${mridir}/Segmentation/lh_mni.nii.gz 90 -1 0 -1 0 -1
	fslmaths ${mridir}/Segmentation/lh_mni.nii.gz -add 1 -bin ${mridir}/Segmentation/lh_mni.nii.gz
	antsApplyTransforms -d 3 -e 0 -i ${mridir}/Segmentation/lh_mni.nii.gz -r ${mridir}/Segmentation/trufi_comb_iso_brain.nii.gz -o ${mridir}/Segmentation/lh.nii.gz -n Linear -t ${mridir}/MNI/coreg1InverseWarp.nii.gz ${mridir}/MNI/coreg0GenericAffine.mat -f 0 -v
	fslmaths ${mridir}/Segmentation/lh.nii.gz -mul ${mridir}/Segmentation/trufi_comb_iso_brain.nii.gz -bin ${mridir}/Segmentation/lh.nii.gz

done
