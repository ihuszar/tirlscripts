#!/usr/bin/env python

"""
Creates a warp weighting volume based on hemisphere masks.

"""

import numpy as np
import nibabel as nib
from scipy.ndimage.morphology import distance_transform_edt

SUBJECTS = [
    "CTL10",
    "CTL12",
    "MND12",
    "MND13",
    "MND14",
    "MND15",
    "MND16",
    "MND17",
    "MND18",
    "MND19",
    "MND20",
    "MND21",
    "MND22",
    "MND23"
]

for subject in SUBJECTS:

    lh = f"/mnt/alpha/mnd/{subject}/mri/Segmentation/lh.label.cerebrum.nii.gz"
    rh = f"/mnt/alpha/mnd/{subject}/mri/Segmentation/rh.label.cerebrum.nii.gz"

    hdr = nib.load(lh).header
    lh = nib.load(lh).get_fdata()
    rh = nib.load(rh).get_fdata()
    print("Distance transform...")
    edt_lh = 1 / (1 + distance_transform_edt(1 - lh)) ** 2
    edt_rh = 1 / (1 + distance_transform_edt(1 - rh)) ** 2
    # edt_lh[edt_rh == 1] = 0
    # edt_rh[edt_lh == 1] = 0
    weights_lh = edt_lh / (edt_lh + edt_rh)
    weights_rh = edt_rh / (edt_lh + edt_rh)
    print("Stacking.")
    vol = np.stack([weights_lh, weights_rh], axis=-1)
    nifti = nib.Nifti1Image(vol, affine=hdr.get_best_affine(), header=hdr)
    nib.save(nifti, f"/mnt/alpha/mnd/{subject}/mri/Segmentation/"
                    f"warpweights.nii.gz")
    print("Done.")
