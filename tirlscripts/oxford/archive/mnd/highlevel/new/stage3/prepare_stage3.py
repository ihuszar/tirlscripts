#!/usr/bin/env python

import os
import re
import json
import tirl
from glob import glob

import imageio
import numpy as np

ROOTDIR = "/mnt/alpha/mnd"
N_CPU = 1
CNF_TEMPLATE = "/mnt/alpha/mnd/config/slice_to_volume.json"
SUBJECTS = [
    "CTL12",
    "MND19",
    "MND21",
    "MND22"
]
PATHS = [
    "/mnt/alpha/mnd/MND16/slices/07/P",
    "/mnt/alpha/mnd/MND16/slices/08/P",
    "/mnt/alpha/mnd/MND17/slices/07/P",
    "/mnt/alpha/mnd/MND17/slices/08/P",
    "/mnt/alpha/mnd/MND20/slices/09/P",
    "/mnt/alpha/mnd/MND20/slices/08/P"
]


def main():
    """ Main program code. """

    # For subjects where the BrainSuite pipeline failed
    # slicedirs = get_slicedirs()

    # For slices where the original slice-to-volume registration was sub par
    slicedirs = PATHS

    for sdir in slicedirs:
        process_slicedir(sdir)


def get_slicedirs():
    slicedirs = []
    for subject in SUBJECTS:
        slicesdir = os.path.join(ROOTDIR, subject, "slices")
        for root, dirs, files in os.walk(slicesdir):
            if "to_vol.bilateral.bak" not in dirs:
                continue
            else:
                slicedirs.append(root)
    return slicedirs


def process_slicedir(slicedir):

    # Copy configurations from the previous template (slab pos!)
    srcdir = os.path.join(slicedir, "to_vol.bilateral.bak")
    with open(os.path.join(srcdir, "configuration.json"), "r") as fp:
        cnf = dict(json.load(fp))

    # Slice file
    sfile = sorted(glob(os.path.join(slicedir, "*_crop_*seg.tif")))[0]
    snum = re.match(r"\d{3}", os.path.basename(sfile)).group(0)

    # Generate bilateral mask
    try:
        lmfile = sorted(glob(os.path.join(
            slicedir, f"{snum}*_handmask_left_new.tif")))[0]
        rmfile = sorted(glob(os.path.join(
            slicedir, f"{snum}*_handmask_right_new.tif")))[0]
        lm = imageio.imread(lmfile)
        rm = imageio.imread(rmfile)
        bm = np.logical_or(lm, rm).astype(np.uint8) * 255
        bmfile = lmfile.replace("left", "bilateral")
        imageio.imsave(bmfile, bm)
    except IndexError:
        print(f"ERROR: no mask in {slicedir}")
        raise
    masks = {
        "left": lmfile,
        "right": rmfile,
        "bilateral": bmfile
    }

    # Refine initial position
    bslice = tirl.load(os.path.join(srcdir, "4_stage4.timg"))
    pos = bslice.domain.pcentre()

    # Create registration configurations for left, right, and bilateral masks
    for side in ("left", "right", "bilateral"):
        trgdir = os.path.join(slicedir, f"to_vol.{side}")
        if not os.path.isdir(trgdir):
            os.makedirs(trgdir)
        # Specify lateral mask
        smfile = masks[side]
        # Specify input files
        mridir = os.path.join(slicedir, "../../../", "mri")
        # vfile = os.path.join(
        #     mridir, "Segmentation", "trufi_comb_iso_brain.nii.gz")
        vfile = os.path.join(
            mridir, "Segmentation", "trufi_comb_iso.bse.nii.gz")
        vmfile = None
        cnf = generate_config(cnf, trgdir, sfile, smfile, vfile, vmfile, pos)
        cfile = os.path.join(
            slicedir, f"to_vol.{side}", f"{snum}_{side}_config.json")
        # Save the renewed configuration file
        print(f"Saving: {cfile}...")
        with open(cfile, "w") as fp:
            json.dump(cnf, fp, indent=4, sort_keys=True)


def generate_config(cnf, trgdir, sfile, smfile, vfile, vmfile, pos):

    # Update general fields
    cnf["general"]["outputdir"] = trgdir
    cnf["general"]["logfile"] = os.path.join(trgdir, "logfile.log")
    cnf["general"]["paramlogfile"] = \
        os.path.join(trgdir, "paramlog.log")

    cnf["slice"]["file"] = sfile
    cnf["slice"]["mask"]["file"] = smfile
    cnf["volume"]["file"] = vfile
    cnf["volume"]["mask"]["file"] = vmfile

    cnf["regparams"]["stage_1"]["n_cpu"] = N_CPU
    cnf["regparams"]["stage_3"]["mask"]["file"] = smfile
    cnf["regparams"]["stage_3"]["support"]["mask"]["file"] = smfile
    cnf["regparams"]["stage_4"]["mask"]["file"] = smfile
    cnf["regparams"]["stage_4"]["support"]["mask"]["file"] = smfile

    # Update position
    cnf["regparams"]["stage_1"]["slab"]["centre"] = pos
    cnf["regparams"]["stage_1"]["slab"]["offset"] = 0

    # Set high-resolution registration
    cnf["regparams"]["stage_1"]["slice_scaling"] = [8, 4, 2, 1]
    cnf["regparams"]["stage_1"]["slice_smoothing"] = [0, 0, 0, 0]
    cnf["regparams"]["stage_1"]["volume_scaling"] = [8, 4, 2, 1]
    cnf["regparams"]["stage_1"]["volume_smoothing"] = [0, 0, 0, 0]

    cnf["regparams"]["stage_2"]["slice_scaling"] = [4, 2, 1]
    cnf["regparams"]["stage_2"]["slice_smoothing"] = [0, 0, 0]
    cnf["regparams"]["stage_2"]["volume_scaling"] = [4, 2, 1]
    cnf["regparams"]["stage_2"]["volume_smoothing"] = [0, 0, 0]

    cnf["regparams"]["stage_3"]["slice_scaling"] = [2, 1]
    cnf["regparams"]["stage_3"]["slice_smoothing"] = [0, 0]
    cnf["regparams"]["stage_3"]["volume_scaling"] = [1, 1]
    cnf["regparams"]["stage_3"]["volume_smoothing"] = [0, 0]
    cnf["regparams"]["stage_3"]["support"]["points"] = 16
    cnf["regparams"]["stage_3"]["support"]["mode"] = "km"

    cnf["regparams"]["stage_4"]["slice_scaling"] = [2, 1]
    cnf["regparams"]["stage_4"]["slice_smoothing"] = [0, 0]
    cnf["regparams"]["stage_4"]["volume_scaling"] = [1, 1]
    cnf["regparams"]["stage_4"]["volume_smoothing"] = [0, 0]
    cnf["regparams"]["stage_4"]["support"]["points"] = 16
    cnf["regparams"]["stage_4"]["support"]["mode"] = "km"

    # Cleanup
    try:
        del cnf["slice"]["file_old"]
    except Exception:
        pass

    return cnf


if __name__ == "__main__":
    main()
