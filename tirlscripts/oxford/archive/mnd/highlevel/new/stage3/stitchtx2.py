#!/usr/bin/env python

"""
Stiches left- and right-hemisphere optimised slice-to-volume registrations into
a single warped image.

The script takes a less trivial approach to avoid the definition of a
non-linear transformation on a non-linearly deformed 3D embedded 2D domain.
This allows the stitch transformation to be computed quickly.

"""
import json
import os

import imageio
import numpy as np
from glob import glob
import multiprocessing as mp
from functools import partial
from scipy.ndimage.morphology import distance_transform_edt as edt

import tirl
from tirl.tfield import TField
from tirl.timage import TImage
from tirl.transformations.displacement import TxDisplacementField
from tirl.interpolation.scipyinterpolator import ScipyInterpolator

N_CPU = 8
ROOTDIR = "/mnt/alpha/mnd"
SUBJECTS = [
    "CTL10",
    "CTL12",
    "MND12",
    "MND13",
    "MND14",
    "MND15",
    "MND16",
    "MND17",
    "MND18",
    "MND19",
    "MND20",
    "MND21",
    "MND22",
    "MND23"
]


def main():
    """ Main program code. """

    for subject in SUBJECTS:
        print(f"Processing subject {subject}...")
        try:
            process_subject(subject)
        except Exception as exc:
            print(f"ERROR while processing subject {subject}")
            print(exc.args)
            continue


def process_subject(subject):
    """
    Hybridises all slice-to-volume registrations of a specific subject.

    """
    slicesdir = os.path.join(ROOTDIR, subject, "slices")
    sdirs = []
    for root, dirs, files in os.walk(slicesdir):
        if ("to_vol.left" in dirs) and ("to_vol.right" in dirs):
            sdirs.append(root)

    workerfunc = partial(safe_process_slice, subject)
    with mp.Pool(N_CPU, maxtasksperchild=1) as pool:
        pool.map(workerfunc, sdirs)


def safe_process_slice(subject, slicedir):
    try:
        process_slice(subject, slicedir)
    except Exception as exc:
        print(f"ERROR while processing slice {slicedir}")
        print(exc.args)


def process_slice(subject, slicedir):
    """
    Hybridises a specific slice of a specific subject given the slice directory.

    """
    print(f"Processing slice {slicedir}...")

    cnf = os.path.join(slicedir, "to_vol.bilateral", "configuration.json")
    with open(cnf, "r") as fp:
        cnf = dict(json.load(fp))
    tfile = os.path.join(ROOTDIR, subject, "mri", "Segmentation",
                         os.path.basename(cnf["volume"]["file"]))
    trufi = TImage(tfile, chain="sform")
    leftdir = os.path.join(slicedir, "to_vol.left")
    rightdir = os.path.join(slicedir, "to_vol.right")
    # sfile = sorted(glob(os.path.join(slicedir, "*_seg.tif")))[0]
    ldom = tirl.load(os.path.join(leftdir, "4_stage4.timg")).domain
    rdom = tirl.load(os.path.join(rightdir, "4_stage4.timg")).domain
    # ldom = TImage(sfile, chain=ldom.chain).domain
    # rdom = TImage(sfile, chain=rdom.chain).domain
    lw, rw = get_weights(subject, slicedir, ldom, rdom)
    lc = ldom.get_physical_coordinates()
    rc = rdom.get_physical_coordinates()

    hc = ((lc.T * lw + rc.T * rw) / (lw + rw)).T
    nans = ~np.isfinite(hc)
    hc[nans] = (lc[nans] + rc[nans]) / 2

    warp, ix = ldom.get_external_transformation("warp", index=True)
    hybrid = ldom[:, :ix].copy()
    x0 = hybrid.get_physical_coordinates()
    diff = (hc - x0).T.reshape(-1, *hybrid.shape)
    field = TField(diff, tshape=3, vshape=hybrid.shape,
                   order="T", domain=hybrid[:])
    linpol = ScipyInterpolator(order=1, prefilter=False)
    warp = TxDisplacementField(field, interpolator=linpol, mode="abs")
    hybrid.chain.append(warp)
    res = trufi.evaluate(hybrid)
    outdir = os.path.join(slicedir, "to_vol.hybrid")
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
    res.save(os.path.join(outdir, "slice.timg"), overwrite=True)
    res.snapshot(os.path.join(outdir, "resampled_mri.png"),
                 overwrite=True)


def get_weights(subject, slicedir, ldom, rdom):
    """
    Calculates pixelwise left-right weighting of the slice domain based on
    either volumetric or planar right-left segmentation masks.

    """
    cfile = os.path.join(slicedir, "to_vol.left", "configuration.json")
    with open(cfile, "r") as fp:
        cnf = json.load(fp)

    # 3D left-right segmentation
    if cnf["volume"]["mask"]["file"]:
        wfile = os.path.join(
            ROOTDIR, subject, "mri", "Segmentation", "warpweights.nii.gz")
        linpol = ScipyInterpolator(order=1, prefilter=False)
        weights = TImage(wfile, chain="sform", interpolator=linpol)
        lw = weights.tensors[0].evaluate(ldom).data.ravel()
        rw = weights.tensors[1].evaluate(rdom).data.ravel()

    # 2D left-right segmentation
    elif not cnf["volume"]["mask"]["file"]:
        lm = glob(os.path.join(slicedir, "*_handmask_left_new.tif"))[0]
        rm = glob(os.path.join(slicedir, "*_handmask_right_new.tif"))[0]
        lw = np.asarray(edt(TImage(lm).resize(*ldom.shape, copy=True).data),
                        dtype=float).ravel()
        rw = np.asarray(edt(TImage(rm).resize(*rdom.shape, copy=True).data),
                        dtype=float).ravel()

    else:
        assert False, "No mask provided for hybridisation."

    return lw, rw


if __name__ == "__main__":
    main()