#!/usr/bin/env python

import os
import re
import json
import shutil

import numpy
import pandas as pd
from glob import glob
import multiprocessing as mp
from functools import partial

import tirl
from tirl.timage import TImage
from scripts.oxdbb.pathology.highlevel import histology_to_volume3 as h2v

INDEX_HMOD = "PLP"
INDEX_MMOD = "TRUFI"
SUBJECTS = [
    "CTL10",
    "CTL12",
    "MND12",
    "MND13",
    "MND14",
    "MND15",
    "MND16",
    "MND17",
    "MND18",
    "MND19",
    "MND20",
    "MND21",
    "MND22",
    "MND23"
]
ROOTDIR = "/mnt/alpha/mnd"
# CNF_TEMPLATE = "/mnt/alpha/mnd/config/histology_to_volume3b.json"
CNF_TEMPLATE = "/home/inhuszar/apps/tirl/scripts/oxdbb/pathology/highlevel/" \
               "histology_to_volume3b.json"
N_CPU = 12


def main():
    """ Main program code. """

    for subject in SUBJECTS:
        print(f"Processing subject {subject}...")
        try:
            process_subject(subject)
        except Exception as exc:
            print(f"ERROR while processing subject {subject}")
            print(exc.args)
            continue


def process_subject(subject):
    """
    Registers PLP images of all blocks to TRUFI for one subject

    """
    subjdir = os.path.join(ROOTDIR, subject)
    blocks = list(filter(lambda x: re.match(r"\d+[RL]?a", x),
                         os.listdir(os.path.join(subjdir, "blocks"))))
    blocks = [os.path.join(subjdir, "blocks", b) for b in blocks]
    blocks = sorted(blocks)
    assert len(blocks) > 0, f"No blocks to register for subject {subject}"

    workerfunc = partial(process_block, subject)
    if N_CPU > 1:
        with mp.Pool(N_CPU, maxtasksperchild=1) as pool:
            pool.map(workerfunc, blocks)
    else:
        for block in blocks:
            workerfunc(block)


def process_block(subject, block):
    # Safe combine stages
    print(f"Combining transformations for {block}...")
    try:
        combine_stages(subject, block)
    except Exception as exc:
        print(f"ERROR while combining transformations for {block}")
        print(exc.args)
        raise ####

    # Safe refine alignment
    print(f"Refining alignment for {block}...")
    try:
        refine_alignment(block)
    except Exception as exc:
        print(f"ERROR while refining alignment for {block}")
        print(exc.args)


def combine_stages(subject, block):
    """
    Registers the PLP image of a specific tissue block to TRUFI
    by combining the transformation chains of stages 1-3.

    """
    subjdir = os.path.join(ROOTDIR, subject)
    regsheet = os.path.join(subjdir, subject + "_registration_sheet.csv")
    regsheet = pd.read_csv(regsheet)
    regsheet["Block"] = regsheet["BlockID"] + regsheet["BlockSurface"]
    record = regsheet[regsheet.Block == os.path.basename(block)].iloc[0]
    slicefile = os.path.join(ROOTDIR, record.SliceFile)

    # Join the piecewise optimised transformation chains to establish a mapping
    # from 8um histology voxel space to TRUFI voxel space.
    h2bfile = os.path.join(
        block, "histology", INDEX_HMOD, "to_block", "fixed4_nonlinear.timg")
    bfile = os.path.join(
        block, "histology", INDEX_HMOD, "to_block", "moving.timg")
    b2sfile = os.path.join(
        block, "photo", "to_slice", "fixed4_nonlinear.timg")
    sfile = os.path.join(
        block, "photo", "to_slice", "moving.timg")

    # Load chains from the files
    h2block = trim_chain(tirl.load(h2bfile).domain.all_tx())
    block2pix = trim_chain(tirl.load(bfile).domain.all_tx()).inverse()
    b2slice = trim_chain(tirl.load(b2sfile).domain.all_tx())
    slice2pix = trim_chain(tirl.load(sfile).domain.all_tx()).inverse()
    slicedir = os.path.dirname(slicefile)
    if "L" in os.path.basename(block):
        sname = os.path.join(slicedir, "to_vol.left", "4_stage4.timg")
        s2volume = trim_chain(tirl.load(sname).domain.all_tx())
    elif "R" in os.path.basename(block):
        sname = os.path.join(slicedir, "to_vol.right", "4_stage4.timg")
        s2volume = trim_chain(tirl.load(sname).domain.all_tx())
    else:
        sname = os.path.join(slicedir, "to_vol.hybrid", "slice.timg")
        try:
            s2volume = trim_chain(tirl.load(sname).domain.all_tx())
        except Exception as exc:
            print(f"Cannot load hybrid Stage-3 result from {sname}")
            sname = os.path.join(slicedir, "to_vol.bilateral", "4_stage4.timg")
            s2volume = trim_chain(tirl.load(sname).domain.all_tx())

    chain = h2block + block2pix + b2slice + slice2pix + s2volume

    # Create outputs
    hfile = glob(os.path.join(block, "histology", INDEX_HMOD, "*.svs"))[0]
    histo = TImage(hfile, loaderkwargs=dict(pages=3))
    histo.domain.internal = []
    histo.domain.chain = chain
    out = os.path.join(
        block, "histology", INDEX_HMOD, "to_" + INDEX_MMOD, "historc.timg")
    if not os.path.isdir(os.path.dirname(out)):
        os.makedirs(os.path.dirname(out))
    histo.save(out, overwrite=True)
    volfile = os.path.join(ROOTDIR, record.MRIVoxelSpace)
    vol = TImage(volfile, chain="sform")
    out = out.replace("historc.timg", "rawcombine.timg")
    prev = vol.evaluate(histo.domain)
    prev.save(out, overwrite=True)
    prev.snapshot(out.replace(".timg", ".png"), overwrite=True)
    return histo


def trim_chain(chain):
    names = [tx.name.lower() for tx in chain]
    if "resolution" not in names:
        return chain
    else:
        while chain and chain[0].name.lower() != "resolution":
            chain.pop(0)
    return chain


def refine_alignment(block):
    """
    Registers the PLP image of a specific tissue block to TRUFI
    by refining the alignment of the histology image in MRI space.

    This stage is referred to as "Stage 4 - histology-to-volume registration"
    in the manuscript.

    """
    for cost in ("mind", "mi"):

        subjdir = os.path.abspath(os.path.join(block, "../../"))
        outdir = os.path.join(
            block, "histology", INDEX_HMOD, "to_" + INDEX_MMOD,
            f"histology2volume.{cost}")
        if not os.path.isdir(outdir):
            os.makedirs(outdir)

        # Create block-specific Stage-4 configuration file
        with open(CNF_TEMPLATE, "r") as fp:
            cnf = json.load(fp)

        # General
        cnf["general"]["cost"] = cost.upper()
        cnf["general"]["outputdir"] = outdir
        cnf["general"]["logfile"] = os.path.join(outdir, "logfile.log")
        cnf["general"]["paramlogfile"] = os.path.join(outdir, "params.log")
        # Histology
        cnf["histology"]["file"] = os.path.join(
            block, "histology", INDEX_HMOD, "to_" + INDEX_MMOD, "historc.timg")
        # Volume
        mfile = os.path.join(subjdir, "mri", "Segmentation",
                             "trufi_comb_iso.bse.nii.gz")
        if not os.path.exists(mfile):
            mfile = os.path.join(subjdir, "mri", "Segmentation",
                                 "trufi_comb_iso_brain.nii.gz")
        cnf["volume"]["file"] = mfile

        # Stage 1 parameters
        cnf["regparams"]["stage_1"]["n_cpu"] = 3

        # Run the histology-to-volume optimisation
        # h2v.run(cnf)
        with open(os.path.join(outdir, "h2v_config.conf"), "w") as fp:
            json.dump(cnf, fp, indent=4, sort_keys=True)


if __name__ == "__main__":
    main()
