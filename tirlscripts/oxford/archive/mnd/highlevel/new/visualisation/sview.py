#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Date: 7 February 2022

__version__ = "2022.2.1"


# DEPENDENCIES

import os
import re
import sys
import numpy as np
from glob import glob
from mayavi import mlab
from scipy.spatial import Delaunay
from tvtk.util.ctf import PiecewiseFunction
from tvtk.util.ctf import ColorTransferFunction

# TIRL IMPORTS

import tirl
from tirl.timage import TImage


# IMPLEMENTATION

def main(args):
    """ Main program code. """
    volume, label = args
    volume = TImage(volume)
    label = TImage(label)
    f = mlab.figure(size=(2000, 2000))
    visualise_transparent_brain(f, volume)
    visualise_labels(f, label)
    set_view(f)
    f.traits_init()
    mlab.show()


def set_view(f):
    # f = mlab.gcf()
    f.scene.show_axes = True
    f.scene.camera.position = \
        [1340.910698864792, -302.82426031464166, 856.847119324887]
    f.scene.camera.focal_point = [288.5, 392.5, 224.5]
    f.scene.camera.view_angle = 30.0
    f.scene.camera.view_up = [
        -0.3081333414548893, 0.34123163052160227, 0.8880398742260471]
    f.scene.camera.clipping_range = [386.907723624564, 2703.5133695365203]
    f.scene.camera.compute_view_plane_normal()
    f.scene.render()


def visualise_transparent_brain(f, volfiles):

    volfiles = (volfiles,) if not isinstance(volfiles, (tuple, list)) \
        else volfiles
    for volfile in volfiles:
        if isinstance(volfile, TImage):
            vol = volfile
        else:
            if not os.path.isfile(volfile):
                raise FileNotFoundError(f"File not found: {volfile}")
            if volfile.lower().endswith(".timg"):
                vol = tirl.load(volfile)
            else:
                vol = TImage(volfile)

        # Intensity normalisation for GPU
        ddata = discretise(vol.data, drange=255)
        dvol = mlab.pipeline.volume(
            mlab.pipeline.scalar_field(ddata, figure=f), figure=f)

        # Make the plot grayscale
        ctf = ColorTransferFunction()
        ctf.range = [0, 1]
        for gray_v in range(256):
            ctf.add_rgb_point(gray_v, gray_v/255, gray_v/255, gray_v/255)
        dvol._volume_property.set_color(ctf)
        dvol._ctf = ctf
        dvol.update_ctf = True

        # Set opacity
        otf = PiecewiseFunction()
        otf.add_point(0, 0)
        otf.add_point(255, 0.02)
        dvol._otf = otf
        dvol._volume_property.set_scalar_opacity(otf)


def visualise_labels(f, volfiles):

    volfiles = (volfiles,) if not isinstance(volfiles, (tuple, list)) \
        else volfiles
    for volfile in volfiles:
        if isinstance(volfile, TImage):
            vol = volfile
        else:
            if not os.path.isfile(volfile):
                raise FileNotFoundError(f"File not found: {volfile}")
            if volfile.lower().endswith(".timg"):
                vol = tirl.load(volfile)
            else:
                vol = TImage(volfile)

        # Intensity normalisation for GPU
        ddata = discretise(vol.data, drange=255)
        dvol = mlab.pipeline.volume(
            mlab.pipeline.scalar_field(ddata, figure=f), figure=f)

        # Make the plot grayscale
        ctf = ColorTransferFunction()
        ctf.range = [0, 1]
        for gray_v in range(256):
            ctf.add_rgb_point(gray_v, gray_v/255, gray_v/255, gray_v/255)
        dvol._volume_property.set_color(ctf)
        dvol._ctf = ctf
        dvol.update_ctf = True

        # Set opacity
        otf = PiecewiseFunction()
        otf.add_point(0, 0)
        otf.add_point(255, 1)
        dvol._otf = otf
        dvol._volume_property.set_scalar_opacity(otf)


def discretise(data, drange=255):
    amin = data.min()
    amax = data.max()
    # return np.round(data / amax * drange).astype(np.uint8)
    return np.round((data - amin) / (amax - amin) * drange).astype(np.uint8)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1:])
    else:
        print("Usage: ./view3d.py <volume> <label>")
