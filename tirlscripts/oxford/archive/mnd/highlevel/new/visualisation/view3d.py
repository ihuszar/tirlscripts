#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2020 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Date: 7 February 2022

__version__ = "2022.2.1"


# DEPENDENCIES

import os
import re
import sys
import numpy as np
from glob import glob
from mayavi import mlab
from scipy.spatial import Delaunay
from tvtk.util.ctf import PiecewiseFunction
from tvtk.util.ctf import ColorTransferFunction

# TIRL IMPORTS

import tirl
from tirl.timage import TImage


# DEFINITIONS

ROOT = "/mnt/nvme/mnd-reduced"


# IMPLEMENTATION

def main(subjects):
    """ Main program code. """

    for subject in subjects:
        print(subject)
        f = mlab.figure(size=(2000, 2000))
        volume, histology_images = get_data(subject)
        create_scene(f, volume, histology_images)
        set_view(f)
        export(f, subject)


def set_view(f):
    # f = mlab.gcf()
    f.scene.show_axes = True
    f.scene.camera.position = \
        [1340.910698864792, -302.82426031464166, 856.847119324887]
    f.scene.camera.focal_point = [288.5, 392.5, 224.5]
    f.scene.camera.view_angle = 30.0
    f.scene.camera.view_up = [
        -0.3081333414548893, 0.34123163052160227, 0.8880398742260471]
    f.scene.camera.clipping_range = [386.907723624564, 2703.5133695365203]
    f.scene.camera.compute_view_plane_normal()
    # from tvtk.pyface import light_manager
    # f.scene.light_manager = light_manager.LightManager(f)
    # f.scene.light_manager.light_mode = "raymond"
    f.scene.render()


def export(f, subject):
    path = os.path.join(ROOT, subject, "3Dview.png")
    f.traits_init()
    mlab.show()
    # mlab.savefig(path, figure=f)


def get_data(subject):

    # Load TRUFI volume
    volfile = os.path.join(ROOT, subject, "mri", "TRUFI", "trufi_comb.nii.gz")
    volume = TImage(volfile)  # TRUFI voxel space

    # Pair PLP histology data with PLP-to-TRUFI chain
    timgs = sorted(glob(
        os.path.join(ROOT, subject, "blocks", "*",
        f"sub-{subject}_block-*_mod-TRUFI_res-8_ipol-lin_toPLP.timg")
    ))
    pngs = []
    for timg in timgs:
        bdir = os.path.dirname(timg)
        fname = os.path.basename(timg)
        subject = re.search(r"(?<=sub-).+?(?=_)", fname).group(0)
        block = re.search(r"(?<=block-).+?(?=_)", fname).group(0)
        resolution = re.search(r"(?<=res-).+?(?=_)", fname).group(0)
        plp = os.path.join(
            bdir, f"sub-{subject}_block-{block}_stain-PLP_res-{resolution}.png")
        pngs.append(plp)

    # Create histology images
    histology_images = []
    for timg, plp in zip(timgs, pngs):
        domain = tirl.load(timg).domain
        img = TImage(plp, domain=domain)
        histology_images.append(img)

    return volume, histology_images


def create_scene(f, volume, histology):

    visualise_transparent_brain(f, volume)
    visualise_registered_histology(f, histology)


def visualise_transparent_brain(f, volfiles):

    volfiles = (volfiles,) if not isinstance(volfiles, (tuple, list)) \
        else volfiles
    for volfile in volfiles:
        if isinstance(volfile, TImage):
            vol = volfile
        else:
            if not os.path.isfile(volfile):
                raise FileNotFoundError(f"File not found: {volfile}")
            if volfile.lower().endswith(".timg"):
                vol = tirl.load(volfile)
            else:
                vol = TImage(volfile)

        # Intensity normalisation for GPU
        ddata = discretise(vol.data, drange=255)
        dvol = mlab.pipeline.volume(
            mlab.pipeline.scalar_field(ddata, figure=f), figure=f)

        # Make the plot grayscale
        ctf = ColorTransferFunction()
        ctf.range = [0, 1]
        for gray_v in range(256):
            ctf.add_rgb_point(gray_v, gray_v/255, gray_v/255, gray_v/255)
        dvol._volume_property.set_color(ctf)
        dvol._ctf = ctf
        dvol.update_ctf = True

        # Set opacity
        otf = PiecewiseFunction()
        otf.add_point(0, 0)
        otf.add_point(255, 0.02)
        dvol._otf = otf
        dvol._volume_property.set_scalar_opacity(otf)


def visualise_registered_histology(f, imfiles):
    """ Plots registered domains with histology textures. """

    imfiles = (imfiles,) if not isinstance(imfiles, (tuple, list)) else imfiles
    for imfile in imfiles:

        # Downsample to 100 um/px resolution
        if isinstance(imfile, str):
            img = tirl.load(imfile)
        elif isinstance(imfile, TImage):
            img = imfile
        factor = img.resolution[0] / 0.100
        img.rescale(factor, copy=False)

        # Create RGB textured mesh
        colors = np.c_[
            img.data.reshape(-1, 3),
            255 * np.ones(img.numel, dtype=np.uint8)]
        s = np.arange(img.numel)
        tri = Delaunay(img.domain.get_voxel_coordinates())
        points = img.domain.get_physical_coordinates()
        tmesh = mlab.triangular_mesh(
            *points.T, tri.simplices, scalars=s, opacity=1, figure=f)
        tmesh.module_manager.scalar_lut_manager.lut.number_of_colors = len(s)
        tmesh.module_manager.scalar_lut_manager.lut.table = colors
        # Remove the default Mayavi lighting
        tmesh.actor.property.lighting = False


def discretise(data, drange=255):
    amin = data.min()
    amax = data.max()
    # return np.round(data / amax * drange).astype(np.uint8)
    return np.round((data - amin) / (amax - amin) * drange).astype(np.uint8)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1:])
    else:
        print("Usage: ./view3d.py <subject> [block block ...]")
