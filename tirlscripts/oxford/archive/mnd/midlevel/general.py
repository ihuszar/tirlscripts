#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2021 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Date: 21 June 2020


# SHBASECOPYRIGHT


# DEPENDENCIES

import logging
import platform
from attrdict import AttrMap


# TIRL IMPORTS

from tirl import utils as tu
from tirl import settings as ts
import scripts.oxdbb.pathology.midlevel


# DEFINITIONS

def get_platform():
    pfm = platform.platform().lower()
    if pfm.startswith("linux"):
        return "linux"
    elif pfm.startswith(("darwin", "macos")):
        return "macos"
    else:
        raise OSError(f"Unsupported operating system: {pfm}")


DEFAULT_INITIALISER_MODULE = {
    "name": f"tirlscript + {tu.timehash()}",
    "system": get_platform(),
    "loglevel": "debug",
    "logfile": None,
    "paramlogfile": None,
    "verbose": False,
    "outputdir": ts.TWD,
    "warnings": True
}
SNAPSHOT_EXT = "png"


# IMPLEMENTATION

def initialise_script(**cnf):
    """
    Prepares the input/output dependencies of the script that is to be run.

    1. Disables warnings (if requested)
    2. Creates the log file and initialises the logger
    3. Creates the output directory if it does not yet exist

    :returns: script-wide attribute map
    :rtype: AttrMap

    """
    # Initialise module parameters
    assert isinstance(cnf, dict)
    p = AttrMap(cnf)
    q = AttrMap(cnf.get("general", DEFAULT_INITIALISER_MODULE.copy()))

    # Reset arguments which cannot be None or empty
    if q.system is None:
        q.system = get_platform()
    if not q.name:
        q.name = f"tirlscript + {tu.timehash()}"

    # Warnings
    if not q.warnings:
        import warnings
        warnings.filterwarnings("ignore")

    # Create output directory if it does not exist yet
    import os
    if not os.path.isdir(q.outputdir):
        os.makedirs(q.outputdir)

    # Create logfile (defaults to the output directory)
    if q.logfile is None:
        q.logfile = os.path.join(q.outputdir, "logfile.log")
    # Update loglevel information in the configurations
    q.loglevel = loglevel(q.loglevel)

    # Use existing logger instance (if provided)
    try:
        if isinstance(p.logger, logging.Logger):
            logger = p.logger

    # Create logger instance if there is no logger in the configurations
    except AttributeError:
        # We set the FileHandlers to be in "append" mode, as multiple stages
        # will create logs after each other, and they should not overwrite
        # previous logs. However, we delete any existing log file to avoid
        # confusion between runs.
        if os.path.isfile(q.logfile):
            os.remove(q.logfile)
        if isinstance(q.paramlogfile, str) and os.path.isfile(q.paramlogfile):
            os.remove(q.paramlogfile)
        logger = create_logger(cnf, mode="a")

    # Configure auxiliary module loggers
    for m in ("inout", "image"):
        name = f"scripts.oxdbb.pathology.midlevel.{m}"
        mlogger = logging.getLogger(name)
        mlogger.handlers = []
        configure_logger(name, q.loglevel, q.logfile, q.verbose, mode="a")

    import sys
    logger.critical(
        f"The program started with the command: {' '.join(sys.argv)}")

    # Update configuration dictionary
    cnf["general"].update(dict(q))

    # Create a local copy of the configuration file in the output directory
    import json
    with open(os.path.join(q.outputdir, "configuration.json"), "w") as fp:
        json.dump(cnf, fp, sort_keys=True, indent=4)

    return AttrMap(cnf), logger


def create_logger(cnf, mode="a"):
    """
    Creates a suitably configured Logger instance based on the configuration
    file of a tirl script. This function is useful when identical logging
    configurations must be set up for multiple processes.

    :param cnf: all configuration options
    :type cnf: dict or AttrMap
    :param mode:
        If "a" (default), new logs will be appended to existing log files or
        new log files will be created if they do not yet exist. Use "w" to
        overwrite exisiting log files.
    :type mode: str
    :returns:
        Logger instance configured to generate the output as required by the
        configurations.
    :rtype: Logger

    """
    q = AttrMap(cnf["general"])

    logger = logging.getLogger(q.name)
    logger.handlers = []  # delete any existing handlers to avoid duplicate logs
    logger.setLevel(1)
    formatter = logging.Formatter(
        fmt="%(asctime)s Process-%(process)d %(levelname)s (%(lineno)d) "
            "- %(message)s",
        datefmt='[%Y-%m-%d %H:%M:%S]')

    # Redirect all logs of interest to the specified logfile
    fh = logging.FileHandler(q.logfile, mode=mode, delay=False)
    fh.setLevel(q.loglevel)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Redirect only warnings/errors to the standard output, unless "verbose"
    ch = logging.StreamHandler()
    if q.verbose:
        ch.setLevel(1)
    else:
        ch.setLevel(logging.WARNING)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    # Redirect parameter updates to the root logger
    if q.paramlogfile is None:
        return logger

    # Redirect parameter update logs to the standard output (terminal)
    elif q.paramlogfile == "stdout":
        ph = logging.StreamHandler()
        ph.setFormatter(logging.Formatter(fmt="%(message)s"))
        ph.setLevel(1)
        pf = logging.Filter()
        pf.filter = lambda \
            record: record.levelno == ts.PARAMETER_UPDATE_LOG_LEVEL
        ph.addFilter(pf)
        logger.addHandler(ph)

    # Redirect parameter update logs to the parameter log file
    else:
        ph = logging.FileHandler(q.paramlogfile, mode=mode, delay=False)
        ph.setFormatter(logging.Formatter(fmt="%(message)s"))
        ph.setLevel(1)
        pf = logging.Filter()
        pf.filter = \
            lambda record: record.levelno == ts.PARAMETER_UPDATE_LOG_LEVEL
        ph.addFilter(pf)
        logger.addHandler(ph)

    return logger


def configure_logger(name, level, logfile, verbose=False, mode="a"):
    """
    Creates a suitably configured Logger instance based on the configuration
    file of a tirl script.

    :param name: name of the logger instance
    :type name: str
    :param name: name of the log file
    :type name: str
    :param level: log level
    :type level: int
    :param mode:
        If "a" (default), new logs will be appended to existing log files or
        new log files will be created if they do not yet exist. Use "w" to
        overwrite existing log files.
    :type mode: str
    :returns:
        Logger instance configured to generate the output as required by the
        configurations.
    :rtype: Logger

    """
    logger = logging.getLogger(name)
    logger.handlers = []  # delete any existing handlers to avoid duplicate logs
    logger.setLevel(1)
    formatter = logging.Formatter(
        fmt='%(asctime)s Process-%(process)d %(levelname)s (%(lineno)d) '
            '- %(message)s',
        datefmt='[%Y-%m-%d %H:%M:%S]')

    # Redirect all logs of interest to the specified logfile
    fh = logging.FileHandler(logfile, mode=mode, delay=False)
    fh.setLevel(level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Redirect only warnings/errors to the standard output, unless "verbose"
    ch = logging.StreamHandler()
    if verbose:
        ch.setLevel(1)
    else:
        ch.setLevel(logging.WARNING)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


def loglevel(level):
    """
    Translated verbosity level from the configuration file to the logging
    utility.

    :param level: verbosity level as defined in the configuration file
    :type level: Union[str, int]

    :returns: logger-compatible verbosity level
    :rtype: int

    """
    if isinstance(level, int):
        return level
    elif isinstance(level, str):
        if level.lower() == "all":
            return 1
        else:
            return getattr(logging, level.upper())
    else:
        raise AttributeError(f"Invalid loglevel: {str(level)}.")


def run_stage(target, source, stage_no, counter, scope, config):
    """
    Runs a registration stage specified by its number.

    """
    import os
    from time import time
    p = AttrMap(config)

    logger = logging.getLogger(p.general.name)
    logger.info(f"STAGE {stage_no}...")
    st = time()
    func = f"stage{stage_no}"
    q = AttrMap(p.regparams.get(f"stage_{stage_no}"))
    stage_cost = scope.get(func)(target, source, p)  # does the job
    logger.info(f"STAGE {stage_no} cost: {stage_cost}")
    logger.info(f"STAGE {stage_no} runtime: {time() - st} seconds")

    # Export stage result as TImage
    ext = ts.EXTENSIONS["TImage"]
    scripts.oxdbb.pathology.midlevel.inout.export(
        target, q.export.timage, default=os.path.join(
            p.general.outputdir, f"{counter}_stage{stage_no}.{ext}"))

    # Create snapshot showing end-stage alignment
    snpfile = os.path.join(
        p.general.outputdir, f"{counter}_stage{stage_no}.{SNAPSHOT_EXT}")
    scripts.oxdbb.pathology.midlevel.inout.snapshot(
        source.evaluate(target.domain, rule=None), q.export.snapshot,
        default=snpfile)

    # Save mask if requested
    if target.tmask() is not None:
        from tirl.timage import TImage
        mask = TImage(target.tmask())
        targetmaskfile = os.path.join(
            p.general.outputdir,
            f"{counter}_stage{stage_no}_targetmask.{SNAPSHOT_EXT}")
        scripts.oxdbb.pathology.midlevel.inout.snapshot(
            mask, q.export.target_mask, default=targetmaskfile)
    if source.tmask() is not None:
        from tirl.timage import TImage
        mask = TImage(target.tmask())
        sourcemaskfile = os.path.join(
            p.general.outputdir,
            f"{counter}_stage{stage_no}_sourcemask.{SNAPSHOT_EXT}")
        scripts.oxdbb.pathology.midlevel.inout.snapshot(
            mask, q.export.source_mask, default=sourcemaskfile)

    # Visualise end-stage alignment
    if q.visualise:
        source_resampled = source.evaluate(target.domain, rule=None)
        source_resampled.preview()
        source_resampled.normalise()
        difference = source_resampled - target / target.max()
        difference.preview()


# The following have been replaced by threadpoolctl that provides a context
# manager for limiting the number of threads locally.

def set_single_threading():
    """
    Set environment variables that force single-threaded computations
    in NumPy. This is to avoid the conflict between multiple processes
    performing computations at the same time.

    Source: https://stackoverflow.com/questions/15414027/
            multiprocessing-pool-makes-numpy-matrix-multiplication-slower

    """
    import os
    os.environ["OMP_NUM_THREADS"] = "1"
    os.environ["MKL_NUM_THREADS"] = "1"
    os.environ["OPENBLAS_NUM_THREADS"] = "1"
    os.environ["VECLIB_MAXIMUM_THREADS"] = "1"
    os.environ["NUMEXPR_NUM_THREADS"] = "1"


def set_multi_threading():
    """
    Unsets environment variables to allow multi-threaded computations in NumPy.

    """
    import os
    os.environ["OMP_NUM_THREADS"] = ""
    os.environ["MKL_NUM_THREADS"] = ""
    os.environ["OPENBLAS_NUM_THREADS"] = ""
    os.environ["VECLIB_MAXIMUM_THREADS"] = ""
    os.environ["NUMEXPR_NUM_THREADS"] = ""


# def get_parameter_logger(cnf):
#     """
#     Returns a Logger instance with a FileHandler that points to the
#     parameter log file. If the parameter log file is not specified, the
#     function returns None, which will redirect logs to the RootLogger.
#
#     This function is useful when the parameters are calculated by a process
#     that is different from the one that defined the main Logger instance.
#
#     """
#     p = AttrMap(cnf)
#     if not p.general.paramlogfile:
#         return None
#     logger = logging.getLogger(p.general.name)
#     ph = logging.FileHandler(p.general.paramlogfile, mode="a", delay=False)
#     ph.setFormatter(logging.Formatter(fmt="%(message)s"))
#     ph.setLevel(1)
#     pf = logging.Filter()
#     pf.filter = lambda record: record.levelno == ts.PARAMETER_UPDATE_LOG_LEVEL
#     ph.addFilter(pf)
#     logger.addHandler(ph)
#     return logger
