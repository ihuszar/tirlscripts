#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2021 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Date: 9 June 2020


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import sys
import argparse
import numpy as np
from tirl.timage import TImage
from skimage.color import rgb2hsv
from skimage.filters import threshold_otsu
from skimage.measure import label, regionprops


# DEFINITIONS

OPTIONS = {
    "distance": 0.25,
    "channel": 0
}


# IMPLEMENTATION

def crop_centre(args):
    """ Main program code. """

    img = TImage(args.image).tensors[:3]
    img.rescale(args.scale, copy=False)
    hue = rgb2hsv(img.data)[..., OPTIONS["channel"]]
    binary = np.where(hue < threshold_otsu(hue), 255, 0)
    labelimg = label(binary, background=0)
    regions = regionprops(labelimg, hue)
    mask = np.zeros_like(binary)

    # Select pixels that belong to the ROI
    for region in regions:
        if region.label == 0:
            continue
        radius = np.mean(np.multiply(OPTIONS["distance"], img.vshape))
        centre = np.divide(img.vshape, 2)
        dist = np.linalg.norm(np.subtract(centre, region.centroid))
        if dist < radius:
            mask[labelimg == region.label] = 1

    # Get bounding box from binary mask
    mask[mask > 0.5] = 1
    mask[mask <= 0.5] = 0

    # TImage(mask).preview()

    # Determine minimum bounding box
    minrow = np.inf
    mincol = np.inf
    maxrow = -np.inf
    maxcol = -np.inf
    for region in regionprops(label(mask, background=0)):
        minr, minc, maxr, maxc = region.bbox
        minrow = int(min(minrow, minr))
        mincol = int(min(mincol, minc))
        maxrow = int(max(maxrow, maxr))
        maxcol = int(max(maxcol, maxc))

    # Crop with margin
    img.rescale(1, copy=False)
    h, w = img.vshape
    minrow /= args.scale
    mincol /= args.scale
    maxrow /= args.scale
    maxcol /= args.scale
    height = maxrow - minrow
    width = maxcol - mincol
    left, right, top, bottom = args.margin
    left = int(width * left)
    right = int(width * right)
    top = int(height * top)
    bottom = int(height * bottom)
    minrow = int(max(minrow - top, 0))
    maxrow = int(min(maxrow + bottom, h))
    mincol = int(max(mincol - left, 0))
    maxcol = int(min(maxcol + right, w))
    imdata = img.data
    img = TImage(imdata[minrow:maxrow, mincol:maxcol], taxes=(2,))

    # Generate file name
    fname, ext = os.path.splitext(args.image)
    fname = fname.rstrip(".") + "_crop" + f".{ext}"
    
    # Flip image if needed
    if args.flip:
        fname = fname.replace("_crop", "_crop_flip")
        img.data[...] = img.data[:, ::-1]

    # Save file
    img.snapshot(fname, overwrite=True)


def create_cli():
    """
    Creates command-line interface.

    """
    descr = "Segments brain slice from dissection photograph."
    parser = argparse.ArgumentParser(prog="crop_centre", description=descr)

    parser.add_argument("-i", "--image", metavar="file", type=str, default="",
                        required=True, help="Input image.")
    parser.add_argument("-s", "--scale", metavar="factor", type=float,
                        default=0.25, required=False, help="Scaling factor.")
    parser.add_argument("-m", "--margin", type=float, nargs=4,
                        default=[0.1, 0.1, 0.1, 0.1], required=False,
                        help="Relative margins (left, right, top, bottom).")
    parser.add_argument("--flip", action="store_true", default=False,
                        required=False, help="Flip input horizontally.")

    return parser


def main(*args):
    """ Main program code. """

    parser = create_cli()
    if args:
        crop_centre(parser.parse_args(args))
    else:
        parser.print_help()


if __name__ == "__main__":
    main(*sys.argv[1:])
