# Changelog

### 28 Mar 2023 -- v3.2

- warn about missing inputs, but do not halt the registration


### 6 Nov 2022 -- v3.1

- only orientation constraints are used in first-pass registration 


### 5 Nov 2022 -- v3.0

- completely redesigned the multislice algorithm (using soft constraints)


### 1 Nov 2022 -- v2.3

- built-in checkpoint to prevent detrimental 3D optimisation


### 30 Oct 2022 -- v2.2.3

- mask resolution in pyramids is now correctly set


### 29 Oct 2022 -- v2.2.2

- loading files in HDD mode at start
- minimised memory impact for registering all slices
- added debugging mode and more detailed log messages


### 20 Oct 2022 -- v2.2.1

- Snapshot resolution is now configurable


### 15 Oct 2022 -- v2.2.0

- Bugfix: slice resolutions are now imported accurately
- Added anisotropic scale optimisation to the 2D rigid registration step to optimise slice resolution
- Bugfix in multislice_to_volume.py: existing output directory no longer throws an exception
- Bugfix: warp.py no longer raises an exception if a slice has no mask


### 14 Oct 2022 -- v2.1.0

- Added: logger instance now records the SHA1 hash of the executing script for strict version control
- rigid2d.py: bugfixes in defining resolutions for multi-resolution registration
- rigid3d.py: bugfixes in defining resolutions for multi-resolution registration
- warp.py: bugfixes in defining resolutions for multi-resolution registration


### 13 Oct 2022 -- v2.0.0

- Multi-resolution registration is now defined by target resolutions instead of scaling factors. (see "resolution" instead of "scaling" in the configuration file)


### 11 Oct 2022 -- v1.0.0

- Project-specific version v1.0.0 assigned to the current state of the library
