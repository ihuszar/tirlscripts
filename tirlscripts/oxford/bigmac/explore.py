#!/usr/bin/env python

import os
import tirl
import pandas as pd


def main():
	""" Main program code. """

	csvfile = "/home/inhuszar/BigMac/Microscopy/Combine/Posterior/input_table_combine_posterior.csv"
	table = pd.read_csv(csvfile)
	targetdir = "/home/inhuszar/BigMac/Microscopy/Combine/Posterior/registration"

	if not os.path.exists(targetdir):
		os.makedirs(targetdir)

	for ix, record in table.iterrows():
		slice_no = int(record.Slice)
		img = tirl.load(record.ImageFile)
		img.resample(0.037875, copy=False)
		slicedir = os.path.join(targetdir, f"{slice_no:04d}")
		fpath, fname = os.path.split(record.ImageFile)
		fname, ext = os.path.splitext(fname)
		if not os.path.exists(slicedir):
			os.makedirs(slicedir)
		sname = f"slice-{slice_no:04d}_{fname}.png"
		sfile = os.path.join(slicedir, sname)
		img.snapshot(sfile, overwrite=True)
		print(f"Saved: {sfile}")




if __name__ == "__main__":
	main()
