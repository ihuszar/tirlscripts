#!/usr/bin/env python

import re
import os
import dill
import tirl
import numpy as np
from glob import glob
import seaborn as sns
import matplotlib.pyplot as plt


REGDIR = "/home/inhuszar/BigMac/Microscopy/Combine/Anterior/registration"
EXCLUDE = (193, 207, 200, 473, 485, 596, 606)


def measure():
    """ Measure slice registration errors for Stage 1 """

    results = []
    print(("Slice No.", "Part"), "Median [mm]", "Mean [mm]")
    for slicedir in sorted(glob(os.path.join(REGDIR, "????"))):
        slice_no = int(re.search(r"\d{4}", slicedir).group(0))
        if slice_no in EXCLUDE:
            continue
        for imfile in sorted(glob(os.path.join(slicedir, "slice-*.timg"))):
            image = tirl.load(imfile)
            capture = image.header["metadata"]["Capture"]
            part = image.header["metadata"]["Part"]
            stain = image.header["metadata"]["Stain"]
            ap = image.header["metadata"]["AP"]
            pyrimfile = glob(os.path.join(
                f"/home/inhuszar/BigMac/Microscopy/{stain}/{ap}"
                f"/PyramidalTImages/{capture}-{part}-*.timg"
            ))[0]
            image = tirl.load(pyrimfile)
            dname = f"slice-{slice_no:04d}_part-{part}_tx-trans3d_domain.dom"
            final = tirl.load(os.path.join(slicedir, dname))
            image = image.resize(*final.shape, copy=True)
            init = image.domain
            if image.mask is not None:
                mask = image.mask.ravel() > 0.5
            else:
                mask = Ellipsis
            ipc = init.get_physical_coordinates()[mask, :]
            fpc = final.get_physical_coordinates()[mask, :]
            diff = np.linalg.norm(np.subtract(fpc, ipc), axis=-1).ravel()
            results.append(((slice_no, part, stain), diff))
            print((slice_no, part, stain), np.median(diff), np.mean(diff))
    else:
        statfile = os.path.join(
            REGDIR, "stats", "stage1_accuracies_per_slice.obj")
        with open(statfile, "wb") as fp:
            dill.dump(results, fp)


def visualise():
    """ Plots the measurement results. """

    sns.set_context("paper")
    sns.set(font_scale=2)
    f = "/home/inhuszar/BigMac/Microscopy/Combine/Anterior/registration/" \
        "stats/stage1_accuracies_per_slice.obj"
    with open(f, "rb") as fp:
        results = dill.load(fp)
    for stain in ("Cresyl", "Gallyas", "PLI"):
        batch = filter(lambda t: t[0][2].lower() == stain.lower(), results)
        x, data = zip(*batch)
        stack = np.concatenate(data)
        avg = np.mean(stack)
        stderr = np.std(stack) / np.sqrt(stack.size)
        print(avg, stderr, 3 * stderr)
        boxplot = sns.boxplot(data=data, showfliers = False)
        boxplot.set(xlabel="Slice No.", ylabel="Initialisation error [mm]")
        boxplot.set_xticks([0, len(x)], [x[0][0], x[-1][0]])
        plt.show()


def main():
    """ Main program code. """

    # measure()
    visualise()


if __name__ == "__main__":
    main()

