#!/usr/bin/env python


import os
import sys
import tirl
import yaml
from glob import glob


def main(regdir):
    """ Main program code. """

    for slicedir in sorted(glob(os.path.join(regdir, f"????"))):
        for imfile in sorted(glob(os.path.join(slicedir, "*.timg"))):
            img = tirl.load(imfile)
            metadata = img.header.get("metadata", dict())
            fname = os.path.basename(imfile)
            fname, ext = os.path.splitext(fname)
            outfile = os.path.join(slicedir, fname + "_metadata.yml")
            with open(outfile, "w") as fp:
                yaml.safe_dump(metadata, fp, sort_keys=False, indent=2,
                               default_flow_style=False)
            print(f"Saved: {outfile}")


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        print("Usage: metadata.py <regdir>")
