#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

"""
bigmac v3 --

Registers serial microscopy sections to MRI using direct slice-to-volume
registration with regularity constraints.

"""


__tirlscript__ = "bigmac3.ms2v"


# DEPENDENCIES

import re
import os
import sys
import dill
import yaml
import logging
import argparse
import subprocess
import numpy as np
import pandas as pd
import multiprocessing as mp
from attrdict import AttrMap
from functools import partial
from types import SimpleNamespace
from scipy.stats import linregress


# TIRL IMPORTS

import tirl
import tirl.utils as tu
import tirl.settings as ts
from tirl.constants import *
from tirl.chain import Chain
from tirl.tfield import TField
from tirl.timage import TImage
from tirl.transformations.scale import TxScale
from tirl.transformations.embedding import TxEmbed
from tirl.transformations.euler import TxEulerAngles
from tirl.transformations.identity import TxIdentity
from tirl.transformations.rotation import TxRotation2D
from tirl.transformations.rotation import TxRotation3D
from tirl.transformations.axisangle import TxAxisAngle
from tirl.transformations.translation import TxTranslation


# TIRLSCRIPTS IMPORTS

from tirlscripts import srchash
from tirlscripts.oxford.bigmac import __version__


# DEFINITIONS

LOGGER = "bigmac3.ms2v"
Z0 = [0, 0, 1]

STAGES = SimpleNamespace(**{
    "rigid3d": dict(
        script="rigid3d.py", tag="rigid3d",
        descr="3D rigid registration"),
    "trans3d": dict(
        script="trans3d.py", tag="trans3d",
        descr="3D translation optimisation"),
    "warp"   : dict(
        script="warp.py", tag="warp",
        descr="non-linear registration")
})
DEFAULT_QUEUE = "short.q"


# IMPLEMENTATION


class BigMacChain(Chain):

    def __int__(self, *transformations):
        names = ["centralise", "scale2d", "rot2d", "embed", "rot3d", "trans3d"]
        if not transformations:
            transformations = (
                TxTranslation(0, 0, name="centralise"),
                TxScale(1, 1, name="scale2d"),
                TxRotation2D(0, mode="deg", name="rot2d"),
                TxEmbed(1, name="embed"),
                TxEulerAngles(0, 0, 0, order="zyx", mode="deg", name="rot3d"),
                TxTranslation(0, 0, 0, name="trans3d")
            )
        if len(transformations) != len(names):
            raise ValueError(f"Expected {len(names)} transformations "
                             f"({names}), got {len(transformations)}")
        super(BigMacChain, self).__init__(*transformations)

        # Set transformations as named attributes
        for name, tx in zip(names, transformations):
            setattr(self, name, tx)


def main():
    """ Main program code. """

    # Prepare output directory
    prepare_output_dir()

    # Copy the input configurations to a file in the target directory
    with open(os.path.join(cnf["outputdir"], "configuration.yml"), "w") as fp:
        yaml.safe_dump(cnf, fp, sort_keys=False, indent=2,
                       default_flow_style=None)

    cmd = " ".join(sys.argv)
    logger.info(f"The program started with the command: {cmd}")

    # Logging detailed version information
    logger.debug(f"tirlscripts bigmac library version: {__version__}")
    logger.debug(f"multislice_to_volume.py SHA1: {srchash(__file__)}")
    scriptdir = os.path.dirname(__file__)
    r3py = STAGES.rigid3d["script"]
    t3py = STAGES.trans3d["script"]
    wpy = STAGES.warp["script"]
    logger.debug(f"{r3py} SHA1: {srchash(os.path.join(scriptdir, r3py))}")
    logger.debug(f"{t3py} SHA1: {srchash(os.path.join(scriptdir, t3py))}")
    logger.debug(f"{wpy} SHA1: {srchash(os.path.join(scriptdir, wpy))}")

    # Input data
    inputtable = pd.read_csv(cnf["slices"])
    images = load_images(inputtable)

    # Disable slice masks if desired
    if cnf["use-slice-masks"] is False:
        for image in images:
            image.mask = None

    # Save reference image
    volume = TImage(cnf["volume"], external="affine", name="mri", storage=MEM)
    if cnf["vmask"] is not None:
        volume.mask = TField(cnf["vmask"], storage=MEM)
    refdir = os.path.join(cnf["outputdir"], "reference")
    if not os.path.exists(refdir):
        try:
            os.makedirs(refdir)
        except Exception as exc:
            logger.exception(
                f"ERROR while creating output directory: {refdir}")
            raise exc
    reference = os.path.join(refdir, "reference.timg")
    volume.save(reference, overwrite=True)
    volume.header["metadata"] = dict(filename=reference)
    logger.info(f"The 3D reference image was saved to {reference}")

    # Save snapshots of the components
    snapext = ts.TIMAGE_DEFAULT_SNAPSHOT_EXT
    for image in images:
        slice_no = image.header["metadata"]["Slice"]
        part = image.header["metadata"]["Part"]
        slicedir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")
        if not os.path.exists(slicedir):
            try:
                os.makedirs(slicedir)
            except Exception as exc:
                logger.exception(
                    f"ERROR while creating output directory: {slicedir}")
                raise exc
        fname = f"slice-{slice_no:04d}_part-{part}_microscopy.{snapext}"
        thumb = os.path.join(slicedir, fname)
        image.snapshot(thumb, overwrite=True)
        logger.info(f"Created snapshot of the microscopy image: {thumb}")
        fname = f"slice-{slice_no:04d}_part-{part}_init.{snapext}"
        initsnap = os.path.join(slicedir, fname)
        mri_init = volume.evaluate(image.domain)
        mri_init.snapshot(initsnap, overwrite=True)
        logger.info(f"Created snapshot of initial alignment: {initsnap}")

    errors = set()

    # Linear multislice optimisation
    try:
        linear_registration(images, volume, cnf, errors)
    except Exception as exc:
        logger.error(
            "The linear registration process terminated with an error.")
        raise exc

    # Save grating parameters
    # with open(os.path.join(cnf["outputdir"], "best_grating.txt"), "w") as fp:
    #     c_ix, domains = zip(*g.domains.items())
    #     chain = ChainTemplate(*domains[0].external)
    #     fp.write(f"Offset (0th slice, mm): {chain.g_trans.parameters[:]}\n")
    #     rotation_angles = chain.g_rot.euler(mode="deg", order="xyz")
    #     fp.write(f"Rotation (deg): {rotation_angles}\n")
    #     gap = np.abs(g.metadata[c_ix[0]]["gap"])  # mm
    #     stderr = np.abs(g.metadata[c_ix[0]]["stderr"])  # mm
    #     fp.write(f"Slicing gap (mm): {gap} ± {stderr}\n")
    #     rvalue = np.abs(g.metadata[c_ix[0]]["rvalue"])
    #     fp.write(f"r-value: {rvalue}")

    # In-plane warping
    try:
        run_stage(STAGES.warp, images, volume, cnf, errors=errors)
    except Exception as exc:
        logger.error("The warping process terminated with an error.")
        raise exc

    n_errors = len(errors)
    if n_errors == 0:
        logger.fatal("The multi-slice registration was successfully completed "
                     "for all input slices.")
    else:
        for (slice_no, part) in errors:
            logger.error(f"Slice {slice_no} Part {part} registration failed.")
        logger.fatal(f"Multi-slice registration complete. "
                     f"Number of slices excluded because of error: {n_errors}")


def prepare_output_dir():
    """
    Creates the full path to the output directory if it does not exist yet.
    If the output directory is not specified, the current directory will be
    used.

    """
    outdir = cnf.get("outputdir", os.path.join(os.getcwd(), "output"))
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    cnf["outputdir"] = outdir


def load_images(table):
    """
    Returns TImages with their respective slice indices.

    """
    logger.info("Loading inputs...")

    # Set primary resource
    resource = str(cnf["primary-resource"]).lower()
    if resource == "memory":
        resource = MEM
    elif resource == "hdd":
        resource = HDD
    else:
        raise ValueError(f"Unrecognised primary resource: {resource}")

    images = []
    for ix, record in table.iterrows():
        imfile = record.ImageFile
        try:
            img0 = tirl.load(imfile)
            logger.info(f"Loaded TImage from {imfile}: {img0}")
        except FileNotFoundError:
            logger.warning(f"File was not found: {imfile}")
            continue
        resolution = float(cnf["snapshot-resolution"])
        img = img0.resample(resolution, copy=True)
        del img0
        img.storage = resource
        img.header["metadata"]["Slice"] = int(record.Slice)
        logger.info(f"Set image resolution to {resolution} mm/px: {img}")
        if img.tsize > 1:
            for jx, layer in enumerate(img.resmgr.layers):
                img.resmgr.layers[jx] = layer.tensors.reduce()
            logger.info(f"Reduced image tensors: {img}")
        images.append(img)
    else:
        images = tuple(images)

    return images


def linear_registration(images, volume, cnf, errors=None):
    """
    Linear registration between the images and the volume.

    :param images: 2D input images
    :type images: Iterable[TImage]
    :param volume: target 3D volume
    :type volume: TImage
    :param cnf: configurations
    :type cnf: dict

    """
    n_images = len(images)
    errors = set() if errors is None else errors

    # Define the order of slice recruitment
    recruitment = str(cnf["regparams"]["recruitment"]).lower()

    # BEST: from the most trustworthy to the least trustworthy
    if recruitment == "best":
        images = sorted(images, key=lambda img:
            img.header["metadata"].get("PreRegistrationCost", np.inf))

    # RANDOM(seed): random permutation
    elif recruitment.startswith("random"):
        seed = re.findall(r"\d+", recruitment)
        seed = int(seed[0]) if seed is not None else 0
        images = tuple(np.random.RandomState(seed).permutation(images))

    elif recruitment == "sequential":
        pass

    else:
        raise ValueError(f"Unrecognised recruitment pattern: {recruitment}")

    # Main loop: iterative optimisation of slice orientation and position
    n_main = int(cnf["regparams"]["loops"])
    for loop in range(n_main):
        logger.info(f"Main loop {loop + 1}/{n_main}...")

        # Robust orientation estimation
        batch = int(min(cnf["regparams"].get("batchsize", n_images), n_images))
        n_iterations = int(np.ceil(n_images / batch))
        iteration = 0
        n = batch
        while True:
            iteration += 1
            logger.info(f"Robust orientation estimation "
                        f"{iteration}/{n_iterations}...")
            # Recruit slices
            logger.info(f"Recruiting {n} slices for registration...")
            stack = images[:n]
            logger.info("Estimating global parameters...")
            gparams = fit(stack)
            # Initialise transformations
            logger.info("Initialising slice chains...")
            initialise(stack, gparams)
            # 3D rigid optimisation
            logger.info("Starting 3D rigid optimisation...")
            run_stage(STAGES.rigid3d, stack, volume, cnf,
                      gparams=gparams, errors=errors)

            # Optimise slice positions with regularity constraint
            logger.info("Fine-tuning 3D slice positions...")
            gparams = fit(stack)
            initialise(stack, gparams)
            run_stage(STAGES.trans3d, stack, volume, cnf,
                      gparams=gparams, errors=errors)

            # Update the slice iterator
            if n < n_images:
                n = int(min(n + batch, n_images))
            else:
                break

    # Finalise with parallel slices and a constant interslice gap
    logger.info("Fine-tuning 3D slice positions...")
    gparams = fit(images)
    initialise(images, gparams)
    cnf["regparams"]["trans3d"]["reject"] = False
    cnf["regparams"]["trans3d"]["regularity"] = 10.0
    run_stage(STAGES.trans3d, images, volume, cnf,
              gparams=gparams, errors=errors)

    return errors


def fit(stack):
    """
    Estimates the slice gap, orientation, and offset of a regular grating that
    best matches the position of the slices in the specified stack.

    :param stack: Input slice stack.
    :type stack: Iterable[TImage]

    """
    vcentres = dict()
    centres = dict()
    normals = dict()
    weights = dict()

    for image in stack:
        slice_no = image.header["metadata"]["Slice"]
        part = image.header["metadata"]["Part"]
        c_ix = (slice_no, part)
        domain = image.domain

        # Calculate the normal vector of the current component
        normal = get_3D_rotation(domain).map(Z0)
        logger.log(5, f"Slice {slice_no} Part {part} normal: {normal}")
        normals.update({c_ix: normal / np.linalg.norm(normal)})

        # Calculate the weighted centre of the current component
        if image.mask is not None:
            vcog = image.mask.ravel() * domain.get_voxel_coordinates().T
            vcog = np.sum(vcog, axis=1).ravel()
            weight = np.sum(image.mask)
            vcog = vcog / weight
        else:
            vcog = domain.vcentre()
            weight = domain.numel

        vcentres.update({c_ix: vcog})
        centres.update({c_ix: domain.map_voxel_coordinates(vcog).ravel()})

        # Store component weights, which will be used to determine the
        # weighted centre of the slice that the components belong to.
        # Ignore this, because if the image resolution is not defined
        # correctly, this may lead to errors. If the multiplication by the
        # image resolution is omitted, we essentially assume that all
        # components are defined at the same resolution.
        # weights.update({c_ix: weight * image.resolution ** 2})
        weights.update({c_ix: weight})

    # Calculate weighted slice centres and weighted slice normals
    slices = sorted(set(img.header["metadata"]["Slice"] for img in stack))
    slice_normals = []
    slice_centres = []
    for slice_no in slices:
        components = [img for img in stack
                      if img.header["metadata"]["Slice"] == slice_no]
        wscentre = 0
        wtotal = 0
        wnormal = 0
        for cmp in components:
            c_ix = (cmp.header["metadata"]["Slice"],
                    cmp.header["metadata"]["Part"])
            wscentre = wscentre + weights[c_ix] * centres[c_ix]
            wnormal = wnormal + weights[c_ix] * normals[c_ix]
            wtotal = wtotal + weights[c_ix]
        else:
            wscentre = wscentre / wtotal
            wnormal = wnormal / wtotal
        slice_centres.append(wscentre)
        slice_normals.append(wnormal / np.linalg.norm(wnormal))

    # Create arrays
    slice_normals = np.vstack(slice_normals)
    slice_centres = np.vstack(slice_centres)

    # Robust orientation estimation by iteratively rejecting outliers
    select = np.full(shape=slice_normals.shape[0], fill_value=True)
    robust_normal = np.mean(slice_normals, axis=0).ravel()
    for i in range(cnf["regparams"]["outlier-iterations"]):
        robust_normal = np.mean(slice_normals[select], axis=0).ravel()
        robust_normal /= np.linalg.norm(robust_normal)
        logger.log(5, f"Outlier removal iteration {i + 1}, robust normal: "
                      f"{robust_normal}")
        diff = np.linalg.norm(slice_normals - robust_normal, axis=-1)
        select[diff > 2 * np.median(diff[select])] = False
    else:
        pass
        # fig = plt.figure()
        # ax = fig.add_subplot(projection='3d')
        # ax.scatter(*slice_normals.T, c="blue", marker="o")
        # ax.scatter(*robust_normal, c="red", marker="x")
        # plt.show()

    # Find the rotation angles corresponding to the robust normal
    angle = np.arccos(np.dot(Z0, robust_normal.reshape(-1, 1)))
    axis = np.cross(Z0, robust_normal)
    axis /= np.linalg.norm(axis)
    g_rotation = TxAxisAngle(angle, axis, mode="rad")
    euler_angles = g_rotation.euler(mode="deg", order="xyz")
    logger.info(f"Grating rotation (robust): {euler_angles} [deg]")

    # Centre of gravity
    # Slices should either carry an equal weight or be weighted by their
    # inverse registration cost to take trustworthiness into account.
    # Weighting by area is discouraged here as large but misaligned slices
    # could bias the result.
    cog = np.mean(slice_centres, axis=0)
    cog_pos = np.dot(robust_normal, np.asarray(cog).reshape(-1, 1))
    logger.info(f"Centre of gravity: {cog} [mm]")

    # Slicing gap
    y = [float(np.dot(robust_normal, c.reshape(-1, 1))) for c in slice_centres]
    result = linregress(slices, y)
    gap = result.slope
    stderr = result.stderr
    rvalue = result.rvalue
    residual = np.mean(np.subtract(y, np.multiply(slices, gap)) ** 2)

    # Grating offset = centre of the 0th slice
    offset = np.add(cog, np.subtract(result.intercept, cog_pos) * robust_normal)

    logger.info(f"Effective slice gap: {gap:.04} +/- {stderr:.04} mm")
    logger.info(f"Correlation coefficient: {rvalue:.04}")
    logger.info(f"Mean residual: {residual} mm^2")
    logger.info(f"0th slice centre: {offset} mm")

    return SimpleNamespace(offset=offset, normal=robust_normal, gap=gap)


def get_2D_rotation(domain):
    rotations = []
    for tx in domain.external:
        if isinstance(tx, TxRotation2D):
            rotations.append(tx)
    else:
        return Chain(rotations)


def get_3D_rotation(domain):
    rotations = []
    for tx in domain.external:
        if isinstance(tx, TxRotation3D):
            rotations.append(tx)
    else:
        return Chain(rotations)


def get_scaling(domain):
    vdim = domain.ndim
    origin = np.zeros(vdim)
    b1 = domain.external.map_vector(np.eye(vdim), np.tile(origin, (vdim, 1)))
    return np.linalg.norm(b1, axis=-1).ravel()


def initialise(stack, gparams=None):

    for image in stack:

        slice_no = image.header["metadata"]["Slice"]
        part = image.header["metadata"]["Part"]

        olddomain = image.domain.copy()
        embed, ix = olddomain.get_transformation("embed", index=True)

        # Set resolution
        image.resolution = \
            np.multiply(image.resolution, get_scaling(image.domain))

        # Set axis flip
        image.domain.external = []
        i_base = image.domain.map_voxel_vectors([1, 0], rule="fs")
        j_base = image.domain.map_voxel_vectors([0, 1], rule="fs")
        iprime_base = olddomain[:ix].map_voxel_vectors([1, 0], rule="fs")
        jprime_base = olddomain[:ix].map_voxel_vectors([0, 1], rule="fs")
        flip_i = float(np.sign(np.dot(i_base, iprime_base.T)).ravel())
        flip_j = float(np.sign(np.dot(j_base, jprime_base.T)).ravel())
        image.domain.external += [TxScale(flip_i, flip_j, name="scale2d")]

        # Set centre
        image.centralise()

        # Get rigid parameters
        old_base = olddomain.map_voxel_vectors(
            np.eye(3), np.tile(olddomain.vcentre(), (3, 1)), rule="fs")
        new_base = image.domain.map_voxel_vectors(
            np.eye(3), np.tile(image.domain.vcentre(), (3, 1)), rule="fs")
        if gparams is not None:
            slice_normal = old_base[2, :3]
            slice_normal /= np.linalg.norm(slice_normal)
            axis = np.cross(slice_normal, gparams.normal)
            sine = np.linalg.norm(axis)
            axis /= sine
            cosine = np.dot(slice_normal, gparams.normal)
            angle = np.arctan2(sine, cosine)
            old_base = TxAxisAngle(angle, axis, mode="rad").map_vector(
                old_base[:, :3], rule="fs")
            assert np.allclose(old_base[2, :3], gparams.normal)
        rotmat = old_base[:3, :3].T @ np.linalg.inv(new_base[:3, :3].T)
        angles = TxRotation3D(rotmat).euler(mode="deg", order="xyz")
        cog = olddomain.pcentre()
        logger.info(f"Initialising Slice {slice_no} Part {part} -- "
                    f"centre of gravity: {cog} [mm], "
                    f"orientation: {angles} [deg]")

        # Set rigid parameters (+ warp placeholder)
        image.domain.external += Chain(
            TxEmbed(1, name="embed"),
            TxIdentity(name="warp"),
            TxEulerAngles(*angles, mode="deg", order="xyz", name="rot3d"),
        )
        shift = np.subtract(cog, image.domain.pcentre())
        image.domain.external += [TxTranslation(*shift, name="trans3d")]


def run_stage(stage, images, volume, cnf, gparams=None, errors=None):
    """
    Runs a specific registration stage.

    :type errors: set

    """
    n_cpu = get_cpu_count(cnf["n_cpu"])
    worker = partial(canonical_worker, **stage)
    errors = set() if errors is None else errors

    if n_cpu > 1:
        with mp.pool.ThreadPool(processes=n_cpu) as pool:
            result = pool.map(worker, get_jobs(images, volume, gparams, cnf))
    else:
        result = []
        for job in get_jobs(images, volume, gparams, cnf):
            result.append(worker(job))

    # Create new indexed stack from the images
    c_indices = []
    for image in images:
        slice_no = image.header["metadata"]["Slice"]
        part = image.header["metadata"]["Part"]
        c_indices.append((slice_no, part))
    else:
        images = dict(zip(c_indices, images))

    # Assign new domains to the images in the new stack
    for c_ix, d_file in result:
        try:
            domain = tirl.load(d_file)
        except FileNotFoundError:
            logger.error(f"File not found: {d_file}")
            errors.add(c_ix)
        else:
            # This line mutates the input TImage instances
            images[c_ix].domain = domain
            # If a slice failed previously, but succeeded now,
            # remove its index from the error set
            try:
                errors.remove(c_ix)
            except KeyError:
                pass

    return errors


def get_cpu_count(n_cpu):
    if int(n_cpu) < 0:
        return int(n_cpu) % (mp.cpu_count() + 1)
    else:
        return int(n_cpu)


def get_jobs(images, volume, gparams, cnf):

    ext_timg = ts.EXTENSIONS["TImage"]

    for image in images:
        slice_no = image.header["metadata"]["Slice"]
        part = image.header["metadata"]["Part"]

        # Create output directory
        outdir = os.path.join(cnf["outputdir"], f"{slice_no:04d}")
        fname = f"slice-{slice_no:04d}_part-{part}.{ext_timg}"
        if not os.path.exists(outdir):
            try:
                os.makedirs(outdir)
            except Exception as exc:
                logger.exception(
                    f"ERROR while creating output directory: {outdir}")
                continue

        # Define inputs on disk
        imfile = os.path.join(outdir, fname)
        if not os.path.isfile(imfile):
            image.save(imfile, overwrite=True)
            logger.info(f"Saved a working copy of the image data to: {imfile}")

        # Save the latest image domain next to the image data
        # in consecutive iterations, only this gets updated, because the image
        # data is not changing.
        ext_dom = ts.EXTENSIONS["Domain"]
        dname = f"slice-{slice_no:04d}_part-{part}_latest_domain.{ext_dom}"
        dname = os.path.join(outdir, dname)
        image.domain.save(dname, overwrite=True)
        logger.info(f"Updated image domain: {dname}")

        r_file = volume.header["metadata"]["filename"]

        yield (slice_no, part), imfile, r_file, gparams, cnf


def canonical_worker(job, script, tag, descr):

    (slice_no, part), imfile, r_file, gparams, cnf = job
    ext_dom = ts.EXTENSIONS["Domain"]

    # Dump the job description to a file
    slicedir = os.path.dirname(imfile)
    scriptdir = os.path.dirname(os.path.abspath(__file__))
    fullscript = os.path.join(scriptdir, script)
    fd, jobfile = tu.tmpfile(prefix=f"{tag}_", dir=slicedir, suffix=".job")
    with open(jobfile, "wb") as fp:
        dill.dump(job, fp)

    # Launch a new task and wait until it's completed

    logfile = os.path.join(
        slicedir, f"slice-{slice_no:04d}_part-{part}_tx-{tag}.log""")
    outputfile = os.path.join(
        slicedir, f"slice-{slice_no:04d}_part-{part}_tx-{tag}_domain.{ext_dom}")
    cmd = _get_cmd_prefix(scheduler=cnf["scheduler"], queue=cnf["queue"]) + \
          [f"{fullscript}", f"{jobfile}", f"{outputfile}", f"{logfile}"]
    if cnf.get("debug", False):
        try:
            externalmodule = __import__(script.replace(".py", ""))
            externalmodule.main(jobfile, outputfile, logfile)
        except Exception as exc:
            returncode = 1
            raise exc
        else:
            returncode = 0
    else:
        proc = subprocess.run(cmd, env=os.environ.copy())
        returncode = proc.returncode

    # Close OS file handle
    try:
        os.close(fd)
    except Exception:
        pass

    # Remove the temporary jobfile
    if cnf["cleanup"]:
        try:
            os.remove(jobfile)
        except Exception:
            pass

    if returncode == 0:
        logger.info(f"Successfully completed \"{descr}\" stage "
                    f"for [Slice {slice_no}, Part {part}].")
    else:
        logger.error(f"There was an error while performing \"{descr}\" stage "
                     f"on [Slice {slice_no}, Part {part}].")

    # Return the output of the child process
    return (slice_no, part), outputfile


def _get_cmd_prefix(scheduler, **kwargs):
    scheduler = str(scheduler).lower() if scheduler else None
    if scheduler == "sge":
        queue = kwargs.get("queue", DEFAULT_QUEUE)
        prefix = [f"fsl_sub", f"-q {queue}", f"--extra=\"-sync y\"", "tirl"]
    elif scheduler == "slurm":
        prefix = [f"sbatch", "-W", "tirl"]
    elif scheduler is None:
        prefix = [f"tirl"]
    else:
        raise NameError(f"Unrecognised task scheduler: {scheduler}")
    return prefix


def create_parser():
    """
    Creates argument parser object.

    """
    parser = argparse.ArgumentParser(
        prog="multislice_to_volume",
        description="Registers parallel sections to a volume.")
    parser.add_argument(
        "--config", metavar="<*.yml>", type=str, required=True,
        help="Configuration file.")
    parser.add_argument(
        "--verbose", action="store_true", default=False, required=False,
        help="Logs to STDOUT.")
    return parser


def create_logger(loggername):
    """
    Creates a logger that sends log messages to the standard output and
    into a file.

    """
    p = AttrMap(cnf)
    global logger
    logger = logging.getLogger(loggername)
    logger.handlers = []  # delete any existing handlers to avoid duplicate logs
    logger.setLevel(1)
    formatter = logging.Formatter(
        fmt='%(asctime)s Process-%(process)d %(levelname)s (%(lineno)d) '
            '- %(message)s',
        datefmt='[%Y-%m-%d %H:%M:%S]')

    # Make sure that the logfile can be created
    logfile = cnf["logfile"]
    if not logfile:
        logfile = os.path.join(p.outputdir, "logfile.log")
    else:
        logdir = os.path.dirname(logfile)
        if not os.path.isdir(logdir):
            os.makedirs(logdir)

    # Redirect all logs of interest to the logfile
    fh = logging.FileHandler(logfile, mode="a", delay=False)
    if isinstance(p.loglevel, str):
        level = getattr(logging, p.loglevel.upper())
    elif isinstance(p.loglevel, (int, float)):
        level = int(p.loglevel)
    elif not p.loglevel:
        level = logging.INFO
    else:
        raise TypeError(f"Invalid logging level: {p.loglevel}")
    fh.setLevel(level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Redirect only warnings/errors to the standard output, unless "verbose"
    ch = logging.StreamHandler()
    if p.verbose:
        ch.setLevel(level)
    else:
        ch.setLevel(logging.WARNING)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


# PROGRAM FLOW CONTROL

if __name__ == "__main__":

    parser = create_parser()

    if len(sys.argv) > 1:

        # Load command-line arguments and configurations
        args = parser.parse_args()
        with open(args.config, "r") as fp:
            cnf = dict(yaml.safe_load(fp))

        # Override verbose setting from the CLI
        if args.verbose:
            cnf["verbose"] = args.verbose

        # Configure logger
        logger = create_logger(LOGGER)
        main()

    else:
        parser.print_help()
