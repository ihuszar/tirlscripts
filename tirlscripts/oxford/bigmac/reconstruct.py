#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

"""

"""


__tirlscript__ = "bigmac3.recon"


# DEPENDENCIES

import os
import re
import yaml
import numpy as np
from glob import glob
import nibabel as nib
import matplotlib.pyplot as plt
from scipy.io import savemat, loadmat
from skimage.exposure import rescale_intensity


# TIRL IMPORTS

import tirl
from tirl.timage import TImage
from scipy.interpolate import griddata


# DEFINITIONS

MRI = "/mnt/alpha/BigMac/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz"
HALF = "Anterior"
ROOTDIR = f"/home/inhuszar/BigMac/Microscopy/Combine/{HALF}/registration"
STAINS = ("Cresyl", "Gallyas", "PLI")
EXCLUDE = ()  # (193, 207, 200, 473, 485, 596, 606)
TX = "warp"


# IMPLEMENTATION


def create_input_for_matlab():
    vol = TImage(MRI, external="sform", name="mri")
    mm2vox = vol.domain.chain.inverse()

    # Locate registered domains
    domainfiles = []
    for root, dirs, files in os.walk(ROOTDIR):
        for f in files:
            if f.endswith(".dom") and (TX in f):
                domainfiles.append(os.path.join(root, f))

    # Collect available microscopy data from the registered components
    for stain in STAINS:
        known_points = []
        data = []
        for dfile in sorted(domainfiles):
            slicedir, dname = os.path.split(dfile)
            slice_no = int(re.search(r"(?<=slice-)\d+", dname).group(0))
            if slice_no in EXCLUDE:
                continue
            part = str(re.search(r"(?<=part-).+?(?=_)", dname).group(0))
            metafile = os.path.join(
                slicedir, f"slice-{slice_no:04d}_part-{part}_metadata.yml")
            with open(metafile, "r") as fp:
                metadata = yaml.safe_load(fp)
            if str(metadata["Stain"]).lower() != stain.lower():
                continue
            capture = metadata["Capture"]
            pyrimfile = glob(os.path.join(
                "/home/inhuszar/BigMac/Microscopy", stain,
                HALF, "PyramidalTImages", f"{capture}-{part}*.timg"))[0]
            regimfile = os.path.join(
                slicedir, f"slice-{slice_no:04d}_part-{part}.timg")
            image = tirl.load(pyrimfile)
            domain = tirl.load(dfile)
            image = image.resize(*domain.shape, copy=True)
            image.domain = domain
            image.resample(0.15, copy=False)
            domain = image.domain

            if stain == "PLI":
                hsvfile = os.path.join(
                    f"/mnt/alpha/BigMac/Microscopy/PLI/{HALF}", capture,
                    "FOM_HSV", "mosaic_lowres.png")
                lowres_exists = True
                if not os.path.isfile(hsvfile):
                    hsvfile = os.path.join(
                        f"/mnt/alpha/BigMac/Microscopy/PLI/{HALF}", capture,
                        "FOM_HSV", "mosaic.tif")
                    lowres_exists = False
                try:
                    hsv = TImage(hsvfile)
                except:
                    print(f"Could not read: {hsvfile}")
                    continue
                hsv = hsv.resize(*domain.shape, copy=True)
                if not lowres_exists:
                    hsv.snapshot(hsvfile.replace(".tif", "_lowres.png"), True)
                hsv.domain = domain
                hsv.mask = image.mask
                hsv.resmgr.sync()
                hsv.maskmgr.sync()
                image = hsv

            print(f"Slice {slice_no}, Part {part}: Domain: {domain.shape}")
            mask = image.mask
            if mask is not None:
                mask = image.mask.ravel() > 0.5
            vc = mm2vox.map(domain.get_physical_coordinates())[mask, :]
            known_points.append(vc)
            data.append(image.data.reshape(-1, image.tsize)[mask, :3])

        x, y, z = np.vstack(known_points).T
        data = np.vstack(data)
        xi, yi, zi = np.meshgrid(*tuple(range(dim) for dim in vol.vshape),
                                 indexing="ij")
        # Save MATLAB outputs
        savemat(
            os.path.join(ROOTDIR, "reconstruction", f"{stain}_input.mat"),
            dict(x=x, y=y, z=z,
                 red=data[..., 0], green=data[..., 1], blue=data[..., 2],
                 xq=xi.astype("f8"), yq=yi.astype("f8"), zq=zi.astype("f8"))
        )

    # Do the interpolation
    # cube = (slice(150, 200), slice(150, 200), slice(150, 200))
    # interpolants = griddata((x, y, z), data,
    #     (xi[cube], yi[cube], zi[cube]), fill_value=0, rescale=True)
    # #pli = interpolants.reshape(vol.data.shape)
    # pli = interpolants.reshape((50, 50, 50))
    # mri_nifti = nib.load(MRI)
    # hdr = mri_nifti.header
    # pli_nifti = nib.Nifti1Image(pli, affine=hdr.get_best_affine(), header=hdr)
    # nib.save(pli_nifti, os.path.join(ROOTDIR, f"{STAIN}_anterior_3D.nii.gz"))#


def reconstruct_from_matlab_output():

    rgb_dtype = np.dtype([('R', 'u1'), ('G', 'u1'), ('B', 'u1')])

    for stain in STAINS:
        mri_nifti = nib.load(MRI)
        hdr = mri_nifti.header
        mat = loadmat(
            os.path.join(ROOTDIR, "reconstruction", f"{stain}_output.mat"))
        red = np.where(np.isnan(mat["out_red"]), 0, mat["out_red"])
        red = red.reshape(mri_nifti.shape).T
        print("Red", red.min(), red.max())
        green = np.where(np.isnan(mat["out_green"]), 0, mat["out_green"])
        green = green.reshape(mri_nifti.shape).T
        print("Green", green.min(), green.max())
        blue = np.where(np.isnan(mat["out_blue"]), 0, mat["out_blue"])
        blue = blue.reshape(mri_nifti.shape).T
        print("Blue", blue.min(), blue.max())

        rgb = np.stack((red, green, blue), axis=-1)
        rgb = np.floor(rgb / rgb.max() * 255).astype("u1")
        data = rgb.view(dtype=rgb_dtype).reshape(mri_nifti.shape)

        nifti = nib.Nifti1Image(data, affine=hdr.get_best_affine())
        nib.save(nifti,
                 os.path.join(ROOTDIR, "reconstruction",
                              f"{stain}{HALF}_RGB_3D.nii.gz"))


def main():

    # create_input_for_matlab()
    reconstruct_from_matlab_output()


if __name__ == "__main__":
    main()
