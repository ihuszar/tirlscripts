#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

"""
rigid3d.py --

This is a satellite script for multislice_to_volume.py that performs the
3D rigid registration step on a single slice. The satellite script allows the
parallel execution of this registration step on a computing cluster, where
jobs are managed by a scheduler.

"""


# DEPENDENCIES

import os
import sys
import dill
import logging
import numpy as np
import multiprocessing as mp


# TIRL IMPORTS

import tirl
import tirl.settings as ts
from tirl.constants import *
from tirl.cost.mind import CostMIND
from tirl.optimisation.optnl import OptNL, OptMethods
from tirl.optimisation.optgroup import OptimisationGroup
from tirl.regularisation.txregulariser import TransformationRegulariser


# TIRSCRIPT IMPORTS

from tirlscripts.oxford.bigmac.constants import *


# DEFINITIONS

np.set_printoptions(precision=4, suppress=True)
LOGGER = None
logger = logging.getLogger(LOGGER)
Z0 = [0, 0, 1]
DEFAULT_XTOL_ABS = [0.01, 0.01, 0.01, 0.01, 0.01, 0.01]
DEFAULT_STEP = 0.5


# IMPLEMENTATION

class Parallelity(TransformationRegulariser):

    def __init__(self, rotation, normal, weight=1.0, logger=None,
                 **metaparameters):
        super(Parallelity, self).__init__(
            rotation, weight, logger, **metaparameters)
        self.normal = np.asarray(normal, dtype=np.float64)
        self.normal = self.normal / np.linalg.norm(self.normal)

    def function(self):
        cross = np.cross(self.transformation.map_vector(Z0),
                         self.normal)
        return np.linalg.norm(cross)  # sine function over distance


class Regularity(TransformationRegulariser):

    def __init__(self, translation, slice_no, gparams, weight=1.0, logger=None,
                 **metaparameters):
        super(Regularity, self).__init__(
            translation, weight, logger, **metaparameters)
        self.gparams = gparams
        self.slice = slice_no

    def function(self):
        position = self.transformation.parameters[:]
        distance = np.subtract(position, self.gparams.offset)
        normal = self.gparams.normal / np.linalg.norm(self.gparams.normal)
        orthodist = np.dot(distance, normal)
        expectation = self.gparams.gap * self.slice
        return (orthodist - expectation) ** 2  # squared distance


def create_logger(loggername, logfile, cnf):
    """
    Creates a logger that sends log messages to the standard output and
    into a file.

    """
    global logger
    logger = logging.getLogger(loggername)
    logger.handlers = []  # delete any existing handlers to avoid duplicate logs
    logger.setLevel(1)
    formatter = logging.Formatter(fmt=LOGFORMAT, datefmt=DATEFORMAT)

    # Make sure that the logfile can be created
    logdir = os.path.dirname(os.path.abspath(logfile))
    if not os.path.isdir(logdir):
        os.makedirs(logdir)

    # Redirect all logs of interest to the logfile
    loglevel = cnf["loglevel"]
    fh = logging.FileHandler(logfile, mode="a", delay=False)
    if isinstance(loglevel, str):
        level = getattr(logging, loglevel.upper())
    elif isinstance(loglevel, (int, float)):
        level = int(loglevel)
    elif not loglevel:
        level = logging.INFO
    else:
        raise TypeError(f"Invalid logging level: {loglevel}")
    fh.setLevel(level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Redirect only warnings/errors to the standard output, unless "verbose"
    ch = logging.StreamHandler()
    if cnf["verbose"]:
        ch.setLevel(level)
    else:
        ch.setLevel(logging.WARNING)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


def load_image(imfile):
    image = tirl.load(imfile)
    slice_no = image.header["metadata"]["Slice"]
    part = image.header["metadata"]["Part"]
    ext = ts.EXTENSIONS["Domain"]
    dname = f"slice-{slice_no:04d}_part-{part}_latest_domain.{ext}"
    latest = os.path.join(os.path.dirname(imfile), dname)
    image.domain = tirl.load(latest)
    image.resmgr.sync()
    return image


def rigid3d_worker(job, outputfile):
    """
    Optimises the component-specific 3D position and orientation.

    """
    (slice_no, part), imfile, r_file, gparams, cnf = job

    # Load image pair
    image = load_image(imfile)
    image.storage = MEM
    reference = tirl.load(r_file)
    img_layer = image.resmgr.active
    ref_layer = reference.resmgr.active
    reference.storage = MEM

    # Measure initial cost with the trusted domain
    # This is to provide a fall-back when the optimisation is detrimental
    initial_cost = CostMIND(reference, image, normalise=True)()
    trusted_domain = image.domain.copy()

    # Load parameters
    params = cnf["regparams"]["rigid3d"]
    resolutions = params["resolution"]
    smoothing = params["smoothing"]
    ldx = np.asarray(params["ldx"], dtype=float)
    udx = np.asarray(params["udx"], dtype=float)

    # Scaling-smoothing iteration
    for resolution, sm in zip(resolutions, smoothing):
        resolution = image.resolution if resolution is None else resolution
        scaling_factors = np.divide(image.layers[0].resolution, resolution)
        sc = np.abs(np.mean(scaling_factors))
        logger.debug(f"Slice scaling factor: 1 : {1 / sc}")
        image.rescale(sc, copy=False)
        image.storage = MEM
        if sm != 0:
            sm_newimg = image.smooth(sm, copy=True)
            sm_newimg.domain = image.domain
        else:
            sm_newimg = image
        rotation = sm_newimg.domain.external["rot3d"]
        offset = sm_newimg.domain.external["trans3d"]
        og = OptimisationGroup(rotation, offset)
        logger.log(ts.PARAMETER_UPDATE_LOG_LEVEL, str(og.parameters[:]))
        l_rot = np.subtract(rotation.parameters[:], np.radians(ldx[:3]))
        u_rot = np.add(rotation.parameters[:], np.radians(udx[:3]))
        l_offset = np.subtract(offset.parameters[:], [*ldx[3:]])
        u_offset = np.add(offset.parameters[:], [*udx[3:]])
        og.set_lower_bounds(np.asarray([*l_rot, *l_offset]))
        og.set_upper_bounds(np.asarray([*u_rot, *u_offset]))
        cost = CostMIND(reference, sm_newimg)
        # pw = float(cnf["regparams"]["rigid3d"]["parallelity"])
        # rw = float(cnf["regparams"]["rigid3d"]["regularity"])
        xtol_abs = np.atleast_1d(cnf["regparams"]["rigid3d"].get(
            "xtol-abs", DEFAULT_XTOL_ABS))
        step = float(cnf["regparams"]["rigid3d"].get("step", DEFAULT_STEP))
        # preg = Parallelity(rotation, gparams.normal, weight=pw, logger=logger)
        # rreg = Regularity(offset, slice_no, gparams, weight=rw, logger=logger)
        OptNL(og, cost, method=OptMethods.LN_BOBYQA,
              normalised=True, xtol_abs=xtol_abs, step=step, logger=logger,
              visualise=params["visualise"])()

        image.domain.external = sm_newimg.domain.external
        image.resmgr.sync()

    # Restore the input resolution
    image.resmgr.set(img_layer)
    reference.resmgr.set(ref_layer)

    # Measure post-optimisation cost and discard worse-than-initial results
    final_cost = CostMIND(reference, image, normalise=True)()
    logger.info(f"Initial cost was {initial_cost}")
    logger.info(f"Post-optimisation cost is {final_cost}")
    if initial_cost < final_cost:
        if cnf["regparams"]["rigid3d"]["reject"]:
            image.domain = trusted_domain
            image.resmgr.sync()
            logger.info("The optimisation was detrimental. "
                        "The optimisation result was rejected.")
        else:
            logger.info("The optimisation was potentially detrimental.")
    else:
        logger.info("The optimisation was successful.")

    # Generate output (optimised or trusted domain)
    image.domain.save(outputfile, overwrite=True)

    # (reference image resampled onto the optimised domain)
    vs = reference.evaluate(image.domain)
    vs.normalise()
    ext = ts.TIMAGE_DEFAULT_SNAPSHOT_EXT
    slicedir = os.path.dirname(imfile)
    s_file = os.path.join(
        slicedir, f"slice-{slice_no:04d}_part-{part}_tx-rigid3d_mri.{ext}")
    vs.snapshot(s_file, overwrite=True)

    return (slice_no, part), outputfile


def main(jobfile, outputfile, logfile):
    """ Main program code. """

    with open(jobfile, "rb") as fp:
        job = dill.load(fp)

    (slice_no, part), imfile, r_file, gparams, cnf = job
    pid = mp.current_process().pid

    global LOGGER
    LOGGER = f"slice-{slice_no}_part-{part}_tx-rigid3d_proc-{pid}"
    create_logger(LOGGER, logfile, cnf)
    cmd = " ".join(sys.argv)
    logger.info(f"The program started with the following command: {cmd}")
    rigid3d_worker(job, outputfile)


if __name__ == "__main__":
    main(*sys.argv[1:])
