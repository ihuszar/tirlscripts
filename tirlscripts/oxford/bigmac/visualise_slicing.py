#!/usr/bin/env python

import os
import vtk
import tirl
import numpy as np
from glob import glob
from mayavi import mlab
from tirl.timage import TImage
from scipy.spatial import Delaunay
from vtk.util.numpy_support import numpy_to_vtk


MRI = "/mnt/alpha/BigMac/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz"
REGDIR = "/home/inhuszar/BigMac/Microscopy/Combine/Anterior/registration"
STEP = 15


def main():
    """ Main program code. """

    # Visualise straight slices
    # show_scene("init")
    show_scene("warp")


def draw_slice(dom, mri=None):

    if hasattr(dom, "__timage__"):
        dom = dom.domain

    vc = dom.get_voxel_coordinates()
    tri = Delaunay(vc[:, [1, 0]])
    pc = dom.get_physical_coordinates()
    if mri is not None:
        pc = mri.domain.map_physical_coordinates(pc)  # MRI voxel coordinates
    tmesh = mlab.triangular_mesh(*pc.T, tri.simplices, colormap="gray")

    tmesh.mlab_source.scalars = np.zeros(dom.shape).ravel()
    return tmesh

    # grid = vtk.vtkImageData()
    # grid.SetDimensions(*dom.shape[::-1], 1)
    # vtkarr = numpy_to_vtk(
    #     np.flip(data.swapaxes(0, 1), axis=(0, 1))
    #         .reshape((-1, 3), order='F'))
    # vtkarr.SetName('Image')
    # grid.GetPointData().AddArray(vtkarr)
    # grid.GetPointData().SetActiveScalars('Image')
    #
    # vtex = vtk.vtkTexture()
    # vtex.SetInputDataObject(grid)
    # vtex.Update()
    #
    # tmesh.actor.mapper.scalar_visibility = False
    # tmesh.actor.enable_texture = True
    # tmesh.actor.tcoord_generator_mode = 'plane'
    # tmesh.actor.actor.texture = vtex
    #
    # return tmesh


def show_mri(mri):

    mri.normalise()
    src = mlab.pipeline.scalar_field(mri.data)
    src.update_image_data = True

    voi = mlab.pipeline.extract_grid(src)
    voi.trait_set()
    surf = mlab.pipeline.iso_surface(
        voi, contours=[0.1], color=(0.5, 0.5, 0.5))

    return surf


def show_scene(args):
    """ Visualising straight slices """

    ilst = os.path.join(REGDIR, "visualisation", "reglist_init_right.txt")
    with open(ilst, "r") as fp:
        imfiles = fp.readlines()

    flst = os.path.join(REGDIR, "visualisation", "reglist_warp_right.txt")
    with open(flst, "r") as fp:
        samples = fp.readlines()

    mlab.figure(1, bgcolor=(0.7, 0.7, 0.7), fgcolor=(0.5, 0.5, 0.5))
    mlab.plot3d([105, 105], [-20, 259], [88, 88], color=(1, 1, 0.5))

    # Show MRI isosurface
    mri = TImage(MRI, external="sform")
    show_mri(mri)

    # Display slices
    imfiles = imfiles[::STEP]
    samples = samples[::STEP]

    for ix, (imfile, sample) in enumerate(zip(imfiles, samples)):
        print(f"Drawing slice {ix + 1}/{len(imfiles)}")
        image = tirl.load(os.path.join(REGDIR, imfile.rstrip("\n")))
        capture = image.header["metadata"]["Capture"]
        part = image.header["metadata"]["Part"]
        stain = image.header["metadata"]["Stain"]
        ap = image.header["metadata"]["AP"]
        pyrimfile = glob(os.path.join(
            f"/home/inhuszar/BigMac/Microscopy/{stain}/{ap}"
            f"/PyramidalTImages/{capture}-{part}-*.timg"
        ))[0]
        image = tirl.load(pyrimfile)
        if args == "init":
            image = image.resample(0.15)
        else:
            sample = tirl.load(os.path.join(REGDIR, sample.rstrip("\n")))
            if hasattr(sample, "domain"):
                sample = sample.domain
            image = image.resize(*sample.shape, copy=True)
            image.domain = sample
            image = image.resample(0.15, copy=True)

        draw_slice(image.domain, mri=mri)

    mlab.view(0, 0)
    mlab.show()


if __name__ == "__main__":
    main()
