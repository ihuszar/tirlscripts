#!/usr/bin/env python

import tirl
from tirl.timage import TImage

histo = tirl.load("/Users/inhuszar/maldi/data/sample2/histology_1um.timg")
maldi = TImage("/Users/inhuszar/maldi/data/sample2/197_control cbllm_870.5371mz_less_obvious_purkinje.tif")

print("Histo")
print(histo)
print("Resolution:", histo.resolution)
print(histo.domain.internal)
print(histo.domain.external)

print("MALDI")
print(maldi)
print("Resolution:", maldi.resolution)
print(maldi.domain.internal)
print(maldi.domain.external)

# maldi.preview()

# Load registrations
moving = tirl.load("/Users/inhuszar/maldi/reg_sample2/reg.hires/moving.timg")
fixed = tirl.load("/Users/inhuszar/maldi/reg_sample2/reg.hires/fixed3_affine.timg")

# moving.evaluate(fixed.domain).preview()
# fixed.evaluate(moving.domain).preview()

print("Moving", moving)
print("Fixed", fixed)

maldi.domain = fixed.domain
maldi.resmgr.sync()
histo.domain.external = moving.domain.external
histo.resmgr.sync()


# hpc = histo.domain.map_voxel_coordinates([9000, 30000])
# print(hpc)
# mvc = maldi.domain.map_physical_coordinates(hpc)
# print(mvc)
# print(maldi.interpolate(mvc))
# exit()


# print("Evaluating histology on MALDI...")
# out = histo.evaluate(maldi.domain)
# out.snapshot("/Users/inhuszar/maldi/reg_sample2/reg.hires/histo_on_maldi.tif")

print("Evaluating MALDI on histology...")
out = maldi.evaluate(histo.domain)
out.snapshot("/Users/inhuszar/maldi/reg_sample2/reg.hires/maldi_on_histo1.tif")

