#!/usr/bin/env python

import os
import sys
import yaml
import tirl
import argparse
import openslide
import numpy as np
from tirl.timage import TImage

np.set_printoptions(suppress=True, precision=4)


def main(args):

    # Load registration configurations
    with open(os.path.join(args.regdir, "configuration.yml"), "r") as f:
        cnf = yaml.safe_load(f)

    # Definitions
    maldi_reg_file = os.path.join(args.regdir, "fixed3_affine.timg")
    histo_reg_file = os.path.join(args.regdir, "moving.timg")
    outputdir = args.regdir
    h_user_res = float(cnf["moving"]["resolution"])
    m_user_res = float(cnf["fixed"]["resolution"])

    try:
        svs_file, h_res = str(args.svs).split(",")
        h_res = float(h_res)
    except ValueError:
        svs_file = str(args.svs)
        h_res = h_user_res
    try:
        maldi_file, m_res = str(args.maldi).split(",")
        m_res = float(m_res)
    except ValueError:
        maldi_file = str(args.maldi)
        m_res = m_user_res

    # Histology
    print("Loading histology image....")
    # orig_res = float(slide.properties["aperio.MPP"]) / 1000  # mm
    # histo = np.asarray(slide.get_thumbnail(slide.level_dimensions[level]))
    slide = openslide.OpenSlide(svs_file)
    level = cnf["moving"]["pages"][-1]
    h_scale = np.divide(h_user_res, h_res).tolist()
    h_shape = np.multiply(slide.level_dimensions[level], h_scale).astype(int)
    histo = TImage(np.asarray(slide.get_thumbnail(h_shape)))
    histo.resolution = \
        h_user_res * np.divide(slide.level_dimensions[level], h_shape)
    histo = TImage(histo)
    histo_reg = tirl.load(histo_reg_file)
    histo.domain.external = histo_reg.domain.external
    h_offset = np.subtract(histo_reg.domain.pcentre(), histo.domain.pcentre())
    print(f"Histology image resolution offset: {h_offset} mm")
    histo.domain.internal["offset"].parameters[:] = h_offset
    print(f"Saving histology image at {histo.resolution} mm/px...")
    h_res_act = np.mean(histo.resolution)
    histo.snapshot(os.path.join(outputdir, f"histology_{h_res_act:.04f}.png"),
                   overwrite=True)

    # MALDI
    print("Loading MALDI image....")
    maldi = TImage(maldi_file)
    maldi.resolution = m_user_res
    m_scale = np.divide(m_user_res, m_res).tolist()
    m_scale = (m_scale,) if not hasattr(m_scale, "__iter__") else m_scale
    maldi.rescale(*m_scale, copy=False)
    maldi_reg = tirl.load(maldi_reg_file)
    maldi.domain.external = maldi_reg.domain.external
    m_offset = np.subtract(maldi_reg.domain.pcentre(), maldi.domain.pcentre())
    print(f"MALDI image resolution offset: {m_offset} mm")
    maldi.domain.internal["offset"].parameters[:] = m_offset
    print(f"Saving MALDI image at {maldi.resolution} mm/px...")
    m_res_act = np.mean(maldi.resolution)
    maldi.snapshot(os.path.join(outputdir, f"maldi_{m_res_act:.04f}.png"),
                   overwrite=True)

    print("Transforming histology image to MALDI...")
    h2m = histo.evaluate(maldi.domain)
    h2m.snapshot(os.path.join(outputdir, "histology_on_maldi.png"),
                 overwrite=True)

    print("Transforming MALDI image to histology...")
    m2h = maldi.evaluate(histo.domain)
    m2h.snapshot(os.path.join(outputdir, "maldi_on_histology.png"),
                 overwrite=True)

    print("Done.")


def create_parser():

    parser = argparse.ArgumentParser()
    parser.add_argument("--svs", required=True, default="", type=str,
                        metavar="<file.svs>", help="Original histology file.")
    parser.add_argument("--maldi", required=True, default="", type=str,
                        metavar="<file.tif>", help="Original MALDI image.")
    parser.add_argument("--regdir", required=True, default="", type=str,
                        metavar="<dir>", help="Registration directory.")
    return parser


if __name__ == "__main__":

    parser = create_parser()
    args = parser.parse_args()
    if len(sys.argv) > 1:
        main(args)
    else:
        parser.print_help()
