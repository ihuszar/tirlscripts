# Changelog


### 7 Jun 2023 -- v1.2.0

- Bugfix: MND Stage 4 chain reduction no longer fails at reading the image resolution


### 19Oct 2022 -- v1.1.0

- Bugfix: masks are no longer removed for padded histology images


### 14 Oct 2022 -- v1.0.1

- Added: logger instance now records the SHA1 hash of the executing script for strict version control


### 11 Oct 2022 -- v1.0.0

- Project-specific version v1.0.0 assigned to the current state of the library
