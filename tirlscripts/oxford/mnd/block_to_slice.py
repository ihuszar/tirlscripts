#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


"""
Oxford Post-Mortem MND Biomarkers Histology-MRI Registration Pipeline, Stage 2:

Registers a tissue block photograph to the corresponding brain slice photograph.
Initialisation for the blocks must be provided either by specifying the voxel
coordinates (row, column) of the centre of the insertion site or by providing a
file with a list of those coordinates for several possible insertion sites.
Such a file can be generated automatically from a series of brain slice
photographs by the accompanying "find_sites" script.

"""

__tirlscript__ = "mnd.b2s"


# DEPENDENCIES

import os
import sys
import logging
import argparse
import numpy as np
from math import radians
from attrdict import AttrMap


# TIRL IMPORTS

import tirl.settings as ts
from tirl.chain import Chain
from tirl.timage import TField
from tirl.timage import TImage
from tirl.cost.mi import CostMI
from tirl.cost.msd import CostMSD
from tirl.cost.mind import CostMIND
from tirl.regularisation.diffusion import DiffusionRegulariser
from tirl.transformations.scale import TxScale
from tirl.transformations.affine import TxAffine
from tirl.transformations.rotation import TxRotation2D
from tirl.transformations.translation import TxTranslation
from tirl.transformations.displacement import TxDisplacementField
from tirl.optimisation.optgroup import OptimisationGroup
from tirl.optimisation.optnl import OptNL
from tirl.optimisation.gnoptdiff import GNOptimiserDiffusion


# TIRLSCRIPT IMPORTS

from tirlscripts import srchash
from tirlscripts.oxford.mnd import __version__
from tirlscripts.oxford.scriptutils import general, image, inout


# DEFINITIONS

from tirl.constants import *


# IMPLEMENTATION

def run(cnf=None, **options):
    """
    Runs TIRL block-to-slice registration (stage 2).

    :param cnf:
        Configuration file. Instead of a file, a dictionary with
        suitable content may also be specified.
    :type cnf: Union[str, dict, None]
    :param options:
        Overriding configuration parameters.
    :type options: Any

    """
    # Load script configuration
    if cnf is not None:
        if isinstance(cnf, dict):
            cnf = dict(cnf)
        elif isinstance(cnf, str):
            cnf = general.load_configurations(cnf)
        else:
            raise TypeError(
                f"Unrecognised configuration format: {cnf.__.class__.__name__}")
    cnf.update(options)
    p, logger = general.initialise_script(**cnf)
    p.logger = logger.name  # avoid globals
    logger.debug(f"tirlscripts mnd library version: {__version__}")
    logger.debug(f"block_to_slice.py SHA1: {srchash(__file__)}")

    # Load and configure input images
    if p.block.export is True:
        ext = ts.EXTENSIONS["TImage"]
        p.block.export = os.path.join(p.general.outputdir, f"block.{ext}")
    block = inout.load_image(**p.block)
    block.rule = None
    if p.slice.export is True:
        ext = ts.EXTENSIONS["TImage"]
        p.slice.export = os.path.join(p.general.outputdir, f"slice.{ext}")
    slice_ = inout.load_image(**p.slice)
    slice_.rule = None

    # Having loaded both images, perform actions on the tissue block photo prior
    # to registration, unless it was loaded from an alternative source.
    # Actions are user-defined functions within the TIRL namespace. Actions
    # can be chain-loaded to perform preparatory analysis steps on the images
    # before registration begins.
    isalternative = p.block.file.lower().endswith(
        (ts.EXTENSIONS["TImage"], ts.EXTENSIONS["TIRLObject"]))
    if not isalternative:
        block = image.perform_image_operations(
            block, *p.preprocessing.block, scope=globals(), other=slice_)
        block.centralise()

    # Perform actions on the brain slice prior to registration, unless it was
    # loaded from a TImage file.
    isalternative = p.slice.file.lower().endswith(
        (ts.EXTENSIONS["TImage"], ts.EXTENSIONS["TIRLObject"]))
    if not isalternative:
        slice_ = image.perform_image_operations(
            slice_, *p.preprocessing.slice, scope=globals(), other=block)

    # Run the registration routine
    try:
        if str(p.general.direction).lower() == "b2s":
            register(block, slice_, p)
        elif str(p.general.direction).lower() == "s2b":
            register(slice_, block, p)
        else:
            raise ValueError(f"Invalid registration direction: "
                             f"{p.general.direction}")
    except Exception as exc:
        logger.error(exc.args)
        logger.fatal(f"The registration terminated with an exception.")
        raise exc
    else:
        logger.fatal("The registration was completed successfully.")


def initialise_transformations(fixed, q):
    """
    Creates transformation chain that will be optimised.

    :returns: linear + nonlinear transformation chain with initial paramters
    :rtype: Chain

    """
    # Site offset
    tx_site = TxTranslation(0, 0, name="site_offset")

    # Scale
    lb = np.asarray(q.init.scale.lb)
    ub = np.asarray(q.init.scale.ub)
    tx_scale = TxScale(*q.init.scale.x0, bounds=(lb, ub), name="scale")

    # Rotation
    if str(q.init.rotation.mode).lower() == "deg":
        lb = radians(float(q.init.rotation.lb))
        ub = radians(float(q.init.rotation.ub))
    else:
        lb = float(q.init.rotation.lb)
        ub = float(q.init.rotation.ub)
    tx_rotation = TxRotation2D(
        float(q.init.rotation.x0), mode=q.init.rotation.mode,
        bounds=(lb, ub), name="rotation")

    # Translation
    lb = np.asarray(q.init.translation.lb)
    ub = np.asarray(q.init.translation.ub)
    tx_trans = TxTranslation(
        *q.init.translation.x0, bounds=(lb, ub), name="translation")

    # Affine
    x0 = np.asarray(q.init.affine.x0).reshape((2, 3))
    lb = np.asarray(q.init.affine.lb)
    ub = np.asarray(q.init.affine.ub)
    tx_affine = TxAffine(x0, bounds=(lb, ub), name="affine")

    # Append linear transformations to the domain equivalent to the fixed image
    linear_chain = Chain(tx_rotation, tx_scale, tx_site, tx_trans, tx_affine)
    domain = fixed.domain[:]
    domain.chain.extend(linear_chain)

    # Nonlinear
    x0 = float(q.init.nonlinear.x0) * np.ones((2, *fixed.vshape))
    if q.init.nonlinear.lb is None:
        lb = None
    else:
        lb = float(q.init.nonlinear.lb) * np.ones_like(x0)
    if q.init.nonlinear.ub is None:
        ub = None
    else:
        ub = float(q.init.nonlinear.ub) * np.ones_like(x0)
    field = TField(
        x0, copy=False, domain=domain[:], taxes=(0,), order=TENSOR_MAJOR)
    tx_nonlinear = TxDisplacementField(
        field, bounds=(lb, ub), name="nonlinear", mode=NL_REL)

    # Return the full transformation chain
    return Chain(*linear_chain, tx_nonlinear)


def register(fixed, moving, cnf):
    """
    Runs the four registration stages: rotation search, rigid, affine, and
    non-linear. The function has no return value. The transformation chain is
    attached to the Domain of the fixed TImage object and is optimised in situ.

    :param fixed:
        fixed image (to which the chain is attached)
    :type fixed: TImage
    :param moving:
        moving image (that defines the coordinate space that the mapping
        is into)
    :type moving: TImage
    :param cnf: all configuration options
    :type cnf: AttrMap or dict

    """
    p = AttrMap(cnf)
    q = p.regparams
    logger = logging.getLogger(p.logger)

    # Create transformation chain (does not change the fixed image)
    # rotation -> scale -> translation -> affine -> nonlinear
    logger.info("Initialising transformation chain...")
    chain = initialise_transformations(fixed, q)
    logger.info("Transformation chain has been initialised.")

    # Set the first part of the chain
    fixed.domain.chain.extend(chain[:-2])
    # Optimise site offset and rotation parameters
    find_best_site(fixed, moving, p)

    # Generate output: (best) initial alignment
    fixed.save(os.path.join(
        p.general.outputdir, "fixed.timg"), overwrite=True)
    fixed.snapshot(os.path.join(
        p.general.outputdir, "fixed.png"), overwrite=True)
    moving.save(os.path.join(
        p.general.outputdir, "moving.timg"), overwrite=True)
    moving.evaluate(fixed.domain).snapshot(os.path.join(
        p.general.outputdir, "moving.png"), overwrite=True)

    # Rigid registration
    if "rigid" in p.general.stages:
        logger.info("Starting rigid registration...")
        rigid2d(fixed, moving, p)
        logger.info("Completed rigid registration.")
        # Generate output
        fixed.save(os.path.join(
            p.general.outputdir, "fixed2_rigid.timg"), overwrite=True)
        moving.evaluate(fixed.domain).snapshot(os.path.join(
            p.general.outputdir, "moving2_rigid.png"), overwrite=True)
    else:
        logger.info("Rigid registration was skipped.")

    # Affine registration
    fixed.domain.chain.append(chain[-2])
    if "affine" in p.general.stages:
        logger.info("Starting affine registration...")
        affine2d(fixed, moving, p)
        logger.info("Completed affine registration.")
        # Generate output
        fixed.save(os.path.join(
            p.general.outputdir, "fixed3_affine.timg"), overwrite=True)
        moving.evaluate(fixed.domain).snapshot(os.path.join(
            p.general.outputdir, "moving3_affine.png"), overwrite=True)
    else:
        logger.info("Affine registration was skipped.")

    # Non-linear registration
    tx_nonlinear = chain[-1]
    tx_nonlinear.domain.chain = fixed.domain.chain[:]
    fixed.domain.chain.append(tx_nonlinear)
    if "nonlinear" in p.general.stages:
        logger.info("Starting non-linear registration...")
        diffreg2d(fixed, moving, p)
        logger.info("Completed non-linear registration.")
        # Generate output
        fixed.save(os.path.join(
            p.general.outputdir, "fixed4_nonlinear.timg"), overwrite=True)
        moving.evaluate(fixed.domain).snapshot(os.path.join(
            p.general.outputdir, "moving4_nonlinear.png"), overwrite=True)
    else:
        logger.info("Non-linear registration was skipped.")


def find_best_site(fixed, moving, cnf):
    """
    Find the best possible initialisation for the block given the insertion
    site cetres.

    """
    p = AttrMap(cnf)
    q = p.regparams
    logger = logging.getLogger(p.logger)

    if isinstance(q.sites, str) and os.path.isfile(q.sites):
        sites = np.loadtxt(q.sites)
    elif hasattr(q.sites, "__iter__"):
        sites = np.atleast_2d(q.sites)
    else:
        raise TypeError(f"Invalid insertion site definition: {q.sites}")
    tx_rotation = fixed.domain.chain["rotation"]
    site_offset = fixed.domain.chain["site_offset"]
    orig_rotation = tx_rotation.parameters.parameters[0]

    costvals = []
    chains = []
    for site_no, site in enumerate(sites):
        # Initialise fixed image at the insertion site
        p_offset = moving.domain.map_voxel_coordinates(np.atleast_2d(site))[0]
        site_offset.parameters.parameters[:] = p_offset
        site_offset.parameters.set_bounds(None, None)
        site_offset.parameters.unlock()
        tx_rotation.parameters.parameters[:] = orig_rotation
        tx_rotation.parameters.set_bounds(None, None)
        tx_rotation.parameters.unlock()
        # Optimise location and position
        site_cost, chain = jiggle(fixed, moving, p)
        # Record cost function value
        costvals.append([site_no, site_cost])
        chains.append(chain)

    # Find best site
    costvals = np.stack(costvals, axis=0)
    best_site = int(costvals[np.argmin(costvals[:, -1]), 0])
    logger.debug(f"Cost values at each site:\n {costvals}")
    logger.info(f"Best site: ({best_site}): {sites[best_site]}")

    # Update the site offset and rotation parameters to the optima
    best_rotation, best_offset = chains[best_site]
    tx_rotation.parameters[:] = best_rotation.parameters[:]
    tx_rotation.parameters.set_bounds(None, None)
    tx_rotation.parameters.unlock()
    site_offset.parameters[:] = best_offset.parameters[:]
    site_offset.parameters.set_bounds(None, None)
    site_offset.parameters.lock()  # no need to optimise this further


def jiggle(fixed, moving, cnf):
    """
    Performs a quick gridsearch of rotation and translation parameters around
    the insertion site, followed by gradient-free refinement at a fixed scale.

    """
    p = AttrMap(cnf)
    q = p.regparams.jiggle
    logger = logging.getLogger(p.logger)

    fixed.rescale(float(q.scale), copy=False)
    moving.rescale(float(q.scale), copy=False)
    tx_rotation = fixed.domain.chain["rotation"]
    tx_site = fixed.domain.chain["site_offset"]

    # Create test grid
    # Space points: voxel coordinates around site centre in a ~10mm range
    # Rotation points: rotations around the full circle in ~4 steps
    y0, x0 = tx_site.parameters.parameters
    yy = y0 + np.linspace(-q.xrange[0] / 2, q.xrange[1] / 2, q.xstops[0])  # mm
    xx = x0 + np.linspace(-q.xrange[0] / 2, q.xrange[1] / 2, q.xstops[1])  # mm
    xstep = np.divide(q.xrange, q.xstops)
    rotations, rstep = np.linspace(-np.pi, np.pi, q.rotsteps,
                                   endpoint=False, retstep=True)

    # Measure grid cost: find N best location + rotation parameters
    if p.general.cost == "MSD":
        cost = CostMSD(moving, fixed, normalise=True)
    elif p.general.cost == "MI":
        cost = CostMI(moving, fixed, normalise=True)
    elif p.general.cost == "MIND":
        cost = CostMIND(moving, fixed, normalise=True, kernel=MK_FULL,
                        sigma=1, truncate=1.5)
    else:
        raise ValueError("Unsupported cost function.")

    gridcost = []
    for y in yy:
        for x in xx:
            tx_site.parameters.set_bounds(None, None)
            tx_site.parameters.parameters[:] = [y, x]
            for r in rotations:
                tx_site.parameters.set_bounds(None, None)
                tx_rotation.parameters.parameters[0] = r
                gridcost.append([y, x, r, cost()])
    else:
        gridcost = np.stack(gridcost, axis=0)
        best = gridcost[np.argsort(gridcost[:, -1])[:int(q.n_best)], :]
        logger.debug(f"Best {q.n_best} initialisations: \n{best}")
        best_params = best[:, :-1]

    # Refine rotation and translation parameters at the N best initialisations
    costvals = []
    og = OptimisationGroup(tx_rotation, tx_site)
    for y, x, r in best_params:
        tx_rotation.parameters.parameters[:] = r
        tx_rotation.parameters.set_bounds(r - rstep / 2, r + rstep / 2)
        tx_site.parameters.parameters[:] = [y, x]
        tx_site.parameters.set_bounds(
            {0: (y - xstep[0] / 2, y + xstep[0] / 2),
             1: (x - xstep[1] / 2, x + xstep[1] / 2)})
        tx_rotation.parameters.unlock()
        tx_site.parameters.unlock()

        # Start optimisation
        OptNL(og, cost, method="LN_BOBYQA", visualise=q.visualise,
              xtol_abs=q.xtol_abs, xtol_rel=q.xtol_rel, step=q.opt_step,
              logger=logger)()
        costvals.append([*og.parameters[:], cost()])

    # Find overall best alignment and measure final cost
    costvals = np.stack(costvals, axis=0)
    best_params = costvals[np.argmin(costvals[:, -1]), :-1]
    logger.info(f"Best initial parameters at site: {best_params}")
    og.set(best_params)
    bounds_delta = [radians(10), 5, 5]
    lb = np.subtract(best_params, bounds_delta)
    ub = np.add(best_params, bounds_delta)
    og.set_bounds(lb, ub)
    tx_rotation.parameters.unlock()
    tx_site.parameters.unlock()
    final_costval = cost()
    chain = Chain(tx_rotation.copy(), tx_site.copy())

    # Resample the images to full size
    fixed.rescale(1, copy=False)
    moving.rescale(1, copy=False)

    return final_costval, chain


def rigid2d(fixed, moving, cnf):
    """
    Optimises rigid-body parameters and scaling.

    """
    p = AttrMap(cnf)
    q = p.regparams.rigid
    logger = logging.getLogger(p.logger)

    # Scaling-smoothing iteration
    for i, (sc, sm) in enumerate(zip(q.scaling, q.smoothing)):
        logger.debug(f"Scale: {sc}, smoothing: {sm} px...")
        # Prepare images for the current iteration
        fixed.rescale(1. / sc, copy=False)
        moving.rescale(1. / sc, copy=False)
        fixed_smooth = fixed.smooth(sm, copy=True)
        moving_smooth = moving.smooth(sm, copy=True)
        # Prepare co-optimised transformations
        tx_rotation = fixed_smooth.domain.chain["rotation"]
        tx_scale = fixed_smooth.domain.chain["scale"]
        tx_translation = fixed_smooth.domain.chain["translation"]
        og = OptimisationGroup(tx_rotation, tx_scale, tx_translation)

        # Set cost function
        if p.general.cost == "MSD":
            cost = CostMSD(moving_smooth, fixed_smooth, normalise=True)
        elif p.general.cost == "MI":
            cost = CostMI(moving_smooth, fixed_smooth, normalise=True)
        elif p.general.cost == "MIND":
            cost = CostMIND(moving_smooth, fixed_smooth, normalise=True,
                            kernel=MK_FULL, sigma=1, truncate=1.5)
        else:
            raise ValueError("Unsupported cost function.")

        # Start optimisation
        OptNL(og, cost, method="LN_BOBYQA", visualise=q.visualise,
              xtol_abs=q.xtol_abs, xtol_rel=q.xtol_rel, step=q.opt_step,
              logger=logger)()
        # Transfer optimised transformations to the non-smoothed images
        fixed.domain = fixed_smooth.domain
        moving.domain = moving_smooth.domain
    else:
        # Restore full resolution of the images
        fixed.rescale(1, copy=False)
        moving.rescale(1, copy=False)


def affine2d(fixed, moving, cnf):
    """
    Optimises a 6-DOF affine transformation after rigid alignment has been
    fine-tuned.

    """
    p = AttrMap(cnf)
    q = p.regparams.affine
    logger = logging.getLogger(p.logger)

    # Scaling-smoothing iterations
    for i, (sc, sm) in enumerate(zip(q.scaling, q.smoothing)):
        logger.debug(f"Scale: {sc}, smoothing: {sm} px...")
        # Prepare images for the current iteration
        fixed.rescale(1. / sc, copy=False)
        moving.rescale(1. / sc, copy=False)
        fixed_smooth = fixed.smooth(sm, copy=True)
        moving_smooth = moving.smooth(sm, copy=True)
        # Prepare transformation to optimise
        tx_affine = fixed_smooth.domain.chain["affine"]

        # Set cost function
        if p.general.cost == "MSD":
            cost = CostMSD(moving_smooth, fixed_smooth, normalise=True)
        elif p.general.cost == "MI":
            cost = CostMI(moving_smooth, fixed_smooth, normalise=True)
        elif p.general.cost == "MIND":
            cost = CostMIND(moving_smooth, fixed_smooth, normalise=True,
                            kernel=MK_FULL, sigma=1, truncate=1.5)
        else:
            raise ValueError("Unsupported cost function.")

        # Start optimisation
        OptNL(tx_affine, cost, method="LN_BOBYQA",
              xtol_rel=q.xtol_rel, xtol_abs=q.xtol_abs,
              visualise=q.visualise, step=q.opt_step, logger=logger)()
        # Transfer optimised transformations to the non-smoothed images
        fixed.domain = fixed_smooth.domain
        moving.domain = moving_smooth.domain
    else:
        # Restore full resolution of the images
        fixed.rescale(1, copy=False)
        moving.rescale(1, copy=False)


def diffreg2d(fixed, moving, cnf):
    """
    Performs a non-linear registration. The transformation is parameterised as
    a dense displacement field. The cost is MIND, and diffusion regularisation
    is used to create smoothness in the deformation field.

    """
    p = AttrMap(cnf)
    q = p.regparams.nonlinear
    logger = logging.getLogger(p.logger)

    # Scaling-smoothing iteration
    for i, (sc, sm) in enumerate(zip(q.scaling, q.smoothing)):
        logger.debug(f"Scale: {sc}, smoothing: {sm} px...")
        # Prepare images for the current iteration
        fixed.rescale(1 / sc, copy=False)
        moving.rescale(1 / sc, copy=False)
        fixed_smooth = fixed.smooth(sm, copy=True)
        moving_smooth = moving.smooth(sm, copy=True)
        # Prepare transformation to optimise
        tx_nonlinear = fixed_smooth.domain.chain[-1]
        # Set cost and regulariser
        cost = CostMIND(moving_smooth, fixed_smooth, sigma=float(q.sigma),
                        truncate=float(q.truncate), kernel=MK_FULL)
        regularisation = DiffusionRegulariser(
            tx_nonlinear, weight=float(q.regweight))
        # Optimise the non-linear transformation
        GNOptimiserDiffusion(
            tx_nonlinear, cost, regularisation, maxiter=int(q.maxiter[i]),
            xtol_rel=q.xtol_rel, xtol_abs=q.xtol_abs, visualise=q.visualise,
            logger=logger)()
        # Transfer optimised transformations to the non-smoothed images
        fixed.domain = fixed_smooth.domain
        moving.domain = moving_smooth.domain
    else:
        # Restore the original resolution of the images
        fixed.rescale(1, copy=False)
        moving.rescale(1, copy=False)


def block_preprocessing(block, history=None, other=None):
    """
    Preprocess the tissue block photograph.

    """
    # Convert to grayscale using the Y channel of the YIQ colour space.
    return image.rgb2yiq(block.tensors[:3]).tensors[0]


def slice_preprocessing(slice_, history=None, other=None):
    """
    Preprocess the brain slice photograph.

    """
    # Convert to grayscale using the Y channel of the YIQ colour space.
    return image.rgb2yiq(slice_.tensors[:3]).tensors[0]


def block_to_slice(args):
    """
    Main program code. Controls program flow, handles command-line arguments.

    """
    cnf = args.config
    if os.path.isfile(cnf):
        cnf = general.load_configurations(cnf)
    else:
        raise FileNotFoundError(f"The provided configuration file "
                                f"does not exist: {args.config}")

    # Override tissue block and brain slice file paths in the configuration
    # file with those provided on the command line.
    if args.block and os.path.isfile(args.block):
        cnf["block"]["file"] = args.block
    if args.slice and os.path.isfile(args.slice):
        cnf["slice"]["file"] = args.slice
    if args.init:
        if len(args.init) > 1:
            y, x = args.init
            cnf["regparams"]["sites"] = np.atleast_2d([float(y), float(x)]).tolist()
        elif os.path.isfile(args.init[0]):
            sites = np.loadtxt(args.init[0])
            cnf["regparams"]["sites"] = np.atleast_2d(sites).tolist()
        else:
            raise ValueError("Invalid insertion site specification.")
    if args.out:
        cnf["general"]["outputdir"] = args.out

    # Override verbose option in configurations
    cnf["general"]["verbose"] = args.verbose

    # Run registration script
    run(cnf)


def create_cli(parser):
    """
    Sets up the CLI argument parser instance.

    """
    parser.add_argument("--block", metavar="image", default=None, type=str,
                        help="Tissue block photo", required=False)
    parser.add_argument("--slice", metavar="image", default=None, type=str,
                        help="Brain slice photo", required=False)
    parser.add_argument("--init", metavar=("y", "x"), nargs="+",
                        help="Insertion site", default=None, required=False)
    parser.add_argument("--out", metavar="dir", default=None, type=str,
                        help="Output directory", required=False)
    parser.add_argument("--config", metavar="cnf_file.yml", default=None,
                        type=str, help="Configuration file", required=True)
    parser.add_argument("-v", "--verbose", default=False, action="store_true",
                        help="Print status messages to the command line",
                        required=False)

    return parser


def main(*args):
    """ Main program code. """

    parser = argparse.ArgumentParser(
        prog="block_to_slice",
        description="Registers a tissue block photograph to "
                    "a corresponding brain slice photograph.")
    parser = create_cli(parser)

    if args:
        block_to_slice(parser.parse_args(args))
    else:
        parser.print_help()


# Program execution starts here
if __name__ == "__main__":
    main(*sys.argv[1:])
