#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


"""
This script uses TIRL to perform histology-to-histology registration.
The script was adapted from Stage 1 (2D histology-to-block registration).
The main difference is that normalised mutual information is used throughout
the linear stages instead of MIND. This is to accommodate feature-less images
that are different on the microscopic scale (e.g. consecutive histology slides
with CD68 immunohistochemistry). MIND works better if the objects in the
images are identical, like in Stage 2 (block-to-slice registration).

"""

__tirlscript__ = "mnd.h2h"


# DEPENDENCIES

import os
import sys
import logging
import argparse
import warnings
import numpy as np
from attrdict import AttrMap
from math import radians, degrees
from skimage.measure import label, regionprops


# TIRL IMPORTS

import tirl.settings as ts
from tirl.beta import beta_function

from tirl.chain import Chain
from tirl.tfield import TField
from tirl.timage import TImage
from tirl.cost.mi import CostMI
from tirl.cost.mind import CostMIND
from tirl.optimisation.optnl import OptNL
from tirl.optimisation.optgroup import OptimisationGroup
from tirl.optimisation.gnoptdiff import GNOptimiserDiffusion
from tirl.regularisation.diffusion import DiffusionRegulariser

from tirl.transformations.scale import TxScale, TxIsoScale
from tirl.transformations.rotation import TxRotation2D
from tirl.transformations.translation import TxTranslation
from tirl.transformations.affine import TxAffine
from tirl.transformations.displacement import TxDisplacementField


# TIRLSCRIPT IMPORTS

from tirlscripts import srchash
from tirlscripts.oxford.mnd import __version__
from tirlscripts.oxford.scriptutils import general, inout, image


# DEFINITIONS

from tirl.constants import *

# Rotation search: number of best initialisations to test
N_BEST = 3
# File extension of the snapshot images
SNAPSHOT_EXT = "png"
# NumPy print formatting
np.set_printoptions(precision=4)
# Largest image scaling
default_scale = 1


# IMPLEMENTATION

def run(cnf=None, **options):
    """
    Runs TIRL histology-to-histology registration.

    :param cnf:
        Configuration file. If no file is specified (default), the default
        parameters will be used that are embedded in this script (see
        Definitions above). Instead of a file, a dictionary with suitable
        content may also be specified.
    :type cnf: Union[str, dict, None]
    :param options:
        Overriding configuration parameters.
    :type options: Any

    """
    # Load script configuration
    if cnf is not None:
        if isinstance(cnf, dict):
            cnf = dict(cnf)
        elif isinstance(cnf, str):
            cnf = general.load_configurations(cnf)
        else:
            raise TypeError(
                f"Unrecognised configuration format: {cnf.__.class__.__name__}")
    cnf.update(options)
    global p
    global logger
    p, logger = general.initialise_script(**cnf)
    p.logger = logger.name  # avoid globals
    logger.debug(f"tirlscripts mnd library version: {__version__}")
    logger.debug(f"histology_to_histology.py SHA1: {srchash(__file__)}")

    # Load and configure input images
    if p.fixed.export is True:
        ext = ts.EXTENSIONS["TImage"]
        p.fixed.export = os.path.join(p.general.outputdir, f"fixed.{ext}")
    try:
        fixed = inout.load_histology(scope=globals(), **p.fixed)
    except:
        fixed = inout.load_image(scope=globals(), **p.fixed)
    finally:
        fixed.rule = None

    if p.moving.export is True:
        ext = ts.EXTENSIONS["TImage"]
        p.moving.export = os.path.join(p.general.outputdir, f"moving.{ext}")
    try:
        moving = inout.load_histology(scope=globals(), **p.moving)
    except:
        moving = inout.load_image(scope=globals(), **p.moving)
    finally:
        moving.rule = None

    # Having loaded both images, perform actions on the fixed histology image
    # prior to registration, unless it was loaded from a TImage.
    # Actions are user-defined functions within the TIRL namespace. Actions
    # can be chain-loaded to perform preparatory analysis steps on the images
    # before registration begins.
    isalternative = p.fixed.file.lower().endswith(
        (ts.EXTENSIONS["TImage"], ts.EXTENSIONS["TIRLObject"]))
    if not isalternative:
        fixed = image.perform_image_operations(
            fixed, *p.preprocessing.fixed, scope=globals(), other=moving,
            cnf=p)
        # Initialise registration frame
        fixed.centralise(weighted=True)

    # Perform actions on the moving histology image prior to registration,
    # unless it was loaded from a TImage file.
    isalternative = p.moving.file.lower().endswith(
        (ts.EXTENSIONS["TImage"], ts.EXTENSIONS["TIRLObject"]))
    if not isalternative:
        moving = image.perform_image_operations(
            moving, *p.preprocessing.moving, scope=globals(),
            other=fixed, cnf=p)
        moving.centralise(weighted=True)

    # Run the registration routine
    try:
        if str(p.general.direction).lower() == "forward":
            register(fixed, moving, p)
        elif str(p.general.direction).lower() == "backward":
            register(moving, fixed, p)
        else:
            raise ValueError(f"Invalid registration direction: "
                             f"{p.general.direction}")
    except Exception as exc:
        logger.error(exc.args)
        logger.fatal(f"The registration terminated with an exception.")
        raise exc
    else:
        logger.fatal("The registration was completed successfully.")
    finally:
        del moving
        del fixed


def labkmeans(timg, **kwargs):
    """
    Segments the foreground (tissue) in a histological slide and sets it as the
    TImage mask.

    This method was tested on slides that were stained with DAB + haematoxylin,
    and scanned with a bright white background. The method was also effectively
    removing the shadow effect close to the slide edge.

    """
    from skimage.color import rgb2lab
    from sklearn.cluster import KMeans
    from skimage.exposure import rescale_intensity
    imdata = np.asarray(
        rescale_intensity(timg.dataview(VOXEL_MAJOR)[..., :3],
                          out_range=np.uint8), dtype=np.uint8)
    lab_a = rgb2lab(imdata)[..., 1]
    X = lab_a.reshape(-1, 1)
    km = KMeans(n_clusters=2, random_state=0).fit(X)
    kc = km.cluster_centers_
    mask = km.labels_.reshape(timg.vshape)
    if kc[0] > kc[1]:       # make sure that the lower intensity is labeled 0
        mask = 1 - mask
    return mask


def initialise_transformations(fixed, p):
    """
    Create transformation chain that will be optimised.

    """
    q = p.regparams

    try:
        ix0 = fixed.domain.get_transformation("centralise", index=True)[1]
    except:
        ix0 = max(len(fixed.domain.chain) - 1, 0)

    # Scale
    try:
        tx_scale = fixed.domain.get_transformation("scale")
    except:
        lb = np.asarray(q.init.scale.lb)
        ub = np.asarray(q.init.scale.ub)
        if p.general.isotropic:
            bounds = (float(lb), float(ub))
            tx_scale = TxIsoScale(
                float(q.init.scale.x0), dim=2, bounds=bounds, name="scale")
        else:
            tx_scale = TxScale(*q.init.scale.x0, bounds=(lb, ub), name="scale")
        # fixed.domain.chain.append(tx_scale)

    # Rotation
    try:
        tx_rotation = fixed.domain.get_transformation("rotation")
    except:
        if str(q.init.rotation.mode).lower() == "deg":
            lb = radians(float(q.init.rotation.lb))
            ub = radians(float(q.init.rotation.ub))
        else:
            lb = float(q.init.rotation.lb)
            ub = float(q.init.rotation.ub)
        tx_rotation = TxRotation2D(
            float(q.init.rotation.x0), mode=q.init.rotation.mode,
            bounds=(lb, ub), name="rotation")
        # fixed.domain.chain.append(tx_rotation)

    # Translation
    try:
        tx_trans = fixed.domain.get_transformation("translation")
    except:
        lb = np.asarray(q.init.translation.lb)
        ub = np.asarray(q.init.translation.ub)
        tx_trans = TxTranslation(
            *q.init.translation.x0, bounds=(lb, ub), name="translation")
        # fixed.domain.chain.append(tx_trans)

    # Affine
    try:
        tx_affine, ix = fixed.domain.get_transformation("affine", index=True)
        ix = len(fixed.domain.chain) - 1
    except:
        x0 = np.asarray(q.init.affine.x0).reshape((2, 3))
        lb = np.asarray(q.init.affine.lb)
        ub = np.asarray(q.init.affine.ub)
        tx_affine = TxAffine(x0, bounds=(lb, ub), name="affine")
        # fixed.domain.chain.append(tx_affine)

    # Append linear transformations to the domain of the fixed image
    linear_chain = Chain(tx_rotation, tx_scale, tx_trans, tx_affine)
    domain = fixed.domain[:ix0 + 1]
    domain.chain.extend(linear_chain)

    # Nonlinear
    try:
        tx_nonlinear = fixed.domain.get_transformation("nonlinear")
    except:
        x0 = float(q.init.nonlinear.x0) * np.ones((2, *fixed.vshape))
        if q.init.nonlinear.lb is None:
            lb = None
        else:
            lb = float(q.init.nonlinear.lb) * np.ones_like(x0)
        if q.init.nonlinear.ub is None:
            ub = None
        else:
            ub = float(q.init.nonlinear.ub) * np.ones_like(x0)
        field = TField(
            x0, taxes=(0,), copy=False, domain=domain[:], order=TENSOR_MAJOR)
        tx_nonlinear = TxDisplacementField(
            field, bounds=(lb, ub), name="nonlinear", mode=NL_REL)
        # fixed.domain.chain.append(tx_nonlinear)

    # Return the full transformation chain
    return fixed.domain.chain[:ix0 + 1] + Chain(*linear_chain, tx_nonlinear)


def register(fixed, moving, cnf):
    """
    Runs the four registration stages: rotation search, rigid, affine, and
    non-linear. The function has no return value. The transformation chain is
    attached to the Domain of the fixed TImage object and is optimised in situ.

    :param fixed:
        fixed image (to which the chain is attached)
    :type fixed: TImage
    :param moving:
        moving image (that defines the coordinate space that the mapping
        is into)
    :type moving: TImage
    :param cnf: all configuration options
    :type cnf: dict or AttrMap

    """
    p = AttrMap(cnf)
    q = p.regparams
    logger = logging.getLogger(p.logger)

    # Create transformation chain (does not change the fixed image)
    # rotation -> scale -> translation -> affine -> nonlinear
    logger.info("Initialising transformation chain...")
    chain = initialise_transformations(fixed, p)
    fixed.domain = fixed.domain[:0]
    logger.info("Transformation chain has been initialised.")

    # Set the first part of the chain
    fixed.domain.chain.extend(chain[:-2])

    # Generate output: initial alignment
    fixed.save(os.path.join(
        p.general.outputdir, "fixed.timg"), overwrite=True)
    fixed.snapshot(os.path.join(
        p.general.outputdir, f"fixed.{SNAPSHOT_EXT}"), overwrite=True)
    moving.save(os.path.join(
        p.general.outputdir, "moving.timg"), overwrite=True)
    moving.snapshot(os.path.join(
        p.general.outputdir, f"moving.{SNAPSHOT_EXT}"), overwrite=True)

    # Rotation search
    if "rotation" in p.general.stages:
        logger.info("Starting rotation search...")
        rotation_search2d(fixed, moving, p)
        logger.info("Completed rotation search.")
        # Generate output
        fixed.save(os.path.join(
            p.general.outputdir, "fixed1_rotation.timg"), overwrite=True)
        moving.evaluate(fixed.domain).snapshot(os.path.join(
            p.general.outputdir, f"moving1_rotation.{SNAPSHOT_EXT}"),
            overwrite=True)
    else:
        logger.info("Rotation search was skipped.")

    # Rigid registration
    if "rigid" in p.general.stages:
        logger.info("Starting rigid registration...")
        rigid2d(fixed, moving, p)
        logger.info("Completed rigid registration.")
        # Generate output
        fixed.save(os.path.join(
            p.general.outputdir, "fixed2_rigid.timg"), overwrite=True)
        moving.evaluate(fixed.domain).snapshot(os.path.join(
            p.general.outputdir, f"moving2_rigid.{SNAPSHOT_EXT}"),
            overwrite=True)
    else:
        logger.info("Rigid registration was skipped.")

    # Affine registration
    fixed.domain.chain.append(chain[-2])
    if "affine" in p.general.stages:
        logger.info("Starting affine registration...")
        affine2d(fixed, moving, p)
        logger.info("Completed affine registration.")
        # Generate output
        fixed.save(os.path.join(
            p.general.outputdir, "fixed3_affine.timg"), overwrite=True)
        moving.evaluate(fixed.domain).snapshot(os.path.join(
            p.general.outputdir, f"moving3_affine.{SNAPSHOT_EXT}"),
            overwrite=True)
    else:
        logger.info("Affine registration was skipped.")

    # Non-linear registration
    tx_nonlinear = chain[-1]
    tx_nonlinear.domain.chain = fixed.domain.chain[:]
    fixed.domain.chain.append(tx_nonlinear)
    if "nonlinear" in p.general.stages:
        logger.info("Starting non-linear registration...")
        diffreg2d(fixed, moving, p)
        logger.info("Completed non-linear registration.")
        # Generate output
        fixed.save(os.path.join(
            p.general.outputdir, "fixed4_nonlinear.timg"), overwrite=True)
        moving.evaluate(fixed.domain).snapshot(os.path.join(
            p.general.outputdir, f"moving4_nonlinear.{SNAPSHOT_EXT}"),
            overwrite=True)
    else:
        logger.info("Non-linear registration was skipped.")

    # Non-linear registration
    if "nonlinear" in p.general.stages:
        logger.info("Starting non-linear registration...")
        diffreg2d_fluid(fixed, moving, p)
        logger.info("Completed non-linear registration.")
        # Generate output
        fixed.save(os.path.join(
            p.general.outputdir, "fixed5_nonlinear.timg"), overwrite=True)
        moving.evaluate(fixed.domain).snapshot(os.path.join(
            p.general.outputdir, f"moving5_nonlinear.{SNAPSHOT_EXT}"),
            overwrite=True)
    else:
        logger.info("Non-linear registration was skipped.")


def rotation_search2d(fixed, moving, cnf):
    """
    Finds the best relative orientation of the images.

    :param fixed: Fixed image.
    :type fixed: TImage
    :param moving: Moving image.
    :type moving: TImage
    :param cnf: all configuration options
    :type cnf: dict or AttrMap

    """
    p = AttrMap(cnf)
    q = p.regparams.rotsearch
    logger = logging.getLogger(p.logger)

    # Part 1: coarse search for N best orientations

    # Coarse search at a predefined scale
    fixed.rescale(float(q.scale), copy=False)
    moving.rescale(float(q.scale), copy=False)
    step = radians(q.coarse)
    tx_rotation = fixed.domain.chain["rotation"]
    lb, ub = tx_rotation.parameters.get_bounds()[0]
    rotations = np.arange(lb, ub, step)
    costvals = []
    for i, angle in enumerate(rotations):
        tx_rotation.parameters.parameters[0] = angle
        tx_rotation.parameters.set_lower_bounds(angle - step / 2)
        tx_rotation.parameters.set_upper_bounds(angle + step / 2)
        try:
            # cost1 = CostMIND(moving, fixed, normalise=True, kernel=MK_FULL,
            #                  ignore_masked_edges=True)()
            cost1 = CostMI(moving, fixed, normalise=True, bins=32)()
        except ValueError:
            cost1 = np.inf
        finally:
            logger.debug(f"{degrees(angle)} deg: {cost1}")
            costvals.append([cost1, angle])
    else:
        costvals = np.asarray(costvals)

    # Test the best N initial rotations
    n_best = max(min(N_BEST, len(rotations)), 1)
    best_angles = costvals[np.argsort(costvals[:, 0]), 1][:n_best].ravel()
    logger.info(f"The {n_best} best initialisation angles: "
                f"{np.rad2deg(best_angles)} deg")

    # Part 2: fine-tune the rotation parameter by simultaneous additional
    # optimisation of translation and scaling, starting from the best three
    # orientations.
    tx_scale = fixed.domain.chain["scale"]
    tx_translation = fixed.domain.chain["translation"]
    og = OptimisationGroup(tx_rotation, tx_scale, tx_translation)
    scale_orig = tx_scale.parameters.parameters.copy()
    translation_orig = tx_translation.parameters.parameters.copy()
    costvals = []
    for angle in best_angles:
        tx_rotation.parameters.parameters[0] = angle
        tx_rotation.parameters.set_lower_bounds(angle - step / 2)
        tx_rotation.parameters.set_upper_bounds(angle + step / 2)
        tx_rotation.parameters.unlock()
        tx_scale.parameters.parameters[:] = scale_orig.copy()
        tx_translation.parameters.parameters[:] = translation_orig.copy()
        # Rescale to the same predefined resolution as above
        fixed.rescale(float(q.scale), copy=False)
        moving.rescale(float(q.scale), copy=False)
        # Set cost function
        # cost1 = CostMIND(moving, fixed, maskmode="and", normalise=True,
        #          ignore_masked_edges=True)
        cost1 = CostMI(moving, fixed, maskmode="and", normalise=True)
        # Start optimisation
        logger.info(f"Co-optimising scale and translation at "
                    f"{degrees(angle)} deg...")

        OptNL(og, cost1, method="LN_BOBYQA", visualise=q.visualise,
              xtol_abs=q.xtol_abs, xtol_rel=q.xtol_rel, step=q.opt_step,
              logger=logger, normalised=True)()
        costvals.append((cost1(), og.parameters[:]))

    # Find best initialisation based on cost
    best_params = min(costvals, key=lambda r: r[0])[1]
    logger.info(f"Best parameters after rotation search: {best_params}")
    og.set(best_params)
    tx_rotation.parameters.set_lower_bounds(best_params[0] - step / 2)
    tx_rotation.parameters.set_upper_bounds(best_params[0] + step / 2)
    tx_rotation.parameters.unlock()

    # Return to full resolution after the rotation search
    fixed.rescale(default_scale, copy=False)
    moving.rescale(default_scale, copy=False)


def rigid2d(fixed, moving, cnf):
    """
    Optimises rigid-body parameters and scaling.

    """
    p = AttrMap(cnf)
    q = p.regparams.rigid
    logger = logging.getLogger(p.logger)

    # Scaling-smoothing iteration
    for i, (sc, sm) in enumerate(zip(q.scaling, q.smoothing)):
        logger.debug(f"Scale: {sc}, smoothing: {sm} px...")
        # Prepare images for the current iteration
        fixed.rescale(1. / sc, copy=False)
        moving.rescale(1. / sc, copy=False)
        fixed_smooth = fixed.smooth(sm, copy=True)
        moving_smooth = moving.smooth(sm, copy=True)
        # Prepare co-optimised transformations
        tx_rotation = fixed_smooth.domain.chain["rotation"]
        tx_scale = fixed_smooth.domain.chain["scale"]
        tx_translation = fixed_smooth.domain.chain["translation"]
        og = OptimisationGroup(tx_rotation, tx_scale, tx_translation)
        lb, ub = og.get_bounds().T
        lb = lb - np.finfo(lb.dtype).eps
        ub = ub + np.finfo(lb.dtype).eps
        og.set_bounds(lb, ub)
        # Set cost function
        cost = CostMI(moving_smooth, fixed_smooth, normalise=True)
        # cost = CostMIND(moving_smooth, fixed_smooth, normalise=True,
        #                 ignore_masked_edges=True)
        # Start optimisation
        OptNL(og, cost, method="LN_BOBYQA", visualise=q.visualise,
              xtol_abs=q.xtol_abs, xtol_rel=q.xtol_rel, step=q.opt_step,
              logger=logger, normalised=True)()
        # Transfer optimised transformations to the non-smoothed images
        fixed.domain = fixed_smooth.domain
        moving.domain = moving_smooth.domain
        del fixed_smooth
        del moving_smooth
    else:
        # Restore full resolution of the images
        fixed.rescale(default_scale, copy=False)
        moving.rescale(default_scale, copy=False)


def affine2d(fixed, moving, cnf):
    """
    Optimises a 6-DOF affine transformation after rigid alignment has been
    fine-tuned.

    """
    p = AttrMap(cnf)
    q = p.regparams.affine
    logger = logging.getLogger(p.logger)

    # Scaling-smoothing iterations
    for i, (sc, sm) in enumerate(zip(q.scaling, q.smoothing)):
        logger.debug(f"Scale: {sc}, smoothing: {sm} px...")
        # Prepare images for the current iteration
        fixed.rescale(1. / sc, copy=False)
        moving.rescale(1. / sc, copy=False)
        fixed_smooth = fixed.smooth(sm, copy=True)
        moving_smooth = moving.smooth(sm, copy=True)
        # Prepare transformation to optimise
        tx_affine = fixed_smooth.domain.chain["affine"]
        # Set cost function
        cost = CostMI(moving_smooth, fixed_smooth, normalise=True)
        # cost = CostMIND(moving_smooth, fixed_smooth, normalise=True,
        #                 ignore_masked_edges=True)
        # Start optimisation
        OptNL(tx_affine, cost, method="LN_BOBYQA",
              xtol_rel=q.xtol_rel, xtol_abs=q.xtol_abs,
              visualise=q.visualise, step=q.opt_step, logger=logger,
              normalised=True)()
        # Transfer optimised transformations to the non-smoothed images
        fixed.domain = fixed_smooth.domain
        moving.domain = moving_smooth.domain
    else:
        # Restore full resolution of the images
        fixed.rescale(default_scale, copy=False)
        moving.rescale(default_scale, copy=False)


def diffreg2d(fixed, moving, cnf):
    """
    Performs a non-linear registration. The transformation is parameterised as
    a dense displacement field. The cost is MIND, and diffusion regularisation
    is used to create smoothness in the deformation field.

    """
    p = AttrMap(cnf)
    q = p.regparams.nonlinear
    logger = logging.getLogger(p.logger)

    # Scaling-smoothing iteration
    for i, (sc, sm) in enumerate(zip(q.scaling, q.smoothing)):
        logger.debug(f"Scale: {sc}, smoothing: {sm} px...")
        # Prepare images for the current iteration
        fixed.rescale(1 / sc, copy=False, update_chain=False)
        moving.rescale(1 / sc, copy=False, update_chain=False)
        fixed_smooth = fixed.smooth(sm, copy=True)
        moving_smooth = moving.smooth(sm, copy=True)
        # Prepare transformation to optimise
        tx_nonlinear = fixed_smooth.domain.chain[-1]
        # Set cost and regulariser
        cost = CostMIND(moving_smooth, fixed_smooth, sigma=float(q.sigma),
                        truncate=float(q.truncate), kernel=MK_FULL,
                        ignore_masked_edges=True, ignore_masked_regions=True)
        regularisation = DiffusionRegulariser(
            tx_nonlinear, weight=float(q.regweight))
        # Optimise the non-linear transformation
        GNOptimiserDiffusion(
            tx_nonlinear, cost, regularisation, maxiter=int(q.maxiter[i]),
            xtol_rel=q.xtol_rel, xtol_abs=q.xtol_abs, visualise=q.visualise,
            logger=logger)()
        # Transfer optimised transformations to the non-smoothed images
        fixed.domain = fixed_smooth.domain
        moving.domain = moving_smooth.domain
    else:
        # Restore the original resolution of the images
        fixed.rescale(default_scale, copy=False)
        moving.rescale(default_scale, copy=False)


def diffreg2d_fluid(fixed, moving, cnf):
    """
    Performs a non-linear registration. The transformation is parameterised as
    a dense displacement field. The cost is MIND, and diffusion regularisation
    is used to create smoothness in the deformation field.

    """
    p = AttrMap(cnf)
    q = p.regparams.nonlinear
    logger = logging.getLogger(p.logger)

    # Scaling-smoothing iteration
    for i, (sc, sm) in enumerate(zip(q.scaling[1:], q.smoothing[1:])):
        logger.debug(f"Scale: {sc}, smoothing: {sm} px...")
        # Prepare images for the current iteration
        fixed.rescale(1 / sc, copy=False, update_chain=False)
        moving.rescale(1 / sc, copy=False, update_chain=False)
        fixed_smooth = fixed.smooth(sm, copy=True)
        moving_smooth = moving.smooth(sm, copy=True)
        # Prepare transformation to optimise
        tx_nonlinear = fixed_smooth.domain.chain[-1]
        # Set cost and regulariser
        cost = CostMIND(moving_smooth, fixed_smooth,
                        sigma=float(q.sigma),
                        truncate=float(q.truncate), kernel=MK_FULL,
                        ignore_masked_edges=True,
                        ignore_masked_regions=True)
        regularisation = DiffusionRegulariser(
            tx_nonlinear, weight=float(q.regweight)/1.5)
        # Optimise the non-linear transformation
        GNOptimiserDiffusion(
            tx_nonlinear, cost, regularisation,
            maxiter=int(q.maxiter[i]),
            xtol_rel=q.xtol_rel, xtol_abs=q.xtol_abs,
            visualise=q.visualise,
            logger=logger)()
        # Transfer optimised transformations to the non-smoothed images
        fixed.domain = fixed_smooth.domain
        moving.domain = moving_smooth.domain
    else:
        # Restore the original resolution of the images
        fixed.rescale(default_scale, copy=False)
        moving.rescale(default_scale, copy=False)


                            # AUXILIARY FUNCTIONS

# These functions may be used within other functions of the script. Their usage
# outside this script (via importing from this module) is discouraged. They are
# included here to prevent too frequent code repetitions.

def rgb2hsv(x):
    assert x.shape[-1] == 3, "The input must be RGB."
    _x = x / 255
    _x = _x.reshape(-1, 3)
    c_min = np.min(_x, axis=-1)
    c_max = np.max(_x, axis=-1)
    delta = c_max - c_min
    r, g, b = _x.reshape(-1, 3).T
    r = r.ravel()
    g = g.ravel()
    b = b.ravel()
    i2 = c_max == r
    i3 = c_max == g
    i4 = c_max == b

    # Calculate Hue
    h = np.zeros(r.size)
    h[i2] = 60 * ((g[i2] - b[i2]) / delta[i2] % 6)
    h[i3] = 60 * ((b[i3] - r[i3]) / delta[i3] + 2)
    h[i4] = 60 * ((r[i4] - g[i4]) / delta[i4] + 4)

    # Calculate Saturation
    s = np.where(np.isclose(c_max, 0), 0, delta / c_max)

    # Calculate Value
    v = c_max

    x_hsv = np.stack((h.ravel(), s.ravel(), v.ravel()), axis=-1)
    return x_hsv


def match_resolution(img, **kwargs):
    """
    Resamples histology image to match the resolution of another 2D image.

    """
    p = AttrMap(kwargs.get("cnf"))
    logger = logging.getLogger(p.logger)
    other = kwargs.get("other")

    res = other.resolution
    factors = np.divide(img.resolution, res)
    logger.info(f"Resampling 2D image by factors {tuple(factors)}...")
    img = img.rescale(*factors, copy=True)
    img.resmgr.reset()
    # img.storage = MEM
    # img.domain.storage = MEM
    logger.info(f"Shape of the 2D image after resampling: {img.shape}.")

    # Save low-resolution image
    # fp, fn = os.pathology.split(p.histology.file)
    # fn, ext = os.pathology.splitext(fn)
    # filename = os.pathology.join(p.general.outputdir, f"{fn}_lowres.{SNAPSHOT_EXT}")
    # img.snapshot(filename, overwrite=True)
    # logger.info(f"Saved low-resolution snapshot of the 2D image to: {filename}")

    return img


def fixed_preprocessing(fixed, **kwargs):
    """
    Preprocess the fixed histology image.

    """
    # Convert to grayscale using the Y channel of the YIQ colour space.
    return image.rgb2yiq(fixed.tensors[:3]).tensors[0]


def moving_preprocessing(moving, **kwargs):
    """
    Preprocess the moving histology image.

    """
    # Convert to grayscale using the Y channel of the YIQ colour space.
    return image.rgb2yiq(moving.tensors[:3]).tensors[0]


def hsv_sat(timg, **kwargs):
    from skimage.color import rgb2hsv
    hsv = rgb2hsv(timg.dataview(VOXEL_MAJOR))
    return hsv[..., 1]


def hsv_invsat(timg, **kwargs):
    from skimage.color import rgb2hsv
    hsv = rgb2hsv(timg.dataview(VOXEL_MAJOR))
    return 1 - hsv[..., 1]


def dilated_object_mask(timg, **kwargs):
    from skimage.color import rgb2hsv
    hsv = rgb2hsv(timg.dataview(VOXEL_MAJOR))
    objmask = hsv[..., 2] > 0.1
    from skimage.morphology import binary_dilation
    objmask = binary_dilation(objmask.astype(np.uint8), selem=np.ones((20, 20)))
    return objmask.astype(np.float32)


def dilated_tissue_mask(timg, **kwargs):
    from skimage.color import rgb2hsv
    hsv = rgb2hsv(timg.dataview(VOXEL_MAJOR))
    v = hsv[..., 2]
    from skimage.filters import threshold_otsu
    objmask = hsv[..., 2] < threshold_otsu(v)
    from skimage.morphology import binary_dilation
    objmask = binary_dilation(objmask.astype(np.uint8), selem=np.ones((20, 20)))
    return objmask.astype(np.float32)


@beta_function
def mask_roi_defects(fixed, moving, p):

    # Highlight non-matching areas between affine-registered images
    tmp = moving.evaluate(fixed.domain)
    tmp = np.where(tmp.data > 0.05, 1, 0)
    binary = np.where(fixed.data > 0.05, 1, 0)
    totalarea = np.count_nonzero(binary)
    # print(totalarea)
    area = p.area * totalarea if p.area < 1 else p.area
    # print(area)
    binary[tmp == 1] = 0
    del tmp

    # Discard small regions, that should actually drive non-linear registration

    # Area filter
    labels = label(binary, background=0, connectivity=binary.ndim)
    regions = regionprops(labels)
    # print(sorted([region["area"] for region in regions])[::-1])
    large_region_labels = [
        region["label"] for region in regions if region["area"] > area]
    binary[...] = 0
    for value in large_region_labels:
        binary[labels == value] = 1

    labels = label(binary, background=0, connectivity=binary.ndim)
    n_defects = np.max(labels)
    # TImage(labels).preview()

    # Warn the user about ROI defects
    if n_defects:
        warnings.warn(f"Number of ROI defects found: {int(n_defects)}.")

    # Add the identified ROI defects to the moving histology mask
    binary = 1 - binary
    bmask = moving.mask.data
    # bmask[bmask == 0.1] = 0
    moving.mask = bmask * binary


def pad(timg, **kwargs):
    """
    Zero-pads the image by 1/6 of the image shape on both ends along each
    spatial dimension.

    """
    padding = []
    for dim in timg.vshape:
        padding.append((dim // 6, dim // 6))
    padding = tuple(padding)
    offset = TxTranslation([-pad[0] for pad in padding], name="padding")
    lpad = ((0, 0),) * timg.tdim
    if timg.order == TENSOR_MAJOR:
        padding = lpad + padding
    else:
        padding = padding + lpad
    imdata = np.pad(timg.data, padding, mode="constant")
    padded = TImage(imdata, taxes=timg.taxes)
    padded.domain.internal = offset + timg.domain.internal
    padded.domain.external = timg.domain.external
    return padded


def edgemask(img, **kwargs):
    mask = np.ones(img.vshape, dtype=ts.DEFAULT_FLOAT_TYPE)
    mask[[0, -1], :] = 0
    mask[:, [0, -1]] = 0
    return mask


def prescale(timg, **kwargs):
    logger.info("Prescaling the input images...")
    scales = p.regparams.rigid.scaling[::-1]
    for sc in scales:
        logger.info(f"Scaling to 1/{sc}...")
        timg.rescale(1 / sc)
    else:
        timg.rescale(default_scale)
    # else:
    #     global default_scale
    #     default_scale = 1 / scales[0]


def histology_to_histology(args):
    """
    Main program code. Controls program flow, handles command-line arguments.

    """
    cnf = args.config
    if os.path.isfile(cnf):
        cnf = general.load_configurations(cnf)
    else:
        raise FileNotFoundError(f"The provided configuration file "
                                f"does not exist: {args.config}")

    # Override histology file paths in the configuration file with
    # those provided on the command line.
    if args.fixed and os.path.isfile(args.fixed):
        cnf["fixed"]["file"] = args.fixed
    if args.moving and os.path.isfile(args.moving):
        cnf["moving"]["file"] = args.moving
    if args.out:
        cnf["general"]["outputdir"] = args.out

    # Override verbose option in configurations
    cnf["general"]["verbose"] = args.verbose

    # Run registration script
    run(cnf)


def create_cli(parser):
    """
    Sets up the CLI argument parser instance.

    """
    parser.add_argument("--fixed", metavar="image",
                        help="Fixed histology slide", default=None, type=str,
                        required=False)
    parser.add_argument("--moving", metavar="image",
                        help="Moving histology slide", default=None, type=str,
                        required=False)
    parser.add_argument("--out", metavar="dir",
                        help="Output directory", default=None, type=str,
                        required=False)
    parser.add_argument("--config", metavar="cnf_file.yml",
                        help="configuration file", default=None, type=str,
                        required=True)
    parser.add_argument("-v", "--verbose", default=False, action="store_true",
                        help="Print status messages to the command line",
                        required=False)

    return parser


def main(*args):
    """ Main program code. """

    parser = argparse.ArgumentParser(
        prog="histology_to_histology",
        description="Registers a histology slide to another histology slide.")
    parser = create_cli(parser)

    if args:
        histology_to_histology(parser.parse_args(args))
    else:
        parser.print_help()


if __name__ == "__main__":
    main(*sys.argv[1:])
