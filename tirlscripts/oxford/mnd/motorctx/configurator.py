#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

"""
Configuration base class factory and task-specific derivatives.

"""


# DEPENDENCIES

import os
import yaml
import numpy as np
from copy import deepcopy
from tirl.utils import verify_fname


# TIRL IMPORTS

import tirl
from tirl.chain import Chain
from tirl.tirlobject import TIRLObject


# IMPLEMENTATION

class Configuration(dict):
    """
    Configuration --

    Base class factory for generating templated configurations.

    """

    def __init__(self, template):
        super().__init__()  # empty dict
        if isinstance(template, str) and os.path.isfile(template):
            with open(template, "r") as fp:
                cnf = yaml.safe_load(fp)
            self.update(cnf)
        elif isinstance(template, dict):
            self.update(deepcopy(template))
        else:
            raise TypeError(
                f"Expected a file or a dict, got "
                f"{template.__class__.__name__} instead."
            )

    def save(self, fn, overwrite=False):
        if verify_fname(fn, overwrite):
            with open(fn, "w") as fp:
                fp.write(yaml.safe_dump(self, sort_keys=False, indent=2,
                                        default_flow_style=False))


class MotorConfiguration(Configuration):
    """
    MotorConfiguration --

    Specific factory class to generate configurations for the registration of
    motor cortical blocks in the MND dataset based on a template and variable
    inputs.

    """

    def __init__(self, imfile, volfile, centre, normal,
                 imres=None, immask=None, volres=None, volmask=None,
                 outputdir=None):
        """
        Initialisation of the MotorConfiguration object.

        :param imfile: path to the 2D input image
        :type imfile: str
        :param volfile: path to the 3D input image
        :type volfile: str
        :param centre:
            (x, y, z) coordinates to define the initial position
            of the 2D input image
        :type centre: np.ndarray
        :param normal:
            unit normal vector to define the initial orientation
            of the 2D input image
        :type normal: np.ndarray
        :param imres:
            Isotropic or anisotropic resolution of the 2D input image. If None
            (default), the resolution will be unchanged relative to the
            template value.
        :type imres: float or np.ndarray or None
        :param immask:
            Masking specification for the 2D image file. One of the following:
            1) path to a mask file
            2) {thr: float, uthr: float} thresholds to create an inclusion mask
            3) custom function name (the function must be implemented in the
            script that processes the configurations)
            4) None: the template configuration remains in place
        :type immask: str or dict or None
        :param volres:
            Isotropic or anisotropic resolution of the 3D input image. If None
            (default), the resolution will be unchanged relative to the
            template value.
        :type volres: float or np.ndarray or None
        :param volmask:
            Masking specification for the 3D image file. One of the following:
            1) path to a mask file
            2) {thr: float, uthr: float} thresholds to create an inclusion mask
            3) custom function name (the function must be implemented in the
            script that processes the configurations)
            4) None: the template configuration remains in place
        :type volmask: str or dict
        :param outputdir:
            Path to the output directory. The specified directory will be
            created if it does not exist. If None (default), a new directory
            will be created next the 2D input image file with the base name of
            the image file and a ".reg" suffix.
        :type outputdir: str

        """

        # Create base configuration
        template = os.path.join(os.path.dirname(__file__), "motorctx.yml")
        super().__init__(template)

        # Update input slice
        self["slice"]["file"] = imfile
        if immask is not None:
            self.set_mask("slice", immask)
        if imres is not None:
            self["slice"]["resolution"] = imres

        # Update input volume
        self["volume"]["file"] = volfile
        if volmask is not None:
            self.set_mask("volume", volmask)
        if volres is not None:
            self["volume"]["resolution"] = volres

        # Update initialisation
        self["regparams"]["stage_1"]["slab"]["centre"] = np.asarray(centre)
        self["regparams"]["stage_1"]["slab"]["normal"] = np.asarray(normal)
        self["regparams"]["stage_1"]["slab"]["offset"] = 0

        # Update output location
        fpath, bname = os.path.split(imfile)
        bname, ext = os.path.splitext(bname)
        default_outputdir = os.path.join(fpath, bname, ".reg")
        outputdir = outputdir or default_outputdir
        if not os.path.isdir(outputdir):
            os.makedirs(outputdir)
        self["general"]["outputdir"] = outputdir
        self["general"]["logfile"] = os.path.join(outputdir, "logfile.log")
        self["general"]["paramlogfile"] = os.path.join(outputdir, "params.log")

    def set_mask(self, inputimage, mask):
        if inputimage not in ("slice", "volume"):
            raise NameError(f"Expected 'slice' or 'volume', got {inputimage}.")
        if isinstance(mask, str):
            if os.path.isfile(mask):
                self[inputimage]["mask"]["file"] = mask
            else:
                self[inputimage]["mask"]["function"] = mask
        elif isinstance(mask, dict):
            self[inputimage]["mask"]["automask"]["thr"] = mask["thr"]
            self[inputimage]["mask"]["automask"]["uthr"] = mask["uthr"]


class TwoLineInitialiser(object):
    """
    TwoLineInitialiser --

    Configuration factory helper class that calculates the exact initialisation
    point and orientation of an image based on two parallel lines and an
    orthogonal offset.

    """

    def __init__(self, line1, l1chain, line2, l2chain, offset=None):
        """
        Initialisation of the TwoLineInitialiser object.

        :param line1:
            (x, y) coordinates of the first line (array or file)
            Note that x=0, y=0 is the top-left corner of the image,
            x is the horizontal, and y is the vertical coordinate
            (default output from Fiji).
        :type line1: np.ndarray or str
        :param l1chain:
            Pixel-to-mm transformation chain for Line 1.
        :type l1chain: Chain or Domain or TImage or TField or str
        :param line2:
            (x, y) coordinates of the second line, (array or file)
            Note that x=0, y=0 is the top-left corner of the image,
            x is the horizontal, and y is the vertical coordinate
            (default output from Fiji).
        :type line2: np.ndarray or str
        :param l2chain:
            Pixel-to-mm transformation chain for Line 2.
        :type l2chain: Chain or Domain or TImage or TField or str
        :param offset:
            Distance (signed) of the initial position from the plane that is
            defined by the two lines.
        :type offset: float

        """
        super().__init__()

        # Transform estimates in the target coordinate system
        self.line1 = self.transform(line1, l1chain)
        self.line2 = self.transform(line2, l2chain)
        self.offset = float(offset)

    @staticmethod
    def transform(line, chain):
        line = TwoLineInitialiser.get_line(line)
        chain = TwoLineInitialiser.get_chain(chain)
        return chain.map(line)

    @staticmethod
    def get_line(line) -> np.ndarray:
        if isinstance(line, str):
            if os.path.isfile(line):
                line = np.loadtxt(line)[:, ::-1]
            else:
                raise FileNotFoundError(f"{line} was not found.")
        else:
            line = np.asarray(line)
        return line

    @staticmethod
    def get_chain(tobj) -> Chain:
        # Load TIRLObject from file
        if isinstance(tobj, str):
            if os.path.isfile(tobj):
                tobj = tirl.load(tobj)
            else:
                raise FileNotFoundError(f"{tobj} was not found.")

        # Tuple or list of transformations
        elif isinstance(tobj, (list, tuple)):
            if all(hasattr(t, "__transformation__") for t in tobj):
                tobj = Chain(tobj)
            else:
                raise TypeError(f"Invalid chain specification: "
                                f"{tobj.__class__.__name__}")

        # At this point, the input is certainly a TIRLObject, so return a
        # Chain object if possible.
        if isinstance(tobj, TIRLObject):
            # Transformation
            if hasattr(tobj, "__transformation__"):
                return Chain([tobj])
            # Chain
            if hasattr(tobj, "__chain__"):
                return tobj.__chain__()
            # Domain
            elif hasattr(tobj, "chain"):
                return tobj.chain
            # TImage or TField
            elif hasattr(tobj, "domain"):
                return tobj.domain.chain
            # Other TIRLObject
            else:
                raise TypeError(f"Invalid chain specification: "
                                f"{tobj.__class__.__name__}")

    @property
    def cog(self) -> np.ndarray:
        """
        Calculates the centre of gravity of all input points. Each point set is
        weighted by the number in the other point set to minimise annotation
        bias.

        """
        # TODO: consider representing the lines by splines instead of weighting
        n1 = self.line1.shape[0]
        n2 = self.line2.shape[0]
        n = n1 + n2
        w1 = n1 / n
        w2 = n2 / n
        return np.sum(np.vstack((w2 * self.line1, w1 * self.line2)), axis=0)

    @property
    def normal(self) -> np.ndarray:
        """
        Calculates the unit normal vector of the initialisation plane.
        By convention, the direction of the normal vector is such that the
        first coordinate (left-to-right in RAS MRI space) is always
        non-negative, i.e. it points to the right. This convention simplifies
        the sign of the offset parameter for tissue blocks that are sampled
        radially from the central sulcus in both hemispheres.

        """
        from sklearn.decomposition import PCA
        pca = PCA(n_components=3)
        pca.fit(np.vstack((np.line1, np.line2)))
        normal = pca.components_[-1, :].ravel()
        # Convention: point to the right in RAS space
        if normal[0] < 0:
            normal *= -1
        return normal / np.linalg.norm(normal)

    @property
    def init(self) -> np.ndarray:
        """
        The initial position is calculated as the centre of gravity plus the
        signed offset along the signed normal vector.

        """
        return self.cog + self.offset * self.normal
