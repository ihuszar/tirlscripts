#!/usr/bin/env python

# DEFINITIONS


# DEPENDENCIES

import os
import re
import tirl
import numpy as np
import pandas as pd


# IMPLEMENTATION

def generate_input_table_for_subject(subjdir, outputfile):
    """
    Generates a table of inputs for 3D insertion-site determination.
    Each record consists of the following fields:

    Annotation_ID*, AnnotationPath, Subject_ID, Slice_ID, RegisteredSlicePath
    (* denotes the primary key)

    """
    table = []
    header = ["AnnotationPath", "Subject_ID", "Slice_ID", "RegisteredSlicePath"]
    regdir = "/Volumes/alpha/mnd/"

    for root, dirs, files in os.walk(subjdir):
        for f in files:
            if f.endswith(".txt") and ("coords" in f):
                try:
                    f_annotation = os.path.join(root, f)
                    subject_id = re.search(r"(CTL|MND)\d{2}", root).group(0)
                    slice_id = re.findall(r"\d{2}[AB]", root)[0]
                    slice_no = slice_id[:-1]
                    slice_face = "A" if slice_id[-1] == "A" else "P"
                    f_slice = os.path.join(
                        regdir, f"{subject_id}", "slices", f"{slice_no}",
                        f"{slice_face}", "to_vol.hybrid", "slice.timg")
                    # Replace with equivalent slice, e.g. 07A -> 06B
                    if not os.path.isfile(f_slice):
                        slice_face = "A" if slice_face == "P" else "P"
                        if slice_face == "A":
                            slice_no = int(slice_no) + 1
                        else:
                            slice_no = int(slice_no) - 1
                        slice_no = f"{slice_no:02d}"
                        f_slice = os.path.join(
                            regdir, f"{subject_id}", "slices", f"{slice_no}",
                            f"{slice_face}", "to_vol.hybrid", "slice.timg")
                        if not os.path.isfile(f_slice):
                            print(f"No registered image: {f_slice}")
                            f_slice = None
                    record = (f_annotation, subject_id, slice_id, f_slice)
                except Exception as exc:
                    print(exc.args)
                    continue
                else:
                    table.append(record)
    else:
        table = pd.DataFrame(table, columns=header)
        table.index.name = "Annotation_ID"
        if os.path.exists(outputfile):
            os.remove(outputfile)
        table.to_csv(outputfile)

    return table


def generate_input_tables():
    """ Generates input tables for all subjects. """

    # Generate input table
    rootdir = "/Volumes/iris/MND/motorctx/"
    subjects = [d for d in os.listdir(rootdir)
                if d.startswith("MND") or d.startswith("CTL")]
    for subject_id in subjects:
        try:
            print(f"Generating an input table of 3D annotations "
                  f"for subject {subject_id}...")
            subjdir = os.path.join(rootdir, subject_id)
            csvout = os.path.join(subjdir, "annotations3D.csv")
            generate_input_table_for_subject(subjdir, csvout)
        except Exception as exc:
            print(f"ERROR: {exc.args}")


def map_annotations(df):
    """ Maps the XY pixel coordinates of the annotations to MRI space. """

    annotations = dict()
    for ix, record in df.iterrows():
        try:
            xy = np.loadtxt(record["AnnotationPath"])[:, ::-1]
            slc = tirl.load(record["RegisteredSlicePath"])
            mri_xy = slc.domain.map_voxel_coordinates(xy)
            annotation_id = record["Annotation_ID"]
            annotations[annotation_id] = mri_xy
        except Exception:
            continue
    else:
        return annotations


def main():
    """ Main program code. """

    generate_input_tables()
    exit()

    f_annotations = "/Volumes/iris/MND/motorctx/MND17/annotations3D.csv"
    table = pd.read_csv(f_annotations)
    print(table.head())
    annotations = map_annotations(table)
    print(annotations)


if __name__ == "__main__":
    main()
