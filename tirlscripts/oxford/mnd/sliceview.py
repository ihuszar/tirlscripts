#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


__tirlscript__ = "mnd.sliceview"


# DEPENDENCIES

import os
import sys
import argparse


# TIRL IMPORTS

import tirl


# IMPLEMENTATION

def sliceview(args):
    """
    Main program code.

    """
    from mayavi import mlab
    from scipy.spatial import Delaunay

    images = []
    if args.slice is not None:
        for imfile in args.slice:
            if os.path.isfile(imfile):
                images.append(tirl.load(imfile))
            else:
                raise FileNotFoundError(f"Image file not found: {imfile}")

    fig = mlab.figure(1, bgcolor=(1, 1, 1), fgcolor=(0.5, 0.5, 0.5))

    # Volume
    if os.path.isfile(args.volume):
        if args.volume.endswith(".timg"):
            mri = tirl.load(args.volume)
        else:
            from tirl.timage import TImage
            mri = TImage(args.volume, external="sform")
        mri.normalise()
        points = mri.domain.get_voxel_coordinates()
        x, y, z = points.T
        x = x.reshape(mri.data.shape)
        y = y.reshape(mri.data.shape)
        z = z.reshape(mri.data.shape)
        mlab.volume_slice(x, y, z, mri.data, colormap="gray")
        mlab.pipeline.volume(mlab.pipeline.scalar_field(mri.data),
                             vmin=0, vmax=0.1)
    else:
        mri = None

    # Label
    if os.path.isfile(args.label):
        if args.label.endswith(".timg"):
            label = tirl.load(args.label)
        else:
            from tirl.timage import TImage
            label = TImage(args.label, external="sform")
        label.normalise()
        mlab.pipeline.volume(mlab.pipeline.scalar_field(mri.data),
                             vmin=0, vmax=1)
    else:
        label = None

    # Slices
    for img in images:
        if hasattr(img, "__tfield__"):
            img = img.tensors[0]
        if hasattr(img, "domain"):
            domain = img.domain
        elif hasattr(img, "__domain__"):
            domain = img
        else:
            raise AttributeError(f"Input has no Domain: {img}")
        tri = Delaunay(domain.get_voxel_coordinates())
        points = domain.get_physical_coordinates()
        if mri is not None:
            points = mri.domain.map_physical_coordinates(points)
        tmesh = mlab.triangular_mesh(*points.T, tri.simplices, colormap="gray")
        if hasattr(img, "__tfield__"):
            tmesh.mlab_source.scalars = img.data.reshape(-1)
            tmesh.actor.mapper.scalar_visibility = True

    mlab.show()


def create_cli():
    """
    Creates command-line interface.

    """
    usage = """./sliceview --slice <slice> [slices ...] --volume <volume>"""
    descr = "View registered slices in a 3D window with the volume."

    parser = argparse.ArgumentParser(
        prog="sliceview", usage=usage, description=descr)
    parser.add_argument("--slice", metavar="timg", help="Registered 2D TImage.",
                        nargs="+", required=False, type=str)
    parser.add_argument("--volume", metavar="volume", help="3D NIfTI/TImage.",
                        required=False, type=str, default="")
    parser.add_argument("--label", metavar="volume", help="3D NIfTI/TImage.",
                        required=False, type=str, default="")

    return parser


def main(*args):
    """ Main program code. """

    parser = create_cli()
    if args:
        sliceview(parser.parse_args(args))
    else:
        parser.print_help()


if __name__ == "__main__":
    main(*sys.argv[1:])
