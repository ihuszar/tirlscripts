# Changelog

### 23 Nov 2022 - v1.1

- Slice orientation is now only optimised on the trusted set of slices


### 13 Nov 2022 -- v1.0

- Project-specific version v1.0.0 assigned to the current state of the library
