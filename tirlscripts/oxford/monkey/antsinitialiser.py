#!/usr/bin/env python


import logging
import numpy as np
from collections import namedtuple
from sklearn.cluster import KMeans

import matplotlib.pyplot as plt

import tirl
from tirl.timage import TImage

from tirl.transformations.affine import TxAffine
from tirl.transformations.scale import TxIsoScale
from tirl.transformations.euler import TxEulerAngles
from tirl.transformations.translation import TxTranslation

from tirl.cost.mind import CostMIND

from tirl.optimisation.optnl import OptNL
from tirl.optimisation.optgroup import OptimisationGroup


RegChain = namedtuple("RegChain", ["centralise", "scale", "rotation",
                                   "shift", "translation", "affine"])
LOGGER = "affine3d"


def initialise():
    """ Initialise inputs from file. """

    # Load inputs
    histostack = TImage(
        "/Volumes/xenon/UrsSchuffelgen/tirlreg/histostack.nii.gz",
        external="affine", storage="hdd")
    print("Histology stack:", histostack)
    histostack.mask = TImage(
        "/Volumes/xenon/UrsSchuffelgen/tirlreg/histostack_mask.nii.gz").data
    plt.imshow(histostack.data[:, :, 64], cmap="gray")
    plt.show()
    mri = TImage(
        "/Volumes/xenon/UrsSchuffelgen/tirlreg/mri_left.nii.gz",
        external="affine", storage="hdd")
    print("MRI:", mri)
    plt.imshow(mri.data[:, :, 64], cmap="gray")
    plt.show()

    # Prescale inputs
    for res in (0.5, 1):
        print(f"Resampling histology stack to {res} mm resolution...")
        histostack.rescale(0.05 / res, copy=False)
        d = histostack.vshape[-1]
        plt.imshow(histostack.data[:, :, d // 2], cmap="gray")
        plt.show()
        print(f"Resampling MRI to {res} mm resolution...")
        mri.rescale(0.5 / res, copy=False)
        d = mri.vshape[-1]
        plt.imshow(mri.data[:, :, d // 2], cmap="gray")
        plt.show()

    # Initialise transformations
    shift = TxTranslation(*mri.domain.pcentre(), name="mri_centre")
    trans = TxTranslation(0, 0, 0, name="translation")
    scale = TxIsoScale(1, 3, name="scale")
    rot = TxEulerAngles(0, 0, 0, order="zyx", mode="deg", name="rotation")
    affine = TxAffine(np.eye(3, 4), name="affine")
    histostack.centralise()
    histostack.domain.external += [scale, rot, shift, trans, affine]
    histostack.resmgr.sync()
    histostack.maskmgr.sync()
    histostack.save(
        "/Volumes/xenon/UrsSchuffelgen/tirlreg/histostack.timg", True)
    mri.resmgr.sync()
    mri.save("/Volumes/xenon/UrsSchuffelgen/tirlreg/mri.timg", True)


def reinit(img):
    regchain = RegChain(*img.domain.external[1:])
    regchain.translation.parameters.parameters[:] = [0, 0, 0]
    regchain.translation.parameters.set_lower_bounds([-10, -10, -10])
    regchain.translation.parameters.set_upper_bounds([10, 10, 10])
    regchain.scale.parameters.parameters[:] = 1
    regchain.scale.parameters.set_lower_bounds(0.8)
    regchain.scale.parameters.set_upper_bounds(1.2)
    regchain.rotation.parameters.parameters[:] = [0, 0, 0]
    regchain.rotation.parameters.set_lower_bounds([-0.7, -0.7, -0.7])
    regchain.rotation.parameters.set_upper_bounds([0.7, 0.7, 0.7])
    regchain.affine.parameters.parameters[:] = np.eye(3, 4).ravel()
    regchain.affine.parameters.set_lower_bounds(
        [0.9, -0.1, -0.1, -5, -0.1, 0.9, -0.1, -5, -0.1, -0.1, 0.9, -5])
    regchain.affine.parameters.set_upper_bounds(
        [1.1, 0.1, 0.1, 5, 0.1, 1.1, 0.1, 5, 0.1, 0.1, 1.1, 5])
    img.resmgr.sync()
    return regchain


def register(hs, mri):

    logger = logging.Logger(name=LOGGER)
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(0)

    # Define alternative initialisations
    n_inits = 10
    mask = (mri.data.ravel() > 0.5)
    km = KMeans(n_inits)
    pc = mri.domain.get_physical_coordinates()[mask, :]
    km.fit(pc)
    inits = np.subtract(km.cluster_centers_, mri.domain.pcentre())
    inits = np.vstack((*inits, [-10, 22, 8]))
    print(inits)

    # fig = plt.figure()
    # ax = fig.add_subplot(projection="3d")
    # ax.scatter(*np.vstack(inits).T)
    # plt.show()

    costs = []
    for init in inits:
        print("Current initialisation:", init)
        regchain = reinit(hs)
        regchain.translation.parameters.parameters[:] = init
        regchain.translation.parameters.set_lower_bounds(np.subtract(init, 10))
        regchain.translation.parameters.set_upper_bounds(np.add(init, 10))
        regchain.translation.parameters.unlock()
        hs.resmgr.sync()
        for sc in (2,):
            print(f"Rescaling histology stack to 1/{sc}...")
            hs.rescale(1 / sc, copy=False)
            hs.storage = "mem"
            print("Centre at ", hs.domain.pcentre())
            print(f"Rescaling MRI to 1/{sc}...")
            mri.rescale(1 / sc, copy=False)
            mri.storage = "mem"
            cost = CostMIND(mri, hs, normalise=True)
            regchain = RegChain(*hs.domain.external[1:])
            print(f"Optimising translation at 1/{sc} scale...")
            OptNL(regchain.translation, cost, method="LN_BOBYQA", step=0.1,
                  xtol_rel=0.01, logger=logger, normalised=True)()
            print(f"Optimising rigid parameters at 1/{sc} scale...")
            rigid = OptimisationGroup(regchain.rotation, regchain.translation)
            current = regchain.translation.parameters.parameters
            regchain.translation.parameters.set_lower_bounds(
                np.subtract(current, 10))
            regchain.translation.parameters.set_upper_bounds(np.add(current, 10))
            regchain.translation.parameters.unlock()
            OptNL(rigid, cost, method="LN_BOBYQA", xtol_rel=0.01, step=0.1,
                  logger=logger, normalised=True)()
            print(f"Optimising similarity parameters at 1/{sc} scale...")
            similarity = OptimisationGroup(
                regchain.scale, regchain.rotation, regchain.translation)
            OptNL(similarity, cost, method="LN_BOBYQA", xtol_rel=0.01, step=0.1,
                  logger=logger, normalised=True)()
            hs.resmgr.sync()
            mri.resmgr.sync()
        else:
            cost = CostMIND(mri, hs, normalise=True)
            costs.append((similarity.parameters[:], cost()))
    else:
        costs = sorted(costs, key=lambda x: x[1])
        print("\n".join(str(item) for item in costs))
        best_init = costs[0][0]
        print(best_init)

    regchain = RegChain(*hs.domain.external[1:])
    similarity = OptimisationGroup(regchain.scale, regchain.rotation, regchain.translation)
    similarity.parameters[:] = best_init
    # print("Best init:", np.add(best_init, mri.domain.pcentre()))

    hs.save("/Volumes/xenon/UrsSchuffelgen/tirlreg/histostack_registered.timg",
            overwrite=True)
    import nibabel as nib
    affine = hs.domain.external.reduce()[0].matrix
    print(affine)
    affine = np.vstack((*affine, [0, 0, 0, 1]))
    hsvol = nib.load("/Volumes/xenon/UrsSchuffelgen/tirlreg/histostack.nii.gz")
    nifti = nib.Nifti1Image(hsvol.get_fdata(), affine)
    nib.save(nifti, "/Volumes/xenon/UrsSchuffelgen/tirlreg/histostack_registered.nii.gz")


def main():
    """ Main program code. """

    # initialise()

    # Load inputs
    hs = tirl.load("/Volumes/xenon/UrsSchuffelgen/tirlreg/histostack.timg")
    hs = TImage(hs.resmgr.get(1))
    hs.mask = None
    print(hs)
    d = hs.vshape[-1]
    plt.imshow(hs.data[:, :, d // 2], cmap="gray")
    plt.show()
    mri = tirl.load("/Volumes/xenon/UrsSchuffelgen/tirlreg/mri.timg")
    mri = TImage(mri.resmgr.get(0))
    print(mri)
    d = mri.vshape[-1]
    plt.imshow(mri.data[:, :, d // 2], cmap="gray")
    plt.show()

    register(hs, mri)


if __name__ == "__main__":
    main()

