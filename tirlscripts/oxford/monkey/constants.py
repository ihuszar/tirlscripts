#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import math
from types import SimpleNamespace
from collections import namedtuple


# DEFINITIONS

ChainTemplate = namedtuple("ChainTemplate", [
    "c_cent", "c_isoscale", "c_scale", "c_rot", "c_offset", "c_affine",
    "s_cent", "s_isoscale", "s_scale", "s_rot", "s_offset", "s_affine",
    "g_embed", "s_warp", "s_distance", "g_rot", "g_trans", "g_warp"
])

STAGES = SimpleNamespace(**{
    "rigid2d": dict(
        script="rigid2d.py", tag="rigid2d", descr="2D rigid registration"),
    "rigid3d": dict(
        script="rigid3d.py", tag="rigid3d", descr="3D rigid registration"),
    "warp"   : dict(
        script="warp.py", tag="warp", descr="non-linear registration")
})

DEFAULT_SCHEDULER = None
DEFAULT_QUEUE = "short.q"

LOGFORMAT = "%(asctime)s Process-%(process)d %(levelname)s (%(lineno)d) " \
            "- %(message)s"

DATEFORMAT = "[%Y-%m-%d %H:%M:%S]"

# 3D SLICE ROTATIONS

CORONAL = [0, 1, 0]
CORONAL_FLIP = [0, -1, 0]
SAGITTAL = [1, 0, 0]
SAGITTAL_FLIP = [-1, 0, 0]
AXIAL = [0, 0, 1]
AXIAL_FLIP = [0, 0, -1]

SLICE_ROTATIONS = {
    "RI": (0, CORONAL),
    "SR": (-90, CORONAL),
    "LS": (180, CORONAL),
    "IL": (90, CORONAL),
    "RS": (0, CORONAL_FLIP),
    "IR": (-90, CORONAL_FLIP),
    "LI": (180, CORONAL_FLIP),
    "SL": (90, CORONAL_FLIP),
    "IA": (0, SAGITTAL),
    "PI": (-90, SAGITTAL),
    "SP": (180, SAGITTAL),
    "AS": (90, SAGITTAL),
    "SA": (0, SAGITTAL_FLIP),
    "PS": (-90, SAGITTAL_FLIP),
    "IP": (180, SAGITTAL_FLIP),
    "AI": (90, SAGITTAL_FLIP),
    "RA": (0, AXIAL),
    "PR": (-90, AXIAL),
    "LP": (180, AXIAL),
    "AL": (90, AXIAL),
    "RP": (0, AXIAL_FLIP),
    "AR": (-90, AXIAL_FLIP),
    "LA": (180, AXIAL_FLIP),
    "PL": (90, AXIAL_FLIP)
}


class DirTree(object):

    def __init__(self, rootdir, table, stages=2):

        if not os.path.exists(rootdir):
            os.makedirs(rootdir)
        self.root = rootdir
        try:
            self.slicepad = math.floor(math.log10(table["Slice"].max())) + 1
        except KeyError:
            self.slicepad = 4
        try:
            self.cmppad = math.floor(math.log10(table["Component"].max())) + 1
        except KeyError:
            self.cmppad = 1
        self.stagepad = math.floor(math.log10(stages)) + 1

    def __call__(self, slice=None, part=None, cmp=None, stage=None):
        path = self.root
        if slice is not None:
            slice = f"slice-{slice:0{self.slicepad}d}"
            path = os.path.join(path, slice)
        if part is not None:
            part = f"part-{part}"
            path = os.path.join(path, part)
        if cmp is not None:
            cmp = f"cmp-{cmp:0{self.cmppad}d}"
            path = os.path.join(path, cmp)
        if stage is not None:
            stage = f"stage-{stage:{self.stagepad}d}"
            path = os.path.join(path, stage)

        if not os.path.exists(path):
            os.makedirs(path)

        return path