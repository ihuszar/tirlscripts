#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2021 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Updated: 24 November 2022


# SHBASECOPYRIGHT


# DESCRIPTION

"""
densewarp.py --

This is a satellite script for multislice_to_volume.py that performs the
non-linear registration step on a single slice. This is a modified version of
the original warp.py method, as it is optimising a pixelwise defined dense
displacement field. The satellite script allows the parallel execution of this
registration step on a computing cluster, where jobs are managed by a scheduler.

"""


# DEPENDENCIES

import os
import sys
import dill
import logging
import numpy as np
import multiprocessing as mp
from attrdict import AttrMap
from datetime import datetime


# TIRL IMPORTS

import tirl
import tirl.settings as ts
from tirl.constants import *
from tirl.tfield import TField
from tirl.timage import TImage
from tirl.cost.mi import CostMI
from tirl.cost.mind import CostMIND
from tirl.optimisation.gnoptdiff import GNOptimiserDiffusion
from tirl.transformations.displacement import TxDisplacementField


# TIRSCRIPT IMPORTS

from tirlscripts.oxford.monkey.constants import *


# DEFINITIONS

NORMALISE = True
LOGGER = None
MK_STAR = "star"
logger = logging.getLogger(LOGGER)
np.set_printoptions(precision=4, suppress=True)


# IMPLEMENTATION

def create_logger(loggername, logfile, cnf):
    """
    Creates a logger that sends log messages to the standard output and
    into a file.

    """
    global logger
    logger = logging.getLogger(loggername)
    logger.handlers = []  # delete any existing handlers to avoid duplicate logs
    logger.setLevel(1)
    formatter = logging.Formatter(fmt=LOGFORMAT, datefmt=DATEFORMAT)

    # Make sure that the logfile can be created
    logdir = os.path.dirname(os.path.abspath(logfile))
    if not os.path.isdir(logdir):
        os.makedirs(logdir)

    # Redirect all logs of interest to the logfile
    loglevel = cnf["loglevel"]
    fh = logging.FileHandler(logfile, mode="a", delay=False)
    if isinstance(loglevel, str):
        level = getattr(logging, loglevel.upper())
    elif isinstance(loglevel, (int, float)):
        level = int(loglevel)
    elif not loglevel:
        level = logging.INFO
    else:
        raise TypeError(f"Invalid logging level: {loglevel}")
    fh.setLevel(level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Redirect only warnings/errors to the standard output, unless "verbose"
    ch = logging.StreamHandler()
    if cnf["verbose"]:
        ch.setLevel(level)
    else:
        ch.setLevel(logging.WARNING)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


def create_cost_object(source, target, cnf):
    """
    Creates a script-wide uniform cost object based on the general cost
    function type setting.

    """
    p = AttrMap(cnf)

    if str(p.cost).lower() == "mind":
        return CostMIND(source, target, normalise=True, sigma=1.0,
                        truncate=1.5, kernel=MK_STAR)
    elif str(p.cost).lower() == "mi":
        return CostMI(source, target, normalise=True, bins=32)
    else:
        raise ValueError(f"Unrecognised cost function specification: "
                         f"{p.cost}")


def absolute_step(lb, ub, step):
    di = ub - lb
    di[~np.isfinite(di)] = 2
    if np.any(di <= 0):
        raise AssertionError("No space for optimisation.")
    return np.multiply(0.5 * di, step)


def create_warp(img):
    """

    """
    warp, ix = img.domain.get_transformation("warp", index=True)
    if isinstance(warp, TxDisplacementField):
        return warp

    # Create warp
    domain = img.domain[:ix]
    field = TField(domain=domain, tshape=(2,), order=TENSOR_MAJOR,
                   dtype=ts.DEFAULT_FLOAT_TYPE, fill_value=0)
    return TxDisplacementField(field, name="warp", invertible=True,
        mode=NL_ABS, vectorder="xy")


def load_image(imfile):
    image = tirl.load(imfile)
    ext = ts.EXTENSIONS["Domain"]
    dname = f"latest_domain.{ext}"
    latest = os.path.join(os.path.dirname(imfile), dname)
    image.domain = tirl.load(latest)
    image.resmgr.sync()
    return image


def warp_worker(job, outputfile):
    """
    Warps a slice/component within the slicing plane.

    """
    cmpdir = os.path.dirname(outputfile)
    (slice_no, part, cmp), imfile, r_file, gparams, cnf = job
    t = AttrMap(cnf["regparams"]["warp"])

    # Load images from disk
    img = load_image(imfile)
    vol = tirl.load(r_file)
    img_layer = img.resmgr.active
    vol_layer = vol.resmgr.active

    # Smoothing iteration
    stage_cost = []

    # Scaling-smoothing iterations
    warp = None
    states = tuple(zip(t.slice_resolution, t.volume_resolution,
                       t.slice_smoothing, t.volume_smoothing))

    for sm_no, (sres, vres, ssm, vsm) in enumerate(states):
        sres = img.resolution if sres is None else sres
        ssc = np.abs(np.mean(np.divide(img.layers[0].resolution, sres)))
        logger.info(f"Smoothing: sres={sres}, ssm={ssm}px, "
                    f"vres={vres}, vsm={vsm}vx.")
        vres = vol.resolution if vres is None else vres
        vsc = np.abs(np.mean(np.divide(vol.layers[0].resolution, vres)))
        logger.debug(f"Slice scaling factor: 1 : {1 / ssc}")
        img.rescale(ssc, copy=False)
        img.storage = MEM
        img_smooth = img.smooth(ssm, copy=True)
        logger.debug(f"Volume scaling factor: 1 : {1 / vsc}")
        vol.rescale(vsc, copy=False)
        # vol.storage = MEM
        if vsm != 0:
            vol_smooth = vol.smooth(vsm, copy=True)
        else:
            vol_smooth = vol

        # Add warp to the chain
        old_warp, ix = \
            img_smooth.domain.get_transformation("warp", index=True)
        warp = create_warp(img_smooth)
        img_smooth.domain.external[ix] = warp
        # Recreate cost object, because the chain was modified
        lineardomain = img_smooth.domain.copy()
        lineardomain.external = \
            [tx for tx in lineardomain.external if tx.name != "warp"]
        vol_2d = vol_smooth.evaluate(lineardomain)
        cost = create_cost_object(vol_2d, img_smooth, cnf)
        logger.info(f"Initial cost: {cost()}")

        # --------------------- OPTIMISE NEW WARP -------------------- #

        # Regularisation
        if t.regweight > 0:
            from tirl.regularisation.diffusion import DiffusionRegulariser
            reg = DiffusionRegulariser(warp, weight=t.regweight, logger=logger)
            cost_items = [cost, reg]
        else:
            cost_items = [cost]

        # Optimisation
        GNOptimiserDiffusion(warp, *cost_items, maxiter=int(t.maxiter[sm_no]),
            xtol_rel=t.xtol_rel, xtol_abs=t.xtol_abs, visualise=t.visualise,
            logger=logger)()

        # Create snapshot
        timestamp = datetime.now().strftime("%Y%m%d_%H%M%S_%f")
        fn = os.path.join(
            cmpdir,
            f"tx-densewarp_"
            f"time-{timestamp}."
            f"{ts.TIMAGE_DEFAULT_SNAPSHOT_EXT}")
        vol_smooth.evaluate(img_smooth.domain).snapshot(fn, overwrite=True)

        # <--- POINT OPTIMISATION SEQUENCE

        # Record the final cost at the current smoothing level
        if sm_no + 1 == len(states):
            stage_cost.append(cost())
            logger.info(f"Final cost @ [ssm={ssm}px vsm={vsm}vx]: "
                        f"{stage_cost[-1]}")

        # Copy optimised transformations before creating a new image from
        # the original. [This may no longer be necessary.]
        img.domain = img_smooth.domain
        img.resmgr.sync()

    # Restore the input resolution
    img.resmgr.set(img_layer)
    vol.resmgr.set(vol_layer)

    # Generate output
    # (optimised domain)
    img.domain.save(outputfile, overwrite=True)

    # (reference image resampled onto the optimised domain)
    vs = vol.evaluate(img.domain)
    vs.normalise()
    s_file = os.path.join(cmpdir, f"tx-densewarp_mri.png")
    vs.snapshot(s_file, overwrite=True)

    return 0


def main(jobfile, outputfile, logfile):
    """ Main program code. """

    with open(jobfile, "rb") as fp:
        job = dill.load(fp)

    (slice_no, part, cmp), imfile, r_file, gparams, cnf = job
    pid = mp.current_process().pid

    global LOGGER
    LOGGER = f"slice-{slice_no}_part-{part}_cmp-{cmp}_tx-densewarp_proc-{pid}"
    create_logger(LOGGER, logfile, cnf)
    cmd = " ".join(sys.argv)
    logger.info(f"The program started with the following command: {cmd}")
    warp_worker(job, outputfile)


if __name__ == "__main__":
    main(*sys.argv[1:])
