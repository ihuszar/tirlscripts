#!/usr/bin/env python

# DEPENDENCIES

import os
import numpy as np
from glob import glob

from skimage.color import rgb2yiq
from skimage.measure import label, regionprops
from skimage.morphology import convex_hull_image
from skimage.morphology.binary import binary_dilation
from skimage.filters.thresholding import threshold_otsu


# TIRL IMPORTS

import tirl
from tirl.tfield import TField
from tirl.timage import TImage


# IMPLEMENTATION

def main(imfiles):
    """ Main program code. """

    for imfile in imfiles:
        img = TImage(imfile)
        # impng = imfile.replace(".nii.gz", ".tif")
        # img.snapshot(impng, overwrite=True)
        # print(f"Saved: {impng}")
        yiq = rgb2yiq(img.data)
        grayscale = yiq[..., 0]
        mask = grayscale < threshold_otsu(grayscale)
        mask = area_filter(mask, area=500)
        mask = mask.astype(np.uint8) * 255
        maskfile = imfile.replace(".tif", "_mask.png")
        TImage(mask).snapshot(maskfile, overwrite=True)
        mask = convex_hull_image(mask)
        # for i in range(5):
        #     mask = binary_dilation(mask, footprint=np.ones((5, 5)))
        mask = mask.astype(np.uint8) * 255
        # maskfile = imfile.replace(".nii.gz", "_mask.png")
        maskfile = imfile.replace(".tif", "_cmask.png")
        TImage(mask).snapshot(maskfile, overwrite=True)
        print(f"Saved: {maskfile}")


def area_filter(img, area=1000):
    labelimg = label(img.astype(np.uint8), background=0)
    exclude = []
    for region in regionprops(labelimg):
        if region.area < area:
            exclude.append(region.label)
    for value in exclude:
        labelimg[labelimg == value] = 0
    return np.where(labelimg > 0, 255, 0)


if __name__ == "__main__":
    imfiles = sorted(glob("/Volumes/xenon/UrsSchuffelgen/"
                          "data/slices/tif/*.tif"))
    main(imfiles)
