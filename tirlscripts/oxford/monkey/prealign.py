#!/usr/bin/env python

# DEPENDENCIES

import os
import sys
import dill
import logging
import argparse
import subprocess
import pandas as pd
import multiprocessing as mp
from functools import partial


# TIRL IMPORTS

import tirl
import tirl.utils as tu
import tirl.settings as ts
from tirl.timage import TImage
from tirl.transformations.rotation import TxRotation2D
from tirl.transformations.translation import TxTranslation
from tirl.transformations.scale import TxScale, TxIsoScale


# DEFINITIONS

LOGGER = "tirlscripts.oxford.monkey.prealign.main"
LOGLEVEL = 5
DEBUG = False
SCHEDULER = None  # "slurm"
QUEUE = "veryshort.q"


# IMPLEMENTATION


def main(args):
    """ Main program code. """

    # Load inputs (sorted)
    table = pd.read_csv(args.table)

    # Generate working directory
    outdir = args.output or os.path.dirname(args.table)
    if os.path.isfile(outdir):
        raise FileExistsError(
            "A file with the name of the output directory already exists.")
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    # Create logger
    logfile = args.log or os.path.join(outdir, "logfile.log")
    logger = create_logger(
        name=LOGGER, logfile=logfile, level=LOGLEVEL, verbose=args.verbose
    )

    # Save input files to disk
    save_inputs(table, outdir, logger)

    # Pairwise rigid alignment
    pairwise_rigid_alignment(table, outdir, logger, n_cpu=args.cpu)

    # Assume initial positions


    # Run individual slice-to-volume registrations

    # Conclude


def create_parser():

    parser = argparse.ArgumentParser()
    parser.add_argument("table", metavar="csv", type=str,
                        help="Table of input slices.")
    parser.add_argument("--output", metavar="dir", type=str,
                        required=False, help="Output directory.")
    parser.add_argument("--log", metavar="file", type=str,
                        required=False, help="Log file.")
    parser.add_argument("--verbose", action="store_true", default=False,
                        help="Print log messages to the standard output.")
    parser.add_argument("--cpu", metavar="n", type=int, default=1,
                        help="Number of parallel processes.")
    return parser


def create_logger(name, logfile, level, verbose):
    """
    Creates a logger that sends log messages to the standard output and
    into a file.

    """
    global logger

    logger = logging.getLogger(name)
    logger.handlers = []  # delete any existing handlers to avoid duplicate logs
    logger.setLevel(1)
    formatter = logging.Formatter(
        fmt='%(asctime)s Process-%(process)d %(levelname)s (%(lineno)d) '
            '- %(message)s',
        datefmt='[%Y-%m-%d %H:%M:%S]')

    # Make sure that the logfile can be created
    logdir = os.path.dirname(logfile)
    if not os.path.isdir(logdir):
        os.makedirs(logdir)

    # Redirect all logs of interest to the logfile
    fh = logging.FileHandler(logfile, mode="a", delay=False)
    if isinstance(level, str):
        level = getattr(logging, level.upper())
    elif isinstance(level, (int, float)):
        level = int(level)
    elif not level:
        level = logging.INFO
    else:
        raise TypeError(f"Invalid logging level: {level}")
    fh.setLevel(level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Redirect only warnings/errors to the standard output, unless "verbose"
    ch = logging.StreamHandler()
    if verbose:
        ch.setLevel(level)
    else:
        ch.setLevel(logging.WARNING)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


def save_inputs(table, outdir, logger):

    ext = ts.EXTENSIONS["TImage"]
    snapext = ts.TIMAGE_DEFAULT_SNAPSHOT_EXT

    for ix, record in table.iterrows():

        # Assemble TImage from its components
        slice_no = int(record["Slice"])
        part = str(record["Part"])
        imname = f"slice-{slice_no:04d}_part-{part}"
        img = TImage(str(record["SliceOriginal"]), name=imname)
        img.resolution = float(record["Resolution"])
        img.mask = TImage(str(record["SliceMask"]))
        img.header["metadata"] = dict(record)

        # Set up transformations
        centre = img.centre(weighted=True)
        c_cent = -TxTranslation(*centre, name="c_centralise")
        c_rot = TxRotation2D(0, mode="deg", name="c_rotation")
        c_trans = TxTranslation(0, 0, name="c_translation")
        img.domain.external = [c_cent, c_rot, c_trans]

        # Save TImage to a working directory
        slicedir = os.path.join(outdir, f"{slice_no:04d}")
        if not os.path.isdir(slicedir):
            os.makedirs(slicedir)
        timgfile = os.path.join(slicedir, imname + f".{ext}")
        img.save(timgfile, overwrite=True)
        logger.info(f"Saved 2D input image: {timgfile}")
        snapfile = os.path.join(slicedir, imname + f".{snapext}")
        img.snapshot(snapfile, overwrite=True)
        logger.info(f"Saved snapshot: {snapfile}")


def file_from_record(record, outdir):
    slice_no = int(record["Slice"])
    part = str(record["Part"])
    imname = f"slice-{slice_no:04d}_part-{part}"
    slicedir = os.path.join(outdir, f"{slice_no:04d}")
    return os.path.join(slicedir, imname + "." + ts.EXTENSIONS["TImage"])


def get_jobs(table, outdir):

    iterator = table.sort_values(by="Slice").iterrows()
    previous = file_from_record(next(iterator)[1], outdir=outdir)
    for ix, record in iterator:
        current = file_from_record(record, outdir=outdir)
        job = previous, current
        previous = current
        yield job


def pairwise_rigid_alignment(table, outdir, logger, n_cpu):
    """
    Aligns the sorted series of slices in a pairwise fashion, to initialise
    their position and orientation in preparation for the direct
    slice-to-volume registration.

    :param table: table of input slices
    :type table: pd.DataFrame
    :param outdir: output directory root
    :type outdir: str
    :param logger: Logger instance
    :type logging.Logger

    """
    n_cpu = n_cpu % mp.cpu_count() if n_cpu < 0 else int(n_cpu)
    n_cpu = int(min(n_cpu, mp.cpu_count()))

    if n_cpu >= 1:
        rigid2d_worker = partial(
            canonical_worker,
            script="rigid2d.py",
            tag="rigid2d",
            descr="2D rigid registration"
        )
        with mp.pool.ThreadPool(processes=n_cpu) as pool:
            results = pool.map(rigid2d_worker, get_jobs(table, outdir))


def _get_cmd_prefix(scheduler, **kwargs):
    scheduler = str(scheduler).lower() if scheduler else None
    if scheduler == "sge":
        queue = kwargs.get("queue")
        prefix = [f"fsl_sub", f"-q {queue}", f"--extra=\"-sync y\"", "tirl"]
    elif scheduler == "slurm":
        prefix = [f"sbatch", "-W", "tirl"]
    elif scheduler is None:
        prefix = [f"tirl"]
    else:
        raise NameError(f"Unrecognised task scheduler: {scheduler}")
    return prefix


def canonical_worker(job, script, tag, descr):

    # Dump the job details to disk
    srcfile, trgfile = job
    slicedir = os.path.dirname(srcfile)
    fd, jobfile = tu.tmpfile(prefix=f"{tag}_", dir=slicedir, suffix=".job")
    with open(jobfile, "wb") as fp:
        dill.dump(job, fp)

    scriptdir = os.path.dirname(os.path.abspath(__file__))
    fullscript = os.path.join(scriptdir, script)
    bname, ext = os.path.splitext(srcfile)
    domext = ts.EXTENSIONS["Domain"]
    outputfile = os.path.join(slicedir, bname + "_prealign" + f".{domext}")

    # Launch a new task and wait until it's completed
    if DEBUG:
        try:
            externalmodule = __import__(script.replace(".py", ""))
            externalmodule.main(srcfile, trgfile, outputfile)
        except Exception as exc:
            returncode = 1
        else:
            returncode = 0
    else:
        cmd = _get_cmd_prefix(scheduler=SCHEDULER, queue=QUEUE) + \
              [f"{fullscript}", f"{srcfile}", f"{trgfile}", f"{outputfile}"]
        proc = subprocess.run(cmd, env=os.environ.copy())
        returncode = proc.returncode

    if returncode == 0:
        logger.info(f"Successfully completed \"{descr}\" stage "
                    f"for {os.path.basename(srcfile)}.")
    else:
        logger.error(f"There was an error while performing \"{descr}\" stage "
                     f"on {os.path.basename(srcfile)}.")

    # Return the output of the child process
    return outputfile


if __name__ == "__main__":

    parser = create_parser()

    if len(sys.argv) > 1:
        args = parser.parse_args()
        main(args)
    else:
        parser.print_help()
