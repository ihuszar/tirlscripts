#!/usr/bin/env python

import os
import tirl
import logging
import numpy as np
import tirl.settings as ts
from datetime import datetime
from tirl.tfield import TField
from types import SimpleNamespace
from tirl.cost.mind import CostMIND
from tirl.optimisation.gnoptdiff import GNOptimiserDiffusion
from tirl.regularisation.diffusion import DiffusionRegulariser
from tirl.transformations.displacement import TxDisplacementField


# DEFINITIONS

from tirl.constants import *

logger = logging.Logger("logger")
logger.addHandler(logging.StreamHandler())
logger.setLevel(0)

states = tuple(zip(
    [0.5, 1, 0.5],
    [0.5, 1, 0.5],
    [0, 0, 0],
    [0, 0, 0]
))
t = SimpleNamespace(regweight=0.4, maxiter=[100, 100, 100], xtol_abs=0.01,
                    xtol_rel=0.001, visualise=True)
cmpdir = "/home/inhuszar/monkey/registration/slice-236/part-left/" \
         "cmp-1/stage-2/test"


# IMPLEMENTATION

def create_warp(img):

    warp, ix = img.domain.get_transformation("warp", index=True)
    if isinstance(warp, TxDisplacementField):
        return warp

    # Create warp
    domain = img.domain[:ix]
    field = TField(domain=domain, tshape=(2,), order=TENSOR_MAJOR,
                   dtype=ts.DEFAULT_FLOAT_TYPE, fill_value=0)
    return TxDisplacementField(
        field, name="warp", invertible=True, mode=NL_ABS, vectorder="xy")


def main():

    img = tirl.load(
        "/home/inhuszar/monkey/registration/slice-236/part-left/"
        "cmp-1/stage-2/microscopy.timg")
    img.domain = tirl.load(
        "/home/inhuszar/monkey/registration/slice-236/part-left/"
        "cmp-1/stage-2/tx-trans3d_domain.dom")
    vol = tirl.load(
        "/home/inhuszar/monkey/registration/reference/reference.timg")

    img.mask = None

    img_layer = img.resmgr.active
    vol_layer = vol.resmgr.active

    stage_cost = []

    for sm_no, (sres, vres, ssm, vsm) in enumerate(states):
        ssc = np.abs(np.mean(np.divide(0.05, sres)))
        logger.info(f"Smoothing: sres={sres}, ssm={ssm}px, "
                    f"vres={vres}, vsm={vsm}vx.")
        vres = vol.resolution if vres is None else vres
        vsc = np.abs(np.mean(np.divide(0.5, vres)))
        logger.debug(f"Slice scaling factor: 1 : {1 / ssc}")
        img.rescale(ssc, copy=False)
        img.storage = MEM
        img_smooth = img.smooth(ssm, copy=True)
        logger.debug(f"Volume scaling factor: 1 : {1 / vsc}")
        vol.rescale(vsc, copy=False)
        # vol.storage = MEM
        if vsm != 0:
            vol_smooth = vol.smooth(vsm, copy=True)
        else:
            vol_smooth = vol

        # Add warp to the chain
        old_warp, ix = \
            img_smooth.domain.get_transformation("warp", index=True)
        warp = create_warp(img_smooth)
        img_smooth.domain.external[ix] = warp
        # Recreate cost object, because the chain was modified
        lineardomain = img_smooth.domain.copy()
        lineardomain.external = \
            [tx for tx in lineardomain.external if tx.name != "warp"]
        vol_2d = vol_smooth.evaluate(lineardomain)
        vol_2d.storage = MEM
        cost = CostMIND(vol_2d, img_smooth, normalise=True, sigma=0.5,
                        truncate=1.5, kernel=MK_FULL)
        logger.info(f"Initial cost: {cost()}")

        # --------------------- OPTIMISE NEW WARP -------------------- #

        # Regularisation
        if t.regweight > 0:
            reg = DiffusionRegulariser(warp, weight=t.regweight, logger=logger)
            cost_items = [cost, reg]
        else:
            cost_items = [cost]

        # Optimisation
        GNOptimiserDiffusion(warp, *cost_items, maxiter=int(t.maxiter[sm_no]),
                             xtol_rel=t.xtol_rel, xtol_abs=t.xtol_abs,
                             visualise=t.visualise,
                             logger=logger)()

        # Create snapshot
        timestamp = datetime.now().strftime("%Y%m%d_%H%M%S_%f")
        fn = os.path.join(
            cmpdir,
            f"tx-densewarp_"
            f"time-{timestamp}."
            f"{ts.TIMAGE_DEFAULT_SNAPSHOT_EXT}")
        vol_smooth.evaluate(img_smooth.domain).snapshot(fn, overwrite=True)

        # <--- POINT OPTIMISATION SEQUENCE

        # Record the final cost at the current smoothing level
        if sm_no + 1 == len(states):
            stage_cost.append(cost())
            logger.info(f"Final cost @ [ssm={ssm}px vsm={vsm}vx]: "
                        f"{stage_cost[-1]}")

        # Copy optimised transformations before creating a new image from
        # the original. [This may no longer be necessary.]
        img.domain = img_smooth.domain
        img.resmgr.sync()

    # Restore the input resolution
    img.resmgr.set(img_layer)
    vol.resmgr.set(vol_layer)

    result = vol.evaluate(img.domain)
    result.save(os.path.join(cmpdir, "warped.timg"), overwrite=True)
    result.snapshot(os.path.join(cmpdir, "warped.png"), overwrite=True)


if __name__ == "__main__":
    main()
