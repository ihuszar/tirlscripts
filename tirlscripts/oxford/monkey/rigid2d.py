#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______   _____   _____    _
#  |__   __| |_   _| |  __ \  | |
#     | |      | |   | |__) | | |
#     | |      | |   |  _  /  | |
#     | |     _| |_  | | \ \  | |____
#     |_|    |_____| |_|  \_\ |______|
#
# Copyright (C) 2018-2021 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar
# Updated: 31 October 2022


# SHBASECOPYRIGHT


# DESCRIPTION

"""
rigid2d.py --

This is a satellite script for prealign.py that performs the 2D rigid
registration step on consecutive slices. The satellite script allows the
parallel execution of this registration step on a computing cluster, where
jobs are managed by a scheduler.

"""


# DEPENDENCIES

import re
import os
import sys
import logging
import numpy as np
import multiprocessing as mp
from math import radians as rad


# TIRL IMPORTS

import tirl
import tirl.settings as ts
from tirl.constants import *
from tirl.cost.mi import CostMI
from tirl.cost.mind import CostMIND
from tirl.optimisation.optnl import OptNL, OptMethods
from tirl.optimisation.optgroup import OptimisationGroup


# TIRSCRIPT IMPORTS

from tirlscripts.oxford.monkey.constants import *


# DEFINITIONS

LOGLEVEL = 5
VERBOSE = True
cnf = dict(
    rotresolution=1,
    resolution=(0.5, 0.25),
    smoothing=(0, 0),
    ldx=(30, 10, 10),
    udx=(30, 10, 10),
    visualise=False
)

np.set_printoptions(precision=4, suppress=True)


# IMPLEMENTATION

def create_logger(name, logfile, level, verbose):
    """
    Creates a logger that sends log messages to the standard output and
    into a file.

    """
    global logger
    logger = logging.getLogger(name)
    logger.handlers = []  # delete any existing handlers to avoid duplicate logs
    logger.setLevel(1)
    formatter = logging.Formatter(fmt=LOGFORMAT, datefmt=DATEFORMAT)

    # Make sure that the logfile can be created
    logdir = os.path.dirname(os.path.abspath(logfile))
    if not os.path.isdir(logdir):
        os.makedirs(logdir)

    # Redirect all logs of interest to the logfile
    fh = logging.FileHandler(logfile, mode="a", delay=False)
    if isinstance(level, str):
        level = getattr(logging, level.upper())
    elif isinstance(level, (int, float)):
        level = int(level)
    elif not level:
        level = logging.INFO
    else:
        raise TypeError(f"Invalid logging level: {level}")
    fh.setLevel(level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Redirect only warnings/errors to the standard output, unless "verbose"
    ch = logging.StreamHandler()
    if verbose:
        ch.setLevel(level)
    else:
        ch.setLevel(logging.WARNING)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


def rigid2d_worker(srcfile, trgfile, outputfile, logger):
    """
    Optimises the component-specific rotation and in-plane offset.

    """
    # Load image pair
    image = tirl.load(srcfile)
    image = image.tensors.reduce()
    reference = tirl.load(trgfile)
    reference = reference.tensors.reduce()
    # Discard transformations that might be simultaneously optimised elsewhere
    reference.domain.external = reference.domain.external[0]
    img_layer = image.resmgr.active
    ref_layer = reference.resmgr.active
    # reference.storage = MEM

    # Load parameters
    resolutions = cnf["resolution"]
    smoothing = cnf["smoothing"]
    ldx = np.asarray(cnf["ldx"], dtype=float)
    ldx[0] = rad(ldx[0])
    udx = np.asarray(cnf["udx"], dtype=float)
    udx[0] = rad(udx[0])

    # Rotation search
    rotres = cnf["rotresolution"]
    logger.info(f"Starting rotation search at {rotres} mm/px...")
    scaling_factors = np.divide(image.layers[0].resolution, rotres)
    sm_img = image.rescale(np.abs(np.mean(scaling_factors)), copy=True)
    cost = CostMI(reference, sm_img, normalise=True)
    results = []
    for rot in range(0, 360, 5):
        sm_img.domain.external[1].angle = rot
        current_cost = cost()
        results.append((rot, current_cost))
        logger.info(f"Cost at {rot} deg: {current_cost}")
    else:
        best_rotation = min(results, key=lambda r: r[1])[0]
        logger.info(f"Best rotation: {best_rotation} deg")
        sm_img.domain.external[1].angle = best_rotation
        image.domain.external = sm_img.domain.external
        image.resmgr.sync()

    # Scaling-smoothing iteration
    logger.info(f"Starting scaling-smoothing iteration...")
    for resolution, sm in zip(resolutions, smoothing):
        resolution = image.resolution if resolution is None else resolution
        scaling_factors = np.divide(image.layers[0].resolution, resolution)
        sc = np.abs(np.mean(scaling_factors))
        logger.debug(f"Slice scaling factor: 1 : {1 / sc}")
        image.rescale(sc, copy=False)
        image.storage = MEM
        if sm != 0:
            sm_img = image.smooth(sm, copy=True)
            sm_img.domain = image.domain
        else:
            sm_img = image
        c_rot, c_trans = sm_img.domain.external[1:]
        c_rot.parameters.set_bounds(None)
        c_trans.parameters.set_bounds(None)
        og = OptimisationGroup(c_rot, c_trans)
        lb = np.subtract(og.parameters[:], ldx)
        ub = np.add(og.parameters[:], udx)
        og.set_lower_bounds(lb)
        og.set_upper_bounds(ub)
        sm_img.storage = MEM
        try:
            cost = CostMIND(reference, sm_img)
        except Exception:
            pass
        else:
            try:
                OptNL(og, cost, method=OptMethods.LN_BOBYQA, normalised=True,
                      xtol_abs=[0.001, 0.01, 0.01], step=0.1,
                      visualise=cnf["visualise"], logger=logger)()
            except Exception:
                pass

        image.domain.external = sm_img.domain.external
        image.resmgr.sync()

    # Restore the input resolution
    image.resmgr.set(img_layer)
    reference.resmgr.set(ref_layer)

    # Generate output
    # (optimised domain)
    image.domain.save(outputfile, overwrite=True)

    # (reference image resampled onto the optimised domain)
    vs = reference.evaluate(image.domain)
    vs.normalise()

    rslice = int(re.search(r"(?<=slice-)\d{4}", trgfile, re.DOTALL).group(0))
    rpart = str(
        re.search(r"(?<=part-).+?(?=[_.])", trgfile, re.DOTALL).group(0))
    sslice = int(re.search(r"(?<=slice-)\d{4}", srcfile, re.DOTALL).group(0))
    spart = str(
        re.search(r"(?<=part-).+?(?=[_.])", srcfile, re.DOTALL).group(0))
    sname = f"slice-{rslice}_part-{rpart}_on_slice-{sslice}_part-{spart}"
    slicedir = os.path.dirname(srcfile)
    ext = ts.TIMAGE_DEFAULT_SNAPSHOT_EXT
    snapfile = os.path.join(slicedir, sname + f".{ext}")
    vs.snapshot(snapfile, overwrite=True)

    return outputfile


def main(srcfile, trgfile, outputfile):
    """ Main program code. """

    slice_no = int(re.search(r"(?<=slice-)\d{4}", srcfile, re.DOTALL).group(0))
    part = str(re.search(r"(?<=part-).+?(?=[_.])", srcfile, re.DOTALL).group(0))
    pid = mp.current_process().pid
    slicedir = os.path.dirname(srcfile)
    bname = f"slice-{slice_no}_part-{part}_tx-rigid2d"
    logfile = os.path.join(slicedir, bname + ".log")
    LOGGER = bname + f"_proc-{pid}"
    logger = create_logger(LOGGER, logfile, LOGLEVEL, VERBOSE)

    cmd = " ".join(sys.argv)
    logger.info(f"The program started with the following command: {cmd}")

    rigid2d_worker(srcfile, trgfile, outputfile, logger)


if __name__ == "__main__":
    main(*sys.argv[1:])
