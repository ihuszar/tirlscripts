---
#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar

# This is a configuration file for 2D-to-3D histology-to-MRI registration.
# The default values in this file are recommended for mouse brain data.

# Further fields might be added under any section at the user's convenience,
# but removing fields may lead to the breakdown of the registration script.
# YAML formatting conventions apply. Syntax errors may cause a breakdown.
# Metric units have been defined here as millimetres, but as long as all inputs
# are consistent (including the sform of the MRI), they may equally represent
# micrometres, inches, or any other unit of length.


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ HEADER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# Configuration file metadata for descriptive and record-keeping purposes.
header:
  author: [Istvan N Huszar, Amy FD Howard, Silei Zhu, Adele Smart,
           Urs Schuffelgen, Valentina Mione]
  title: 'Histology-to-volume image registration routine'
  description: 'Optimised for pre-clinical images of post-mortem primate brains'


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ GENERAL ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# Configurations to define the context, the flow, and the output of the
# registration script.
general:
  # Operating system: linux or macosx
  system: macos
  # Order of the registration substages
  # Stage 1 - best rigid alignment
  # Stage 2 - best affine alignment
  # Stage 3 - in-plane slice deformations
  # Stage 4 - in-plane + out-of-plane slice deformations
  # Stage 5 - affine re-estimation (discards non-linear deformations)
  # Any substage may be skipped or repeated (e.g. [1, 3, 4, 5, 4]). This also
  # allows the registration to be continued from a previous result by loading
  # the appropriate TImage file as the slice input (see later).
  stages: [1]
  # Cost function used to quantify the alignment of the images.
  # MIND: modality-independent neighbourhood descriptor
  # MI: normalised mutual information
  cost: MIND
  # Type of scaling transformation: isotropic (true), anisotropic (false)
  # If false, the initial values and boundaries must be given for both
  # scaling parameters (see later).
  isotropic: true
  # Output directory (it will be created if it does not exist)
  outputdir: null
  # Status messages of at least 'loglevel' priority will be included in the logs
  # Priority levels (defined in settings.py at the TIRL installation):
  # parameter updates (5), cost function evaluations (7), debug messages (10)
  # status messages (20), warnings (30), errors (40), critical (50)
  loglevel: 10
  # Display warnings: true or false
  warnings: false
  # Display status messages (logs): true or false
  verbose: true
  # Name of the internal logger instance (shared between parallel processes)
  name: monkey.slice-to-volume
  # Log file (overwrites existing logs); contents are defined by 'loglevel'.
  # If null, no logs will be saved to disk, but they may still be displayed.
  logfile: null
  # Parameter log file (overwrites existing parameter logs)
  # If null, parameter updates will not be recorded.
  paramlogfile: null


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ INPUT IMAGES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# Requirements for the inputs are loosely defined, but the "slice" must be a
# 2D image, and the "volume" must be a 3D image. They can be given in any
# commonly used image format, or TImage format. For satisfactory results, the
# images must exhibit similar contrast properties, e.g. for brains, a visible
# grey-white matter boundary is usually key.


# :::::::::::::::::::::: Slice - 2D image of any modality :::::::::::::::::::: #

slice:
  # Input path (common photo formats: PNG, TIFF, JPG; native histology: SVS)
  # If the input is a TImage, all other fields in this section are ignored.
  file: null
  # If the input file is a multi-page TIFF or SVS, the relevant page(s) must
  # be specified here. For other inputs, this field is ignored. For SVS
  # resolution pyramids, consecutive pages most commonly refer to images of
  # decreasing resolution, a slide thumbnail, and a slide label reading. The
  # purpose of choosing a lower-resolution image is to save computation time if
  # the registration is to be carried out at the lower resolution of the 3D
  # image. If in doubt, use the BioFormats importer of Fiji/ImageJ to explore
  # the pages of your input image. The page numbers here are zero-indexed, and
  # multiple pages can be loaded e.g., as [3, 4]. The input data will be stored
  # in TImage format, and separate pages will be stored as layers of the same
  # TImage, hence the pages must have the same aspect ratio.
  pages: [3]
  # Image resolution in mm/px. If multiple pages are loaded, this should be
  # the resolution the page that was loaded last.
  resolution: 0.004   # mm/px
  # Data type in NumPy convention (e.g., f4: 32-bit floating-point)
  dtype: f4
  # If mem, the image data is loaded to memory, if hdd, it is kept on the disk.
  storage: mem
  # Specify or auto-generate a cost function mask for the slice image.
  mask:
    # Specify mask image using a full path to the image file. The mask image
    # must have the same aspect ratio as the input image. No mask: null.
    file: null
    # Create a binary mask by segmenting the image using both a lower and an
    # upper threshold value. Values are relative to the maximum intensity.
    # Pixels with intensities lower than the thr, or higher than the uthr will
    # be excluded from the registration. No mask: thr=0, uthr=1.
    automask: { thr: 0.0, uthr: 1.0 }  # normalised to the maximum intensity
    # Execute a function to generate a mask. The function must be defined in
    # the registration script under the same name as it is referenced by here.
    # See the source code for function templates. No mask: null.
    function: null
    # Normalise mask values to the [0, 1] range: true or false.
    normalise: true
  # Preview the slice image after preprocessing.
  preview: false
  # Save the processed slice image in TImage format to the output directory.
  export: true
  # Create a PNG version of the input image after preprocessing.
  snapshot: true


# ::::::::::::::::::::: Volume - 3D image of any modality :::::::::::::::::::: #

volume:
  # Input path (NIfTI or TImage format)
  # If the input is a TImage, all other fields in this section are ignored.
  file: /Volumes/xenon/UrsSchuffelgen/data/mri/preop/preop_smallfov.bfc.nii.gz
  # Image resolution in mm/vx. For anisotropic MRI volumes, please specify a
  # 3-element array with the voxel sizes: e.g., [0.25, 0.25, 2.2]. If null,
  # the resolution is assumed to be 1 mm/vx. Instead of directly specifying
  # the resolution, this can be inferred from the metadata (see next).
  resolution: null   # <scalar> or [x, y, z] mm/vx
  # If the input is a NIfTI file, and usesqform is true, the MRI will be
  # loaded with the sform matrix (or the qform if the sform is not set). If
  # this is the preferred option, the resolution setting above should be left
  # as null.
  usesqform: true
  # Data type in NumPy convention (e.g., f4: 32-bit floating-point)
  dtype: f4
  # If mem, the image data is loaded to memory, if hdd, it is kept on the disk.
  storage: mem
  # Specify or auto-generate a cost function mask for the 3D image.
  mask:
    # Specify mask image using a full path to the image file. The mask image
    # must have the same aspect ratio as the input image. No mask: null.
    file: null
    # Create a binary mask by segmenting the image using both a lower and an
    # upper threshold value. Values are relative to the maximum intensity.
    # Pixels with intensities lower than the thr, or higher than the uthr will
    # be excluded from the registration. No mask: thr=0, uthr=1.
    automask: {thr: 0.0, uthr: 1.0}  # normalised to the maximum intensity
    # Execute a function to generate a mask. The function must be defined in
    # the registration script under the same name as it is referenced by here.
    # See the source code for function templates. No mask: null.
    function: null
    # Normalise mask values to the [0, 1] range: true or false.
    normalise: true
  # Preview a central slice of the volume after preprocessing.
  preview: false
  # Save the processed volume in TImage format to the output directory.
  export: true


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ PREPROCESSING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# Operations to be carried out on the input images before registration.
# For each input, the sequence of operations should be defined here, such that
# the result of the first operation will be the input of the second operation.
# Leave the fields empty ([]) for no operation. The operations must be
# implemented in the registration script as functions under the same name by
# which they are referenced here. For function templates, please see the source
# code of the registration script.

# Examples used here:
# - slice_preprocessing/volume_preprocessing: convert to grayscale and
# normalise intensity values to [0, 1]
# - match_volume_resolution: downsample the slice photo to the same resolution
# as the MRI volume (this reduces the computational burden)

preprocessing:
  # The slice transformations must be in place for optimisation, hence the
  # preprocessing chain must feautre either "manualinit" or "autoinit".
  # For manual initialisation, the parameter values are taken from the
  # specifications under regparams.init. For autoinitialisation, the parameter
  # values will be estimated from the input chain (for TImage slice inputs).
  slice: [slice_preprocessing, match_volume_resolution, autoinit]
  volume: [volume_preprocessing]


# ~~~~~~~~~~~~~~~~~~~~~~~~~ REGISTRATION PARAMETERS ~~~~~~~~~~~~~~~~~~~~~~~~~~ #

# This section defines initial values and optimisation bounds for the
# transformation parameters within each substage of the registration process.
# The transformations are defined by the registration script as a chain of
# elementary coordinate transformations. The first tx of the chain operates on
# the mm coordinates of the slice image pixels, and the last one maps these
# into the metric (physical) MRI space (as defined by the sform/qform matrix
# or the voxel sizes).

# The transformation chain has the following components (in order):
# 1. 2D scaling (scale2d): (s) if isotropic or (s_x', s_y') if anisotropic
# 2. 2D rotation (rot2d): phi angle of rotation about the slice centre
# 3. 2D translation (trans2d): (t_x', t_y') mm
# 4. 3D embedding (embed): no parameter
# 5. 3D deformation field (warp): 3 x N parameters (N: number of anchor points)
# 6. 3D rotation (rot3d): 3 parameters (r_z, r_y, r_x)
# 7. 3D translation (trans3d): 3 parameters (t_x, t_y, t_z)
# 8. 3D affine (affine3d): 12 parameters of the affine matrix (LR, top-down)

regparams:

# ::::::::::::::::::::::::::::::: Initial values ::::::::::::::::::::::::::::: #
  init:
    # In-plane slice scaling: (s) if isotropic or (s_x', s_y') if anisotropic
    scale2d: 1.0   # [1.0, 1.0] if the scaling is anisotropic
    # In-plane rotation: phi degrees of rotation about the slice centre.
    # Note that the initial rotation is -90 degrees. In TIRL, coordinate axes
    # follow the matrix indexing convention, hence the x-axis is along the
    # vertical, and the y-axis is along the horizontal edge of the 2D image.
    # For this reason, the image must be rotated by 90 degrees in one direction.
    rot2d: 0   # deg
    # In-plane slice offset: 2D translation (trans2d): (t_x', t_y') mm
    trans2d: [0, 0]   # mm
    # 3D slice rotation: 3 Euler angles (r_z, r_y, r_x) in degrees
    rot3d: [0, 0, 0]   # deg
    # 3D translation in millimetres
    trans3d: [0, 0, 0]   # mm
    # 3D affine matrix parameters
    affine3d: [1, 0, 0, 0,
               0, 1, 0, 0,
               0, 0, 1, 0]


# ::::::::::::::::::::::::: Stage 1 - rigid alignment :::::::::::::::::::::::: #

  # Stage 1 estimates the rigid transformation parameters of the slice as well
  # as its scaling. This section specifies crucial information for this process.
  stage_1:

    # Slab: thin rectangular region that defines the initial position and
    # orientation of the 2D image in 3D space, and the extent of the parameter
    # search during the rigid optimisation stage. The more accurate the slab is
    # positioned, the more accurate the registration outcome will be.
    slab:
      # Millimetre-coordinates of the slab centre. The recommended approach to
      # determine these is to load the 3D image in FSLeyes, look for a slice
      # that resembles the 2D image, centre the cursor on that slice,
      # and record the mm position of the cursor.
      centre: [0, 0, 0]   # mm
      # Orientation of the slab as defined by its normal vector. The axes
      # follow from the sform initialisation. Most often it corresponds to the
      # axis order of MNI152 space: x: LR, y: PA, z: IS. Therefore, a coronal
      # slice orientation is defined by a sagittal vector: [0, 1, 0].
      # The 2D image will be flipped if the normal vector is inverted, e.g.
      # if [0, -1, 0].
      normal: [0, 1, 0]
      # Offset of the slab centre from the above defined centre point in the
      # direction of the above defined normal vector. If configuration files
      # are batch-generated for several parallel slices, it is enough to
      # determine the centre and normal vector for one slice, and the slab can
      # be easily shifted for consecutive slices by gradually increasing or
      # decreasing this offset in millimetres. No offset: 0
      offset: 0   # mm
      # Slab thickness. Defines two times the largest distance to which the
      # slice is allowed to be shifted away from the centre of the slab during
      # parameter search.
      thickness: 10   # mm
      # Number of different initial positions to be tested within the thickness
      # of the slab, along the normal vector. An odd number allows the middle
      # test to be carried out precisely at the above defined centre of the
      # slab. Too many initialisations lead to the combinatorial explosion of
      # the computation time (see later).
      n_positions: 5
      # Full range of rotations (in degrees) about each axis, relative to the
      # initial orientation of the slab.
      range: [ 20, 20, 20 ]   # deg, full-range (half in each direction)
      # Number of different initial rotations to be tested about each axis
      # during the parameter search. Specifying an odd number means that the
      # middle test will be aligned with the initial orientation of the slab.
      # Specifying too many initialisations will lead to the combinatorial
      # explosion of the computation time.
      n_orientations: [ 1, 5, 5 ]

    # Number of main-loop iterations. A main loop starts with a grid search of
    # rotation and translation parameters (defined by the number of
    # initialisations and the range of initial values), proceeds by fine-
    # tuning the rigid parameters from all initialisations (see later), and
    # ends with a single set of parameter estimates. In consecutive main-loop
    # iterations, the slab is repositioned based on the new estimates, and the
    # search range of the parameters are further reduced to facilitate
    # convergence on the most probable set of rigid parameters. The
    # computational time increases linearly with the number of main-loop
    # iterations.
    iterations: 1
    # Keeper ratio. Each initial set of parameters are fine-tuned by local
    # gradient-free bounded optimisation (BOBYQA), which is carried out at
    # increasingly higher resolutions (see later). The keeper ratio defines the
    # percentage of parameter sets at each resolution level that are retained
    # for further optimisation at the next resolution level. This way many
    # suboptimal parameter sets can be eliminated at the lowest computational
    # cost. On the other hand, a keeper ratio that is too small may lead to the
    # elimination of good parameter sets, because their results cannot be
    # accurately distinguished from others at the coarsest resolution.
    top: 0.3
    # Use the bounded BOBYQA optimisation method instead of the non-bounded
    # NEWUOA variant: true or false.
    constrained: true
    # Make the last rigid optimisation unbounded: true or false.
    try_unconstrained: false
    # Parameter changes normalised to the full range of the parameters at the
    # start of the optimisation. Larger values may facilitate faster
    # convergence if the cost function is smooth. Otherwise, the accuracy of the
    # optimisation may be limited.
    opt_step: 0.1
    # Downsampling factors relative to the initial resolution of the slice
    # image. The number and order of these factors define the resolution levels
    # for the optimisation.
    slice_scaling: [4, 2, 1]
    # Gaussian smoothing radius (sigma) in pixels at each resolution level.
    # The number of smoothing radii must match the number of resolution levels
    # defined above.
    slice_smoothing: [0, 0, 0]
    # Downsampling factors relative to the initial resolution of the 3D image.
    # The number of factors must match the number of resolution levels defined
    # above.
    volume_scaling: [4, 2, 1]
    # Gaussian smoothing radius (sigma) in voxels at each resolution level.
    # The number of smoothing radii must match the number of resolution levels
    # defined above.
    volume_smoothing: [0, 0, 0]

    # The rigid transformation parameters in Stage 1 are optimised sequentially
    # in two groups: stage 1a (2D) and stage 1b (3D + scaling). Here we
    # specify upper and lower bounds for these parameter groups by defining
    # delta values. The absolute parameter bounds are calculated by subtracting
    # or adding these delta values to the current parameter values, which is
    # managed internally by the registration script, following a carefully
    # controlled schedule.

    # Stage 1a: optimisation of in-plane rigid parameters + scaling
    stage_1a:
      scale2d_lower_delta: 0.1
      scale2d_upper_delta: 0.1
      rot2d_lower_delta: 20.0           # deg
      rot2d_upper_delta: 20.0           # deg
      trans2d_lower_delta: [2.0, 2.0]   # mm
      trans2d_upper_delta: [2.0, 2.0]   # mm
      # Absolute parameter tolerances (stopping condition). The values
      # correspond to the unified stage 1a parameter vector: 1 scaling +
      # 1 rotation (radians) + 2 translation (mm). In case of anisotropic
      # scaling, the xtol_abs vector should be extended to 5 dimensions, and
      # the first two parameters would correspond to the scaling factors.
      # Instead of or in addition to absolute parameter value tolerances, it is
      # possible to define a tolerance condition that is relative to the
      # previous parameter value. See the NLOpt documentation for more details
      # on xtol_rel, as well as on other conditions, such as ftol_abs,
      # ftol_rel, and maxiter.
      xtol_abs: [0.01, 0.01, 0.01, 0.01]   # units: 1, rad, mm, mm

    # Stage 1b: optimisation of 3D rigid parameters + scaling
    stage_1b:
      scale2d_lower_delta: 0.05
      scale2d_upper_delta: 0.05
      rot3d_lower_delta: [5.0, 10.0, 10.0]   # deg
      rot3d_upper_delta: [5.0, 10.0, 10.0]   # deg
      trans3d_lower_delta: [1.0, 1.0, 1.0]   # mm
      trans3d_upper_delta: [1.0, 1.0, 1.0]   # mm
      # Absolute parameter tolerances (stopping condition). The values
      # correspond to the unified stage 1b parameter vector:
      # 1 scaling + 3 rotations (rad) + 3 translations (mm)
      # In case of anisotropic scaling, the xtol_abs vector should be extended
      # to 8 dimensions, and the first two parameters would correspond to the
      # scaling factors.
      xtol_abs: [0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01]

    # Number of parallel processes. If 1x5x5 initial orientations are tested at
    # 5 different positions, and the number of main-loop iterations is 1, a
    # total number of 125 initial parameter sets must be fine-tuned. These are
    # distributed among the number of parallel processes. If n_cpu = -1, all
    # available CPU cores will be recruited for this task.
    n_cpu: -1
    # Delete temporary files that are created during Stage 1. These include
    # suboptimal parameter sets and misaligned images that are saved
    # temporarily under "stage1_temp" in the output directory.
    cleanup: true
    # Live view of the Stage-1 registration process. Only recommended for
    # debugging, because it significantly slows down the registration.
    visualise: false
    # Generate output from the results after the successful completion of
    # Stage 1.
    export:
      # Saves the Stage-1 registered slice as a TImage
      timage: true
      # Resamples the 3D image on the registered slice and saves it as PNG
      snapshot: true
      # Export the 3D mask
      source_mask: false
      # Export the 2D mask
      target_mask: true


# :::::::::::::::::::::::: Stage 2 - affine alignment :::::::::::::::::::::::: #

  # Stage 2 estimates the 12 affine parameters in a single optimisation
  # that starts at a coarse resolution and terminates at the finest
  # resolution. This section defines the resolution levels, the parameter
  # bounds, the stopping conditions, and the methods of reporting in a way that
  # is highly similar to Stage 1. Please find the relevant descriptions there
  # accordingly.
  stage_2:

    # Downsampling factors to define resolution levels.
    slice_scaling: [ 4, 2, 1]
    # Gaussian smoothing radii (sigma) in pixels at each resolution level.
    slice_smoothing: [ 0, 0, 0 ]
    # Downsampling factors
    volume_scaling: [ 4, 2, 1 ]
    # Gaussian smoothing radii (sigma) in voxels at each resolution level.
    volume_smoothing: [ 0, 0, 0 ]
    # Parameter changes normalised to the full range of the parameters at the
    # start of the optimisation.
    opt_step: 0.1
    # Delta values to define the lower and upper bound of the affine parameters.
    lower_delta: [0.1, 0.1, 0.1, 1.0,
                  0.1, 0.1, 0.1, 1.0,
                  0.1, 0.1, 0.1, 1.0]
    upper_delta: [0.1, 0.1, 0.1, 1.0,
                  0.1, 0.1, 0.1, 1.0,
                  0.1, 0.1, 0.1, 1.0]
    # Relative parameter tolerance (stopping condition).
    xtol_rel: 0.01
    # Live visualisation of the registration process. Only for debugging.
    visualise: false
    # Generate output from the result of Stage 2.
    export:
      # Saves the Stage-2 registered slice as a TImage
      timage: true
      # Resamples the 3D image on the registered slice and saves it as PNG
      snapshot: true
      # Export the 3D mask
      source_mask: false
      # Export the 2D mask
      target_mask: true
      # Export a 2D heatmap of the pixelwise evaluated cost function
      # This feature only works with MIND, and not with MI
      costmap: false


# :::::::::::::::::::::::::: Stage 3 - in-plane warp ::::::::::::::::::::::::: #

  # Stage 3 generates a set of N evenly scattered anchor points on the slice
  # image, and optimises the in-plane deformations at each of these points.
  # This section contains important information about the definition of the
  # anchor points, and the optimisation of the 2xN deformation parameters.
  # Note that the phrases "anchor point", "control point", "support point" are
  # used interchangeably.
  # The Stage 3 configurations are very similar to those of Stage 4, since the
  # two stages optimise the same transformation, but the through-plane
  # deformation parameters are locked in for Stage 3.
  stage_3:

    # Definition of the anchor points.
    support:
      # Region of interest to be populated by anchor points.
      mask:
        file: null
        automask: { thr: 0.0, uthr: 0.96 }
        function: null
        normalise: true
      # Anchor point distribution scheme:
      # K-Means ("km"): achieves perfectly even separation of the points and
      # complete filling of the region of interest. Recommended option.
      # Halton ("halton"): uses the Halton sequence to generate pseudorandom
      # coordinates. [deprecated]
      # Adaptive ("cost_adaptive"): puts more anchor points to those regions,
      # where the pixelwise calculated cost is higher. While it provides more
      # control in the necessary areas, this method may lead to very uneven
      # point distributions. [deprecated]
      mode: km
      # Number of anchor points (N). For a gradually refined optimisation
      # process with increasing number of points, one may define an array of
      # integers instead of a scalar (see also Stage 4).
      # Computation time scales with N^2.
      points: 8

    # Active deformation parameters at each anchor point. The order of the axes
    # defines the layout of parameters in the joint 2xN parameter vector
    # (also compare with Stage 4).
    vectorder: xy
    # Radial basis function that is used to calculate deformations between the
    # anchor points. See the docs of scipy.interpolate.Rbf for alternative
    # options.
    model: gaussian
    # Anchor point recruitment rate. If the total number of points (N) is
    # defined as a scalar (see above), and optsize < N, an increasing number of
    # points are recruited for optimisation, where the increment between
    # consecutive optimisations is optsize. If N is defined as an array, the
    # optsize field is ignored.
    optsize: 8
    # Parameter changes normalised to the full range of the parameters at the
    # start of the optimisation.
    opt_step: 0.1
    # Weight factor of the regularisation term in the cost function.
    regweight: 0
    # Downsampling factors for the 2D image to define the resolution levels for
    # the optimisation. The number of anchor points are only varied at the
    # first resolution level, and stays constant (N) for all subsequent
    # resolution levels.
    slice_scaling: [1]
    # Gaussian smoothing radii (sigma) in pixels at each resolution level.
    slice_smoothing: [1]
    # Downsampling factors for the 3D image at each resolution level.
    volume_scaling: [1]
    # Gaussian smoothing radii (sigma) in voxels at each resolution level.
    volume_smoothing: [1]

    # Collective definition of lower and upper bound deltas for the x and y
    # (in-plane) deformation components at each anchor point.
    lower_dxy: 0.5   # mm
    upper_dxy: 0.5   # mm
    # Absolute parameter tolerance (stopping condition).
    xtol_abs: 0.01  # mm
    # Live view of the registration. For debugging only (see also earlier).
    visualise: false
    # Generate output from the Stage-3 registration result.
    export:
      # Save the in-plane warped slice as a TImage.
      timage: true
      # Evaluate the 3D volume on the registered slice and save as PNG.
      snapshot: true
      # Export 3D mask
      source_mask: false
      # Export 2D mask
      target_mask: true
      # Plot the current set of control points over the slice image.
      control_points: true


# ::::::::::::::::::::::::: Stage 4 - free-form warp ::::::::::::::::::::::::: #

  # Stage 4 redefines the anchor points of the non-linear transformation, and
  # optimises all 3 deformation parameters at each anchor point. This section
  # contains important information about the definition of the anchor points,
  # and the optimisation of the 3xN deformation parameters.
  # The Stage 4 configurations are very similar to those of Stage 3.
  stage_4:
    # Definition of the anchor points.
    support:
      # Region of interest to be populated by anchor points.
      mask:
        file: null
        automask: {thr: 0.0, uthr: 0.96}
        function: null
        normalise: true
      # Anchor point distribution scheme (see earlier in Stage 3)
      mode: km
      # Number of anchor points, defined explicitly for 4 optimisation steps.
      # Computation time scales with N^2.
      points: [8, 16, 32, 64]

    # Active deformation parameters at each anchor point. The order of the axes
    # defines the layout of parameters in the joint 3xN parameter vector
    # (also compare with Stage 3).
    vectorder: xyz
    # Radial basis function that is used to calculate deformations between the
    # anchor points.
    model: gaussian
    # Parameter changes normalised to the full range of the parameters at the
    # start of the optimisation.
    opt_step: 0.1
    # Anchor point recruitment rate. Ignored, if the number of control points
    # are defined explicitly (using an array) for each optimisation step.
    optsize: 8
    # Weight factor of the regularisation term in the cost function.
    regweight: 0
    # Downsampling factors for the 2D image to define the resolution levels for
    # the optimisation. The number of anchor points are only varied at the
    # first resolution level, and stays constant (N) for all subsequent
    # resolution levels.
    slice_scaling: [1]
    # Gaussian smoothing radii (sigma) in pixels at each resolution level.
    slice_smoothing: [0]
    # Downsampling factors for the 3D image at each resolution level.
    volume_scaling: [1]
    # Gaussian smoothing radii (sigma) in voxels at each resolution level.
    volume_smoothing: [0]

    # Collective definition of lower and upper bound deltas for the xy
    # (in-plane) and the z (out-of-plane) deformation components at each anchor
    # point.
    lower_dxy: 0.5   # mm
    lower_dz: 0.5    # mm
    upper_dxy: 0.5   # mm
    upper_dz: 0.5    # mm
    # Absolute parameter tolerance (stopping condition).
    xtol_abs: 0.01   # mm
    # Live view of the registration. For debugging only (see also earlier).
    visualise: false
    # Generate output from the Stage-4 registration result.
    export:
      # Save the in-plane warped slice as a TImage.
      timage: true
      # Evaluate the 3D volume on the registered slice and save as PNG.
      snapshot: true
      # Export 3D mask
      source_mask: false
      # Export 2D mask
      target_mask: true
      # Plot the current set of control points over the slice image.
      control_points: true


# :::::::::::::::::::::: Stage 5 - affine re-estimation :::::::::::::::::::::: #

  # Stage 5 discards all in-plane deformations, and re-estimates the affine
  # transformation based on the result of the previous stage. The rationale is
  # that the non-linear optimisation stages can compensate for minor
  # shortcomings of the initial rigid and affine alignment, and these
  # improvements are better represented by an updated affine. At the end of
  # Stage 5, all non-linear transformations are discarded.

  stage_5:
    # The re-estimation is based on a region of interest to prevent outlier
    # bias. The region of interest is can be defined by a binary mask, which
    # can be defined by the usual 3 methods (see details earlier).
    mask:
      file: null
      automask: {thr: 0.001, uthr: 1.0}
      function: null
      normalise: true
    # Live view - this is less relevant for Stage 5, as it is instantaneous.
    visualise: false
    # Generate outputs from the result of Stage 5 (see details earlier).
    export:
      timage: true
      snapshot: true
      source_mask: false
      target_mask: true
...