#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

"""
monkey.st1prep --

Based on a table of input microscopy images, this script generates binary
masks, identifies separate slice components, and initialises the Stage-1
(direct microscopy-to-MRI) registration of the components.

After this script completes, Stage-1 registrations can be carried out in
parallel for all components independently.

Based on the Stage-1 registration results, Stage 2 (multislice-to-volume
registration) fine-tunes the registration based on two assumptions: the slices
are regularly sampled and parallel.

Finally, 3D microscopy volumes are reconstructed from the Stage-2 results.

"""


__tirlscript__ = "monkey.st1prep"


# DEPENDENCIES

import os
import cv2
import sys
import yaml
import logging
import imageio
import argparse
import numpy as np
import pandas as pd


from tirl.timage import TImage
from skimage.color import rgb2yiq
from skimage.morphology import binary_opening
from scipy.interpolate import splprep, splev
from skimage.measure import label, regionprops
from skimage.exposure import rescale_intensity
from skimage.morphology import convex_hull_image
from skimage.filters.thresholding import threshold_otsu


# TIRL IMPORTS

import tirl.settings as ts
from tirl.transformations.rotation import TxRotation2D
from tirl.transformations.translation import TxTranslation
from tirl.settings import TIMAGE_DEFAULT_SNAPSHOT_EXT as SNAPEXT


# TIRLSCRIPTS IMPORTS

from tirlscripts.oxford.monkey.constants import DirTree
from tirlscripts.oxford.monkey.constants import SLICE_ROTATIONS


# DEFINITIONS

LOGGER = "tirlscripts.oxford.monkey.st1prep"


# IMPLEMENTATION


def main(cnf):
    """ Main program code. """

    # Load input table and save a copy to the output directory
    logger.info("Loading input data...")
    table = pd.read_csv(cnf["input-table"])
    table.to_csv(os.path.join(cnf["outputdir"], "input_table.csv"),
                 index=False)

    # Initialise output directory structure
    outdir = DirTree(rootdir=cnf["outputdir"], table=table, stages=2)

    # Fill initial position estimates
    if cnf["interpolate-positions"]:
        logger.info("Interpolating initial position estimates...")
        table = interpolate_positions(table)
        table.to_csv(os.path.join(outdir(), "input_table_positions.csv"),
                     index=False)

    # Fill initial orientation estimates (normal vectors)
    if cnf["interpolate-normals"]:
        logger.info("Interpolating initial orientation estimates...")
        table = interpolate_normals(table)
        table.to_csv(os.path.join(outdir(), "input_table_orientations.csv"),
                     index=False)

    # Reinitialise output directory structure after updating the input table
    outdir = DirTree(rootdir=outdir(), table=table, stages=2)

    # Split inputs into components - label by numbers, sorted by area
    if cnf["generate-masks"]:
        logger.info("Generating masks...")
        table = generate_masks(table, outdir)
        table.to_csv(os.path.join(outdir(), "input_table_masks.csv"),
                     index=False)

    logger.info("Defining components...")
    table = split_components(table, outdir, split=cnf["split-components"])
    table.to_csv(os.path.join(outdir(), "input_table_components.csv"),
                 index=False)

    # Initialise Stage-1 registrations
    logger.info("Initialising Stage-1 registration directories...")
    with open(cnf["stage1-template"], "r") as fp:
        cnf = yaml.safe_load(fp)
    stage1_init(table, cnf, outdir)


def interpolate_positions(table: pd.DataFrame):

    table = table.copy()

    # Interpolate positions
    nullpos = table.PosX.isnull() | table.PosY.isnull() | table.PosZ.isnull()
    given = table[~nullpos].sort_values(by="Slice")
    slices, *positions = np.asarray(given[["Slice", "PosX", "PosY", "PosZ"]]).T
    tck, slices = splprep(positions, u=slices)
    interpolants = pd.DataFrame(table["Slice"].apply(splev, tck=tck).tolist())
    table[["PosX", "PosY", "PosZ"]] = interpolants

    return table


def interpolate_normals(table: pd.DataFrame):

    table = table.copy()

    # Interpolate normal vectors (orientation)
    nullrot = table.NormX.isnull() | table.NormY.isnull() | table.NormZ.isnull()
    given = table[~nullrot].sort_values(by="Slice")
    slices, *normals = np.asarray(given[["Slice", "NormX", "NormY", "NormZ"]]).T
    tck, slices = splprep(normals, u=slices)
    interpolants = pd.DataFrame(table["Slice"].apply(splev, tck=tck).tolist())
    table[["NormX", "NormY", "NormZ"]] = interpolants

    return table


def generate_masks(table: pd.DataFrame, outdir: DirTree):
    newtable = []

    for ix, record in table.iterrows():
        slice_no = int(record["Slice"])
        part = str(record["Part"])
        logger.log(5, f"Generating mask for Slice {slice_no}, Part {part}...")
        mask = get_8bit_mask(record)  # one or more 8-bit component labels
        smfile = os.path.join(
            outdir(slice_no, part),
            f"slice-{slice_no}_part-{part}_mask.{SNAPEXT}"
        )
        imageio.imsave(smfile, rescale_intensity(mask, out_range=np.uint8))
        logger.info(f"Saved mask file: {smfile}")
        newrecord = record.copy()
        newrecord["SliceMask"] = smfile
        newtable.append(newrecord)
    else:
        newtable = pd.DataFrame(newtable, columns=table.columns)

    return newtable


def split_components(table: pd.DataFrame, outdir: DirTree, split: bool):
    newtable = []

    for ix, record in table.iterrows():
        mask = imageio.imread_v2(record["SliceMask"])
        slice_no = record["Slice"]
        part = record["Part"]
        if not split:
            mask = np.where(mask > 0.5, 255, 0).astype(np.uint8)
        regions = sorted(regionprops(mask), key=lambda r: r["area"])[::-1]
        logger.info(f"Number of components in Slice {slice_no} Part {part}: "
                    f"{len(regions)}")
        xx, yy = np.meshgrid(
            *tuple(range(dim) for dim in mask.shape), indexing="ij")
        coords = np.vstack([xx.ravel(), yy.ravel()])

        for cmp, region in enumerate(regions):
            newrecord = record.copy()
            newrecord["Component"] = cmp + 1
            binary_cmp_mask = np.zeros_like(mask)
            binary_cmp_mask[mask == region["label"]] = 255
            cog_cmp = coords * binary_cmp_mask.ravel()
            cog_cmp = np.sum(cog_cmp, axis=1) / np.sum(binary_cmp_mask)
            cog_cmp *= float(record["Resolution"])
            newrecord["CmpOffsetX"] = float(cog_cmp[0])
            newrecord["CmpOffsetY"] = float(cog_cmp[1])
            mfile = os.path.join(
                outdir(slice=slice_no,
                       part=part,
                       cmp=newrecord["Component"]),
                f"mask.{SNAPEXT}"
            )
            imageio.imsave(mfile, binary_cmp_mask)
            cmask = convex_hull_image(binary_cmp_mask).astype(np.uint8) * 255
            mfile = mfile.replace("mask", "cmask")
            imageio.imsave(mfile, cmask)
            newrecord["SliceMask"] = mfile
            newtable.append(newrecord)
    else:
        newtable = pd.DataFrame(newtable, columns=list(newtable[0].keys()))

    return newtable


def get_8bit_mask(record):
    if not record["SliceMask"] is not None:
        mask = mask_from_maskfile(record["SliceMask"])
    else:
        mask = mask_from_imfile(record["SliceOriginal"], area=2000)
    return mask


def mask_from_maskfile(maskfile):
    mask = imageio.imread_v2(maskfile)
    if mask.ndim > 2:
        mask = rgb2yiq(mask)[..., 0]
    return mask


def mask_from_imfile(imfile, area=None):
    img = imageio.imread_v2(imfile)
    if img.ndim > 2:
        img = cv2.pyrMeanShiftFiltering(img, 10, 10)
        img = rgb2yiq(img)[..., 0]

    # threshold = threshold_otsu(img)
    threshold = np.max(img) * 0.9
    mask = np.asarray(img < threshold, dtype=np.uint8) * 255
    mask = binary_opening(mask, np.ones((5, 5)))
    if area is not None:
        mask = area_filter(mask, area=area)
    else:
        mask = label(mask)
    return mask


def area_filter(img, area):
    labelimg = label(img, background=0)
    exclude = []
    for region in regionprops(labelimg):
        if region["area"] < area:
            exclude.append(region["label"])
    for value in exclude:
        labelimg[labelimg == value] = 0
    return labelimg.astype(np.uint8)


def stage1_init(table: pd.DataFrame, cnf: dict, outdir: DirTree):

    cnf = cnf.copy()

    # Initialise volume input
    vol = init_volume(cnf, outdir)

    # Initialise input components
    for ix, record in table.iterrows():
        slice_no = record["Slice"]
        part = record["Part"]
        cmp = record["Component"]
        cmpdir = outdir(slice=slice_no, part=part, cmp=cmp, stage=1)
        img = init_component(cnf, record, cmpdir)
        cfile = os.path.join(
            cmpdir,
            f"slice-{slice_no}_part-{part}_cmp-{cmp}_stage-1.yml"
        )
        with open(cfile, "w") as fp:
            yaml.safe_dump(
                cnf, fp, default_flow_style=False,
                sort_keys=False, indent=2
            )


def init_volume(cnf, outdir):
    if cnf["volume"]["usesqform"]:
        vol = TImage(cnf["volume"]["file"], external="affine")
    else:
        vol = TImage(cnf["volume"]["file"])
        vol.resolution = cnf["volume"]["resolution"]
    mfile = cnf["volume"]["mask"]["file"]
    if mfile is not None:
        vol.mask = TImage(mfile).data
    timgext = ts.EXTENSIONS["TImage"]
    refdir = os.path.join(outdir(), "reference")
    volfile = os.path.join(refdir, f"reference.{timgext}")
    if not os.path.exists(refdir):
        os.makedirs(refdir)
    vol.save(volfile, overwrite=True)
    cnf["volume"]["file"] = volfile
    return vol


def init_component(cnf, record, cmpdir):

    cnf["general"]["outputdir"] = cmpdir
    cnf["general"]["logfile"] = os.path.join(cmpdir, "logfile.log")
    cnf["general"]["paramlogfile"] = os.path.join(cmpdir, "params.log")

    timgext = ts.EXTENSIONS["TImage"]
    img = TImage(record["SliceOriginal"])
    img.resolution = record["Resolution"]
    img.mask = TImage(record["SliceMask"]).data
    img.header["metadata"] = dict(record)
    offset = -np.asarray(record[["CmpOffsetX", "CmpOffsetY"]])
    rot2d, normal = SLICE_ROTATIONS[str(record["Orientation"]).upper()]
    centralise = TxTranslation(*offset, name="centralise")
    rotation = TxRotation2D(rot2d, mode="deg", name="rotation_2d")
    cnf["regparams"]["init"]["rot2d"] = rot2d
    img.domain.external = [centralise, rotation]

    slice_no = record["Slice"]
    part = record["Part"]
    cmp = record["Component"]
    timgfile = os.path.join(
        cmpdir, f"slice-{slice_no}_part-{part}_cmp-{cmp}.{timgext}")
    img.save(timgfile, overwrite=True)
    cnf["slice"]["file"] = timgfile
    cnf["slice"]["resolution"] = record["Resolution"]
    cnf["slice"]["mask"]["file"] = record["SliceMask"]

    cnf["regparams"]["stage_1"]["slab"]["centre"] = \
        np.asarray(record[["PosX", "PosY", "PosZ"]], dtype=float).tolist()
    custom_normal = np.asarray(record[["NormX", "NormY", "NormZ"]], dtype=float)
    if np.dot(custom_normal, normal) < 0:
        custom_normal = -custom_normal
    cnf["regparams"]["stage_1"]["slab"]["normal"] = custom_normal.tolist()
    cnf["regparams"]["stage_1"]["slab"]["offset"] = 0

    return img


def create_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-c", "--config", type=str, required=True,
        help="Template configuration file (.yml)")
    parser.add_argument(
        "--verbose", action="store_true", default=False, required=False,
        help="Logs to STDOUT.")
    return parser


def create_logger(loggername):
    """
    Creates a logger that sends log messages to the standard output and
    into a file.

    """
    global logger
    logger = logging.getLogger(loggername)
    logger.handlers = []  # delete any existing handlers to avoid duplicate logs
    logger.setLevel(1)
    formatter = logging.Formatter(
        fmt='%(asctime)s Process-%(process)d %(levelname)s (%(lineno)d) '
            '- %(message)s',
        datefmt='[%Y-%m-%d %H:%M:%S]')

    # Make sure that the logfile can be created
    logfile = cnf["logfile"]
    if not logfile:
        logfile = os.path.join(cnf["outputdir"], "logfile.log")
    else:
        logdir = os.path.dirname(logfile)
        if not os.path.isdir(logdir):
            os.makedirs(logdir)

    # Redirect all logs of interest to the logfile
    fh = logging.FileHandler(logfile, mode="a", delay=False)
    loglevel = cnf["loglevel"]
    if isinstance(loglevel, str):
        level = getattr(logging, loglevel.upper())
    elif isinstance(loglevel, (int, float)):
        level = int(loglevel)
    elif not loglevel:
        level = logging.INFO
    else:
        raise TypeError(f"Invalid logging level: {loglevel}")
    fh.setLevel(level)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    # Redirect only warnings/errors to the standard output, unless "verbose"
    ch = logging.StreamHandler()
    if cnf["verbose"]:
        ch.setLevel(level)
    else:
        ch.setLevel(logging.WARNING)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


if __name__ == "__main__":

    parser = create_parser()

    if len(sys.argv) > 1:

        # Load command-line arguments and configurations
        args = parser.parse_args()
        with open(args.config, "r") as fp:
            cnf = dict(yaml.safe_load(fp))

        # Override verbose setting from the CLI
        if args.verbose:
            cnf["verbose"] = args.verbose

        # Configure logger
        logger = create_logger(LOGGER)
        main(cnf)

    else:
        parser.print_help()
