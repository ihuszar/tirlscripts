# Changelog


### 14 Oct 2022 -- v1.1.0

- Added: logger instance now records the SHA1 hash of the executing script for strict version control
- The slice_to_volume.py script can now load histology from pyramidal TIFF files
- Bugfix: MRI resolution detection is now independent of axis orientation


### 11 Oct 2022 -- v1.0.0

- Project-specific version v1.0.0 assigned to the current state of the library
