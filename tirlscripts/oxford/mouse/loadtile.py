#!/usr/bin/env python

from tirlscripts.oxford.scriptutils.inout import load_histology

tile = "/Users/inhuszar/aurea/jalapeno/MBtest_Post_Silicone_Trans_PLP_3/" \
       "region_01.tif"
h = load_histology(file=tile, storage="hdd", dtype="u1", pages=[3])
