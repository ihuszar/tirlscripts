#!/usr/bin/env python

import numpy as np
import nibabel as nib
from tirl.timage import TImage
from tirl.minc import load_xfm

dwi = TImage(
    "/Users/inhuszar/minc/example1/9_1b_b0s_gibbs_eddy.mnc",
    external="affine"
)
std = TImage(
    "/Users/inhuszar/minc/diffstd/VCAN-diffusion-pipeline-nlin-3.mnc",
    external="affine"
)
xfm = load_xfm(
    "/Users/inhuszar/minc/example1/"
    "9_1b_b0s_gibbs_eddy__concat_lsq6_N_I_lsq6_lsq12_and_nlin.xfm"
)
dwi.domain.external += xfm

registered_dwi = dwi.evaluate(std.domain)
registered_dwi.save("/Users/inhuszar/minc/example1/registered_dwi_new.timg")

# Save as NIfTI
affine = np.eye(4)
affine[:3, :] = std.domain.external[0].matrix
hdr = nib.Nifti1Header()
hdr.set_qform(affine, 2)
nifti = nib.Nifti1Image(registered_dwi.data, affine=affine, header=hdr)
nib.save(nifti, "/Users/inhuszar/minc/example1/registered_dwi_new.nii.gz")
