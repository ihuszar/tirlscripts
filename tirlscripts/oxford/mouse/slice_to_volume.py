#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


__tirlscript__ = "mouse.s2v"


# DEPENDENCIES

import os
import sys
import logging
import argparse
import numpy as np
from numbers import Number
import multiprocessing as mp

from attrdict import AttrMap
from datetime import datetime
from collections import namedtuple
from skimage.measure import regionprops


# TIRL IMPORTS

import tirl

from tirl import settings as ts

from tirl.tfield import TField
from tirl.timage import TImage
from tirl.domain import Domain

from tirl.operations.tensor import TensorOperator
from tirl.transformations.identity import TxIdentity
from tirl.transformations.embedding import TxEmbed
from tirl.transformations.scale import TxScale, TxIsoScale
from tirl.transformations.rotation import TxRotation2D
from tirl.transformations.euler import TxEulerAngles
from tirl.transformations.axisangle import TxAxisAngle
from tirl.transformations.translation import TxTranslation
from tirl.transformations.affine import TxAffine
from tirl.transformations.rbf_displacement import \
    TxRbfDisplacementField
from tirl.transformations.transformation import TransformationGroup

from tirl.cost.mi import CostMI
from tirl.cost.mind import CostMIND
from tirl.regularisation.normtxreg import NormTxRegulariser
from tirl.regularisation.bending_energy import RegBendingEnergy

from tirl.optimisation.optnl import OptNL
from tirl.optimisation.optnl import OptMethods
from tirl.optimisation.optimiser import OptimisationGroup


# TIRLSCRIPT IMPORTS

from tirlscripts import srchash
from tirlscripts.oxford.scriptutils import general, inout, image


# DEFINITIONS

from tirl.constants import *

# Named transformation chain
NamedChain = namedtuple(
    "NamedChain", ["tx_scale2d", "tx_rot2d", "tx_trans2d", "tx_embed",
                   "tx_warp", "tx_rot3d", "tx_affine3d", "tx_trans3d"])
# NumPy print formatting
np.set_printoptions(precision=4)

SNAPSHOT_EXT = "png"    # extension of TImage snapshots
NORMALISE = True        # optimise parameters in the [-1, 1] normalised range

# Stage 5: global parameters to track the results of previous stage 3s & 4s
prev_cycle_coords = 0
cycle_counter = 0

# 2D slices are assumed to be in neurological orientation
DEFAULT_SLICE_ORIENTATION = [0, 0, -1]  # xy plane, neurological orientation


# IMPLEMENTATION

def run(cnf=None, **options):
    """
    Runs TIRL slice-to-volume registration (stage 3).

    :param cnf:
        Configuration file. Instead of a file, a dictionary with
        suitable content may also be specified.
    :type cnf: Union[str, dict, None]
    :param options:
        Overriding configuration parameters.
    :type options: Any

    """
    # Load script configuration
    if cnf is not None:
        if isinstance(cnf, dict):
            cnf = dict(cnf)
        elif isinstance(cnf, str):
            cnf = general.load_configurations(cnf)
        else:
            raise TypeError(
                f"Unrecognised configuration format: {cnf.__.class__.__name__}")
    cnf.update(options)
    p, logger = general.initialise_script(**cnf)
    p.logger = logger.name  # avoid globals for multiprocessing
    logger.debug(f"slice_to_volume.py SHA1: {srchash(__file__)}")

    # Load and configure input images
    if p.slice.export is True:
        ext = ts.EXTENSIONS["TImage"]
        p.slice.export = os.path.join(p.general.outputdir, f"slice.{ext}")
    try:
        slice_ = inout.load_histology(scope=globals(), **p.slice)
    except:
        slice_ = inout.load_image(scope=globals(), **p.slice)

    slice_.storage = p.slice.storage
    slice_.rule = None

    if p.volume.export is True:
        ext = ts.EXTENSIONS["TImage"]
        p.volume.export = os.path.join(p.general.outputdir, f"volume.{ext}")
    volume = inout.load_volume(scope=globals(), **p.volume)
    volume.storage = p.volume.storage
    volume.rule = None

    # Having loaded both images, perform actions on the brain slice photo prior
    # to registration, unless it was loaded from an alternative source.
    # Actions are user-defined functions within the TIRL namespace. Actions
    # can be chain-loaded to perform preparatory analysis steps on the images
    # before registration begins.
    isalternative = p.slice.file.lower().endswith(
        (ts.EXTENSIONS["TImage"], ts.EXTENSIONS["TIRLObject"]))
    if not isalternative:
        slice_ = image.perform_image_operations(
            slice_, *p.preprocessing.slice, scope=globals(), other=volume,
            cnf=p)
        # Initialise registration frame
        #slice_.centralise(weighted=True)
        slice_.centralise(weighted=False)
        initialise_transformations(slice_, p.regparams.init, p)

    # Perform actions on the volume prior to registration, unless it was loaded
    # from a TImage file.
    isalternative = p.volume.file.lower().endswith(
        (ts.EXTENSIONS["TImage"], ts.EXTENSIONS["TIRLObject"]))
    if not isalternative:
        volume = image.perform_image_operations(
            volume, *p.preprocessing.volume, scope=globals(), other=slice_,
            cnf=p)
        # Move the isocentre of the volume to the origin
        # volume.centralise()

    # Run the registration routine
    try:
        register(slice_, volume, p)
    except Exception as exc:
        logger.error(exc.args)
        logger.fatal(f"The registration terminated with an exception.")
        raise exc
    else:
        logger.fatal("The registration was completed successfully.")


def register(img, vol, cnf):
    """
    Registers a 2D image (img) to a 3D image (vol) according to a predefined
    multi-stage protocol. Note that the function assigns transformations to the
    domain of the 2D image and optimises the parameters thereof, hence it
    changes the input image in place.

    :param img: 2D image that needs to be registered
    :type img: TImage
    :param vol: 3D image that will serve as the reference
    :type vol: TImage
    :param cnf: all configurations
    :type cnf: dict or AttrMap

    :returns: 2D image in optimal alignment with the 3D image
    :rtype: TImage

    """
    p = AttrMap(cnf)
    q = p.regparams

    # Check prerequisites first (better to fail here than later)
    assert isinstance(img, TImage)
    assert isinstance(vol, TImage)
    for stage_no in p.general.stages:
        dict(q).get(f"stage_{stage_no}")
    if not os.path.isdir(p.general.outputdir):
        raise FileNotFoundError(
            f"Output directory is not ready at {p.general.outputdir}")

    # Run registration stages
    for counter, stage_no in enumerate(p.general.stages):
        general.run_stage(
            img, vol, stage_no, counter + 1, scope=globals(), config=p)

    return img


def create_cost_object(source, target, cnf):
    """
    Creates a script-wide uniform cost object based on the general cost
    function type setting.

    """
    p = AttrMap(cnf)

    if str(p.general.cost).lower() == "mind":
        return CostMIND(source, target, normalise=True, sigma=1.0,
                        truncate=1.5, kernel=MK_STAR)
    elif str(p.general.cost).lower() == "mi":
        return CostMI(source, target, normalise=True, bins=32)
    else:
        raise ValueError(f"Unrecognised cost function specification: "
                         f"{p.general.cost}")


def initialise_transformations(img, init, cnf):
    """
    Assigns a predefined transformation chain to the 2D image domain as a
    preparatory step to registration. This function modifies the input, hence
    it has no return value.

    :param img: input 2D image
    :type img: TImage
    :param init: initial parameters from configuration file regparams.init
    :type init: dict or AttrMap
    :param cnf: all configurations
    :type cnf: dict or AttrMap

    :returns: 2D image with modified domain
    :rtype: TImage

    """
    p = AttrMap(cnf)
    q = AttrMap(init)
    logger = logging.getLogger(p.logger)
    logger.info("Initialising transformation chain...")

    # Pre-warp transformations

    # Scale: isotropic or anisotropic?
    if p.general.isotropic:
        tx_scale = TxIsoScale(float(q.scale2d), dim=2, name="scale_2d")
    else:
        if isinstance(q.scale2d, Number):
            tx_scale = TxScale(q.scale2d, q.scale2d, name="scale_2d")
        elif hasattr(q.scale2d, "__iter__"):
            tx_scale = TxScale(*q.scale2d, name="scale_2d")
        else:
            raise ValueError(f"Invalid scale parameter specification: "
                             f"{q.scale2d}")

    prewarp = [
        tx_scale,
        TxRotation2D(float(q.rot2d), mode="deg", name="rotation_2d"),
        TxTranslation(*q.trans2d, name="translation_2d"),
        TxEmbed(1, name="embed")
    ]
    img.domain.chain.extend(prewarp)

    # Post-warp transformations
    # TxIdentity is a placeholder for TxRbfDisplacement that will be added
    # after stage 1. The reason is to avoid unnecessary calculations.
    postwarp = [
        TxIdentity(0, name="warp"),
        TxEulerAngles(*q.rot3d, order="zyx", mode="deg", centre=None,
                      name="rotation_3d"),
        TxTranslation(*q.trans3d, name="translation_3d"),
        TxAffine(np.asarray(q.affine3d), name="affine_3d"),
    ]
    img.domain.chain.extend(postwarp)


def create_warp_from_points(img, points, t, cnf):
    """
    Creates empty warp field based on the voxel coordinates of the support
    points on the fixed image domain.

    """
    p = AttrMap(cnf)
    dim = len(t.vectorder)
    _, ix = img.domain.get_transformation("embed", index=True)
    domain = Domain(points, transformations=img.domain.chain[:ix + 1],
                    offset=img.domain.offset)
    field = TField(domain=domain, tshape=(dim,), order=TENSOR_MAJOR,
                   dtype=ts.DEFAULT_FLOAT_TYPE, fill_value=0)

    if t.export.control_points:
        timestamp = datetime.now().strftime("%Y%m%d_%H%M%S_%f")
        fn = os.path.join(p.general.outputdir,
                          f"control_points_{timestamp}.png")
        np.savetxt(fn.replace(".png", ".txt"), points)
        save_control_point_image(img, points, fn, p)

    # Define bounds
    if t.vectorder == "xyz":
        lbxy = float(t.lower_dxy) * np.ones((2, field.domain.numel))
        lbz = float(t.lower_dz) * np.ones((1, field.domain.numel))
        ubxy = float(t.upper_dxy) * np.ones((2, field.domain.numel))
        ubz = float(t.upper_dz) * np.ones((1, field.domain.numel))
        lb = -np.concatenate((lbxy, lbz), axis=0).ravel()
        ub = np.concatenate((ubxy, ubz), axis=0).ravel()
    elif t.vectorder == "xy":
        lb = -float(t.lower_dxy) * np.ones((2, field.domain.numel))
        ub = float(t.upper_dxy) * np.ones((2, field.domain.numel))
    elif t.vectorder == "z":
        lb = -float(t.lower_dz) * np.ones(field.domain.numel)
        ub = float(t.upper_dz) * np.ones(field.domain.numel)
    else:
        raise ValueError(f"Invalid vector order: {t.vectorder}")

    # Create warp
    tx_warp = TxRbfDisplacementField(
        field, bounds=(lb, ub), name="warp", invertible=False,
        mode=NL_ABS, vectorder=t.vectorder, dense_shape=img.domain.shape)
    tx_warp.interpolator.model = t.model

    return tx_warp


def calc_adaptive_control_points(n_new, n_old, cost, stparams, cnf):
    """
    Uses superpixel segmentation to generate an approximate number of tiles,
    distributed evenly and compactly across the ROI of the target image, as
    defined by a mask. The control points are expected to have a higher
    frequency around non-matching regions, as measured by the regional cost.

    :param n_new: number of new points
    :type n_new: int
    :param n_old: number of existing points
    :type n_old: int
    :param cost: cost function object
    :type cost: Cost
    :param stparams: stage-specific parameters
    :type stparams: Any
    :param cnf: all configurations (used to access the logger)
    :type cnf: dict or AttrMap

    :returns: (n_points, 2) voxel coordinate array of control points
    :rtype: np.ndarray

    """
    from sklearn.cluster import KMeans
    p = AttrMap(cnf)
    t = AttrMap(stparams)
    logger = logging.getLogger(p.logger)

    # Create cost map
    costmap = cost.costmap()
    if costmap.tsize > 1:
        costmap = costmap.tensors.reduce()  # L2-norm
    else:
        costmap = np.abs(costmap)

    # Generate control regions
    n_points = int(t.support.points)
    mask = image.get_mask(TImage(cost._trg), scope=globals(), **t.support.mask)
    # Ensure that the mask is compatible with the slice image
    mask = TImage(mask).resize(*cost._trg.domain.shape, copy=True).data
    coords = np.vstack(np.where(mask > 0.5)).T
    km = KMeans(n_clusters=n_old+n_new, random_state=0)
    segmented = np.zeros_like(mask, dtype=int)
    labels = km.fit_predict(coords) + 1  # 0 is reserved for the bg
    segmented[mask > 0.5] = labels

    # Smooth the cost map to match the average support size of the ctrl points
    # Constrain the smoothing to the ROI
    area = np.count_nonzero(mask > 0.5)

    assert n_old + n_new <= n_points
    fwhm = 2 * np.sqrt(area / n_points / np.pi)
    sigma = fwhm / 2.355
    costmap.smooth(sigma, copy=False)
    costmap /= TImage(mask).smooth(sigma, copy=True)
    costmap.apply_mask(mask)  # hard masking
    costmap.data[~np.isfinite(costmap.data)] = 0
    logger.info(f"Smoothed cost function map by sigma = {sigma} px")

    # Sort the segments by their contribution to the cost
    cm = costmap.data.astype(np.float64)
    regions = regionprops(segmented, cm)
    regions = sorted(regions, key=lambda r: r.mean_intensity)[::-1]

    # Create control points
    centroids = [r.centroid for r in regions[:n_new]]
    # xi = np.stack(centroids[n_old:n_old + n_new], axis=0)
    xi = np.stack(centroids, axis=0)
    logger.info(f"Created {xi.shape[0]} new control points: \n{xi}")

    # Measure the average diameter of the segments
    mean_diameter = np.mean([r.equivalent_diameter for r in regions])

    # Measure the average distance between segment centroids
    edges = np.max(xi, axis=0) - np.min(xi, axis=0)
    edges = edges[np.nonzero(edges)]
    mean_distance = np.power(np.prod(edges) / len(regions), 1. / edges.size)

    # Report segmentation result
    logger.info(f"Mean segment diameter: {mean_diameter} px")
    logger.info(f"Mean segment distance: {mean_distance} px")

    # Return the voxel coordinates of the sorted control points
    return xi


def calc_km_control_points(n_new, img, stparams, cnf):
    """
    Uses k-means clustering to generate an exact number of tiles,
    distributed evenly and compactly across the ROI of the target image, as
    defined by a mask.

    :param n_new: number of new points
    :type n_new: int
    :param img: target image
    :type img: TImage
    :param stparams: stage-specific parameters
    :type stparams: Any
    :param cnf: all configurations
    :type cnf: dict or AttrMap

    :returns: (n_points, 2) voxel coordinate array of control points
    :rtype: np.ndarray

    """
    from sklearn.cluster import KMeans
    p = AttrMap(cnf)
    t = AttrMap(stparams)
    logger = logging.getLogger(p.logger)

    # Define ROI for the cost map
    tmp = img.copy().tensors.reduce()
    tmp.normalise()
    mask = image.get_mask(tmp, scope=globals(), **t.support.mask)
    mask = TImage(mask).resize(*tmp.domain.shape, copy=True).data
    coords = np.vstack(np.where(mask > 0.5)).T
    n_points = n_new
    km = KMeans(n_clusters=n_points, random_state=0)
    km.fit(coords)
    xi = km.cluster_centers_
    logger.info(f"Created {xi.shape[0]} new control points: \n{xi}")

    # Measure the average distance between segment centroids
    edges = np.max(xi, axis=0) - np.min(xi, axis=0)
    edges = edges[np.nonzero(edges)]
    mean_distance = np.power(np.prod(edges) / xi.shape[0], 1. / edges.size)
    logger.info(f"Mean control point distance: {mean_distance} px")

    # Return the voxel coordinates of the sorted control points
    return xi


def calc_halton_points(n_new, n_old, cost, stparams, cnf):
    """
    Returns an array of voxel coordinates, designated as "Halton support
    points". These pseudorandomly distributed points provide a uniform coverage
    of the ROI.

    """
    p = AttrMap(cnf)
    logger = logging.getLogger(p.logger)

    from skimage.filters import threshold_otsu
    if cost.target.tsize > 1:
        tmp = cost.target.copy().tensors.reduce()
        flatimg = tmp.data
    else:
        flatimg = cost.target.tensors[0].data
    th = threshold_otsu(flatimg)
    flatimg = np.squeeze(np.where(flatimg < th, 0, 1))
    bbox = _get_bbox(flatimg)
    points = _halton(stparams["support"]["points"], 0, **bbox)[:, ::-1]
    xi = points[n_old:n_old + n_new]
    logger.info(f"Created {xi.shape[0]} new control points: \n{xi}")

    return xi


def _get_bbox(mask):
    """
    Get 2D bounding box from binary image mask.

    :param mask: binary image mask
    :type mask: np.ndarray

    :return:
        Bounding box parameters.
        xmin: left border
        xmax: right border
        ymin: bottom border
        ymax: top border
    :rtype: dict

    """
    # Ensure that the mask is binary
    mask[mask > 0] = 1
    sy, sx = mask.shape

    # Determine tight bounding box
    x_proj = np.max(mask, axis=0)
    y_proj = np.max(mask, axis=1)
    x_nonzero = np.nonzero(x_proj)
    xmin = np.min(x_nonzero)
    xmax = np.max(x_nonzero)
    y_nonzero = np.nonzero(y_proj)
    ymin = np.min(y_nonzero)
    ymax = np.max(y_nonzero)

    # Create loose bounding box
    rel_margin = 0.05
    x_margin = int(rel_margin * (xmax - xmin))
    xmin = max(0, xmin - x_margin)
    xmax = min(sx, xmax + x_margin)
    y_margin = int(rel_margin * (ymax - ymin))
    ymin = max(0, ymin - y_margin)
    ymax = min(sy, ymax + y_margin)

    # Return bounding box parameters
    return {"xmin": xmin,
            "xmax": xmax,
            "ymin": ymin,
            "ymax": ymax}


def _halton(n_points, seed, **box):
    """
    Computes the location of Halton points for a given number of points and
    2D bounding box.

    :param n_points:
        Number of control points.
    :type n_points: int
    :param box:
        Bounding box definition by keyword arguments.

    :returns:
        (n_points, 2) table of Halton points, with xy layout (image convention).

    """
    import ghalton

    # Verify inputs
    assert isinstance(n_points, (int, np.integer))
    assert n_points > 0
    xmin = box.get("xmin", 0)
    xmax = box.get("xmax", xmin + 1)
    ymin = box.get("ymin", 0)
    ymax = box.get("ymax", ymin + 1)
    for boundval in [xmin, xmax, ymin, ymax]:
        assert isinstance(boundval, (int, float, np.floating, np.integer))
    assert xmax > xmin
    assert ymax > ymin

    # Create Halton grid
    sequencer = ghalton.Halton(2)
    if seed is not None:
        sequencer.seed(seed)
    points = sequencer.get(n_points)
    points = np.asarray(points)
    points[:, 0] = xmin + (xmax - xmin) * points[:, 0]
    points[:, 1] = ymin + (ymax - ymin) * points[:, 1]
    return points


def save_control_point_image(photo, cpoints, filename, cnf):
    p = AttrMap(cnf)
    logger = logging.getLogger(p.logger)
    # Creating the figure requires a window management system, so check this
    # first, and change the backend accordingly if it cannot be found.
    from tirl.utils import to_img
    if not os.getenv("DISPLAY") or not ts.ENABLE_VISUALISATION:
        import matplotlib
        matplotlib.use("agg")
    else:
        import matplotlib
        matplotlib.use(ts.MPL_BACKEND)
    import matplotlib.pyplot as plt

    # Create figure with photo and control points
    fig = plt.figure()
    ax = fig.add_subplot(111)
    order = photo.order
    photo.order = VOXEL_MAJOR
    if photo.tshape == ():
        ax.imshow(to_img(photo.data), cmap='gray', zorder=1)
    else:
        ax.imshow(to_img(photo.data), zorder=1)
    photo.order = order
    y, x = cpoints.T
    ax.scatter(x, y, c="magenta", zorder=2)

    # Save figure
    dot_file = os.path.join(filename)
    try:
        fig.savefig(dot_file)
    except Exception as exc:
        logger.error(exc.args)
        logger.info("Control point image could not be saved due to an error.")
    else:
        logger.info(f"Saved control point figure: {filename}")


def get_chain(img):

    # Get transformation handles
    tx_scale2d = img.domain.get_transformation("scale_2d")
    tx_rot2d = img.domain.get_transformation("rotation_2d")
    tx_trans2d = img.domain.get_transformation("translation_2d")
    tx_embed = img.domain.get_transformation("embed")
    tx_warp = img.domain.get_transformation("warp")
    tx_rot3d = img.domain.get_transformation("rotation_3d")
    tx_affine3d = img.domain.get_transformation("affine_3d")
    tx_trans3d = img.domain.get_transformation("translation_3d")

    return NamedChain(tx_scale2d, tx_rot2d, tx_trans2d, tx_embed,
                      tx_warp, tx_rot3d, tx_affine3d, tx_trans3d)


def set_parameters(img, initparams, cnf):
    """
    Assigns a set of parameters to a predefined transformation chain.
    The parameters are copied and modified in-place, hence the function has no
    return value and the values in the parameter source will not be changed
    even when the transformation chain gets updated during optimisation.

    :param img: input image with transformation chain
    :type img: TImage
    :param initparams: source of parameters
    :type initparams: dict
    :param cnf:
        all configurations (used to determine whether the scaling is
        anisotropic)
    :type cnf: dict or AttrMap

    """
    rad = np.deg2rad  # shorthand
    p = AttrMap(cnf)
    logger = logging.getLogger(p.logger)
    logger.debug("Setting the parameters of the transformation chain...")

    # Get transformation chain
    c = get_chain(img)

    # Get initial parameter values from input
    sxy = initparams.get("scale2d")
    if p.general.isotropic:
        c.tx_scale2d.parameters[:] = sxy
    else:
        if isinstance(sxy, Number):
            sx = sy = initparams.get("scale2d")
        elif hasattr(sxy, "__iter__"):
            sx, sy = initparams.get("scale2d")
        else:
            assert False  # this should never be reached
        c.tx_scale2d.parameters[:] = np.asarray([sx, sy])

    r2 = initparams.get("rot2d")
    c.tx_rot2d.parameters[0] = rad(float(r2))

    t2x, t2y = initparams.get("trans2d")
    dtype = c.tx_trans2d.parameters.dtype
    c.tx_trans2d.parameters[:] = np.asarray([t2x, t2y], dtype=dtype)

    c.tx_warp.parameters[:] = 0

    r3z, r3y, r3x = initparams.get("rot3d")
    dtype = c.tx_rot3d.parameters.dtype
    c.tx_rot3d.parameters[:] = rad(np.asarray([r3z, r3y, r3x], dtype=dtype))

    t3x, t3y, t3z = initparams.get("trans3d")
    dtype = c.tx_trans3d.parameters.dtype
    c.tx_trans3d.parameters[:] = np.asarray([t3x, t3y, t3z], dtype=dtype)

    affine3d = initparams.get("affine3d")
    dtype = c.tx_affine3d.parameters.dtype
    c.tx_affine3d.parameters[:] = np.asarray(affine3d, dtype=dtype)


def update_bounds(img, deltas, cnf):
    """
    Updates the optimisation bounds on the transformations of a predefined
    transformation chain. The boundary values are change in-place, hence the
    function has no return value and the source of the boundary updates will
    not change even when the transformation chain is being optimised.

    :param img: input image with transformation chain
    :type img: TImage
    :param deltas: optimisation boundary updates
    :type deltas: dict
    :param cnf: all configurations (used to inform about isotropic scaling)
    :type cnf: dict or AttrMap

    """
    rad = np.deg2rad  # shorthand
    p = AttrMap(cnf)  # global parameters are problematic in multiprocessing
    logger = logging.getLogger(p.logger)
    logger.debug("Updating parameter bounds of the transformation chain...")

    # Get transformation chain
    c = get_chain(img)

    # Get parameter bound updates
    s2l = deltas.get("scale2d_lower_delta", None)
    s2u = deltas.get("scale2d_upper_delta", None)
    if not p.general.isotropic:
        if isinstance(s2l, Number):
            s2lx = s2ly = deltas.get("scale2d_lower_delta", (None, None))
        elif hasattr(s2l, "__iter__"):
            s2lx, s2ly = deltas.get("scale2d_lower_delta", (None, None))
        else:
            assert False
        if isinstance(s2u, Number):
            s2ux = s2uy = deltas.get("scale2d_upper_delta", (None, None))
        elif hasattr(s2u, "__iter__"):
            s2ux, s2uy = deltas.get("scale2d_upper_delta", (None, None))
        else:
            assert False
    r2l = deltas.get("rot2d_lower_delta", None)
    r2u = deltas.get("rot2d_upper_delta", None)
    t2lx, t2ly = deltas.get("trans2d_lower_delta", (None, None))
    t2ux, t2uy = deltas.get("trans2d_upper_delta", (None, None))
    wlz = deltas.get("warp_lower_dz", None)
    wuz = deltas.get("warp_upper_dz", None)
    r3lz, r3ly, r3lx = deltas.get("rot3d_lower_delta", (None, None, None))
    r3uz, r3uy, r3ux = deltas.get("rot3d_upper_delta", (None, None, None))
    a3l = deltas.get("affine3d_lower_delta", None)
    a3u = deltas.get("affine3d_upper_delta", None)
    t3lx, t3ly, t3lz = deltas.get("trans3d_lower_delta", (None, None, None))
    t3ux, t3uy, t3uz = deltas.get("trans3d_upper_delta", (None, None, None))

    # Set new bounds
    if p.general.isotropic:
        if s2l is not None:
            sxy = c.tx_scale2d.parameters[:]
            c.tx_scale2d.parameters.set_bounds(
                (sxy - float(s2l), sxy + float(s2u)))
    else:
        if (s2lx is not None) and (s2ly is not None) and \
           (s2ux is not None) and (s2uy is not None):
            sx, sy = c.tx_scale2d.parameters[:]
            c.tx_scale2d.parameters.set_bounds({
                0: (sx - float(s2lx), sx + float(s2ux)),
                1: (sy - float(s2ly), sy + float(s2uy))
            })
    if (r2l is not None) and (r2u is not None):
        r2rad = c.tx_rot2d.parameters[0]
        c.tx_rot2d.parameters.set_bounds((r2rad - rad(float(r2l)),
                                          r2rad + rad(float(r2u))))
    if (t2lx is not None) and (t2ly is not None) and \
            (t2ux is not None) and (t2uy is not None):
        d2x, d2y = c.tx_trans2d.parameters[:]
        c.tx_trans2d.parameters.set_bounds(
            {0: (d2x - float(t2lx), d2x + float(t2ux)),
             1: (d2y - float(t2ly), d2y + float(t2uy))})
    if (wlz is not None) and (wuz is not None):
        warp_params = c.tx_warp.parameters[:]
        c.tx_warp.parameters.set_bounds(
            warp_params - float(wlz), warp_params + float(wuz))
    if (r3lz is not None) and (r3ly is not None) and (r3lx is not None) and \
            (r3uz is not None) and (r3uy is not None) and (r3ux is not None):
        rz, ry, rx = c.tx_rot3d.parameters[:]
        c.tx_rot3d.parameters.set_bounds(
            {0: (rz - rad(float(r3lz)), rz + rad(float(r3uz))),
             1: (ry - rad(float(r3ly)), ry + rad(float(r3uy))),
             2: (rx - rad(float(r3lx)), rx + rad(float(r3ux)))})
    if (a3l is not None) and (a3u is not None):
        aparam = c.tx_affine3d.parameters[:]
        c.tx_affine3d.parameters.set_bounds(aparam - a3l, aparam + a3u)
    if (t3lz is not None) and (t3ly is not None) and (t3lx is not None) and \
            (t3uz is not None) and (t3uy is not None) and (t3ux is not None):
        d3x, d3y, d3z = c.tx_trans3d.parameters[:]
        c.tx_trans3d.parameters.set_bounds(
            {0: (d3x - float(t3lx), d3x + float(t3ux)),
             1: (d3y - float(t3ly), d3y + float(t3uy)),
             2: (d3z - float(t3lz), d3z + float(t3uz))})


def remove_bounds(img, cnf):
    """
    Removes the optimisation bounds on the transformations of a predefined
    transformation chain. The boundary values are change in-place, hence the
    function has no return value.

    :param img: input image with transformation chain
    :type img: TImage

    """
    p = AttrMap(cnf)
    logger = logging.getLogger(p.logger)
    logger.debug("Removing parameter bounds from the transformation chain...")

    # Get transformation chain
    c = get_chain(img)

    # Remove bounds
    c.tx_scale2d.parameters.set_bounds(None)
    c.tx_rot2d.parameters.set_bounds(None)
    c.tx_trans2d.parameters.set_bounds(None)
    c.tx_warp.parameters.set_bounds(None)
    c.tx_rot3d.parameters.set_bounds(None)
    c.tx_affine3d.parameters.set_bounds(None)
    c.tx_trans3d.parameters.set_bounds(None)

###############################################################################
#                                  STAGES                                     #
###############################################################################


def stage1(img, vol, cnf):
    """
    Stage 1 aims to optimise the scale of the brain slice photograph, as well
    as to find the best initial position and orientation of the slice in MR
    space.

    """
    # Initialise stage-specific parameters
    p = AttrMap(cnf)
    logger = logging.getLogger(p.logger)
    t = p.regparams.stage_1
    s = t.slab

    # Initialise grid search parameters (slab)

    # Slab parameter: Position
    s.normal = np.divide(s.normal, np.linalg.norm(s.normal))  # unit normal vect
    kpos, s.posstep = np.linspace(-s.thickness / 2., s.thickness / 2.,
                                  s.n_positions, endpoint=True, retstep=True)
    offset = np.asarray(s.offset)
    if offset.size == 1:
        offset = float(offset) * s.normal
    s.positions = s.centre + offset + np.outer(kpos, s.normal)

    # Slab parameter: Orientation
    s.orientation = _calc_rotation(DEFAULT_SLICE_ORIENTATION, s.normal)
    phis = np.meshgrid(*tuple(
        np.linspace(r0 - r / 2, r0 + r / 2, n, endpoint=True)
        for r0, r, n in zip(s.orientation, np.deg2rad(s.range),
                            s.n_orientations)))
    s.rotations = np.stack(phis, axis=-1).reshape(-1, 3)
    s.rotstep = np.divide(np.deg2rad(s.range), np.asarray(s.n_orientations) + 1)

    logger.info(f"Slab centre: {s.centre + offset}")
    logger.info(f"Slab orientation: {np.rad2deg(s.orientation)} deg")
    logger.info(f"Test positions ({len(s.positions)}) "
                f"[a.u.] (1 position per row): \n{s.positions}")
    logger.info(f"Test orientations ({len(s.rotations)}) [deg] "
                f"(1 orientation per row): \n{np.rad2deg(s.rotations)}")
    logger.info(f"Number of stage-1 iterations: {t.iterations}")
    n_regs_current = len(s.positions) * len(s.rotations)
    n_regs_total = int(t.iterations) * n_regs_current
    if t.n_cpu < 0:
        n_cpu = max(min(int(t.n_cpu) % mp.cpu_count() + 1, n_regs_current), 1)
    else:
        n_cpu = max(min(int(t.n_cpu), n_regs_current), 1)

    logger.info(f"Altogether {n_regs_total} registrations remaining on "
                f"{n_cpu} CPU core(s).")

    # Raise error if nothing to do
    if (len(s.positions) == 0) and (len(s.rotations) == 0):
        raise AssertionError("Missing initialisation in stage 1.")

    sigfilter = None
    lowest_cost = np.inf
    best_position = None
    best_orientation = None
    best_mapping = None
    best_signature = None
    states = tuple(zip(t.slice_scaling, t.volume_scaling,
                       t.slice_smoothing, t.volume_smoothing))

    # Initialise a listener process and a pool of workers in multi-CPU setting
    if n_cpu > 1:
        queue = mp.Queue()  # for exchanging log messages
        listener = mp.Process(target=listener_process, args=(queue, p))
        listener.start()
        workers = mp.Pool(n_cpu,
                          initializer=stage1_worker_initialiser,
                          initargs=(queue,))

    # Use threading if only a single CPU is utilised
    else:
        import threading
        import queue
        queue = queue.Queue(-1)  # for exchanging log messages
        listener = threading.Thread(target=listener_process, args=(queue, cnf))
        listener.start()

    for state, (ssc, vsc, ssm, vsm) in enumerate(states):
        img.rescale(1. / ssc, copy=False)
        img_smooth = img.smooth(ssm, copy=True)
        vol.rescale(1. / vsc, copy=False)
        if vsm != 0:
            vol_smooth = vol.smooth(vsm, copy=True)
        else:
            vol_smooth = vol

        # Generate jobs for the workers
        jobs = get_stage1_jobs(
            img_smooth, vol_smooth, dict(p), s.positions, s.rotations,
            state, ssc, ssm, vsc, vsm, sigfilter)

        # It is necessary to force single-threaded computation to
        # avoid a performance drop when using multiple processes.
        if n_cpu > 1:
            general.set_single_threading()

        # Run stage 1
        # Using .map instead of .imap to report the number of filtered/executed
        # jobs in the logfile prior to the actual registrations taking place.
        if n_cpu > 1:
            import threadpoolctl
            try:
                api = threadpoolctl.threadpool_info()[0]["user_api"]
            except:
                api = "openblas"
            with threadpoolctl.threadpool_limits(1, user_api=api):
                parallel_result = workers.map(stage1_worker, jobs)
        else:
            stage1_worker_initialiser(queue)
            parallel_result = map(stage1_worker, jobs)

        if state == len(states) - 1:
            state_costs, signatures = zip(*parallel_result)
            best = np.argmin(state_costs)
            tmpdir = os.path.join(p.general.outputdir, "stage1_temp")
            tgfile = os.path.join(tmpdir, signatures[best] + ".tg")
            best_mapping = tg = tirl.load(tgfile)
            lowest_cost = tg.metaparameters.get("cost")
            best_position = tg.metaparameters.get("pos")
            best_orientation = tg.metaparameters.get("rot")
            best_signature = signatures[best]
        else:
            # Only proceed with the computation for the top x % (at least 1)
            state_costs, signatures = zip(*parallel_result)
            best = np.argsort(state_costs)
            best = best[:int(max(int(len(best) * float(t.top)), 1))]
            sigfilter = np.asarray(signatures)[best]

        img.domain.chain = img_smooth.domain.chain

    # Terminate any existing parallel processes
    if n_cpu > 1:
        workers.terminate()
        workers.join()

    # Terminate the listener process or thread
    queue.put_nowait(None)
    listener.join()

    # Revert to full scale and assign the best parameters
    img.rescale(1)
    vol.rescale(1)
    assert best_mapping is not None
    img.domain.chain = best_mapping.elements

    # Report best stats
    logger.info("Stage 1 main iteration complete.")
    logger.info(f"Best position: {best_position} a.u.")
    logger.info(f"Best orientation: {np.rad2deg(best_orientation)} "
                f"[deg] (internal rotations: z->y->x)")
    logger.info(f"Final cost function value: {lowest_cost}")
    logger.info(f"Best signature: {best_signature}")

    t.iterations -= 1

    if (t.iterations == 0) and t.try_unconstrained:
        # Try an unconstrained optimisation at full resolution from the best
        # initial position, with the parameters reset to default. If the cost is
        # higher, proceed with the best bounded optimisation result.
        logger.info("Performing non-constrained optimisation from the best "
                    "initial position and orientation...")
        t.constrained = False
        backup_best_bounded_result = img.domain.chain.copy()
        set_parameters(img, p.regparams.init, p)
        c = get_chain(img)
        remove_bounds(img, p)
        c.tx_trans3d.parameters[:] = best_position.copy()
        c.tx_rot3d.parameters[:] = best_orientation.copy()
        cost = create_cost_object(vol, img, p)
        stage1_2d(img, vol, p, cost=cost, logger=logger)
        unc_cost = stage1_3d(img, vol, p, cost=cost, logger=logger)
        logger.info(f"Unconstrained optimisation final cost: {unc_cost}")
        if unc_cost > lowest_cost:
            logger.info("Unconstrained optimisation yielded higher cost. "
                        "Falling back on the best bounded optimisation "
                        "result.")
            img.domain.chain = backup_best_bounded_result
            stage_cost = lowest_cost
        else:
            logger.info("Unconstrained optimisation yielded lower cost. "
                        "Keeping the new parameters.")
            stage_cost = unc_cost

        c = get_chain(img)
        st1chain = [c.tx_scale2d, c.tx_rot3d, c.tx_trans2d,
                    c.tx_rot3d, c.tx_rot3d]
        params = np.concatenate([tx.parameters[:] for tx in st1chain])
        logger.info(f"Stage 1 final parameters:\n{params}")

    # Decide on further iterations of stage 1
    # This condition reads as: "if there is stuff to iterate..."
    elif (t.iterations > 0) and \
            ((len(s.positions) > 1) or (len(s.rotations) > 1)):
        s.normal = \
            TxEulerAngles(*best_orientation, order="zyx", mode="rad")\
            .map(DEFAULT_SLICE_ORIENTATION).ravel()
        offset = np.asarray(s.offset)
        if offset.size == 1:
            offset = float(offset) * s.normal
        s.centre = best_position - offset
        s.thickness = s.posstep * 2
        s.range = np.rad2deg(s.rotstep * 2)
        stage_cost = stage1(img, vol, p)

    else:
        stage_cost = lowest_cost

    if t.cleanup is True:
        try:
            import shutil
            shutil.rmtree(os.path.join(p.general.outputdir, "stage1_temp"))
        except Exception as exc:
            logger.error(exc.args)
            logger.warning("An error occurred while deleting temporary files: ")
        else:
            logger.info("Removed stage 1 temporary files.")
    else:
        logger.info("Keeping stage 1 temporary files.")

    # Re-enable multi-threaded computation in NumPy
    if n_cpu > 1:
        general.set_multi_threading()

    return stage_cost


def get_stage1_jobs(img, vol, cnf, positions, rotations, state,
                    ssc, ssm, vsc, vsm, sigfilter):

    p = AttrMap(cnf)
    logger = logging.getLogger(p.logger)

    # Dump images to make them accessible by parallel processes
    tmpdir = os.path.join(p.general.outputdir, "stage1_temp")
    try:
        os.makedirs(tmpdir)
    except Exception:
        pass
    imfile = os.path.join(tmpdir, "fixed.timg")
    volfile = os.path.join(tmpdir, "moving.timg")
    img.save(imfile, overwrite=True)
    vol.save(volfile, overwrite=True)

    filtered = 0
    executed = 0
    for pos in positions:
        for rot in rotations:
            # Filter jobs based on their signature (to reduce workload)
            if sigfilter is not None:
                if get_signature(pos, rot) in sigfilter:
                    j = imfile, volfile, cnf, state, pos, rot, \
                        ssc, ssm, vsc, vsm
                    executed += 1
                    yield j
                else:
                    filtered += 1
            else:
                j = imfile, volfile, cnf, state, pos, rot, ssc, ssm, vsc, vsm
                executed += 1
                yield j
    else:
        logger.info(f"Filtered {filtered} job(s).")
        logger.info(f"Executed {executed} job(s).")


def get_signature(pos, rot):
    import hashlib
    h = hashlib.md5()
    params = np.concatenate((pos, rot)).astype(ts.DEFAULT_FLOAT_TYPE)
    h.update(params.tobytes())
    return h.hexdigest()


def listener_process(queue, cnf):
    logger = general.create_logger(cnf)
    while True:
        try:
            record = queue.get()
            if record is None:  # tells the listener to quit
                break
            logger.handle(record)
        except Exception:
            import sys, traceback
            print("Error while logging from worker process:", file=sys.stderr)
            traceback.print_exc(file=sys.stderr)


def stage1_worker_initialiser(queue):
    """
    Initialises worker process to allow safe logging from multiple simultaneous
    workers.

    """
    import logging.handlers
    h = logging.handlers.QueueHandler(queue)
    name = f"Process-{mp.current_process().pid}"
    root = logging.getLogger(f"slice_to_volume.stage1.{name}")
    # Avoid duplicate logs on repeated initialisation of the same process
    root.handlers = []
    root.addHandler(h)
    root.setLevel(1)


def stage1_worker(job):
    """
    Job function for stage 1 worker process. The calls in this function are
    executed in parallel for multiple initialisations (position & orientation).

    """
    name = f"Process-{mp.current_process().pid}"
    # Load the process-specific logger instance
    logger = logging.getLogger(f"slice_to_volume.stage1.{name}")
    logger.debug(f"Worker started: {name}")

    imfile, volfile, cnf, state, pos, rot, ssc, ssm, vsc, vsm = job

    p = AttrMap(cnf)
    t = p.regparams.stage_1

    # Load images
    _img = tirl.load(imfile)
    _img.storage = MEM
    _vol = tirl.load(volfile)
    _vol.storage = MEM

    # Name of the transformation chain dump on disk
    signature = get_signature(pos, rot)
    tgfile = os.path.join(os.path.dirname(imfile), signature + ".tg")

    # Initialise parameters: set to initial values, or load from previous run
    if state == 0:
        set_parameters(_img, p.regparams.init, p)
        c = get_chain(_img)
        c.tx_trans3d.parameters[:] = pos
        c.tx_rot3d.parameters[:] = rot
    else:
        # Load parameters that were previously optimised at the given position
        # and orientation, usually at a coarser resolution or with more
        # smoothing.
        tg = tirl.load(tgfile)
        for itx, tx in enumerate(_img.domain.chain):
            tx.parameters[:] = tg.elements[itx].parameters[:]

    # Update bounds
    if t.constrained:
        update_bounds(_img, t.stage_1a, p)
        update_bounds(_img, t.stage_1b, p)
    else:
        remove_bounds(_img, p)

    # Create cost object
    cost = create_cost_object(_vol, _img, p)

    # Run local 2D + 3D optimisation
    # proc = f"[Process {os.getpid()}]"
    drot = np.rad2deg(rot)
    logger.debug(f"Initial cost @ pos={pos} rot={drot} "
                 f"ssc={ssc}, ssm={ssm}, vsc={vsc}, vsm={vsm} "
                 f"(sig={signature}): {cost()}")
    _vol.evaluate(_img.domain).snapshot(
        tgfile.replace(".tg", "_init.png"), True)
    lcost = stage1_2d(_img, _vol, p, cost, logger)
    logger.debug(f"Updated cost after 2D optimisation "
                 f"@ pos={pos} rot={drot} "
                 f"ssc={ssc}, ssm={ssm}, vsc={vsc}, vsm={vsm} "
                 f"(sig={signature}): {lcost}")
    _vol.evaluate(_img.domain).snapshot(tgfile.replace(".tg", "_a.png"), True)
    lcost = stage1_3d(_img, _vol, p, cost, logger)
    logger.debug(f"Updated cost after 3D optimisation "
                 f"@ pos={pos} rot={drot} "
                 f"ssc={ssc}, ssm={ssm}, vsc={vsc}, vsm={vsm} "
                 f"(sig={signature}): {lcost}")
    _vol.evaluate(_img.domain).snapshot(tgfile.replace(".tg", "_b.png"), True)

    # Dump optimised parameters for the next state or for the main process,
    # which will select the best parameters based on the lowest cost.
    tg = TransformationGroup(*_img.domain.chain)
    c = get_chain(_img)
    newpos = c.tx_trans3d.parameters[:]
    newrot = c.tx_rot3d.parameters[:]
    tg.metaparameters.update(pos=newpos, rot=newrot, cost=lcost)  # for main
    tg.save(tgfile, overwrite=True)

    # This resolves a memory leak that I spent half a day
    # finding on 20 July 2020. The leak is caused by the CostMIND object that
    # stores a copy of the images. The reference to this array must be first
    # deleted from the cost object, then from the _vol object.
    del tg  # this is not strictly necessary
    del cost._source
    del cost._target
    del cost._src
    del cost._trg
    del cost
    del _img, _vol
    # Direct invocation of the garbage collector is not necessary.
    # import gc
    # gc.collect()

    return lcost, signature


def stage1_2d(img, vol, cnf, cost, logger):
    """
    Stage 1a: optimises the 2D linear subchain.

    """
    # print(img.centre())
    # print(vol.centre())
    # (vol.evaluate(img.domain) - img).preview()
    p = AttrMap(cnf)
    t = p.regparams.stage_1

    if t.constrained:
        update_bounds(img, t.stage_1a, p)
    else:
        remove_bounds(img, p)

    # Create optimisation group
    c = get_chain(img)
    og2d = OptimisationGroup(c.tx_scale2d, c.tx_rot2d, c.tx_trans2d)

    # Adjust step size
    if NORMALISE:
        initial_step = t.opt_step
    else:
        lb, ub = og2d.get_bounds().T
        initial_step = absolute_step(lb, ub, t.opt_step)

    # Optimise parameters and update bounds
    xtol_abs = np.asarray(t.stage_1a.xtol_abs)
    if (not p.general.isotropic) and (xtol_abs.size == 4):
        xtol_abs = np.append(np.asarray(xtol_abs[0]), xtol_abs).ravel()
    method = OptMethods.LN_BOBYQA if t.constrained else OptMethods.LN_NEWUOA
    OptNL(og2d, cost, method=method, xtol_abs=xtol_abs, logger=logger,
          step=initial_step, visualise=t.visualise, normalised=NORMALISE)()
    update_bounds(img, t.stage_1a, p)

    return cost()


def stage1_3d(img, vol, cnf, cost, logger):
    """
    Stage 1a: optimises the 3D linear subchain.

    """
    # (vol.evaluate(img.domain) - img).preview()
    p = AttrMap(cnf)
    t = p.regparams.stage_1

    if t.constrained:
        update_bounds(img, t.stage_1b, p)
    else:
        remove_bounds(img, p)

    # Define optimiser
    c = get_chain(img)
    og3d = OptimisationGroup(c.tx_scale2d, c.tx_rot3d, c.tx_trans3d)
    method = OptMethods.LN_BOBYQA if t.constrained else OptMethods.LN_NEWUOA

    if NORMALISE:
        initial_step = t.opt_step
    else:
        lb, ub = og3d.get_bounds().T
        initial_step = absolute_step(lb, ub, t.opt_step)

    # Optimise parameters and update bounds
    xtol_abs = np.asarray(t.stage_1b.xtol_abs)
    if (not p.general.isotropic) and (xtol_abs.size == 7):
        xtol_abs = np.append(np.asarray(xtol_abs[0]), xtol_abs).ravel()
    OptNL(og3d, cost, method=method, xtol_abs=xtol_abs, logger=logger,
          step=initial_step, visualise=t.visualise, normalised=NORMALISE)()
    update_bounds(img, t.stage_1b, p)

    return cost()


def absolute_step(lb, ub, step):
    di = ub - lb
    di[~np.isfinite(di)] = 2
    if np.any(di <= 0):
        raise AssertionError("No space for optimisation.")
    return np.multiply(0.5 * di, step)


def stage2(img, vol, cnf):
    """
    Stage 2 - performs 2D/3D affine registration from the best rigid
    registration as a starting position.

    :param img: 2D image with registration chain
    :type img: TImage
    :param vol: 3D image with no non-linear transformations
    :type vol: TImage
    :param cnf: all configurations
    :type cnf: dict or AttrMap

    :returns: 2D image at best affine alignment with the 3D image.
    :rtype: TImage

    """
    p = AttrMap(cnf)
    t = p.regparams.stage_2
    logger = logging.getLogger(p.logger)

    stage_cost = []
    states = tuple(zip(t.slice_scaling, t.volume_scaling,
                       t.slice_smoothing, t.volume_smoothing))
    for i, (ssc, vsc, ssm, vsm) in enumerate(states):

        logger.info(f"Optimising affine alignment @ "
                    f"(ssc={ssc}, vsc={vsc}, ssm={ssm}px, vsm={vsm}px)")

        remove_bounds(img, p)
        img.rescale(1. / ssc, copy=False)  # resampling in place
        img_smooth = img.smooth(ssm, copy=True)
        vol.rescale(1. / vsc, copy=False)  # resampling in place
        if vsm != 0:
            vol_smooth = vol.smooth(vsm, copy=True)
        else:
            vol_smooth = vol

        # Update parameter bounds
        c = get_chain(img_smooth)
        params = c.tx_affine3d.parameters[:]
        ldx = t.lower_delta
        udx = t.upper_delta

        # Create cost function
        cost = create_cost_object(vol_smooth, img_smooth, p)
        logger.info(f"Initial cost: {cost()}")

        # Optimise
        c.tx_affine3d.parameters.set_bounds(params - ldx, params + udx)
        if NORMALISE:
            initial_step = t.opt_step
        else:
            lb, ub = c.tx_affine3d.parameters.get_bounds().T
            initial_step = absolute_step(lb, ub, t.opt_step)
        OptNL(c.tx_affine3d, cost, method=OptMethods.LN_BOBYQA,
              visualise=t.visualise, logger=logger, xtol_rel=t.xtol_rel,
              step=initial_step, normalised=NORMALISE)()
        logger.info(f"Updated cost: {cost()}")

        if i + 1 == len(states):
            stage_cost.append(cost())

        img.domain = img_smooth.domain

    img.rescale(1)
    vol.rescale(1)

    logger.info(f"Stage 2 final cost: {stage_cost[-1]}")
    tx = img.domain.get_transformation("affine_3d")
    logger.info(f"Stage 2 final parameters:\n{tx.parameters[:]}")

    return stage_cost[-1]


def stage3(img, vol, cnf):
    """
    Stage 3 - transforms the slice non-linearly along one, two or all three
    spatial axes. Depending on the type of restriction ("z", "xy") this
    stage can be used as a preround for free-form deformation, aiming to
    compensate either in-plane ("xy") or through-plane ("z") deformations
    independently from the other.

    :param img: 2D image with registration chain
    :type img: TImage
    :param vol: 3D image
    :type vol: TImage
    :param cnf: all configurations
    :type cnf: dict or AttrMap

    :returns: 2D image with optimised off-plane curvatures.
    :rtype: TImage

    """
    p = AttrMap(cnf)
    t = p.regparams.stage_3
    logger = logging.getLogger(p.logger)

    # Smoothing iteration
    stage_cost = []

    # Initialise warp handle
    warp = None

    states = tuple(zip(t.slice_scaling, t.volume_scaling,
                       t.slice_smoothing, t.volume_smoothing))

    for sm_no, (ssc, vsc, ssm, vsm) in enumerate(states):
        logger.info(f"Smoothing: ssc={ssc}, ssm={ssm}px, vsc={vsc}, "
                    f"vsm={vsm}vx.")
        img.rescale(1. / ssc, copy=False)
        img.storage = MEM
        img_smooth = img.smooth(ssm, copy=True)
        vol.rescale(1. / vsc, copy=False)
        vol.storage = MEM
        if vsm != 0:
            vol_smooth = vol.smooth(vsm, copy=True)
        else:
            vol_smooth = vol
        cost = create_cost_object(vol_smooth, img_smooth, p)
        logger.info(f"Initial cost: {cost()}")

        # How many support points?
        # Read from configuration file.
        if isinstance(t.support.points, int):
            n_points = t.support.points
            calculate_points = True
            points_iter = None
        elif isinstance(t.support.points, (list, tuple)):
            n_points = len(t.support.points)
            points_iter = t.support.points
            calculate_points = True
        # Read from external file
        elif isinstance(t.support.points, str) and \
                os.path.isfile(t.support.points):
            xi = np.loadtxt(t.support.points)
            n_points = xi.shape[0]
            calculate_points = False
            points_iter = None
        # Invalid input
        else:
            raise ValueError("Invalid support point specification.")

        # How many points should be optimised at once?
        # Optimise all points for any subsequent iteration, as little benefit
        # is anticipated from repeating the whole optimisation sequence.
        if sm_no == 0:
            optsize = int(min(int(t.optsize), n_points))
            points_iter = points_iter or \
                (*range(optsize, n_points, optsize), n_points)
        else:
            points_iter = (n_points,)

        # ---> POINT OPTIMISATION SEQUENCE
        for n_current in points_iter:

            # --------------------- CREATE NEW WARP -------------------- #

            # Create new point set
            if calculate_points:
                if str(t.support.mode).lower() == "halton":
                    xi_current = calc_halton_points(n_current, 0, cost, t, p)
                elif str(t.support.mode).lower() == "km":
                    xi_current = calc_km_control_points(n_current, img, t, p)
                elif str(t.support.mode).lower() == "cost_adaptive":
                    xi_current = calc_adaptive_control_points(
                        n_current, 0, cost, t, p)
                else:
                    raise ValueError(f"Unrecognised support point scheme: "
                                     f"{t.support.mode}")
            else:
                xi_current = xi[:n_current]

            # Create new transformation with the old and the new points
            newwarp = create_warp_from_points(img, xi_current, t, p)

            # If a transformation was previously optimised, fit the parameters
            # of the new transformation to recreate the displacement field.
            # This carries forward the information from previous optimisations.
            if warp is not None:
                newwarp.fit(warp.std(3).absolute().field())

            warp = newwarp

            # Update transformation parameter bounds
            params = warp.parameters[:]
            params = params.reshape(len(t.vectorder), -1)
            lb = params.copy()
            ub = params.copy()
            if t.vectorder == "xyz":
                lb[:2] -= float(t.lower_dxy)
                lb[2] -= float(t.lower_dz)
                ub[:2] += float(t.upper_dxy)
                ub[2] += float(t.upper_dz)
            elif t.vectorder == "xy":
                lb[:] -= float(t.lower_dxy)
                ub[:] += float(t.upper_dxy)
            elif t.vectorder == "z":
                lb[:] -= float(t.lower_dz)
                ub[:] += float(t.upper_dz)

            if NORMALISE:
                initial_step = t.opt_step
            else:
                initial_step = absolute_step(lb.ravel(), ub.ravel(), t.opt_step)
            warp.parameters.set_bounds(lb.ravel(), ub.ravel())
            warp.parameters.unlock()

            # Add warp to the chain
            _, ix = img_smooth.domain.get_transformation("warp", index=True)
            img_smooth.domain.chain[ix] = warp
            # Recreate cost object, because the chain was modified
            cost = create_cost_object(vol_smooth, img_smooth, p)

            # --------------------- OPTIMISE NEW WARP -------------------- #

            # Regularisation
            if t.regweight > 0:
                reg = RegBendingEnergy(warp, weight=t.regweight, logger=logger)
                # reg = NormTxRegulariser(warp, order=2, weight=t.regweight)
                cost_items = [cost, reg]
            else:
                cost_items = [cost]

            # Optimisation
            logger.debug(f"Optimising the following control points:"
                         f"\n{warp.domain.get_voxel_coordinates()}")
            OptNL(warp, *cost_items, method=OptMethods.LN_BOBYQA,
                  step=initial_step, xtol_abs=t.xtol_abs, logger=logger,
                  visualise=t.visualise, normalised=NORMALISE)()

            # Create snapshot
            timestamp = datetime.now().strftime("%Y%m%d_%H%M%S_%f")
            fn = os.path.join(p.general.outputdir,
                f"stage3_{n_current}pts_{timestamp}."
                f"{ts.TIMAGE_DEFAULT_SNAPSHOT_EXT}")
            vol_smooth.evaluate(img_smooth.domain).snapshot(fn, overwrite=True)

        # <--- POINT OPTIMISATION SEQUENCE

        # Record the final cost at the current smoothing level
        if sm_no + 1 == len(states):
            stage_cost.append(cost())
            logger.info(f"Final cost @ [ssm={ssm}px vsm={vsm}vx]: "
                        f"{stage_cost[-1]}")

        # Copy optimised transformations before creating a new image from
        # the original. [This may no longer be necessary.]
        img.domain = img_smooth.domain

    img.rescale(1)
    vol.rescale(1)

    return stage_cost[-1]


def stage4(img, vol, cnf):
    """
    Stage 4 - performs regularised free-form deformation in all three spatial
    dimensions to achieve an optimal alignment with the 3D image.

    :param img: 2D image with registration chain
    :type img: TImage
    :param vol: 3D image
    :type vol: TImage
    :param cnf: all configurations
    :type cnf: dict or AttrMap

    :returns: 2D image with optimised off-plane curvatures.
    :rtype: TImage

    """
    p = AttrMap(cnf)
    t = p.regparams.stage_4
    logger = logging.getLogger(p.logger)

    # Smoothing iteration
    stage_cost = []

    states = tuple(zip(t.slice_scaling, t.volume_scaling,
                       t.slice_smoothing, t.volume_smoothing))

    for sm_no, (ssc, vsc, ssm, vsm) in enumerate(states):
        logger.info(f"Smoothing: ssc={ssc}, ssm={ssm}px, vsc={vsc}, "
                    f"vsm={vsm}vx.")
        img.rescale(1. / ssc, copy=False)
        img.storage = MEM
        img_smooth = img.smooth(ssm, copy=True)
        vol.rescale(1. / vsc, copy=False)
        vol.storage = MEM
        if vsm != 0:
            vol_smooth = vol.smooth(vsm, copy=True)
        else:
            vol_smooth = vol
        cost = create_cost_object(vol_smooth, img_smooth, p)
        logger.info(f"Initial cost: {cost()}")

        # How many support points?
        # Read from configuration file.
        if isinstance(t.support.points, int):
            n_points = t.support.points
            calculate_points = True
            points_iter = None
        elif isinstance(t.support.points, (list, tuple)):
            n_points = int(max(t.support.points))
            points_iter = t.support.points
            calculate_points = True
        # Read from external file
        elif isinstance(t.support.points, str) and \
                os.path.isfile(t.support.points):
            xi = np.loadtxt(t.support.points)
            n_points = xi.shape[0]
            calculate_points = False
            points_iter = None
        # Invalid input
        else:
            raise ValueError("Invalid support point specification.")

        # How many points should be optimised at once?
        # Optimise all points for any subsequent iteration, as little benefit
        # is anticipated from repeating the whole optimisation sequence.
        if sm_no == 0:
            optsize = int(min(int(t.optsize), n_points))
            points_iter = points_iter or \
                (*range(optsize, n_points, optsize), n_points)
        else:
            points_iter = (n_points,)

        # Obtain stage3-optimised warp. This will not exist if stage 3 is
        # skipped.
        try:
            # Note: the first line also raises IndexError if tx is not found
            warp = img_smooth.domain.get_transformation("warp")
            if not isinstance(warp, TxRbfDisplacementField):
                raise IndexError
        except IndexError:
            warp = None

        # ---> POINT OPTIMISATION SEQUENCE
        for n_current in points_iter:

            # --------------------- CREATE NEW WARP -------------------- #

            # Create new point set
            if calculate_points:
                if str(t.support.mode).lower() == "halton":
                    xi_current = calc_halton_points(n_current, 0, cost, t, p)
                elif str(t.support.mode).lower() == "km":
                    xi_current = calc_km_control_points(n_current, img, t, p)
                elif str(t.support.mode).lower() == "cost_adaptive":
                    xi_current = calc_adaptive_control_points(
                        n_current, 0, cost, t, p)
                else:
                    raise ValueError(f"Unrecognised support point scheme: "
                                     f"{t.support.mode}")
            else:
                xi_current = xi[:n_current]

            # Create new transformation with the old and the new points
            newwarp = create_warp_from_points(img, xi_current, t, p)

            # If a transformation was previously optimised, fit the parameters
            # of the new transformation to recreate the displacement field.
            # This carries forward the information from previous optimisations.
            if warp is not None:
                # Note: std() is necessary, as we potentially change from a
                # unidirectional field to a 3D vector field here.
                newwarp.fit(warp.std(3).absolute().field())

            # Avoid fitting to the initial zero field by largely fluctuating
            # points
            # if n_old == 0:
            #     warp.parameters.parameters[:2 * n_new] = 0

            warp = newwarp

            # Update transformation parameter bounds
            params = warp.parameters[:]
            params = params.reshape(len(t.vectorder), -1)
            lb = params.copy()
            ub = params.copy()
            if t.vectorder == "xyz":
                lb[:2] -= float(t.lower_dxy)
                lb[2] -= float(t.lower_dz)
                ub[:2] += float(t.upper_dxy)
                ub[2] += float(t.upper_dz)
            elif t.vectorder == "xy":
                lb[:] -= float(t.lower_dxy)
                ub[:] += float(t.upper_dxy)
            elif t.vectorder == "z":
                lb[:] -= float(t.lower_dz)
                ub[:] += float(t.upper_dz)

            if NORMALISE:
                initial_step = t.opt_step
            else:
                initial_step = absolute_step(lb.ravel(), ub.ravel(), t.opt_step)
            warp.parameters.set_bounds(lb.ravel(), ub.ravel())
            warp.parameters.unlock()

            # Add warp to the chain
            _, ix = img_smooth.domain.get_transformation("warp", index=True)
            img_smooth.domain.chain[ix] = warp

            # Recreate cost object, because the chain was modified
            cost = create_cost_object(vol_smooth, img_smooth, p)

            # --------------------- OPTIMISE NEW WARP -------------------- #

            # Regularisation
            if t.regweight > 0:
                reg = RegBendingEnergy(warp, weight=t.regweight, logger=logger)
                # reg = NormTxRegulariser(warp, order=2, weight=t.regweight)
                cost_items = [cost, reg]
            else:
                cost_items = [cost]

            # Optimisation
            logger.debug(f"Optimising the following control points:"
                         f"\n{warp.domain.get_voxel_coordinates()}")
            OptNL(warp, *cost_items, method=OptMethods.LN_BOBYQA,
                  step=initial_step, xtol_abs=t.xtol_abs, logger=logger,
                  visualise=t.visualise, normalised=NORMALISE)()

            # Create snapshot
            timestamp = datetime.now().strftime("%Y%m%d_%H%M%S_%f")
            fn = os.path.join(p.general.outputdir,
                f"stage4_{n_current}pts_{timestamp}."
                f"{ts.TIMAGE_DEFAULT_SNAPSHOT_EXT}")
            vol_smooth.evaluate(img_smooth.domain).snapshot(fn, overwrite=True)

        # <--- POINT OPTIMISATION SEQUENCE

        # Record the final cost at the current smoothing level
        if sm_no + 1 == len(states):
            stage_cost.append(cost())
            logger.info(f"Final cost @ [ssm={ssm}px, vsm={vsm}vx]: "
                        f"{stage_cost[-1]}")

        # Copy optimised transformations before creating a new image from
        # the original. [This may no longer be necessary.]
        img.domain = img_smooth.domain

    img.rescale(1)
    vol.rescale(1)

    return stage_cost[-1]


def stage5(img, vol, cnf):
    """
    Stage 5 re-estimates the parameters of the 3D affine transformation in
    light of the result of the non-linear transformation(s). At the same time,
    the parameters of the non-linear transformation are set to zero.

    :param img: 2D image
    :type img: TImage
    :param vol: 3D image
    :type vol: TImage
    :param cnf: all configurations
    :type cnf: dict or AttrMap

    :return: stage 5 cost
    :rtype: float

    """
    p = AttrMap(cnf)
    t = p.regparams.stage_5
    logger = logging.getLogger(p.logger)
    c = get_chain(img)

    # Not an elegant solution, I know, but this is much simpler than the
    # alternatives.
    # I use a script-wide global variable to store the coordinates of the image
    # after the last stage that preceded stage 5. The current coordinates from
    # the preceding optimisation is then averaged with the coordinates from
    # cycle before. These coordinates are then approximated by an affine, using
    # the current chain as the starting point.
    global prev_cycle_coords
    global cycle_counter

    # Define ROI for which the linear re-estimation is calculated
    mask = image.get_mask(img, scope=globals(), **t.mask)

    # Generate homogeneous image coordinates from the optimised non-linear
    # and the equivalent linearised domain.
    nonlinear_domain = img.domain[:]
    nonlinear_domain.chain.append(TxIdentity(1))  # for homogeneous coordinates
    linear_domain = nonlinear_domain.copy()
    warp, ix = nonlinear_domain.get_transformation("warp", index=True)
    # Discard the in-plane component of the transformation, as this would
    # unnecessarily rotate the plane out of the best affine position.
    if warp.vectorder == (0, 1, 2):
        inplane = warp.parameters[:int(round(warp.parameters.size * 2 / 3))]
        warp.parameters[:int(round(warp.parameters.size*2/3))] = 0
    else:
        inplane = 0
    linear_domain.chain.pop(ix)  # removing all non-linear part
    # Note: affine calculation is inaccurate if coordinates are floats
    # Average coordinates with the result of the previous non-linear mapping
    # from the equivalent stage.
    y = nonlinear_domain.get_physical_coordinates().T.astype(np.float64)
    prev_cycle_coords = prev_cycle_coords + y[:3]
    cycle_counter += 1
    y = prev_cycle_coords / cycle_counter
    yl = linear_domain.get_physical_coordinates().T.astype(np.float64)

    # Calculate best affine linear->nonlinear mapping
    # Restrict this calculation to the ROI, otherwise far-off edges may
    # contribute a significant rotation component.
    mask = mask.ravel() > 0.1

    from numpy.linalg import lstsq
    y = np.pad(y, ((0, 1), (0, 0)), mode="constant", constant_values=1)
    A = lstsq(yl[:, mask] @ yl[:, mask].T, yl[:, mask] @ y[:, mask].T)[0].T
    A[-1, :] = 0
    A[-1, -1] = 1
    logger.info(f"Best affine for linearisation: \n{A}")

    # Apply the newly estimated affine correction
    # This assumes that the affine is the last transformation in the chain
    ix = img.domain.get_transformation("affine_3d", index=True)[-1]
    assert ix == len(img.domain.chain) - 1
    homo = np.eye(4)
    homo[:3, :] = c.tx_affine3d.matrix
    c.tx_affine3d.parameters[:] = (A @ homo)[:3, :].ravel()
    # Remove affine parameter bounds
    c.tx_affine3d.parameters.set_bounds(None)
    c.tx_affine3d.parameters.unlock()

    # Reset through-plane deformation non-linear parameters
    if warp.vectorder == (0, 1, 2):
        n_xy = int(round(warp.parameters.size * 2 / 3))
        c.tx_warp.parameters[:n_xy] = inplane
        c.tx_warp.parameters[n_xy:] = 0
    else:
        c.tx_warp.parameters[...] = 0
    # Remove warp parameter bounds
    c.tx_warp.parameters.set_bounds(None)
    c.tx_warp.parameters.unlock()


                            # PROGRAM EXECUTION CONTROL

def slice_to_volume(args):
    """
    Main program code. Controls program flow, handles command-line arguments.

    """
    cnf = args.config
    if os.path.isfile(cnf):
        cnf = general.load_configurations(cnf)
    else:
        raise FileNotFoundError(f"The provided configuration file "
                                f"does not exist: {args.config}")

    # Override tissue block and brain slice file paths in the configuration
    # file with those provided on the command line.
    if (args.slice is not None) and os.path.isfile(args.slice):
        cnf["slice"]["file"] = args.slice

    if (args.volume is not None) and os.path.isfile(args.volume):
        cnf["volume"]["file"] = args.volume

    if args.out is not None:
        cnf["general"]["outputdir"] = args.out

    # Override verbose option in configurations
    cnf["general"]["verbose"] = args.verbose

    # Run registration script
    run(cnf)


def create_cli():
    """
    Sets up the CLI argument parser instance.

    """
    descr = "This utility registers a 2D image (slice) to a 3D image (volume)."
    usage = "slice_to_volume -c cnf_slice_to_volume.yml " \
            "[-i <image> -v <volume>] "
    parser = argparse.ArgumentParser(
        prog="slice_to_volume", usage=usage, description=descr)

    # Add command-line arguments
    parser.add_argument("--config", metavar="cnf_file.yml", type=str,
                        default="", required=True, help="Configuration file.")
    parser.add_argument("--slice", metavar="2D", type=str, default=None,
                        required=False, help="2D image file")
    parser.add_argument("--volume", metavar="3D", type=str, default=None,
                        required=False, help="3D image file")
    parser.add_argument("--out", metavar="dir", type=str, default=None,
                        required=False, help="Output directory")
    parser.add_argument("-v", "--verbose", default=False, action="store_true",
                        help="Print status messages to the command line",
                        required=False)
    return parser


                            # AUXILIARY FUNCTIONS

# These functions are used within other functions of the script. Their usage
# outside this script (via importing from this module) is discouraged. They are
# included here to prevent too frequent code repetitions.

def slice_preprocessing(slice_, history=None, other=None, **kwargs):
    """
    Action function to preprocess the brain slice photograph.

    """
    # Convert to grayscale using the Y channel of the YIQ colour space.
    if slice_.tsize >= 3:
        mat = np.asarray([[0.299, 0.587, 0.114],
                          [0.5959, -0.2746, -0.3213],
                          [0.2115, -0.5227, 0.3112]])
        yiq = TensorOperator(lambda t: np.einsum("ij,...j", mat, t))
        slice_ = yiq(slice_.tensors[:3]).tensors[0]

    # Normalise image values (for optimised display)
    slice_.normalise()

    return slice_


def volume_preprocessing(volume, history=None, other=None, **kwargs):
    """
    Action function to preprocess the MRI volume.

    """
    # Normalise image values (for optimised display)
    volume.normalise()

    return volume


def _calc_rotation(a, b):
    """
    Calculates rotation between two 3D vectors and returns Euler angles.

    :param a: vector A in 3D space
    :type a: np.ndarray or list or tuple
    :param b: vector B in 3D space
    :type b: np.ndarray or list or tuple

    :returns: Euler angles ("zyx" convention)
    :rtype: tuple[float]

    """
    a = np.asarray(a, dtype=np.float64)
    b = np.asarray(b, dtype=np.float64)
    assert a.size == b.size == 3, "Vectors must be 3-dimensional."
    a = a / np.linalg.norm(a)
    b = b / np.linalg.norm(b)
    axis = np.cross(a, b)
    s = np.linalg.norm(axis)
    c = np.sum(a * b)
    angle = np.arctan2(s, c)
    angles = TxAxisAngle(angle, axis, mode="rad").euler(mode="rad")

    return angles


                            # USER DEFINITIONS

"""
This section is dedicated to the user-defined action functions that are
performed on either or both the volume and the slice data before registration
begins.

Action functions may have an arbitrary name, but they must have a common 
signature. All actions must take the image as their first positional argument, 
and the function must take keyword arguments. The image is either the original 
image (for the first action in the chain) or the output of the previous action. 
An action may return the modified image or make implicit changes. In the case 
of implicit changes, the function must return None. If an action does not 
return an image, the next action receives the last available image from the 
action_history dict, which is also supplied as a keyword argument. 

def action_func(img, **kwargs):
    [...]
    return new_img or None

.. where:
    img - the image input (volume/slice image will be automatically assigned)

.. automatically assigned keyword arguments:  
    .. action_history
        a dictionary that stores the return values of all preceding actions in 
        the following format: {action_func: return_value}
    .. other
        other TImage: for a slice image, this is the volume data, and for the 
        volume data, this is the slice image 

"""


def match_volume_resolution(img, **kwargs):
    """
    Resamples the 2D image to match the resolution of the 3D image.

    """
    p = AttrMap(kwargs.get("cnf"))
    logger = logging.getLogger(p.logger)
    vol = kwargs.get("other")

    # MRI coronal slice resolution (RAS convention)
    for tx in vol.domain.external:
        if tx.name in ("affine", "sform", "qform"):
            q, r = np.linalg.qr(tx.matrix[:3, :3])
            meanvolres = np.abs(np.mean(np.diag(r)))
            break
    else:
        meanvolres = np.abs(np.mean(vol.resolution))

    factors = np.divide(img.resolution, meanvolres)
    logger.info(f"Resampling 2D image by factors {tuple(factors)}...")
    img = img.rescale(*factors, copy=True)
    img.resmgr.reset()
    img.storage = p.slice.storage
    img.domain.storage = p.slice.storage
    logger.info(f"Voxel shape of the 2D image after resampling: {img.shape}.")

    # Save low-resolution image
    fp, fn = os.path.split(p.slice.file)
    fn, ext = os.path.splitext(fn)
    ext = ext.lstrip(".").lower()
    import imageio
    try:
        ext = imageio.formats[ext].extensions[0].lstrip(".")
    except (KeyError, IndexError):
        ext = ts.TIMAGE_DEFAULT_SNAPSHOT_EXT
    filename = os.path.join(p.general.outputdir, f"{fn}_mrires.{ext}")
    img.snapshot(filename, overwrite=True)
    logger.info(f"Saved low-resolution snapshot of the 2D image to: {filename}")
    return img


def prescale(img, **kwargs):
    img.rescale(0.5, copy=False)
    img.rescale(0.25, copy=False)
    img.rescale(0.125, copy=False)
    img.rescale(1, copy=False)
    return img


def flip_x(img, **kwargs):
    """
    Flips the image along the horizontal axis.

    """
    return image.flip_x(img, **kwargs)


def flip_y(img, **kwargs):
    """
    Flips the image along the horizontal axis.

    """
    return image.flip_y(img, **kwargs)


def lab_b(img, **kwargs):
    """
    Extracts the 'b' channel after converting image to LAB colour space.

    :param img: target tensor image
    :type img: TImage

    :returns: b channel of the image after converted into LAB colour space
    :rtype: TImage

    """
    from tirl.operations.spatial import SpatialOperator
    p = AttrMap(kwargs.get("cnf"))
    logger = logging.getLogger(p.logger)

    def _tmajor_rgb2lab(img, *args, **kwargs):
        img = np.moveaxis(img, 0, -1)
        from skimage.color import rgb2lab
        res = rgb2lab(img, *args, **kwargs)
        return np.moveaxis(res, -1, 0)

    logger.info("Extracting LAB-b channel...")
    sop = SpatialOperator(_tmajor_rgb2lab)
    img = sop(img).tensors[-1]
    img.normalise()
    return img


def otsu(timg, **kwargs):
    """
    Generates TImage mask based on the Otsu threshold.

    """
    from skimage.filters import threshold_otsu
    tmp = timg.copy().tensors.reduce()
    tmp.normalise()
    tmp.data[...] *= 255
    th = threshold_otsu(tmp.data)
    mask = np.where(tmp.data > th, 1, 0).astype(ts.DEFAULT_FLOAT_TYPE)
    return mask


def main(*args):
    """ Main program code. """

    parser = create_cli()
    if args:
        slice_to_volume(parser.parse_args(args))
    else:
        parser.print_help()


# Program execution starts here
if __name__ == "__main__":
    main(*sys.argv[1:])
