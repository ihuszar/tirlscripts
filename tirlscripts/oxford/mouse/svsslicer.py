#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2022 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES

import os
import sys

import cv2
import yaml
import pyvips
import imageio
import argparse
import tifffile
import openslide

import numpy as np
import multiprocessing as mp
from functools import partial
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

from tirl.timage import TImage

from skimage.color import rgb2yiq
from skimage.color import label2rgb
from skimage.filters import threshold_otsu
from skimage.segmentation import clear_border
from skimage.measure import label, regionprops
from skimage.morphology import opening, square
from skimage.color import gray2rgb

from sklearn.cluster import KMeans


# DEFINITIONS

FULL_RESOLUTION = 0.25  # um/px
LOW_RESOLUTION = 32     # um/px
SEG_RESOLUTION = 4      # um/px
BRIGHTFIELD = True      # Is the background brighter than the tissue?
TILE = (256, 256)       # width, height
QUALITY = 80            # percent
N_CPU = 1               # number or parallel processes


# IMPLEMENTATION

def main(args):

    svsfile = args.svs
    area = args.area
    n_tiles = args.tiles
    n_rows = args.rows
    margin = args.margin
    visualise = args.show
    outputdir = args.out
    segmentation = args.seg

    print("Creating output directory...")
    # Ensure that the directory exists
    if outputdir == "":
        outputdir, ext = os.path.splitext(svsfile)
    if not os.path.isdir(outputdir):
        os.makedirs(outputdir)

    # Save parameters into a file
    g_dict = dict(
        FULL_RESOLUTION=FULL_RESOLUTION,  # um/px
        LOW_RESOLUTION=LOW_RESOLUTION,  # um/px
        SEG_RESOLUTION=SEG_RESOLUTION,  # um/px
        BRIGHTFIELD=BRIGHTFIELD,
        TILE=TILE,
        QUALITY=QUALITY,  # percent
        N_CPU=N_CPU,
    )
    with open(os.path.join(outputdir, "svsslicer.log"), "w") as f:
        cmd = " ".join(sys.argv)
        f.write(f"The program started with the following command: {cmd}\n")
        f.write(f"\n___CONFIGURATIONS___\n")
        g_dict.update(args.__dict__)
        configs = yaml.safe_dump(g_dict, indent=2, sort_keys=True,
                                 default_flow_style=None)
        f.write(configs)

    print("Generating tiles...")
    tiles, fig = generate_tiles(svsfile, n_tiles, n_rows, area, margin)
    if visualise:
        fig.show()

    print("Saving tiles to disk...")
    fig.savefig(os.path.join(outputdir, "region_summary.png"))
    r_files = export_regions(svsfile, tiles, outputdir, N_CPU)

    print("Creating segmentations for each extracted region...")
    if segmentation:
        segment_regions(sorted(r_files), SEG_RESOLUTION, area)

    print("Done.")


def generate_tiles(svsfile, n_tiles, n_row, area=0, margin=0):

    # Generate low-resolution grayscale image
    slide = openslide.OpenSlide(svsfile)
    scale = LOW_RESOLUTION / FULL_RESOLUTION
    shape = np.divide(slide.level_dimensions[0], scale).astype(int)
    lowres = np.asarray(slide.get_thumbnail(tuple(shape)))
    lowres = cv2.pyrMeanShiftFiltering(lowres, 10, 10)
    lowres = convert_to_grayscale(lowres)

    # 1st-Pass Segmentation
    label_image = cmp_mask(lowres)
    regions = regionprops(label_image)

    # Apply area filter
    scaled_area = area / FULL_RESOLUTION ** 2 / scale ** 2
    regions = [region for region in regions if region.area >= scaled_area]

    # 2nd-Pass Segmentation (Clustering)
    centroids = [region.centroid for region in regions]
    km = KMeans(n_clusters=n_tiles).fit(centroids)
    new_label_image = np.zeros_like(label_image)
    for region, new_label in zip(regions, km.labels_):
        new_label_image[label_image == region.label] = new_label + 1
    
    # Redefine regions after 2nd-pass segmentation (clustering)
    regions = regionprops(new_label_image)

    # Calculate tiles
    scaled_tiles = calculate_tiles(regions, lowres.shape, margin)
    scaled_tiles = sort_tiles(np.vstack(scaled_tiles), n_row)

    # Visualise tiles
    segmented_overlay = label2rgb(new_label_image, lowres, bg_label=0)
    fig = visualise_tiles(scaled_tiles, segmented_overlay)

    # Return full-scale tiles in order
    tiles = [np.multiply(tile, scale) for tile in scaled_tiles]
    tiles = sort_tiles(np.vstack(tiles), n_row)

    return tiles, fig


def calculate_tiles(regions, shape, margin=0):

    tiles = []
    for region in regions:
        minr, minc, maxr, maxc = region.bbox
        width = maxc - minc
        height = maxr - minr
        i_min = j_min = 0
        i_max, j_max = np.subtract(shape[:2], 1)
        i0 = max(i_min, int(minr - height * margin))
        i1 = min(i_max, int(maxr + height * margin))
        j0 = max(j_min, int(minc - width * margin))
        j1 = min(j_max, int(maxc + height * margin))
        tiles.append([i0, j0, i1 - i0, j1 - j0])
    else:
        return tiles


def visualise_tiles(tiles, overlay=None):
    fig, ax = plt.subplots(figsize=(10, 6))
    if overlay is not None:
        ax.imshow(overlay)
    for tile_no, (i0, j0, h, w) in enumerate(tiles):
        rect = mpatches.Rectangle(
            (j0, i0), w, h, fill=False, edgecolor='red', linewidth=2
        )
        ax.add_patch(rect)
        ax.text(
            j0 + 5, i0 + 5, f"Region {tile_no + 1:02d}",
            horizontalalignment="left", verticalalignment="top",
            fontsize=12, color="red"
        )
    ax.set_axis_off()
    plt.tight_layout()
    return fig


def sort_tiles(tiles, n_row):
    """ Sorts the tiles from left-to-right, top-to-bottom. """
    km = KMeans(n_clusters=n_row)
    row_labels = km.fit_predict(tiles[:, [0]])
    if np.argsort(km.cluster_centers_.ravel())[0] != 0:
        row_labels = -row_labels
    return tiles[np.lexsort((tiles[:, 1], row_labels))]


def export_regions(svsfile, tiles, outputdir=None, n_cpu=N_CPU):

    worker = partial(export_job, svsfile=svsfile, outputdir=outputdir)
    if n_cpu > 1:
        with mp.Pool(processes=n_cpu) as pool:
            r_files = pool.map(worker, enumerate(tiles))
    else:
        r_files = []
        for tile in enumerate(tiles):
            r_files.append(worker(tile))

    return r_files

            
            
def export_job(job, svsfile, outputdir):
    slide = openslide.OpenSlide(svsfile)
    tile_no, (y, x, h, w) = job
    # Generate full-size TIFF from the histology
    location = (int(x), int(y))
    size = (int(w), int(h))
    proc_id = mp.current_process().pid
    print(f"Process {proc_id} - Region {tile_no + 1}: {location} + {size}")
    h_slice = slide.read_region(location=location, level=0, size=size)
    h_slice = np.asarray(h_slice, dtype="u1")
    basename = os.path.join(outputdir, f"region_{tile_no + 1:02d}")
    vips_img = pyvips.Image.new_from_array(h_slice, interpretation="rgb")
    if vips_img.hasalpha():
        vips_img = vips_img[:-1]
    height = vips_img.height
    bands = vips_img.bands
    # vips_img = pyvips.Image.arrayjoin(vips_img.bandsplit(), across=1)
    vips_img.set_type(pyvips.GValue.gint_type, "page-height", height)
    vips_img.set_type(pyvips.GValue.gstr_type, "interpretation", "rgb")
    vips_img.set_type(pyvips.GValue.gstr_type, "image-description",
    f"""<?xml version="1.0" encoding="UTF-8"?>
        <OME xmlns="http://www.openmicroscopy.org/Schemas/OME/2016-06"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.openmicroscopy.org/Schemas/OME/2016-06 
        http://www.openmicroscopy.org/Schemas/OME/2016-06/ome.xsd">
        <Image ID="Image:0">
            <Pixels DimensionOrder="XYCZT" 
                    ID="Pixels:0" 
                    SizeC="{bands}"
                    SizeT="1"
                    SizeX="{vips_img.width}"
                    SizeY="{height}"
                    SizeZ="1"
                    Type="uint8">
            </Pixels>
            <Channel ID="Channel:0:0" SamplesPerPixel="3">
                <LightPath/>
            </Channel>
        </Image>
    </OME>""")
    r_file = basename + ".tif"
    vips_img.tiffsave(
        r_file, compression="jpeg", tile=True, Q=QUALITY,
        tile_width=TILE[0], tile_height=TILE[1], pyramid=True, subifd=True
    )
    return r_file


def segment_regions(r_files, resolution, area):
    scale = resolution / FULL_RESOLUTION
    for tile_no, r_file in enumerate(r_files):
        print(f"Creating a segmentation mask for region {tile_no + 1:02d}...")
        with tifffile.TiffFile(r_file) as t:
            newshape = np.divide(t.series[0].levels[0].shape, scale).astype(int)
            for level in t.series[0].levels[::-1]:
                if all(l_dim >= s_dim for (l_dim, s_dim) in
                       zip(level.shape[:2], newshape[:2])):
                    image = TImage(level.asarray())
                    break
        image.resize(*newshape[:2], copy=False)
        scaled_area = area / FULL_RESOLUTION ** 2 / scale ** 2
        seg, seg_rgb = segment(image.data, scaled_area)
        basename, ext = os.path.splitext(r_file)
        tifffile.imwrite(basename + "_seg.tif", seg, compression=5)
        imageio.imwrite(basename + "_seg_rgb.png", seg_rgb, format="png")


def cmp_mask(image):
    thresh = threshold_otsu(image)
    if BRIGHTFIELD:
        bw = opening(image < thresh, square(3))
    else:
        bw = opening(image >= thresh, square(3))
    cleared = clear_border(bw)
    return label(cleared)


def segment(image, scaled_area=0):
    """
    Creates a multi-valued component mask for an image.

    """
    smooth = cv2.pyrMeanShiftFiltering(image, 10, np.sqrt(scaled_area) / 2)
    label_image = cmp_mask(convert_to_grayscale(smooth))
    for region in regionprops(label_image):
        if region.area < scaled_area:
            label_image[label_image == region.label] = 0
    rgb = label2rgb(label_image, image, bg_label=0)
    rgb = np.asarray(rgb * 255, dtype=np.uint8)
    return label_image, rgb


def convert_to_grayscale(image):
    """ Custom function that turns an RGB image into grayscale. """
    return rgb2yiq(image)[..., 0]


def create_parser():
    parser = argparse.ArgumentParser("svsslicer")
    parser.add_argument("svs", metavar="input file", type=str,
                        help="Input SVS file.")
    parser.add_argument("--area", metavar="A", type=float, default=0,
                        help="Smallest area of interest. Given in um2.")
    parser.add_argument("--tiles", metavar="n", type=int, required=True,
                        help="Number of tiles in the input image.")
    parser.add_argument("--rows", metavar="m", type=int, required=True,
                        help="Number of sample rows (for consistent "
                             "labelling).")
    parser.add_argument("--margin", metavar="0-1", type=float, default=0,
                        help="Relative margin to extend the identified "
                             "regions.")
    parser.add_argument("--out", metavar="dir", type=str, default="",
                        help="Output directory. If not specified, the a "
                             "directory will be created next to the SVS input "
                             "file with an identical name.")
    parser.add_argument("--show", action="store_true", default=False,
                        help="User feedback: plot the identified regions.")
    parser.add_argument("--seg", action="store_true", default=False,
                        help="Create segmentation mask for each identified "
                             "region.")
    return parser


# Program execution starts here
if __name__ == "__main__":

    parser = create_parser()
    if len(sys.argv) > 1:
        main(parser.parse_args())
    else:
        parser.print_help()
