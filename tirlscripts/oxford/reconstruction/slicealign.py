#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

"""
2022-05-21 Istvan N Huszar:
This script aims to correct corrupted 4D MRI data, whose odd and even slices
are misaligned by a constant shift and rotation due to erroneous reconstruction
by the scanner. Originally conceptualised and implemented upon a request from
Ying-Qiu Zheng.

"""


# DEPENDENCIES

import sys
import os.path
import logging
import numpy as np
import nibabel as nib
from math import radians as rad

from tirl.chain import Chain
from tirl.timage import TImage
from tirl.cost.mi import CostMI
from tirl.cost.msd import CostMSD
from tirl.cost.mind import CostMIND
from tirl.optimisation.optnl import OptNL
from tirl.optimisation.optgroup import OptimisationGroup
from tirl.transformations.rotation import TxRotation2D
from tirl.transformations.translation import TxTranslation
from tirl.interpolation.scipyinterpolator import ScipyInterpolator


# DEFINITIONS

SMOOTHING = 2
ROTATION = True
costfn = CostMSD
LINEAR_INTERPOLATOR = ScipyInterpolator(order=1, presmooth=False)
SPLINE_INTERPOLATOR = ScipyInterpolator(order=3, presmooth=True)
interpolator = SPLINE_INTERPOLATOR


# IMPLEMENTATION

# Create logger (global)

logger = logging.getLogger("undistort.py")
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


def report_settings():
    logger.info(f"Smoothing: {SMOOTHING} px")
    logger.info(f"Rotation allowed: {bool(ROTATION)}")
    logger.info(f"Cost function: {costfn}")
    logger.info(f"Interpolation order: {interpolator.order}")


def load_mri(f):
    mri = nib.load(f)
    hdr = mri.header
    data = mri.get_fdata()
    logger.info(f"Loaded MRI from: {f}")
    logger.info(f"MRI data: {data.shape}, {data.dtype}")
    return data, hdr


def estimate_slice_offset(data):

    # Create chain
    cog = TImage(data).centre(weighted=True)
    # centre = TxTranslation(*np.divide(data.shape[:2], 2), name="centralise")
    centre = TxTranslation(*cog[:2], name="centralise")
    tx_rot = TxRotation2D(0, mode="deg", name="rotation")
    tx_rot.parameters.set_lower_bounds(-rad(10))
    tx_rot.parameters.set_upper_bounds(rad(10))
    tx_trans = TxTranslation(0, 0, name="translation")
    tx_trans.parameters.set_lower_bounds([-10, -10])
    tx_trans.parameters.set_upper_bounds([10, 10])
    chain = [centre, tx_rot, -centre, tx_trans]

    # Slice series A
    a = data[..., 0::2, 0]
    series_a = [TImage(a[..., i]) for i in range(a.shape[-1])]
    # Slice series B
    b = data[..., 1::2, 0]
    series_b = [TImage(b[..., i], external=chain, interpolator=interpolator)
                for i in range(b.shape[-1])]

    # Create cost pairs
    if SMOOTHING > 0:
        smooth_a = [ai.smooth(SMOOTHING) for ai in series_a]
        smooth_b = [bi.smooth(SMOOTHING) for bi in series_b]
        costs = [costfn(bi, ai) for ai, bi in zip(smooth_b, smooth_a)]
    else:
        costs = [costfn(bi, ai) for ai, bi in zip(series_b, series_a)]

    # Optimisation
    rigid_xy = OptimisationGroup(tx_rot, tx_trans)
    logger.info("Optimising translation parameters...")
    OptNL(tx_trans, *costs, normalised=True, logger=logger)()
    logger.info(f"Translation parameters: "
                f"{tx_trans.parameters.parameters[:]} px")

    if ROTATION:
        logger.info("Co-optimising translation and rotation parameters...")
        OptNL(rigid_xy, *costs, normalised=True, logger=logger)()
        logger.info(f"Rotation angle: {tx_rot.angle} deg")
        logger.info(f"Translation parameters: "
                    f"{tx_trans.parameters.parameters[:]} px")

    return Chain(chain)


def restack(data, chain):

    # Resample the B series
    logger.info("Applying slice transformations... ")

    # Slice series A
    a = data[..., 0::2, :]
    series_a = [TImage(a[..., i, :], taxes=-1) for i in range(a.shape[2])]
    domain_a = series_a[0].domain
    # Slice series B
    b = data[..., 1::2, :]
    series_b = [TImage(b[..., i, :], taxes=-1, external=chain,
                       interpolator=interpolator) for i in range(b.shape[2])]

    series_b = [bi.evaluate(domain_a) for bi in series_b]

    # Restack the volume
    data[..., 1::2, :] = np.stack([bi.data for bi in series_b], axis=2)

    return data


def export_nifti(vol, f, hdr):
    tag = "-rigid" if ROTATION else "-shift"
    out = nib.Nifti1Image(vol, affine=hdr.get_best_affine(), header=hdr)
    fn, ext = os.path.splitext(f)
    f_out = fn + "_undistorted" + tag + ext
    nib.save(out, f_out)
    logger.info(f"Exported undistorted NIfTI: {f_out}")


def export_chain(chain, f):
    fn, ext = os.path.splitext(f)
    tag = "-rigid" if ROTATION else "-shift"
    f_out = fn + "_slicetx" + tag + ".chain"
    chain.save(f_out, overwrite=True)
    logger.info(f"Exported slice transformation chain: {f_out}")


def main(f):
    """ Main program code. """

    report_settings()
    data, hdr = load_mri(f)
    chain = estimate_slice_offset(data)
    export_chain(chain, f)
    vol = restack(data, chain)
    export_nifti(vol, f, hdr)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1])
    else:
        print("Run this script as: python undistort.py <path_to_mri_file> "
              "> undistort.log")

