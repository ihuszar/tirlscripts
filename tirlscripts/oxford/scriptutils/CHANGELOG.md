# Changelog


### 20 Oct 2022 -- v1.0.1

- Added resolution context manager


### 11 Oct 2022 -- v1.0.0

- Project-specific version v1.0.0 assigned to the current state of the library
