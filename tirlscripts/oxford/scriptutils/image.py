#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEPENDENCIES
import logging
import contextlib
import numpy as np
from attrdict import AttrMap


# DEFINITIONS

logger = logging.getLogger("scripts.oxdbb.pathology.midlevel.image")  # do not change this

EPS = np.finfo(np.float32).eps

DEFAULT_MASK_MODULE = {
    "file": None,
    "normalise": True,
    "function": None,
    "automask": {
        "thr": 0,
        "uthr": 1.0
    }
}


# IMPLEMENTATION

def set_mask(img, scope=None, **maskparammodule):
    """
    Sets TImage mask based on the inputs in the corresponding mask parameter
    module.

    """
    img.mask = get_mask(img, scope=scope, **maskparammodule)


def get_mask(img, scope=None, **maskparammodule):
    """
    Creates a TImage mask based on the inputs in the corresponding mask
    parameter module.

    """
    # Get mask parameters from module (user input overrides defaults)
    q = DEFAULT_MASK_MODULE.copy()
    q.update(maskparammodule)
    # Set scope (where function definitions will be looked in)
    scope = globals() if scope is None else scope
    assert isinstance(scope, dict), "Scope must be a dict."
    q = AttrMap(q)

    # Load from image file and normalise if asked
    if q.file is not None:
        from tirl.tfield import TField
        mask = TField(q.file, dtype="f4")
        mask = mask.tensors.reduce()
        if q.normalise:
            mask = mask / np.max(mask.data)
        return mask.data

    # Create mask by calling a function in 'scope'
    elif q.function is not None:
        from pydoc import locate
        func = scope.get(q.function, locate(q.function))
        mask = func(img)
        return mask

    # Create a mask by thresholding the normalised intensities
    elif (q.automask.thr != 0.0) or (q.automask.uthr != 1.0):
        mask = mask_from_threshold(
            img, thr=q.automask.thr, uthr=q.automask.uthr)
        return mask

    else:
        return img.mask


def mask_from_threshold(img, thr=None, uthr=None):
    """
    Automatic masking based on foreground segmentation by thresholding.
    Normalised intensities that fall between the specified lower and upper
    intensity thresholds are segmented as foreground, and the rest of the
    pixels are masked out.

    """
    # Winsorize image data (handle outliers)
    tmp = img.tensors.reduce()
    low = np.percentile(tmp.data, 0.5)
    high = np.percentile(tmp.data, 99.5)
    tmp.data[...] = np.clip(tmp.data, low, high)
    amin, amax = np.min(tmp.data), np.max(tmp.data)

    # Generate mask by thresholding
    mask = np.ones(tmp.vshape, dtype=np.float32)
    if thr is not None:
        assert 1 + EPS >= thr >= 0 - EPS
        mask[((tmp.data - amin) / (amax - amin)) < thr] = 0
    if uthr is not None:
        assert 1 + EPS >= thr >= 0 - EPS
        mask[((tmp.data - amin) / (amax - amin)) > uthr] = 0

    return mask


def perform_image_operations(img, *actions, scope=None, other=None, **kwargs):
    """
    Calls the sequence of action functions on the specified TImage.

    The input image should not be modified by the action functions. The result
    of each action is used as the input for the next action. The action
    functions should have the following signature:

        actionfunc(img: TImage, other=None, history=()) -> result: TImage

    The result of the last action is returned by this function.

    :param img: TImage on which the actions are performed
    :type img: TImage
    :param actions:
        Sequence of functions from the TIRL namespace defined by their full
        module pathology. For example, to call the 'filter' function from the user
        module 'histology' in package 'pkg', enter
        'tirl.usermodules.pkg.histology.filter'.
    :type actions: str
    :param scope: where functions are looked for
    :type scope: dict
    :param other: other TImage that is used in the registration (optional)
    :type other: TImage
    :param kwargs:
        Further arguments that are necessary for the image operation.
        The global configurations (as loaded from a configuration file) should
        be added under the "cnf" key by the caller.
    :type kwargs: Any

    :returns: pre-processed image
    :rtype: TImage

    """
    if not actions:
        return img

    from pydoc import locate

    action_history = []
    lastimg = img
    for action in actions:
        func = scope.get(action, locate(action))
        res = func(lastimg, other=other, history=action_history, **kwargs)
        lastimg = res or lastimg
        action_history.append((action, lastimg))
    else:
        from tirl.timage import TImage
        assert isinstance(lastimg, TImage), \
            "Actions must not change the TImage type"
        return lastimg


@contextlib.contextmanager
def get_resolution(img, *resolution):
    img.resmgr.sync()
    active_layer = img.resmgr.active
    yield img.resample(*resolution)
    img.resmgr.set(active_layer)


def flip_x(img, **kwargs):
    """
    Flips the image along the horizontal axis.

    """
    from tirl.transformations.affine import TxAffine
    pos = img.domain.map_voxel_coordinates([np.subtract(img.vshape, 1)])[0]
    swapmat = np.eye(img.vdim + 1)
    swapmat[0, 0] *= -1
    swapmat[0, -1] = pos[0]
    swap = TxAffine(swapmat[:-1, :], name="flip_x")
    img.domain.chain.prepend(swap)
    # img.data[...] = img.data[::-1]
    # if img.tmask() is not None:
    #     img.mask[...] = img.mask[::-1]


def flip_y(img, **kwargs):
    """
    Flips the image along the horizontal axis.

    """
    from tirl.transformations.affine import TxAffine
    pos = img.domain.map_voxel_coordinates([np.subtract(img.vshape, 1)])[0]
    swapmat = np.eye(img.vdim + 1)
    swapmat[1, 1] *= -1
    swapmat[1, -1] = pos[1]
    swap = TxAffine(swapmat[:-1, :], name="flip_y")
    img.domain.chain.prepend(swap)
    # img.data[...] = img.data[:, ::-1, ...]
    # if img.tmask() is not None:
    #     img.mask[...] = img.mask[:, ::-1]


def rgb2yiq(timg):
    """
    Convert RGB to YIQ, or leave as grayscale.

    """
    from tirl.operations.tensor import TensorOperator

    if timg.tsize >= 3:
        mat = np.asarray([[ 0.299,   0.587,   0.114],
                          [0.5959, -0.2746, -0.3213],
                          [0.2115, -0.5227,  0.3112]])
        return TensorOperator(lambda t: np.einsum("ij,...j", mat, t))(timg)
    else:
        return timg
