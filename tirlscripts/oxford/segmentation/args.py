#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DEVELOPMENT NOTES

"""
23-June-2020 Istvan N Huszar:
This is a legacy argument parser, that will be replaced with argparse in
future versions. Currently only the 'stripbg' uses this module.

"""


# DEPENDENCIES

import sys


def argexist(argv, subarg=False):
    """The argexist subroutine returns whether the given argument was specified
    via the command line. When subarg is True, the result will be False if the
    argument is found and it is supposed to have a subargument that is missing.
    """

    if argv in sys.argv:
        if subarg:
            if len(sys.argv)-1 > sys.argv.index(argv):
                if not sys.argv[sys.argv.index(argv)+1].startswith('-'):
                    return True
                else:
                    return False
            else:
                return False
        else:
            return True
    else:
        return False


def subarg(argv, default_value="", splitat=','):
    """The subarg subroutine returns the subargument list of a given argument,
    if its was specified via the command line. When the subargument is missing,
    a preset default value will be returned. Note that the return value is
    always a list."""

    if argexist(argv, subarg=True):
        arglist = []
        for arg in sys.argv[sys.argv.index(argv)+1:]:
            if not arg.startswith('-'):
                for p in arg.split(splitat):
                    arglist.append(p)
            else:
                break
        return arglist
    else:
        return str(default_value).split(splitat)


def confirmed_to_proceed(forceanswer=True):
    """Forces user input for continuing the execution of the program."""

    no = {'no', 'n'}
    if forceanswer:
        yes = {'yes', 'y', 'ye'}
    else:
        yes = {'yes', 'y', 'ye', ''}

    choice = input().lower()
    if choice in yes:
        return True
    elif choice in no:
        return False
    else:
        sys.stdout.write("Please respond with 'yes' or 'no': ")
        choice = confirmed_to_proceed()
        return choice
