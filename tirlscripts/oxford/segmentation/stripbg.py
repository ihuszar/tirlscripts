#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


__tirlscript__ = "stripbg"


# DEVELOPMENT NOTES

"""
2020-Jun-13 Istvan N Huszar:
Updated for Python 3. This is a legacy script, which predates even v0.3. 
(This was the first ever script that I wrote for the MND project.) 

2017-Jun-14 Istvan N Huszar:
Revision of the original segment-by-hue.py algorithm. Goal: fix improper
segmentation on bluish-tinted brain slices.

"""

# DEPENDENCIES

import os
import numpy as np
from PIL import Image
import tifffile as tiff
from skimage import color
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from skimage.measure import label, regionprops
from scipy.ndimage import median_filter


# TIRL IMPORTS

from tirlscripts.oxford.segmentation.args import *


# DEFINITIONS

# Command-line argument flags
CLFLAGS = {'tiff': '--tif',
           'dims': '--dims',
           'weight': '-w',
           'clusters': '-c',
           'demean': '--demean',
           'normvar': '--normvar',
           'show': '--show',
           'save': '--save',
           'mask': '--mask',
           'median': '--median',
           'meanshift': '--meanshift',
           'invert': '--inv',
           'corner': '--corner',
           'segval': '-s',
           'area': '--area',
           'format': '-f'}

#  Layer identifiers
LAYERIDS = {'red': 'r',
            'green': 'g',
            'blue': 'b',
            'hue': 'h',
            'saturation': 's',
            'value': 'v',
            'Lightness': 'L',
            'LabA': 'A',
            'LabB': 'B'}

FMTS = {'BMP': 'bms', 'EPS': 'eps', 'GIF': 'gif', 'ICNS': 'icns', 'ICO': 'ico',
        'IM': 'im', 'JPEG': 'jpg', 'JPEG 2000': 'jpg', 'MSP': 'msp',
        'PCX': 'pcx', 'PNG': 'png', 'PPM': 'ppm', 'SGI': 'sgi',
        'SPIDER': 'spider', 'TIFF': 'tif', 'WebP': 'webp', 'XBM': 'xbm'}

DISCURSIVE = False


# IMPLEMENTATION

def main(*args):
    """ Main program code. """

    err = 0
    sys.argv = ("stripbg.py",) + args  # a little hack

    imfilelist = subarg(sys.argv[0])
    if imfilelist[0] == '':
        print('No image was specified.')
        err = err + 1
        exit()

    for arg in sys.argv[1+len(imfilelist):]:
        if str(arg).startswith('-'):
            if not (arg in CLFLAGS.values()):
                print('Invalid argument: {}'.format(arg))
                err = err + 1
                exit()

    # Get user-specified layers
    lset = []
    if argexist(CLFLAGS['dims']):
        if argexist(CLFLAGS['dims'], True):
            for l in list(subarg(CLFLAGS['dims'])[0]):
                if not (l in LAYERIDS.values()):
                    err = err + 1
                    print('Invalid layer specification.')
                else:
                    lset.append(l)
        else:
            print('Invalid layer specification.')
            err = err + 1
            exit()
    else:
        lset = ['auto']

    # Get weights for the feature layers
    weights = subarg(CLFLAGS['weight'], 'auto')
    if lset[0] != 'auto':
        if weights[0] != 'auto':
            if len(lset) != len(weights):
                err = err + 1
                print("Weights don't match the input after {}."
                      .format(CLFLAGS['dims']))
                exit()
    try:
        if not (weights[0] == 'auto'):
            weights = np.array([float(weight) for weight in weights])
    except Exception:
        print('Invalid weight specification.')
        err = err + 1
        exit()

    # Read the number of clusters
    try:
        N_CLUSTERS = int(subarg(CLFLAGS['clusters'], 2)[0])
    except Exception:
        print('Invalid cluster number specification.')
        err = err + 1
        exit()

    # Read segmentation label
    try:
        tmp = subarg(CLFLAGS['segval'], 0)
        SEGVALS = np.array([int(segval) for segval in tmp])
        del tmp

    except Exception:
        print('Invalid segmentation label specification.')
        err = err + 1
        exit()

    # Read area limit
    if argexist(CLFLAGS['area']):
        if argexist(CLFLAGS['area'], True):
            try:
                area_limit = int(subarg(CLFLAGS['area'])[0])
            except Exception:
                print('Invalid area limit.')
                err = err + 1
                exit()
        else:
            print('Missing specification for area limit.')
            exit()
    else:
        area_limit = None

    # Read format
    fmt = subarg(CLFLAGS['format'], 'TIFF')[0].upper()
    if not (fmt in FMTS.keys()):
        print('Invalid format specification. Supported formats: \n{}'
              .format(FMTS.keys()))
        err = err + 1
        exit()
    if argexist(CLFLAGS['tiff']) & (fmt != 'TIFF'):
        print("When the {} switch is used, only the default 'TIF' format "
              "can be specified.".format(CLFLAGS['tiff']))
        err = err + 1
        exit()

    # Read median filtering kernel size
    if argexist(CLFLAGS['median']):
        if argexist(CLFLAGS['median'], True):
            try:
                medkernel = int(subarg(CLFLAGS['median'])[0])
            except Exception:
                err = err + 1
                print('Invalid median fileter kernel size.')
                exit()
        else:
            err = err + 1
            print('A positive integer kernel size must be provided when the {} '
                  'switch is used.'.format(CLFLAGS['median']))
            exit()

    # Read corner constraint
    try:
        corner = int(subarg(CLFLAGS['corner'], '0')[0])
        if (corner > 3) | (corner < 0):
            raise ValueError
    except Exception:
        err = err + 1
        print('Invalid corner constraint.')
        exit()

    # Summarize task and insist on user confirmation before proceeding
    if DISCURSIVE:
        line = "\n{:d} file(s) will be processed:".format(len(imfilelist))
        print(line)
        print('=' * len(line) + '\n')
        print("\n".join(imfilelist))
        print("\nWould you like to start the process? [yes/no]: ")
        if not confirmed_to_proceed():
            exit()

    # Process image(s)
    for imfile in imfilelist:
        # Validate file pathology
        if not os.path.isfile(imfile):
            err = err + 1
            print(f'{imfile} could not be recognised, '
                  f'therefore it was skipped.')
            continue

        fpath = os.path.split(imfile)[0]
        fname = os.path.split(imfile)[1]

        # Open file
        if argexist(CLFLAGS['tiff']):
            img = tiff.imread(imfile)
        else:
            img = np.array(Image.open(imfile))

        # Make sure that the image has 3 dimensions
        if not (len(img.shape) in (2, 3)):
            err = err + 1
            print('Invalid number of image dimensions in {}. '
                  'The file was skipped.'.format(imfile))
            continue
        if len(img.shape) == 2:
            img = img[:, :, np.newaxis]
        h, w, ch = img.shape
        print('Processing {}...'.format(imfile))
        print(img.shape)

        if argexist(CLFLAGS['meanshift']):
            import cv2
            arg = subarg(CLFLAGS["meanshift"], default_value=None, splitat=" ")
            if arg is None:
                sp, sr = 21, 51
            else:
                sp, sr = arg
            original = img
            img = cv2.pyrMeanShiftFiltering(img, float(sp), float(sr))
        else:
            original = None

        # Prepare layers to be used as dimensions for the k-Means classifier
        lstack = img
        if ch != 1:
            try:
                # Ensuring that the starting point is indeed RGB
                pilimg = Image.open(imfile)
                rgbimg = np.array(Image.fromarray(img, pilimg.mode)
                                  .convert('RGB'))
            except Exception:
                err = err + 1
                print('Colour space conversion failed for {}. The image was '
                      'skipped.')
                continue
            hsv = lambda: np.asarray(color.rgb2hsv(rgbimg))
            lab = lambda: np.asarray(color.rgb2lab(rgbimg))

            # Getter functions for the individual layers
            LAYERS = {LAYERIDS['red']: lambda: img[:, :, 0],
                      LAYERIDS['green']: lambda: img[:, :, 1],
                      LAYERIDS['blue']: lambda: img[:, :, 2],
                      LAYERIDS['hue']: lambda: hsv()[:, :, 0],
                      LAYERIDS['saturation']: lambda: hsv()[:, :, 1],
                      LAYERIDS['value']: lambda: hsv()[:, :, 2],
                      LAYERIDS['Lightness']: lambda: lab()[:, :, 0],
                      LAYERIDS['LabA']: lambda: lab()[:, :, 1],
                      LAYERIDS['LabB']: lambda: lab()[:, :, 2]}

            if not lset[0] == 'auto':
                lstack = []
                for l in lset:
                    lstack.append(LAYERS[l]())
                lstack = np.dstack(lstack)
        n_features = lstack.shape[-1]

        # Re-calculate weights
        if weights[0] == 'auto':
            weights = np.array([1] * n_features)
        weights = np.sqrt(weights / np.sum(weights).astype(np.float64))
        if len(weights) != n_features:
            err = err + 1
            print("Weights did not match the number of layers in {}. "
                  "The file was skipped.".format(imfile))
            continue

        # Perform global k-Means classification of pixels
        if argexist(CLFLAGS['weight']):
            print('Weights:', weights)
            data = lstack.reshape(-1, n_features) * \
                   weights.reshape(1, n_features)
        else:
            data = lstack.reshape(-1, n_features)
        if argexist(CLFLAGS['demean']):
            data = data - np.mean(data, axis=0)
        if argexist(CLFLAGS['normvar']):
            data = data / np.var(data, axis=0)
        kmeans = KMeans(n_clusters=N_CLUSTERS, random_state=0).fit(data)
        mask = np.asarray(kmeans.labels_, dtype=np.int8).reshape((h, w))
        if argexist(CLFLAGS['invert']):
            mask = np.max(mask) - mask
        if argexist(CLFLAGS['median'], True):
            mask = median_filter(mask, medkernel)
        if argexist(CLFLAGS['corner']):
            if corner == 0:
                yx = (0, 0)
            elif corner == 1:
                yx = (0, w-1)
            elif corner == 2:
                yx = (h-1, 0)
            else:
                yx = (h-1, w-1)
            watchpoint = mask[yx]
            if not watchpoint == 0:
                current_label = mask[yx]
                tmplabel = np.max(mask) + 1
                mask[mask == current_label] = tmplabel
                mask[mask == 0] = current_label
                mask[mask == tmplabel] = 0

        if original is not None:
            img = original

        # Area filtering
        if area_limit is not None:
            cleanmask = np.copy(mask)
            bg_val = np.max(mask) + 1
            cleanmask[
                np.in1d(mask, SEGVALS).reshape(mask.shape)] = bg_val
            cleanmask[cleanmask != bg_val] = bg_val + 1
            labels = label(cleanmask, connectivity=1, background=bg_val)
            regions = regionprops(labels)
            cleanmask[np.in1d(labels, np.asarray(
                [region['label'] for region in regions if
                 region['area'] < area_limit])).reshape(
                cleanmask.shape)] = \
                bg_val
            segmentation = np.copy(img)
            segmentation[(cleanmask == bg_val).reshape(cleanmask.shape)] = 0
            mask[cleanmask == bg_val] = SEGVALS[0]
        else:
            segmentation = np.copy(img)
            segmentation[np.in1d(mask, SEGVALS).reshape(mask.shape)] = 0

        if argexist(CLFLAGS['save']):
            fn = os.path.join(fpath, fname[:-4])
            if argexist(CLFLAGS['tiff']):
                fnseg = fn + '_seg.tif'
                try:
                    tiff.imsave(fnseg, segmentation)
                    print('SAVED: {}'.format(fnseg))
                except Exception:
                    err = err + 1
                    print('ERROR: {} could not be created.'.format(fnseg))
                fnmask = fn + '_mask.tif'
                if argexist(CLFLAGS['mask']):
                    try:
                        tiff.imsave(fnmask, mask.astype(np.uint8))
                        print('SAVED: {}'.format(fnmask))
                    except Exception:
                        err = err + 1
                        print('ERROR: {} could not be created.'.format(fnmask))
            else:
                fnseg = fn+'_seg.'+FMTS[fmt]
                try:
                    Image.fromarray(segmentation).save(fnseg, fmt)
                    print('SAVED: {}'.format(fnseg))
                except Exception:
                    err = err + 1
                    print('ERROR: {} could not be created.'.format(fnseg))
                if argexist(CLFLAGS['mask']):
                    fnmask = fn+'_mask.'+FMTS[fmt]
                    try:
                        Image.fromarray(mask).save(fnmask, fmt)
                        print('SAVED: {}'.format(fnmask))
                    except Exception:
                        err = err + 1
                        print('ERROR: {} could not be created.'.format(fnmask))

        if argexist(CLFLAGS['show']):
            if argexist(CLFLAGS['mask']):
                plt.subplot(131)
                plt.imshow(img)
                plt.title('Original')
                plt.subplot(132)
                plt.imshow(mask, cmap='gray')
                plt.title('Mask')
                plt.subplot(133)
                plt.imshow(segmentation)
                plt.title('Segmentation')
            else:
                plt.subplot(121)
                plt.imshow(img)
                plt.title('Original')
                plt.subplot(122)
                plt.imshow(segmentation)
                plt.title('Segmentation')
            plt.show()

    if err == 0:
        print('All tasks were successfully completed.')
    else:
        print('The program encountered {:d} error(s).'.format(err))


if __name__ == '__main__':

    if (len(sys.argv) > 1) and (sys.argv != ["tirl", "stripbg"]):
        main(*sys.argv[1:])

    else:
        print("""
        The stripbg utility can be used to extract brain slices from images
        that also contain a background. The background must be visually 
        distinct from the brain tissue.
         
        Supported file formats: BMP, EPS, GIF, ICNS, ICO, IM, JPEG, JPEG 2000, 
        MSP, PCX, PNG, PPM, SGI, SPIDER, TIFF, WebP, XBM.
           
        The core process is an adjustable global k-Means classification, that 
        has the following options.
        
        Usage: ./strip-bg.py <input> [options]
        
        Options:
            --tif               Forces to use tifffile.py for opening and saving
                                images (PIL is used by default)
            --dims <rgbhsvLAB>  Specifies which colour channel(s) to use
                                (default: all image channels)
            -w [w1,w2...wn]     Weights for the colour channels
                                (default: equal weights for all channels)
            -c                  Number of clusters (default: 2)
            --demean            Demean channel values
                                (default: off)
            --normvar           Normalise channel values by their variance
                                (default: off)
            --mask              Output the mask.
            --median <size>     Perform median filtering on the mask with fixed
                                kernel size. (default: off)
            --meanshift <sp> <sr> Perform mean shift filtering on the image 
                                before segmentation. (default: off)
            --inv               Invert the mask. (default: off)
            --corner <c>        Forces the specified corner to be labeled with 
                                zero. (0: Top-left 1: top-right, 2: bottom-left
                                3: bottom-right) (default: off, 0 when on)
            -s <s1,s2...sn>     Mask values used for segmentation (default: 0)
            --area <area>       Erases standalone parts under the area limit.
                                (default: off)
            --show              Show output on screen. (default: off)
            --save              Save output to file. (default: off)
            -f      <format>    Output file format. See above what is supported.
        """)
