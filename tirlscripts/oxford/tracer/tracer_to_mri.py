import tirl
from tirl.timage import TImage

# Chain 1 - histology mm-space to blockface mm-space
blockface = tirl.load("/Volumes/xenon/SaadJbabdi/tracer2mri/HISTO2BF/mr212BF.022/fixed5_nonlinear.timg")
histology = tirl.load("/Volumes/xenon/SaadJbabdi/tracer2mri/HISTO2BF/mr212BF.022/moving.timg")
# The trick here is that both the blockface and the histology images
# were centralised before registration, whereas this centralisation
# is not applied automatically when we load the images from the original files,
# so we have to add the centralisation of the histology image here
# as the first transformation.
hmm_to_bfmm = histology.domain.external + blockface.domain.external.inverse()

# Chain 2 - blockface mm-space to MRI mm-space
blockface = tirl.load("/Volumes/xenon/SaadJbabdi/tracer2mri/BF2MRI/mr212BF.022/3_stage3.timg")
bfmm_to_mrimm = blockface.domain.external

# Evaluate MRI data on the registered histology domain
mri = TImage("/Volumes/xenon/SaadJbabdi/tracer2mri/Input/T1w_brain.nii.gz", external="affine")
histology = TImage("/Volumes/xenon/SaadJbabdi/tracer2mri/Input/tn_mr212TH-SERT_c07_s22nDF.jpg")
histology.resolution = 0.052  # mm
histology.domain.external = hmm_to_bfmm + bfmm_to_mrimm
mri2d = mri.evaluate(histology.domain)
histology.snapshot("/Volumes/xenon/SaadJbabdi/tracer2mri/Combined/histology.png", overwrite=True)
mri2d.snapshot("/Volumes/xenon/SaadJbabdi/tracer2mri/Combined/mri2histology.png", overwrite=True)
