#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   _______ _____ _____  _                    _       _
#  |__   __|_   _|  __ \| |                  (_)     | |
#     | |    | | | |__) | |     ___  ___ _ __ _ _ __ | |_ ___
#     | |    | | |  _  /| |    / __|/ __| '__| | '_ \| __/ __|
#     | |   _| |_| | \ \| |____\__ \ (__| |  | | |_) | |_\__ \
#     |_|  |_____|_|  \_\______|___/\___|_|  |_| .__/ \__|___/
#                                              | |
#                                              |_|
#
# Copyright (C) 2018-2023 University of Oxford
# Part of the FMRIB Software Library (FSL)
# Author: Istvan N. Huszar


# SHBASECOPYRIGHT


# DESCRIPTION

"""
Modifies the tirl.yml user configuration file to register the scripts.
After registration, the scripts can be called from the command line by their
shortcuts as "tirl <script_name> [arguments]".

"""


# DEPENDENCIES

import os
import re
import sys

import yaml
import importlib.util
import importlib.machinery

import tirl.settings as ts


# DEFINITIONS


# IMPLEMENTATION

def create_tirlscripts_dict(rootdir):
    """ Walks through the installed """

    scriptlib = dict()
    regex = re.compile(r"__tirlscript__.+=.+")
    for root, dirs, files in os.walk(rootdir):
        files = [os.path.join(root, f) for f in files
                 if f.lower().endswith(".py")]
        for fpath in files:
            with open(fpath, "r") as fp:
                code = fp.read()
                if regex.search(code):
                    loader = importlib.machinery.SourceFileLoader("scr", fpath)
                    spec = importlib.util.spec_from_loader("scr", loader)
                    scr = importlib.util.module_from_spec(spec)
                    loader.exec_module(scr)
                    key = getattr(scr, "__tirlscript__", None)
                else:
                    key = None
            if key:
                scriptlib.update({key: fpath})
    return scriptlib


def replace_tirlscripts_block(code, newblock):
    regex = re.compile(r"tirlscripts:.*?\n(?=\S)", re.DOTALL)
    match = regex.search(code)
    # Replace the exact block if it exists
    if match:
        oldblock = match.group(0)
    # Extend the file if it does not exist
    else:
        oldblock = "..."
        newblock += "\n..."
    # Perform replacement
    return code.replace(oldblock, newblock)


def register_scripts_from_dirs(*rootdirs):
    """ Main program code. """

    # Read tirl.yml
    with open(ts.CONFIGFILE, "r") as fp:
        code = fp.read()

    # Identify tirlscripts
    scriptlib = dict()
    for rootdir in rootdirs:
        scriptlib.update(create_tirlscripts_dict(rootdir))
    else:
        for k, v in scriptlib.items():
            print(f"Found {k}: {v}")

    # Replace tirlscripts definitions in tirl.yml
    if scriptlib:
        newblock = yaml.safe_dump({"tirlscripts": scriptlib})
    else:
        newblock = "tirlscripts: null\n"
    code = replace_tirlscripts_block(code, newblock + "\n")

    # Save the altered configuration file
    with open(ts.CONFIGFILE, "w") as fp:
        fp.write(code)


if __name__ == "__main__":
    try:
        import tirl
    except ImportError:
        print("The tirl package must be installed before running this script.")
    else:
        if len(sys.argv) > 1:
            rootdirs = sys.argv[1:]
        else:
            import tirlscripts
            rootdirs = [tirlscripts.home()]

        # Add scripts from the root directory
        register_scripts_from_dirs(rootdirs)
